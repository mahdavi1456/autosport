<?php
ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../session-sport'));

include"gt-include/class/base/db.php";
include"gt-include/class/base/gt-user.php";
include"gt-include/class/base/prime.php";
include"gt-include/class/base/gt-option.php";
include"gt-include/class/base/engine.php";

include"gt-include/class/school/gt-classroom.php";
include"gt-include/class/school/gt-exam.php";
include"gt-include/class/school/gt-grade-year.php";
include"gt-include/class/school/gt-grade.php";
include"gt-include/class/school/gt-lesson.php";
include"gt-include/class/school/gt-item.php";
include"gt-include/class/school/gt-question.php";
include"gt-include/class/school/gt-register.php";
include"gt-include/class/school/gt-school.php";
include"gt-include/class/school/gt-student.php";
include"gt-include/class/school/gt-teacher.php";
include"gt-include/class/school/gt-teaching.php";