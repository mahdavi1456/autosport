<?php
include "gt-loader.php";
$db = new db();
$gt_option = new gt_option();
if ((isset($_GET['login']) && $_GET['login'] == "ok")) {
    $opt_home = $gt_option->get_option('opt_home');
    $url = $opt_home . "dashboard";
    ?>
    <script type="text/javascript">
        window.location.href = "<?php echo $url; ?>";
    </script>
    <?php
}

if (isset($_GET['logout']) || !isset($_SESSION['user_id'])) {
    $_SESSION = [];
    $opt_home = $gt_option->get_option('opt_home');
    $url = $opt_home . "login";
}

$request = $_SERVER['REQUEST_URI'];
$req_list = explode("?", $request);
$req = $req_list[0];
switch ($req) {
    case '/' :
        require __DIR__ . '/gt-content/themes/sport/login.php';
        break;
    case '/forgot' :
        require __DIR__ . '/gt-content/themes/sport/forgot.php';
        break;
    case '/login' :
        require __DIR__ . '/gt-content/themes/sport/login.php';
        break;
    case '/dashboard' :
        require __DIR__ . '/gt-content/themes/sport/dashboard.php';
        break;
    case '/player' :
        require __DIR__ . '/gt-content/themes/sport/player.php';
        break;
    case '/coach' :
        require __DIR__ . '/gt-content/themes/sport/coach.php';
        break;
    case '/station' :
        require __DIR__ . '/gt-content/themes/sport/station.php';
        break;
    case '/test_result' :
        require __DIR__ . '/gt-content/themes/sport/test-result.php';
        break;
    case '/food_plan' :
        require __DIR__ . '/gt-content/themes/sport/food-plan.php';
        break;
    case '/untropo' :
        require __DIR__ . '/gt-content/themes/sport/untropo.php';
        break;
    case '/parameter' :
        require __DIR__ . '/gt-content/themes/sport/parameter.php';
        break;
    case '/screening' :
        require __DIR__ . '/gt-content/themes/sport/screening.php';
        break;
    case '/screening_index' :
        require __DIR__ . '/gt-content/themes/sport/screening-index.php';
        break;
    case '/ages' :
        require __DIR__ . '/gt-content/themes/sport/ages.php';
        break;
    case '/sport_register' :
        require __DIR__ . '/gt-content/themes/sport/sport-register.php';
        break;
    case '/player_list' :
        require __DIR__ . '/gt-content/themes/sport/player-list.php';
        break;
    case '/coach_list' :
        require __DIR__ . '/gt-content/themes/sport/coach-list.php';
        break;
    case '/list_players_coach' :
        require __DIR__ . '/gt-content/themes/sport/list-players-coach.php';
        break;
    case '/station_list' :
        require __DIR__ . '/gt-content/themes/sport/station-list.php';
        break;
    case '/duties' :
        require __DIR__ . '/gt-content/themes/sport/duties.php';
        break;
    case '/score_list' :
        require __DIR__ . '/gt-content/themes/sport/score-list.php';
        break;
    case '/food_list' :
        require __DIR__ . '/gt-content/themes/sport/food-list.php';
        break;
    case '/screening_list' :
        require __DIR__ . '/gt-content/themes/sport/screening-list.php';
        break;
    case '/untropo_result' :
        require __DIR__ . '/gt-content/themes/sport/untropo-result.php';
        break;
    case '/profile' :
        require __DIR__ . '/gt-content/themes/sport/profile.php';
        break;
    case '/profile_player' :
        require __DIR__ . '/gt-content/themes/sport/profile-player.php';
        break;
    case '/about_us' :
        require __DIR__ . '/gt-content/themes/sport/about-us.php';
        break;
    case '/rollcall' :
        require __DIR__ . '/gt-content/themes/sport/rollcall.php';
        break;
    case '/reports' :
        require __DIR__ . '/gt-content/themes/sport/reports.php';
        break;
	case '/rollcall_list' :
        require __DIR__ . '/gt-content/themes/sport/rollcall-list.php';
        break;
}