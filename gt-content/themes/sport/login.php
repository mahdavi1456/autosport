<?php session_start();
$_SESSION = []; ?>
<!DOCTYPE html>	
<html lang="fa">
<head>
	<?php
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/engine.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-coach.php";
	$engine = new engine();
	?>
	<meta http-equiv="content-type" content="text/html" charset="utf-8">
	<title></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo $engine->get_theme_url(); ?>css/style.css">
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/scripts.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/owl.carousel.min.js"></script>
</head>
<body class="BgGradiant">
	<div class="container heightVh">
		<div class="LoginBox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
			<div class="GtForm FormLogin">
				<h2 class="center">ورود</h2>
				<p class="center">وارد حساب کاربری خود شوید</p>
				<form action="" method="post">
					<div class="form-group">
					  <input type="text" class="form-control" id="email" name="username" placeholder="نام کاربری">
					</div>
					<div class="form-group FormLoginPassword">
						<input type="password" class="form-control FormLoginPasswordInput" id="pwd" name="password" placeholder="رمز عبور">
						<div class="FormLoginPasswordInputBtn"><i class="icofont-eye-alt"></i></div>
					</div>
					<div>
						<select class="Cselect" name="level">
							<option value="player">بازیکن</option>
							<option value="coach">مربی</option>
							<option value="manager">مدیر</option>
						</select>
					</div>
					<button type="submit" class="btn btn-default" name="login">ورود</button>
				</form>
				<div class="col-md-12 text-center">
					<!--a class="btn btn-link p-x-0" href="forgot" class="btn btn-default">فراموشی رمز عبور</a-->
				</div>
			</div>
			<?php
			if(isset($_POST['login'])) {
				$db = new db();
				$username = $_POST['username'];
				$password = $_POST['password'];
				$level = $_POST['level'];
				if($level == 'coach') {
					$gt_coach = new gt_coach();
					$login = $gt_coach->check_login_coach($username, $password);
				}
				if($level == 'player') {
					$gt_player = new gt_player();
					$login = $gt_player->check_login_player($username, $password);
					$ID = $gt_player->get_player_id($username);
				}
				if($level == 'manager') {
					$gt_coach = new gt_coach();
					$login = $gt_coach->check_login_manager($username, $password);
				}
				if($login == 1) {
					$opt_home =  $gt_option->get_option('opt_home');
					$url = $opt_home . "dashboard";
					if($level == 'coach') {
						$ID = $gt_coach->get_coach_id($username);
						$_SESSION["user_id"] = $ID;
						$user_info = $db->get_select_query("select * from coach where ID = $ID");
						$_SESSION["name"] = $user_info[0]['c_name'];
						$_SESSION["family"] = $user_info[0]['c_family'];
						$_SESSION["username"] = $user_info[0]['c_username'];
						$_SESSION["level"] = $user_info[0]['c_access_level'];
					} 
					else if($level == 'manager') {
						$ID = $gt_coach->get_coach_id($username);
						$_SESSION["user_id"] = $ID;
						$user_info = $db->get_select_query("select * from coach where ID = $ID");
						$_SESSION["name"] = $user_info[0]['c_name'];
						$_SESSION["family"] = $user_info[0]['c_family'];
						$_SESSION["username"] = $user_info[0]['c_username'];
						$_SESSION["level"] = $user_info[0]['c_access_level'];
					} else {
						$ID = $gt_player->get_player_id($username);
						$_SESSION["user_id"] = $ID;
						$user_info = $db->get_select_query("select * from player where ID = $ID");
						$_SESSION["name"] = $user_info[0]['p_name'];
						$_SESSION["family"] = $user_info[0]['p_family'];
						$_SESSION["username"] = $user_info[0]['p_username'];
						$_SESSION["level"] = "player";
					}
					?>
					<script type="text/javascript">
						window.location.href = "<?php echo $url; ?>";
					</script>
					<?php
				} else {
					?>
					<br><div class="alert alert-danger">نام کاربری یا رمز وارد شده صحیح نمی باشد</div>
				<?php
				}
			}
			?>
		</div>
		<!--<div class="sup col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
			<h3>پشتیبانی</h3>
		</div>-->
	</div>
</body>
</html>