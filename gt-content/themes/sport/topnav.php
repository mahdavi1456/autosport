<?php
$opt_home =  $gt_option->get_option('opt_home');
$url = $opt_home . "login";
$level = $_SESSION["level"];
$gt_player = new gt_player();
$ID = $_SESSION['user_id'];
$db = new db();
if($level == 'manager' || $level == 'coach') { 
	$filename = $db->get_var_query("select c_file from coach where ID = $ID");
} else{
	$filename = $db->get_var_query("select p_file from player where ID = $ID");
}
$url = $engine->get_the_url(); ?>
<div class="TopContent">
	<div class="LogoSideNav">
		<img src="<?php echo $engine->get_theme_url(); ?>images/logodDd.png" class="img-responsive">
	</div>
	<div class="MenuButton"></div>
	<div class="LogoutBtnWlcPr"><a href="<?php echo $url; ?>"><i class="icofont-ui-power"></i></a></div>
	<div class="Usernamewlc">
		<div class="profile-username"><?php echo $_SESSION["name"] . " " . $_SESSION["family"]; ?></div>
	</div>
    <?php if($_SERVER['REQUEST_URI']!= '/dashboard') {?>
    <div class="GoBack">
        <button class="btn btn-danger" id="backBTN" onclick="goBack()">بازگشت به صفحه قبل</button>
	</div>
    <?php } ?>
    <div class="topnav-modal-profile element-animation">
		<h6>پروفایل</h6>
		<img class="profile-images img-responsive" src="<?php if($filename == ""){ echo $engine->get_theme_url() . "/images/profile.jpg"; } else { echo $url . "/gt-content/uploads/profile/" . $filename; } ?>">
		<div class="center"><h5 class="user-grade"><?php if($level == 'manager') { echo "مدیریت"; } else if($level == 'coach') { echo "مربی"; } else { echo "بازیکن"; }?></h5></div>
		<ul>
			<li><a href="<?php if($level == 'manager' || $level == 'coach') { echo "profile"; } else { echo "profile_player"; } ?>"><i class="icofont-ui-edit"></i> ویرایش پروفایل</a></li>
			<li><a href="<?php echo $url; ?>"><i class="icofont-ui-power"></i> خروج از حساب کاربری</a></li>
		</ul>
	</div>
</div>