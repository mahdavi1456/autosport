/*function showloader(){
	document.body.innerHTML += "<div id='gt-loading'></div>";
}

function hideloader() {
	var el = document.getElementById('gt-loading');
	el.remove();
}*/
(function($){
	$(document).ready(function() {
		$('.timepicker').timepicki({
		show_meridian:false,
		min_hour_value:0,
		max_hour_value:23,
		overflow_minutes:true,
		increase_direction:'up',
		disable_keyboard_mobile: true});
		$(".date-picker").persianDatepicker();
		$('.select2').select2();
		$( '#loading').click(function() {
			//showloader();
		});
		
		$('#timepicker').timepicki({
		show_meridian:false,
		min_hour_value:0,
		max_hour_value:23,
		overflow_minutes:true,
		increase_direction:'up',
		disable_keyboard_mobile: true});
		$(".date-picker").persianDatepicker();
		$('.select2').select2();
		$( '#loading').click(function() {
			//showloader();
		});
	});
    $(window).on("load",function(){
		$("#SideNav").mCustomScrollbar();
		/*Menu*/
        function CloseMenu() {
            $("#SideNav").addClass("SideNavClose");
            $("#Content").addClass("ContentOpen");
        }
        function OpenMenu() {
            $("#SideNav").removeClass("SideNavClose");
            $("#Content").removeClass("ContentOpen");
        }
		
        $( ".MenuButton" ).click(function() {
			$('body').toggleClass('OpenMenus');
			if ($('body').hasClass('OpenMenus')) {
				OpenMenu();
				
			} else{
				CloseMenu();
			}
		});
		/*Modal Profile*/
		$( ".Usernamewlc" ).click(function() {
			$(this).toggleClass('OpenUsernamewlc');
			if ($(this).hasClass('OpenUsernamewlc')) {
				$(".topnav-modal-profile").css("display", "block");
				$(".TopContent .Usernamewlc .profile-username").css("background", "#ff5000");
				$(".TopContent .Usernamewlc .profile-username").css("color", "#Fff");
			} else{
				$(".topnav-modal-profile").css("display", "none");
				$(".TopContent .Usernamewlc .profile-username").css("background", "transparent");
				$(".TopContent .Usernamewlc .profile-username").css("color", "#fff");
			}
		});
		$(document).mouseup(function(e) 
		{
			var box = $(".topnav-modal-profile");
			if (!box.is(e.target) && box.has(e.target).length === 0) 
			{
				$(".Usernamewlc").removeClass('OpenUsernamewlc');
				$(".topnav-modal-profile").css("display", "none");
				$(".TopContent .Usernamewlc .profile-username").css("background", "transparent");
				$(".TopContent .Usernamewlc .profile-username").css("color", "#Fff");
			}
		});
		//Sub Menu
		if($(".menu-has-sub > ul").hasClass("active-current-menu")){
			$(".active-current-menu").prev("a").addClass('menu-ico-arrdwn');
		}else{
			$(".active-current-menu").prev("a").removeClass('menu-ico-arrdwn');
		}
		$(".menu-has-sub > a").removeAttr('href');
		$('.menu-has-sub').click(function() {
			$(this).toggleClass('OpenSubMenu');
			if ($(this).hasClass('OpenSubMenu')) {
				$(this).find("ul").css("display", "block");
				$(this).find("a").addClass('menu-ico-arrdwn');
			} else{
				$(this).find("ul").css("display", "none");
				$(this).find("a").removeClass('menu-ico-arrdwn');
			}
		});
		//SHow Password
		$(".FormLoginPasswordInputBtn").click(function() {
		  $(this).find('i').toggleClass("icofont-eye-blocked");
		  if ($('.FormLoginPasswordInput').attr("type") == "password") {
			$('.FormLoginPasswordInput').attr("type", "text");
		  } else {
			$('.FormLoginPasswordInput').attr("type", "password");
		  }
		});
		
		$('.Cselect').each(function(){
			var $this = $(this), numberOfOptions = $(this).children('option').length;
		  
			$this.addClass('select-hidden'); 
			$this.wrap('<div class="select"></div>');
			$this.after('<div class="select-styled"></div>');

			var $styledSelect = $this.next('div.select-styled');
			$styledSelect.text($this.children('option').eq(0).text());
		  
			var $list = $('<ul />', {
				'class': 'select-options'
			}).insertAfter($styledSelect);
		  
			for (var i = 0; i < numberOfOptions; i++) {
				$('<li />', {
					text: $this.children('option').eq(i).text(),
					rel: $this.children('option').eq(i).val()
				}).appendTo($list);
			}
		  
			var $listItems = $list.children('li');

		 
		  
		  $styledSelect.click(function(e) {
			 if($('.select-options').is(':visible')) {
				e.stopPropagation();
				$styledSelect.text($(this).text()).removeClass('active');
				$this.val($(this).attr('rel'));
			  
				$list.hide();
				//console.log($this.val());   

			 } else {
				e.stopPropagation();
				$('div.select-styled.active').each(function(){
					$(this).removeClass('active').next('ul.select-options').hide();
				});
				$(this).toggleClass('active').next('ul.select-options').toggle();
			 }//end if
			});
		  
			$listItems.click(function(e) {
				e.stopPropagation();
				$styledSelect.text($(this).text()).removeClass('active');
				$this.val($(this).attr('rel'));
				$list.hide();
				//console.log($this.val());
			});
			
			$(document).click(function() {
				$styledSelect.removeClass('active');
				$list.hide();
			});

		});
		
		
    });
})(jQuery);

function printFunc() {
    var divToPrint = document.getElementById('printarea');
    var htmlToPrint = '' +
        '<style type="text/css">' +
        'body {' +
			'direction:rtl;' +
			'font-family: "iranyekan";' +
		'}' +
		'table {' +
			'width:100%;' +
		'}' +
        'table th, table td {' +
        'border-top:1px solid #ddd;' +
        'direction:rtl;' +
		'text-align:right;' +
        'padding;8px 0;' +
		'font-size: 9pt;' +
        '}' +
        '</style>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
}