<?php 
function check_active($page, $st) {
	$request = $_SERVER['REQUEST_URI'];
	if($page == $request) {
		if($st == 1) {
			echo "active-current-menu";
		} else {
			echo "iactive";
		}
	}
}
$level = $_SESSION["level"];
 ?>
<div id="SideNav" data-mcs-theme="minimal-dark">
	<div class="LogoSideNav LogoSideNavMob">
		<img src="<?php echo $engine->get_theme_url(); ?>images/logodDd.png" width="180px" class="img-responsive">
	</div>
	<div class="Navbox">
		<div class="Menu">
			<?php
			if($level == 'manager'){ ?>
				<ul>
					<li class="<?php check_active("/dashboard", 0); ?>"><a href="dashboard"><i class="icofont-brand-windows"></i> داشبرد</a></li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-ui-user"></i>مدیریت کاربران</a>
						<ul class="<?php check_active("/player", 1); check_active("/coach", 1); check_active("/player_list", 1); check_active("/coach_list", 1);?>">
							<li class="<?php check_active("/player", 0); ?>"><a href="player">بازیکنان</a></li>
							<li class="<?php check_active("/coach", 0); ?>"><a href="coach">مربیان</a></li>
							<li class="<?php check_active("/player_list", 0); ?>"><a href="player_list">لیست بازیکنان</a></li>
							<li class="<?php check_active("/coach_list", 0); ?>"><a href="coach_list">لیست مربیان</a></li>
						</ul>
					</li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-ui-user"></i>عملیات</a>
						<ul class="<?php check_active("/sport_register", 1); check_active("/station", 1); check_active("/test_result", 1); check_active("/food_plan", 1); check_active("/duties", 1); check_active("/rollcall", 1); ?>">
							<li class="<?php check_active("/sport_register", 0); ?>"><a href="sport_register">ثبت نام</a></li>
							<li class="<?php check_active("/station", 0); ?>"><a href="station">ایستگاه ها</a></li>
							<li class="<?php check_active("/test_result", 0); ?>"><a href="test_result">نتایج آزمون</a></li>
							<li class="<?php check_active("/food_plan", 0); ?>"><a href="food_plan">برنامه غذایی</a></li>
							<li class="<?php check_active("/duties", 0); ?>"><a href="duties">تمرینات</a></li>
							<li class="<?php check_active("/rollcall", 0); ?>"><a href="rollcall">حضور و غیاب</a></li>
						</ul>
					</li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-list"></i>غربالگری</a>
						<ul class="<?php check_active("/screening", 1); check_active("/parameter", 1); check_active("/screening_index", 1); check_active("/ages", 1); ?>">
							<li class="<?php check_active("/screening_index", 0); ?>"><a href="screening_index">شاخص ها</a></li>
							<li class="<?php check_active("/parameter", 0); ?>"><a href="parameter">پارامترها</a></li>
							<li class="<?php check_active("/screening", 0); ?>"><a href="screening">تست غربالگری</a></li>
						</ul>
					</li>
					<li class="<?php check_active("/untropo", 0); ?>"><a href="untropo"><i class="icofont-list"></i> پیکرسنجی</a></li>
					<li class="<?php check_active("/reports", 0); ?>"><a href="reports"><i class="icofont-bullhorn"></i>گزارشات</a></li>
					<li class="<?php check_active("/about_us", 0); ?>"><a href="about_us"><i class="icofont-info"></i> درباره ما</a></li>
				</ul>
				<?php
			}
			else if($level == 'coach'){ ?>
				<ul>
					<li class="<?php check_active("/dashboard", 0); ?>"><a href="dashboard"><i class="icofont-brand-windows"></i> داشبرد</a></li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-ui-user"></i>مدیریت کاربران</a>
						<ul class="<?php check_active("/player_list", 1); ?>">
							<li class="<?php check_active("/player_list", 0); ?>"><a href="player_list">لیست بازیکنان</a></li>
						</ul>
					</li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-ui-user"></i>عملیات</a>
						<ul class="<?php check_active("/sport_register", 1); check_active("/station", 1); check_active("/test_result", 1); check_active("/food_plan", 1); check_active("/duties", 1); check_active("/rollcall", 1); ?>">
							<li class="<?php check_active("/test_result", 0); ?>"><a href="test_result">نتایج آزمون</a></li>
							<li class="<?php check_active("/food_plan", 0); ?>"><a href="food_plan">برنامه غذایی</a></li>
							<li class="<?php check_active("/duties", 0); ?>"><a href="duties">تمرینات</a></li>
							<li class="<?php check_active("/rollcall", 0); ?>"><a href="rollcall">حضور و غیاب</a></li>
						</ul>
					</li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-list"></i>غربالگری</a>
						<ul class="<?php check_active("/screening", 1); check_active("/parameter", 1); check_active("/screening_index", 1); check_active("/ages", 1); ?>">
							<li class="<?php check_active("/screening", 0); ?>"><a href="screening">تست غربالگری</a></li>
						</ul>
					</li>
					<li class="<?php check_active("/untropo", 0); ?>"><a href="untropo"><i class="icofont-list"></i> پیکرسنجی</a></li>
					<li class="<?php check_active("/reports", 0); ?>"><a href="reports"><i class="icofont-bullhorn"></i>گزارشات</a></li>
					<li class="<?php check_active("/about_us", 0); ?>"><a href="about_us"><i class="icofont-info"></i> درباره ما</a></li>
				</ul>
				<?php
			}
			else if($level == 'player'){ ?>
				<ul>
					<li class="<?php check_active("/dashboard", 0); ?>"><a href="dashboard"><i class="icofont-brand-windows"></i> داشبرد</a></li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-ui-user"></i>مدیریت کاربران</a>
						<ul class="<?php check_active("/player", 1); check_active("/coach", 1); check_active("/player_list", 1); check_active("/coach_list", 1);?>">
							<li class="<?php check_active("/player", 0); ?>"><a href="player">بازیکنان</a></li>
							<li class="<?php check_active("/coach", 0); ?>"><a href="coach">مربیان</a></li>
							<li class="<?php check_active("/player_list", 0); ?>"><a href="player_list">لیست بازیکنان</a></li>
							<li class="<?php check_active("/coach_list", 0); ?>"><a href="coach_list">لیست مربیان</a></li>
						</ul>
					</li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-ui-user"></i>عملیات</a>
						<ul class="<?php check_active("/sport_register", 1); check_active("/station", 1); check_active("/test_result", 1); check_active("/food_plan", 1); check_active("/duties", 1); check_active("/rollcall", 1); ?>">
							<li class="<?php check_active("/sport_register", 0); ?>"><a href="sport_register">ثبت نام</a></li>
							<li class="<?php check_active("/station", 0); ?>"><a href="station">ایستگاه ها</a></li>
							<li class="<?php check_active("/test_result", 0); ?>"><a href="test_result">نتایج آزمون</a></li>
							<li class="<?php check_active("/food_plan", 0); ?>"><a href="food_plan">برنامه غذایی</a></li>
							<li class="<?php check_active("/duties", 0); ?>"><a href="duties">تمرینات</a></li>
							<li class="<?php check_active("/rollcall", 0); ?>"><a href="rollcall">حضور و غیاب</a></li>
						</ul>
					</li>
					<li class="menu-has-sub"><a href="#"><i class="icofont-list"></i>غربالگری</a>
						<ul class="<?php check_active("/screening", 1); check_active("/parameter", 1); check_active("/screening_index", 1); check_active("/ages", 1); ?>">
							<li class="<?php check_active("/screening_index", 0); ?>"><a href="screening_index">شاخص ها</a></li>
							<li class="<?php check_active("/parameter", 0); ?>"><a href="parameter">پارامترها</a></li>
							<li class="<?php check_active("/screening", 0); ?>"><a href="screening">تست غربالگری</a></li>
						</ul>
					</li>
					<li class="<?php check_active("/untropo", 0); ?>"><a href="untropo"><i class="icofont-list"></i> پیکرسنجی</a></li>
					<li class="<?php check_active("/reports", 0); ?>"><a href="reports"><i class="icofont-bullhorn"></i>گزارشات</a></li>
					<li class="<?php check_active("/about_us", 0); ?>"><a href="about_us"><i class="icofont-info"></i> درباره ما</a></li>
				</ul>
				<?php
			} ?>
		</div>
	</div>
</div>