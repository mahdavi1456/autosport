<?php include "header.php"; ?>
<div class="container-fluid no-padd">
	<?php
	include 'nav.php';
	include 'topnav.php';
	$coach = new gt_coach();
	$c_id = $_GET['ID'];
	$coach_name = $coach->get_coach_name($c_id);
	?>
	<div id="Content">
		<div class="MainContent">
			<div class="gtrow col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="gtrow-title">
					<h3><i class="icofont-drag2"></i> بازیکنان مربوط به <?PHP echo $coach_name; ?></h3>
				</div>
				<div class="create-form">
					<?php
					echo $coach->list_players_coach_view();
					?>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?php $_SERVER['DOCUMENT_ROOT']; ?>/gt-include/script/sport/sc-coach.js"></script>
</div>