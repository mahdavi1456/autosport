<!DOCTYPE html>	
<html lang="fa">
<head>
	<?php
	$root = $_SERVER['DOCUMENT_ROOT'] . "/Autotech";
	include $root . "/gt-loader.php";
	$gt_user = new gt_user();
	$gt_option = new gt_option(); ?>
	<meta http-equiv="content-type" content="text/html" charset="utf-8">
	<title></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="theme-color" content="#c31b1c">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="../../dist/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="../../dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../dist/js/script.js"></script>
	<script type="text/javascript" src="../../dist/js/owl.carousel.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padd">
			<div id="Content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="Contentbox col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="GtForm">
						<form action="" method="post">
							<div class="form-group">
							  <label for="email">نام کاربری</label>
							  <input type="text" class="form-control" id="email" name="username">
							</div>
							<div class="form-group">
							  <label for="pwd">رمز ورود</label>
							  <input type="password" class="form-control" id="pwd" name="password">
							</div>
							<div class="checkbox">
							  <label><input type="checkbox"> Remember me</label>
							</div>
							<button type="submit" class="btn btn-default" name="login">ورود</button>
						</form>
					</div>
					<?php
					if(isset($_POST['login'])){
						$username = $_POST['username'];
						$password = $_POST['password'];
						$login = $gt_user->check_login($username, $password);
						if($login == 1) {
							$user_id = $gt_user->get_user_id($username);
							$opt_home =  $gt_option->get_option('opt_home');
							$url = $opt_home . "full.php" . $user_id;
							?>
							<script type="text/javascript">
								window.location.href = "<?php echo $url; ?>";
							</script>
							<?php
						} else { ?>
							<br><div class="alert alert-danger">نام کاربری یا رمز وارد شده صحیح نمی باشد</div>
						<?php
						}
					} ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>