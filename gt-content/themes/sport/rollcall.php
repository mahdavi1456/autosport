<?php include "header.php"; ?>
<div class="container-fluid no-padd">
	<?php
	include 'nav.php';
	include 'topnav.php';
	?>
	<div id="Content">
		<div class="MainContent">
			<div class="gtrow col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="gtrow-title">
					<h3><i class="icofont-drag2"></i> حضور و غیاب</h3>
				</div>
				<div class="create-form">
					<?php
					$rollcall = new gt_rollcall();
					echo $rollcall->create_form();
					?>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?php $_SERVER['DOCUMENT_ROOT']; ?>/gt-include/script/sport/sc-rollcall.js"></script>
</div>