<?php include "header.php"; ?>
<div class="container-fluid no-padd">
	<?php
	include 'nav.php';
	include 'topnav.php';
	$gt_player = new gt_player();
	?>
	<div id="Content">
		<div class="MainContent">
			<div class="gtrow col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="gtrow-title">
					<h3><i class="icofont-drag2"></i> نتایج پیکرسنجی <?php echo $gt_player->get_player_name($_GET['p_id']); ?></h3>
				</div>
				<div class="create-form">
					<?php
					$coach = new gt_coach();
					echo $coach->list_untropo_result();
					?>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?php $_SERVER['DOCUMENT_ROOT']; ?>/gt-include/script/sport/sc-untropo.js"></script>
</div>