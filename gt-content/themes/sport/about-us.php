<?php include "header.php"; ?>
<div class="container-fluid no-padd">
	<?php
	include 'nav.php';
	include 'topnav.php';
	?>
	<div id="Content">
		<div class="MainContent">
			<div class="gtrow col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="gtrow-title">
					<h3><i class="icofont-drag2"></i> درباره ما</h3>
				</div>
				<div>
					<h4 style="margin-top:10px;color:#ff5000;"><strong>فلسفه باشگاه مس رفسنجان</strong></h4>
					<p style="line-height:2;margin-top:10px;">
						با فوتبال می‌توان یک شهر را جهانی کرد بله یک شهر، شهری با قدمت تاریخی رفسنجان را. مگر نه این که ما در این شهر در خیلی موارد اولین (بهترین) هستیم. مگر نه اینکه بهترین پسته دنیا را داریم. مگر نه اینکه بزرگترین معدن مس روباز دنیا از آن ماست. مگر نه اینکه بزرگترین خانه ی خشتی جهان در شهر ما قرار دارد. پس به احترام اینان نیز شده باید در فوتبال نیز اولین (بهترین) باشیم. ما به فوتبال نیازمندیم. فوتبال بی معناست مگر اینکه خود به آن معنا دهیم. به زیبا بازی کردن و فوتبال تهاجمی ایمان داریم یعنی انتخابی نداریم جز اینکه انتخاب کنیم که زیبا و هجومی بازی کنیم. بنابراین باشگاه صنعت مس رفسنجان متعهد است که از تمامی ظرفیت ها و پتانسیل های خود در جهت رشد و توسعه فوتبال رده های سنی استفاده نماید.
					</p>
					</br>
					<center>
						<h4>شعار باشگاه</h4>
						<p style="margin-top:10px;color:#ff5000;font-weight:bold;">
							یک تیم، یک شهر، اتحاد و افتخار
						</p>
					</center>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?php $_SERVER['DOCUMENT_ROOT']; ?>/gt-include/script/sport/sc-ages.js"></script>
</div>