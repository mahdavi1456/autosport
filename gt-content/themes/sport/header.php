<?php session_start(); ?>
<!DOCTYPE html>	
<html lang="fa">
<head>
	<?php
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/engine.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-option.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-coach.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-station.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-food-plan.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-parameter.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-screening.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-test-result.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-untropo.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-screening-index.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-ages.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-sport-register.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-duties.php";
	include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-rollcall.php";
    include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-report.php";
	$engine = new engine();
	$db = new db();
	$gt_option = new gt_option();
	if(!isset($_SESSION['user_id'])) {
		$_SESSION = [];
		$opt_home =  $gt_option->get_option('opt_home');
		$url = $opt_home . "login";
		?>
		<script type="text/javascript">
			window.location.href = "<?php echo $url; ?>";
		</script>
		<?php
	}
	$gt_date = new gt_date();
	$todate88 = $gt_date->jgdate("1388/10/11");
	$fromdate87 = $gt_date->jgdate("1387/10/11");
	$todate87 = $gt_date->jgdate("1387/10/12");
	$fromdate86 = $gt_date->jgdate("1386/10/10");
	$todate86 = $gt_date->jgdate("1386/10/11");
	$fromdate85 = $gt_date->jgdate("1385/10/10");
	$todate85 = $gt_date->jgdate("1385/10/11");
	$fromdate84 = $gt_date->jgdate("1384/10/10");
	$todate84 = $gt_date->jgdate("1384/10/11");
	$fromdate83 = $gt_date->jgdate("1383/10/11");
	$todate83 = $gt_date->jgdate("1383/10/12");
	$fromdate82 = $gt_date->jgdate("1382/10/10");
	$todate82 = $gt_date->jgdate("1382/10/11");
	$fromdate79 = $gt_date->jgdate("1379/10/11");
	$todate79 = $gt_date->jgdate("1379/10/12");
	$fromdate77 = $gt_date->jgdate("1377/10/10");
	$todate77 = $gt_date->jgdate("1377/10/11");
	$fromdate65 = $gt_date->jgdate("1365/10/10");
	$playeru21 = $db->get_select_query("select * from player where (date_birth between '$fromdate65' and '$todate77')");
	if(count($playeru21) > 0) {
		foreach($playeru21 as $row21) {
			$p_id = $row21['ID'];
			$db->ex_query("update player set p_ages = '21', p_category = 4 where ID = $p_id");
		}
	}
	$playeru19 = $db->get_select_query("select * from player where (date_birth between '$fromdate77' and '$todate79')");
	if(count($playeru19) > 0) {
		foreach($playeru19 as $row19) {
			$p_id = $row19['ID'];
			$db->ex_query("update player set p_ages = '19', p_category = 2 where ID = $p_id");
		}
	}
	$playeru16 = $db->get_select_query("select * from player where (date_birth between '$fromdate79' and '$todate82')");
	if(count($playeru16) > 0) {
		foreach($playeru16 as $row16) {
			$p_id = $row16['ID'];
			$db->ex_query("update player set p_ages = '16', p_category = 2 where ID = $p_id");
		}
	}
	$playeru15 = $db->get_select_query("select * from player where (date_birth between '$fromdate82' and '$todate83')");
	if(count($playeru15) > 0) {
		foreach($playeru15 as $row15) {
			$p_id = $row15['ID'];
			$db->ex_query("update player set p_ages = '15', p_category = 1 where ID = $p_id");
		}
	}
	$playeru14 = $db->get_select_query("select * from player where (date_birth between '$fromdate83' and '$todate84')");
	if(count($playeru14) > 0) {
		foreach($playeru14 as $row14) {
			$p_id = $row14['ID'];
			$db->ex_query("update player set p_ages = '14', p_category = 1 where ID = $p_id");
		}
	}
	$playeru13 = $db->get_select_query("select * from player where (date_birth between '$fromdate84' and '$todate85')");
	if(count($playeru13) > 0) {
		foreach($playeru13 as $row13) {
			$p_id = $row13['ID'];
			$db->ex_query("update player set p_ages = '13', p_category = 0 where ID = $p_id");
		}
	}
	$playeru12 = $db->get_select_query("select * from player where (date_birth between '$fromdate85' and '$todate86')");
	if(count($playeru12) > 0) {
		foreach($playeru12 as $row12) {
			$p_id = $row12['ID'];
			$db->ex_query("update player set p_ages = '12', p_category = 0 where ID = $p_id");
		}
	}
	$playeru11 = $db->get_select_query("select * from player where (date_birth between '$fromdate86' and '$todate87')");
	if(count($playeru11) > 0) {
		foreach($playeru11 as $row11) {
			$p_id = $row11['ID'];
			$db->ex_query("update player set p_ages = '11', p_category = 0 where ID = $p_id");
		}
	}
	$playeru10 = $db->get_select_query("select * from player where (date_birth between '$fromdate87' and '$todate88')");
	if(count($playeru10) > 0) {
		foreach($playeru10 as $row10) {
			$p_id = $row10['ID'];
			$db->ex_query("update player set p_ages = '10', p_category = 0 where ID = $p_id");
		}
	}
	
	/*$player = $db->get_select_query("select * from player");
	if(count($player) > 0) {
		foreach($player as $row) {
			$p_id = $row['ID'];
			$personnel_id = $row['personnel_id'];
			$pass = md5($personnel_id);
			$db->ex_query("update player set p_username = '$personnel_id', p_password = '$pass' where ID = $p_id");
		}
	}*/
	?>
	<meta http-equiv="content-type" content="text/html" charset="utf-8">
	<title></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="theme-color" content="#ff5000">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo $engine->get_theme_url(); ?>css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $engine->get_theme_url(); ?>css/TimeCircles.css">
    <link href="<?php echo $engine->get_theme_url(); ?>js/summernote/summernote.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/scripts.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/select2.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/persianDatepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/Chart.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/Chart.bundle.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/timepicki.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/TimeCircles.js"></script>
	<script type="text/javascript" src="<?php echo $engine->get_theme_url(); ?>js/canvasjs.min.js"></script>
    <script src="<?php echo $engine->get_theme_url(); ?>js/summernote/summernote.min.js"></script>
</head>
<body class="OpenMenus">
	<input type="hidden" id="home-url" value="<?php echo $engine->get_home_url(); ?>">