<?php include "header.php"; ?>
<div class="container-fluid no-padd">
	<?php
	include 'nav.php';
	include 'topnav.php';
	$player = new gt_player();
	$coach = new gt_coach();
	$p_id = $_GET['p_id'];
	$player_name = $player->get_player_name($p_id);
	?>
	<div id="Content">
		<div class="MainContent">
			<div class="gtrow col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="gtrow-title">
					<h3><i class="icofont-drag2"></i> گزارش حضور و غیاب <?PHP echo $player_name; ?></h3>
				</div>
				<div class="create-form">
					<?php
					echo $coach->rollcall_list_view();
					?>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?php $_SERVER['DOCUMENT_ROOT']; ?>/gt-include/script/sport/sc-coach.js"></script>
</div>