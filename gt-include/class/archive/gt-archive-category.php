<?php
//مقطع
class gt_archive_category {
	
	public function add_archive_category($array) {
		$db = new db();
		$ac_name = $array['ac_name'];
		$ac_parent = $array['ac_parent'];
		$sql = "insert into archive_category(ac_name, ac_parent) values('$ac_name', $ac_parent)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_archive_category($array) {
		$db = new db();
		$ID = $array['ID'];
		$ac_name = $array['ac_name'];
		$ac_parent = $array['ac_parent'];
		$sql = "update archive_category set ac_name = '$ac_name', ac_parent = $ac_parent where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_archive_category($ID) {
		$db = new db();
		$res = $db->get_select_query("select * from archive_category where ac_parent = $ID");
		if(count($res) > 0) {
			foreach($res as $row) {
				$ac_id = $row['ID'];
				$res2 = $db->get_select_query("select * from archive_document where ac_id = $ac_id");
				if(count($res2) > 0) {
					foreach($res2 as $row2) {
						$ad_id = $row2['ID'];
						$link = $db->get_var_query("select ad_file from archive_document where ID = $ad_id");
						$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/archive/" . $link;
						if(unlink($path)){
							$sql = $db->ex_query("delete from archive_document where ID = $ad_id");
						}
					}
				}
				$sql = $db->ex_query("delete from archive_category where ID = $ac_id");
			}
		}
		
		$res2 = $db->get_select_query("select * from archive_document where ac_id = $ID");
		if(count($res2) > 0) {
			foreach($res2 as $row2) {
				$ad_id = $row2['ID'];
				$link = $db->get_var_query("select ad_file from archive_document where ID = $ad_id");
				$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/archive/" . $link;
				if(unlink($path)){
					$sql = $db->ex_query("delete from archive_document where ID = $ad_id");
				}
			}
		}
		$db->ex_query("delete from archive_category where ID = $ID");
	}
	
	public function get_archive_category_name($ID) {
		$db = new db();
		$ac_name = $db->get_var_query("select ac_name from archive_category where ID = $ID");
		return $ac_name;
	}
	
	public function create_form($ID = null) {
		$db = new db();
		if($ID == null) {
			$ac_name = "";
			$ac_parent = "";
		} else {
			$asb = $db->get_select_query("select * from archive_category where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$ac_name = $roww['ac_name'];
					$ac_parent = $roww['ac_parent'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-archive-category-view">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>نام دسته بندی <span class="red">*</span></label>
								<input type="text" name="ac_name" value="<?php echo $ac_name; ?>" placeholder="نام دسته بندی" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>دسته بندی والد <span class="red">*</span></label>
								<select name="ac_parent" class="form-control select2">
									<option value="0" <?php if($ac_parent == 0) { echo 'selected';} ?>></option>
									<?php
									$db = new db();
									$res = $db->get_select_query("select * from archive_category order by ID desc");
									if(count($res) > 0) {
										foreach($res as $row) {
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $ac_parent) { echo 'selected';} ?>><?php echo $row["ac_name"]; ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_archive_category" class="btn btn-sm btn-success add-archive-category">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_archive_category" class="btn btn-sm btn-warning update-archive-category">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-archive-category-view-result"><?php $this->list_archive_category_view(); ?></div>
		<?php
	}

    public function list_archive_category_view() {
        $db = new db();
        ?>
        <div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام دسته بندی</th>
							<th>دسته بندی والد</th>
							<th>مدیریت</th>							
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$list = $db->get_select_query("select * from archive_category order by ID desc");
                        if(count($list) > 0) {
                            foreach($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $pr->per_number($row['ac_name']); ?></td> 
									<td><?php $name = $this->get_archive_category_name($row['ac_parent']); echo $pr->per_number($name); ?></td>  									
									<td>
										<button class="btn btn-info btn-xs update-archive-category-form" data-id="<?php echo $row['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $row['ID']; ?>" class="btn btn-danger btn-xs remove-archive-category">حذف</button>
									</td>									
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
							?>
							<tr>
								<td colspan="4">هیچ دسته بندی تاکنون تعریف نشده است</td>
							</tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }
}