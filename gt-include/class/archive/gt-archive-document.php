<?php
//مقطع
class gt_archive_document {
	
	public function add_archive_document($array) {
		$db = new db();
		$ad_file = $array['ad_file'];
		$ac_id = $array['ac_id'];
		$ad_name = $array['ad_name'];
		$ad_note = $array['ad_note'];
		$sql = "insert into archive_document(ad_file, ac_id, ad_name, ad_note) values('$ad_file' ,$ac_id, '$ad_name', '$ad_note')";
		$last_id = $db->ex_query($sql);
		return $sql;
	}
	
	public function update_archive_document($array) {
		$db = new db();
		$ID = $array['ID'];
		$ac_id = $array['ac_id'];
		$ad_name = $array['ad_name'];
		$ad_note = $array['ad_note'];
		$sql = "update archive_document set ac_id = $ac_id, ad_name = '$ad_name', ad_note = '$ad_note' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_archive_document($ID) {
		$db = new db();
		$link = $db->get_var_query("select ad_file from archive_document where ID = $ID");
		$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/archive/" . $link;
		if(unlink($path)){
			$sql = "delete from archive_document where ID = $ID";
			$db->ex_query($sql);
		}
		return $path;
	}
	
	public function create_form($ID = null) {
		$db = new db();
		$ac_parent = $_GET['id'];
		$count = 0;
		$contt = 0;
		$ary = array();
		$parent = $ac_parent;
		while($count >= 0){
			$ary[$count] = $parent;
			$parent = $db->get_var_query("select ac_parent from archive_category where ID = $parent");
			$count++;
			if($parent == 0){
				$count = -1;
			}
		}
		$ary = array_reverse($ary);
		$contt = count($ary);
		?>
		<div class="row">
			<div class="col-md-12">
				<a href="/archive_document?id=0">اسناد</a>
				<?php
				if($ac_parent !=0){
					for($i=0; $i < $contt; $i++){
						$id = $ary[$i];
						$sql = $db->get_select_query("select * from archive_category where ID = $id");?>
						<a href="/archive_document?id=<?php echo $sql[0]['ID']; ?>">/<?php echo $sql[0]['ac_name']; ?></a>
						<?php
					}
				} ?>
				<div style="float: left;">
					<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">ساخت پوشه</button>
					<?php
					if($ac_parent !=0){ ?>
						<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#uploadModal">آپلود سند</button>
					<?php } ?>
				</div>
			</div>
		</div><hr>
		<div class="row" id="archive-category">
			<div class="col-md-12">
				<?php
				$res = $db->get_select_query("select * from archive_category where ac_parent = $ac_parent order by ID desc");
				if(count($res) > 0) {
					foreach($res as $row2) {
						?>
						<a href="/archive_document?id=<?php echo $row2["ID"]; ?>"><i class="icofont-folder"></i> <?php echo $row2["ac_name"]; ?></a>
						<button type="button" class="btn btn-danger btn-xs remove-category" data-id="<?php echo $row2['ID']; ?>" data-acid="<?php echo $ac_parent; ?>"><i class="icofont-close"></i></button>
						<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#updatecategory<?php echo $row2['ID']; ?>"><i class="icofont-ui-edit"></i></button>&nbsp;
						<div id="updatecategory<?php echo $row2['ID']; ?>" class="modal fade" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">ویرایش پوشه</h4>
									</div>
									<div class="modal-body">
										<div id="update-archive-category-view">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label>نام پوشه <span class="red">*</span></label>
														<input type="text" placeholder="نام پوشه" class="form-control required1<?php echo $row2['ID']; ?>" value="<?php echo $row2['ac_name']; ?>" name="ac_name<?php echo $row2['ID']; ?>">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<center>
											<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
											<button class="btn btn-primary btn-sm update-archive-category" data-id="<?php echo $row2['ID']; ?>" data-acid="<?php echo $ac_parent; ?>" type="submit">ذخیره</button>
										</center>
									</div>
								</div>
							</div>
						</div>
						<?php
					}
				}
				?>
			</div>
		</div><hr>
		<div class="row" id="archive-document">
			<div class="col-md-12">
				<?php
				$list = $db->get_select_query("select * from archive_document where ac_id = $ac_parent");
				if(count($list) > 0) {
					foreach($list as $l) {
					?>
					<a href="http://gtserver.ir/gt-content/uploads/archive/<?php echo $l['ad_file']; ?>" target="_blank">
						<img style="width: 10%; border-radius: 4px; padding: 1px; border: 2px solid #ddd; display: inline;" src="http://gtserver.ir/gt-content/uploads/archive/<?php echo $l['ad_file']; ?>" class="upload-preview image-preview img-responsive">
					</a>
					<button type="button" class="btn btn-danger btn-xs remove-document" data-id="<?php echo $l['ID']; ?>" data-acid="<?php echo $ac_parent; ?>"><i class="icofont-close"></i></button>
					<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#updatedocument<?php echo $l['ID']; ?>"><i class="icofont-ui-edit"></i></button>
					<div id="updatedocument<?php echo $l['ID']; ?>" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">ویرایش فایل</h4>
								</div>
								<div class="modal-body">
									<div id="update-archive-document-view">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>عنوان فایل <span class="red">*</span></label>
													<input type="text" placeholder="عنوان فایل" class="form-control required3<?php echo $l['ID']; ?>" value="<?php echo $l['ad_name']; ?>" name="ad_name<?php echo $l['ID']; ?>">
													<input type="hidden" name="ac_parent<?php echo $l['ID']; ?>" value="<?php echo $ac_parent; ?>">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label>یادداشت</label>
													<input type="text" placeholder="یادداشت" class="form-control" name="ad_note<?php echo $l['ID']; ?>" value="<?php echo $l['ad_note']; ?>">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<center>
										<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
										<button class="btn btn-primary btn-sm update-archive-document" data-id="<?php echo $l['ID']; ?>" data-iid="<?php echo $l['ID']; ?>" type="submit">ذخیره</button>
									</center>
								</div>
							</div>
						</div>
					</div>
					<?php
					}
				}
				?>
			</div>
		</div>
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">ساخت پوشه</h4>
					</div>
					<div class="modal-body">
						<div id="add-archive-category-view">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>نام پوشه <span class="red">*</span></label>
										<input type="text" placeholder="نام پوشه" class="form-control required" name="ac_name" id="ac-name">
										<input type="hidden" name="ac_parent" value="<?php echo $ac_parent; ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<center>
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
							<button class="btn btn-primary btn-sm add-archive-category" data-id="<?php echo $ac_parent; ?>" type="submit">ذخیره</button>
						</center>
					</div>
				</div>
			</div>
		</div>
		<div id="uploadModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<form id="uploadForm" action="" method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">آپلود سند</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>عنوان فایل <span class="red">*</span></label>
										<input type="text" placeholder="عنوان فایل" class="form-control required2" name="ad_name" id="ad-name">
									</div>
								</div>
								<div class="col-md-6">
									<label>انتخاب فایل <span class="red">*</span></label>
									<input name="ad_file" type="file" class="inputFile" id="ad-file" value="0"/><br/>
									<input name="ac_id" type="hidden" value="<?php echo $ac_parent; ?>">
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>یادداشت</label>
										<input type="text" placeholder="یادداشت" class="form-control" name="ad_note" id="adnote">
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<center>
								<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
								<button class="btn btn-primary btn-sm btnSubmit" type="submit">ذخیره</button>
							</center>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
	}
	
	public function list_archive_document_view() {
        $db = new db();
        ?>
        <div class="row">
			<div class="col-md-12">
			
            </div>
        </div>
        <?php
    }
}