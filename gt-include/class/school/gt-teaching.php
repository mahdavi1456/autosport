<?php
//تدریس
class gt_teaching {
	
	public function add_teaching($array) {
		$db = new db();
		$c_id = $array['c_id'];
		$t_id = $array['t_id'];
		$l_id = $array['l_id'];
		$sql = "insert into teaching(c_id, t_id, l_id) values($c_id, $t_id, $l_id)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_teaching($array) {
		$db = new db();
		$ID = $array['ID'];
		$c_id = $array['c_id'];
		$t_id = $array['t_id'];
		$l_id = $array['l_id'];
		$sql = "update teaching set c_id = $c_id, t_id = $t_id, l_id = $l_id where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_teaching($ID) {
		$db = new db();
		$sql = "delete from teaching where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function create_form($ID = null) {
		$gt_grade_year = new gt_grade_year();
		$gt_teacher = new gt_teacher();
		$gt_school = new gt_school();
		$db = new db();
		if($ID == null) {
			$c_id = "";
			$t_id = "";
			$l_id = "";
		} else {
			$asb = $db->get_select_query("select * from teaching where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$c_id = $roww['c_id'];
					$t_id = $roww['t_id'];
					$l_id = $roww['l_id'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-teaching-view">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>نام درس <span class="red">*</span></label>
								<select name="l_id" class="form-control select2">
									<?php
									$lesson = $db->get_select_query("select * from lesson");
									if(count($lesson > 0)) {
										foreach($lesson as $row){
											?>
											<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $l_id) { echo 'selected';} ?>><?php $g_y_id = $row['g_y_id']; echo $row['l_name'] . " " . $gt_grade_year->get_grade_year_name($g_y_id); ?></option>
											<?php
										}
									} ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>کلاس <span class="red">*</span></label>
								<select name="c_id" class="form-control select2">
									<?php
									$school = $db->get_select_query("select * from classroom");
									if(count($school > 0)) {
										foreach($school as $row) {
											?>
											<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $c_id) { echo 'selected';} ?>><?php $s_id = $row['s_id']; echo $row['c_name'] . " " . $gt_school->get_school_name($s_id); ?></option>
											<?php
										}
									} ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>معلم مربوط <span class="red">*</span></label>
								<select name="t_id" class="form-control select2">
									<?php
									$teacher = $db->get_select_query("select * from teacher");
									if(count($teacher > 0)) {
										foreach($teacher as $row){
											?>
											<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $t_id) { echo 'selected';} ?>><?php echo $gt_teacher->get_teacher_name($row["ID"]); ?></option>
											<?php
										}
									} ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_teaching" class="btn btn-sm btn-success add-teaching">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_teaching" class="btn btn-sm btn-warning update-teaching">ویرایش</button>
								<input type="hidden" name="ID" value="<?php echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-teaching-view-result"><?php $this->list_teaching_view(); ?></div>
		<?php
	}
		
	public function list_teaching_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام درس</th>
							<th>نام کلاس</th>
							<th>معلم مربوط</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$db = new db();
						$list = $db->get_select_query("select * from teaching order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php
										$gt_lesson = new gt_lesson();
										$gt_grade_year = new gt_grade_year();
										$l_id = $l['l_id'];
										$g_y_id = $db->get_var_query("select g_y_id from lesson where ID = $l_id");
										$teach = $gt_lesson->get_lesson_name($l['l_id']) . " " . $gt_grade_year->get_grade_year_name($g_y_id);
										echo $pr->per_number($teach);
										?>
									</td>
									<td><?php
										$gt_classroom = new gt_classroom();
										$gt_school = new gt_school();
										$c_id = $l['c_id'];
										$s_id = $db->get_var_query("select s_id from classroom where ID = $c_id");
										$class_name = $gt_classroom->get_classroom_name($l['c_id']) . " " . $gt_school->get_school_name($s_id);
										echo $pr->per_number($class_name);
										?>
									</td>
									<td><?php
										$gt_teacher = new gt_teacher();
										$t_id = $gt_teacher->get_teacher_name($l['t_id']);
										echo $pr->per_number($t_id);
										?>
									</td>
									<td>
										<button class="btn btn-info btn-xs update-teaching-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-teaching">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="6">تاکنون تدریسی ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}

}