<?php
//مدرسه
class gt_school{
	
		
	public function add_school($array) {
		$db = new db();
		$s_name = $array['s_name'];
		$s_type = $array['s_type'];
		$g_id = $array['g_id'];
		$t_id = $array['t_id'];
		$s_address = $array['s_address'];
		$sql = "insert into school(s_name, s_type, g_id, t_id, s_address) values('$s_name', '$s_type', $g_id, $t_id, '$s_address')";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_school($array) {
		$db = new db();
		$ID = $array['ID'];
		$s_name = $array['s_name'];
		$s_type = $array['s_type'];
		$g_id = $array['g_id'];
		$t_id = $array['t_id'];
		$s_address = $array['s_address'];
		$sql = "update school set s_name = '$s_name', s_type = '$s_type', g_id = $g_id, t_id = $t_id, s_address = '$s_address' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_school($ID) {
		$db = new db();
		$sql = "delete from school where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_school_name($ID) {
		$db = new db();
		$s_name = $db->get_var_query("select s_name from school where ID = $ID");
		return $s_name;
	}
	
	public function create_form($ID = null) {
		$gt_teacher = new gt_teacher();
		$db = new db();
		if($ID == null) {
			$s_name = "";
			$s_type = "";
			$g_id = "";
			$t_id = "";
			$s_address = "";
		} else {
			$asb = $db->get_select_query("select * from school where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$s_name = $roww['s_name'];
					$s_type = $roww['s_type'];
					$g_id = $roww['g_id'];
					$t_id = $roww['t_id'];
					$s_address = $roww['s_address'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-school-view">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>نام مدرسه <span class="red">*</span></label>
								<input type="text" name="s_name" value="<?php echo $s_name; ?>" placeholder="نام مدرسه" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نوع مدرسه <span class="red">*</span></label>
								<select name="s_type" class="form-control select2">
									<option value="دولتی" <?php if($s_type == 'دولتی') { echo 'selected';} ?>>دولتی</option>
									<option value="نمونه دولتی" <?php if($s_type == 'نمونه دولتی') { echo 'selected';} ?>>نمونه دولتی</option>
									<option value="فرزانگان" <?php if($s_type == 'فرزانگان') { echo 'selected';} ?>>فرزانگان</option>
									<option value="شاهد" <?php if($s_type == 'شاهد') { echo 'selected';} ?>>شاهد</option>
									<option value="فرهنگیان" <?php if($s_type == 'فرهنگیان') { echo 'selected';} ?>>فرهنگیان</option>
									<option value="هنرستان" <?php if($s_type == 'هنرستان') { echo 'selected';} ?>>هنرستان</option>
									<option value="فنی حرفه ای" <?php if($s_type == 'فنی حرفه ای') { echo 'selected';} ?>>فنی حرفه ای</option>
									<option value="خصوصی" <?php if($s_type == 'خصوصی') { echo 'selected';} ?>>خصوصی</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>مقطع <span class="red">*</span></label>
								<select name="g_id" class="form-control select2">
									<?php
									$db = new db();
									$res = $db->get_select_query("select * from grade order by ID desc");
									if(count($res) > 0) {
										foreach($res as $row) {
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $g_id) { echo 'selected';} ?>><?php echo $row["g_name"]; ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>مدیر <span class="red">*</span></label>
								<select name="t_id" class="form-control select2">
									<?php
									$teacher = $db->get_select_query("select * from teacher order by ID desc");
									if(count($teacher > 0)) {
										foreach($teacher as $row) {
											?>
											<option value="<?php echo $row['ID']; ?>" <?php if($row["ID"] == $t_id) { echo 'selected';} ?>><?php echo $gt_teacher->get_teacher_name($row["ID"]); ?></option>
											<?php
										}
									} ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>آدرس</label>
								<textarea name="s_address" placeholder="آدرس" class="form-control"><?php echo $s_address; ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_school" class="btn btn-sm btn-success add-school">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_school" class="btn btn-sm btn-warning update-school">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-school-view-result"><?php $this->list_school_view(); ?></div>
		<?php
	}

	public function list_school_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام مدرسه</th>
							<th>نوع مدرسه</th>
							<th>مقطع تحصیلی</th>
							<th>مدیر</th>
							<th>آدرس</th>
							<th>عملیات</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$db = new db();
						$gt_grade = new gt_grade();
						$gt_teacher = new gt_teacher();
						$list = $db->get_select_query("select * from school order by ID desc");
						if(count($list) > 0) {
							foreach($list as $l) {
								$g_id = $gt_grade->get_grade_name($l['g_id']);
								$t_id = $gt_teacher->get_teacher_name($l['t_id']);								
								?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['s_name']); ?></td>
									<td><?php echo $pr->per_number($l['s_type']); ?></td>
									<td><?php echo $pr->per_number($g_id); ?></td>
									<td><?php echo $pr->per_number($t_id); ?></td>
									<td><?php echo $pr->per_number($l['s_address']); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-school-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-school">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else { ?>
							<tr>
								<td colspan="7">هیچ مدرسه ای تاکنون ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}

}