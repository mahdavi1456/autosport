<?php
//ثبت نام
class gt_register {
	
	public function add_register($array) {
		$db = new db();
		$gt_date = new gt_date();
		$st_id = $array['st_id'];
		$c_id = $array['c_id'];
		$r_date = $array['r_date'];
		$r_date = $gt_date->jgdate($r_date);
		$r_time = $array['r_time'];
		$r_time = $gt_date->PersianToEnglish($r_time);
		$t_id = $array['t_id'];
		$sql = "insert into register(st_id, c_id, r_date, r_time, t_id) values($st_id, $c_id, '$r_date', '$r_time', $t_id)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_register($array) {
		$db = new db();
		$gt_date = new gt_date();
		$ID = $array['ID'];
		$st_id = $array['st_id'];
		$c_id = $array['c_id'];
		$r_date = $array['r_date'];
		$r_date = $gt_date->jgdate($r_date);
		$r_time = $array['r_time'];
		$r_time = $gt_date->PersianToEnglish($r_time);
		$t_id = $array['t_id'];
		$sql = "update register set st_id = $st_id, c_id = $c_id, r_date = '$r_date', r_time = '$r_time', t_id = $t_id where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_register($ID) {
		$db = new db();
		$sql = "delete from register where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_register_date($ID) {
		$db = new db();
		$r_date = $db->get_var_query("select r_date from register where ID = $ID");
		return $r_date;
	}
	
	public function create_form($ID = null) {
		$gt_teacher = new gt_teacher();
		$gt_student = new gt_student();
		$gt_school = new gt_school();
		$db = new db();
		if($ID == null) {
			$st_id = "";
			$c_id = "";
			$r_date = "";
			$r_time = "";
			$t_id = "";
		} else {
			$asb = $db->get_select_query("select * from register where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$st_id = $roww['st_id'];
					$c_id = $roww['c_id'];
					$r_date = $roww['r_date'];
					$r_time = $roww['r_time'];
					$t_id = $roww['t_id']; 
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-register-view">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>دانش آموز <span class="red">*</span></label>
								<select name="st_id" class="form-control select2">
									<?php		
									$student = $db->get_select_query("select * from student order by ID desc");
									if(count ($student > 0)) {
										foreach($student as $row) {
										?>
										<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $st_id) { echo 'selected';} ?>><?php echo $gt_student->get_student_name($row["ID"]); ?></option>
										<?php
										}
									} ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>کلاس <span class="red">*</span></label>
								<select name="c_id" class="form-control select2">
									<?php
									$school = $db->get_select_query("select * from classroom");
									if(count($school > 0)) {
										foreach($school as $row) {
											?>
											<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $c_id) { echo 'selected';} ?>><?php $s_id = $row['s_id']; echo $row['c_name'] . " " . $gt_school->get_school_name($s_id); ?></option>
											<?php
										}
									} ?>
								</select>
							</div>
						</div>
					<input type="hidden" name="r_date" value="<?php echo jdate('Y/m/d'); ?>">
					<input type="hidden" name="r_time" value="<?php echo jdate('H:i:s'); ?>">
						<div class="col-md-4">
							<div class="form-group">
								<label>مسئول ثبت نام <span class="red">*</span></label>
								<select name="t_id" class="form-control select2">
									<?php
									$teacher = $db->get_select_query("select * from teacher");
									if(count ($teacher > 0)) {
										foreach($teacher as $row){
											?>
											<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $t_id) { echo 'selected';} ?>><?php echo $gt_teacher->get_teacher_name($row["ID"]); ?></option>
											<?php
										}
									} ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_register" class="btn btn-sm btn-success add-register">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_register" class="btn btn-sm btn-warning update-register">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-register-view-result"><?php $this->list_register_view(); ?></div>
		<?php
	}
		
	public function list_register_view() {
		$db = new db();
		$gt_student = new gt_student();
		$gt_teacher = new gt_teacher();
		$gt_school = new gt_school();
		$gt_classroom = new gt_classroom();
		$gt_date = new gt_date();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>دانش آموز</th>
							<th>نام کلاس</th>
							<th>تاریخ</th>
							<th>ساعت</th>
							<th>مسئول ثبت نام</th>								
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$list = $db->get_select_query("select * from register order by ID desc");
						if(count($list) > 0) {
							foreach($list as $l) {
								$st_id = $gt_student->get_student_name($l['st_id']);
								?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($st_id); ?></td>
									<td><?php
										$c_id = $l['c_id'];
										$s_id = $db->get_var_query("select s_id from classroom where ID = $c_id");
										$class_name = $gt_classroom->get_classroom_name($l['c_id']) . " " . $gt_school->get_school_name($s_id);
										echo $pr->per_number($class_name);
										?>
									</td>
									<td><?php
									$date = $gt_date->jgdate($l['r_date']);
									echo $pr->per_number($date); ?></td>
									<td><?php echo $pr->per_number($l['r_time']); ?></td>
									<td><?php
									$t_id = $gt_teacher->get_teacher_name($l['t_id']);
									echo $pr->per_number($t_id); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-register-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-register">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else { ?>
							<tr>
								<td colspan="8">تا کنون هیچ ثبت نامی انجام نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}

}