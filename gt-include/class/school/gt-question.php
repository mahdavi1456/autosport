<?php
//سوال
class gt_question {
	
	public function add_question($array) {
		$db = new db();
		$gt_date = new gt_date();
		$q_type = $array['q_type'];
		$e_id = $array['e_id'];
		$q_deadline = $array['q_deadline'];
		$q_score = $array['q_score'];
		$q_date = $array['q_date'];
		$q_date = $gt_date->jgdate($q_date);
		$q_time = $array['q_time'];
		$t_id = $array['t_id'];
		$q_hardness = $array['q_hardness'];
		$q_opt1 = "";
		$q_opt2 = "";
		if($q_type == 3){
			$q_opt1 = "صحیح";
			$q_opt2 = "غلط";
		}
		$sql = "insert into question(q_type, e_id, q_deadline, q_score, q_date, t_id, q_hardness, q_opt1, q_opt2) values($q_type, $e_id, $q_deadline, $q_score, '$q_date', $t_id, $q_hardness, '$q_opt1' ,'$q_opt2')";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_question($array) {
		$db = new db();
		$gt_date = new gt_date();
		$ID = $array['ID'];
		$q_type = $array['q_type'];
		$e_id = $array['e_id'];
		$q_deadline = $array['q_deadline'];
		$q_score = $array['q_score'];
		$q_date = $array['q_date'];
		$q_date = $gt_date->jgdate($q_date);
		$q_time = $array['q_time'];
		$t_id = $array['t_id'];
		$q_hardness = $array['q_hardness'];
		$sql = "update question set q_type = $q_type, e_id = $e_id, q_deadline = $q_deadline, q_score = $q_score, q_date = '$q_date', t_id = $t_id, q_hardness = $q_hardness where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_question($ID) {
		$db = new db();
		$sql = "delete from question where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_question_type($q_type) {
		if($q_type == 0) {
			$type = "تستی";
		} else if($q_type == 1) {
			$type = "تشریحی";
		} else if($q_type == 2) {
			$type = "جاخالی";
		} else {
			$type = "صحیح و غلط";
		}
		return $type;
	}
	
	public function get_question_hardness($q_hardness) {
		$db = new db();
		if($q_hardness == 0) {
			$hardness = "آسان";
		} else if($q_hardness == 1) {
			$hardness = "معمولی";
		} else {
			$hardness = "سخت";
		}
		return $hardness;
	}
	
	public function create_form($e_id, $ID = null) { 
		$db = new db();
		$gt_date = new gt_date();
		if($ID == null) {
			$q_type = "";
			$q_deadline = "";
			$q_score = "";
			$q_date = "";
			$q_time = "";
			$t_id = "";
			$q_hardness = "";
		} else {
			$asb = $db->get_select_query("select * from question where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$q_type = $roww['q_type'];
					$q_deadline = $roww['q_deadline'];
					$q_score = $roww['q_score'];
					$q_date = $roww['q_date'];
					$q_date = $gt_date->jgdate($q_date);
					$q_time = $roww['q_time'];
					$t_id = $roww['t_id'];
					$q_hardness = $roww['q_hardness'];
				}
			 }
		} ?>
		<div class="row" id="add-question-view">
			<div class="col-md-12">
				<input type="hidden" name="q_date" value="<?php echo jdate("Y/m/d"); ?>">
				<input type="hidden" name="t_id" value="<?php echo $_SESSION['user_id']; ?>">
				<input type="hidden" name="e_id" value="<?php echo $e_id; ?>">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>نوع سوال <span class="red">*</span></label>
							<select name="q_type" class="form-control select2">
								<option value="0" <?php if($q_type == 0) { echo 'selected';} ?>>تستی</option>
								<option value="1" <?php if($q_type == 1) { echo 'selected';} ?>>تشریحی</option>
								<option value="2" <?php if($q_type == 2) { echo 'selected';} ?>>جاخالی</option>
								<option value="3" <?php if($q_type == 3) { echo 'selected';} ?>>صحیح و غلط</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>وقت سوال <span class="red">*</span> (به ثانیه)</label>
							<input type="text" name="q_deadline" placeholder="وقت سوال" value="<?php echo $q_deadline; ?>" class="form-control">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>درجه سختی <span class="red">*</span></label>
							<select name="q_hardness" class="form-control select2">
								<option value="0" <?php if($q_hardness == 0) { echo 'selected';} ?>>آسان</option>
								<option value="1" <?php if($q_hardness == 1) { echo 'selected';} ?>>معمولی</option>
								<option value="2" <?php if($q_hardness == 2) { echo 'selected';} ?>>سخت</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>بارم سوال <span class="red">*</span></label>
							<input type="text" name="q_score" placeholder="بارم سوال" value="<?php echo $q_score; ?>" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
						if($ID == null) {
							?>
							<button class="btn btn-success add-question" id="add-question" data-eid="<?php echo $e_id; ?>">ایجاد سوال</button>
							<?php
						} else {
							?>
							<button type="button" name="update_question" class="btn btn-sm btn-warning update-question" data-eid="<?php echo $e_id; ?>">ویرایش</button>
							<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
							<?php
						} ?>
					</div>
				</div>
				<hr>
				<div id="list-question-view-result"><?php $this->list_question_view($e_id); ?></div>
			</div>
		</div>
		<?php
	}
	
	public function list_question_view($e_id) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>کد سوال</th>
							<th>نوع سوال</th>
							<th>وقت سوال (به ثانیه)</th>
							<th>بارم سوال</th>
							<th>تاریخ طراحی سوال</th>
							<th>درجه سختی</th>
							<th>نام معلم</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$db = new db();
						$gt_date = new gt_date();
						$gt_teacher = new gt_teacher();
						$gt_exam = new gt_exam();
						$list = $db->get_select_query("select * from question where e_id = $e_id order by ID desc");
						if(count($list) > 0) {
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['ID']); ?></td>
									<td><?php
									$q_type = $this->get_question_type($l['q_type']);
									echo $pr->per_number($q_type); ?>
									</td>
									<td><?php echo $pr->per_number($l['q_deadline']); ?></td>
									<td><?php echo $pr->per_number($l['q_score']); ?></td>
									<td><?php
									$date = $gt_date->jgdate($l['q_date']);
									echo $pr->per_number($date); ?>
									</td>
									<td><?php
									$q_hardness = $this->get_question_hardness($l['q_hardness']);
									echo $q_hardness; ?>
									</td>
									<td><?php
									$t_id = $gt_teacher->get_teacher_name($l['t_id']);
									echo $pr->per_number($t_id); ?>
									</td>
									<td>
										<a class="btn btn-info btn-xs update-question-form" data-id="<?php echo $l['ID']; ?>" data-eid="<?php echo $e_id; ?>">مشاهده و ویرایش</a>
										<button class="btn btn-warning btn-xs open-design-box" data-id="<?php echo $l['ID']; ?>">طراحی سوال</button>
										<?php
										if($l['q_type'] == 0) {	
										?>
										<button class="btn btn-primary btn-xs open-option-box" data-id="<?php echo $l['ID']; ?>">گزینه های سوال</button>
										<?php
										} ?>
										<button data-id="<?php echo $l['ID']; ?>" data-eid="<?php echo $e_id; ?>" class="btn btn-danger btn-xs remove-question">حذف</button>
									</td>
								</tr>
								<tr id="design-question-box<?php echo $l['ID']; ?>" class="design-question-box" style="display: none">
									<td colspan="9">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>محتوای سوال <span class="red">*</span></label>
													<textarea dir="auto" rows="5" name="q_content"  placeholder="محتوای سوال" class="form-control q_content" data-id="<?php echo $l['ID']; ?>"><?php echo $l['q_content']; ?></textarea>

                                                </div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">	
												<div class="form-group">
													<label>تصاویر سوال</label><br>	
													<form class="uploadForm" action="" method="post">
														<div id="targetLayer<?php echo $l['ID']; ?>"><?php $this->get_media($l['ID']); ?></div>
														<div class="uploadFormLayer">
															<input name="userImage" type="file" class="inputFile" /><br/>
															<input type="submit" value="Submit" class="btnSubmit" />
															<input type="hidden" class="q_id" name="q_id" value="<?php echo $l['ID']; ?>">
															<input type="hidden" name="type" value="question">
														</div>
													</form>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>پاسخ صحیح <span class="red">*</span></label>
													<textarea rows="5" name="correct_answer" placeholder="پاسخ صحیح" class="form-control correct_answer" data-id="<?php echo $l['ID']; ?>"><?php echo $l['correct_answer']; ?></textarea>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr id="option-question-box<?php echo $l['ID']; ?>" class="option-question-box" style="display: none">
									<td colspan="9">										
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label>عنوان گزینه</label>
													<input type="text" class="form-control q_opt" data-id="<?php echo $l['ID']; ?>" data-num="1" value="<?php echo $l['q_opt1']; ?>">
													<br>
													<label>تصویر گزینه</label>
													<div id="item_target_layer<?php echo $l['ID']; ?>-1"><?php $this->get_item_media($l['ID'], 1); ?></div>
													<form class="upload_item_form" action="" method="post">
														<input name="item_image" type="file" class="upload_item_input">
														<input type="hidden" class="q_id" name="q_id" value="<?php echo $l['ID']; ?>">
														<input type="hidden" class="opt_id" name="opt_id" value="1">
														<input type="hidden" name="type" value="item">
														<input type="submit" class="btn btn-info btn-xs" value="ذخیره تصویر">
													</form>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label>عنوان گزینه</label>
													<input type="text" class="form-control q_opt" data-id="<?php echo $l['ID']; ?>" data-num="2" value="<?php echo $l['q_opt2']; ?>">
													<br>
													<label>تصویر گزینه</label>
													<div id="option_target_layer<?php echo $l['ID']; ?>-2"><?php $this->get_item_media($l['ID'], 2); ?></div>
													<form class="upload_item_form" action="" method="post">
														<input name="item_image" type="file" class="upload_item_input">
														<input type="hidden" class="q_id" name="q_id" value="<?php echo $l['ID']; ?>">
														<input type="hidden" class="opt_id" name="opt_id" value="2">
														<input type="hidden" name="type" value="item">
														<input type="submit" class="btn btn-info btn-xs" value="ذخیره تصویر">
													</form>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label>عنوان گزینه</label>
													<input type="text" class="form-control q_opt" data-id="<?php echo $l['ID']; ?>" data-num="3" value="<?php echo $l['q_opt3']; ?>">
													<br>
													<label>تصویر گزینه</label>
													<div id="option_target_layer<?php echo $l['ID']; ?>-3"><?php $this->get_item_media($l['ID'], 3); ?></div>
													<form class="upload_item_form" action="" method="post">
														<input name="item_image" type="file" class="upload_item_input">
														<input type="hidden" name="q_id" value="<?php echo $l['ID']; ?>">
														<input type="hidden" name="opt_id" value="3">
														<input type="hidden" name="type" value="item">
														<input type="submit" class="btn btn-info btn-xs" value="ذخیره تصویر">
													</form>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
												<label>عنوان گزینه</label>
													<input type="text" class="form-control q_opt" data-id="<?php echo $l['ID']; ?>" data-num="4" value="<?php echo $l['q_opt4']; ?>">
													<br>
													<label>تصویر گزینه</label>
													<div id="option_target_layer<?php echo $l['ID']; ?>-4"><?php $this->get_item_media($l['ID'], 4); ?></div>
													<form class="upload_item_form" action="" method="post">
														<input name="item_image" type="file" class="upload_item_input">
														<input type="hidden" name="q_id" value="<?php echo $l['ID']; ?>">
														<input type="hidden" name="opt_id" value="4">
														<input type="hidden" name="type" value="item">
														<input type="submit" class="btn btn-info btn-xs" value="ذخیره تصویر">
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="13">هیچ  سوالی ثبت شده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
	public function get_media($q_id, $size = "small") {
		$db = new db();
		$list = $db->get_select_query("select * from question_media where q_id = $q_id");
		if(count($list) > 0) {
			foreach($list as $l) {
			?>
			<a href="http://gtserver.ir/gt-content/uploads/question/<?php echo $l['filename']; ?>" target="_blank">
				<img style="width: <?php if($size == "small") { echo '18%'; } else { echo '100%'; } ?> border-radius: 4px; padding: 1px; border: 2px solid #ddd;" src="http://gtserver.ir/gt-content/uploads/question/<?php echo $l['filename']; ?>" class="upload-preview image-preview img-responsive">
			</a>
			<button type="button" class="btn btn-danger btn-xs remove-file" data-id="<?php echo $l['ID']; ?>" data-qid="<?php echo $q_id; ?>">حذف</button>
			<?php
			}
		}
	}
	
	public function get_item_media($q_id, $opt_id) {
		$db = new db();
		$field_name = "q_opt_src" . $opt_id;
		$link = $db->get_var_query("select $field_name from question where ID = $q_id");
		if($link) { ?>
			<a href="http://gtserver.ir/gt-content/uploads/item/<?php echo $link; ?>" target="_blank">
				<img style="width: 18%; border-radius: 4px; padding: 1px; border: 2px solid #ddd;" src="http://gtserver.ir/gt-content/uploads/item/<?php echo $link; ?>" class="upload-preview image-preview img-responsive">
			</a>
			<?php
		}
	}
	
}