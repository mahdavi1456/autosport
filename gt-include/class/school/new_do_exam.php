<?php function do_exam($ID, $step)
    {
        echo $step;
        $db = new db();
        $question = new gt_question();
        $st_id = $_SESSION['user_id'];
        $max = $db->get_var_query("select count(ID) from question where e_id = $ID");

        if ($step == -1) {
            $this->start_exam($ID);
        }
        else if ($max == $step) {
            $this->finish_exam();
        }
        else if ($step == 0) {
            $counts = $db->get_var_query("select count(st_id) from on_performing_exam where e_id = $ID");
            if ($counts == $max) {
                $sql1 = "select * from on_performing_exam where e_id = $ID and st_id = $st_id limit 1";
                $res = $db->get_select_query($sql1);
            } else {
                $sql1 = "select * from question where e_id = $ID order by ID desc";
                $res = $db->get_select_query($sql1);

                shuffle($res);

                foreach ($res as $row) {
                    $question_id = $row['ID'];
                    $sql2 = "insert into on_performing_exam(e_id, st_id,q_id) values($ID,$st_id, $question_id)";
                    $db->ex_query($sql2);
                }
                $res = array_slice($res, 0, 1);

            }
            //            $sql3 = "insert into do_exam(st_id, e_id) values($st_id, $ID)";
            //            $db->ex_query($sql3);
        }
        else{
            echo $step;
            $sql = "select * from question inner join on_performing_exam on question.e_id = on_performing_exam.e_id where on_performing_exam.st_id = $st_id and on_performing_exam.e_id = $ID limit $step, 1";

            $res = $db->get_select_query($sql);
        }

        if (count($res) > 0) {
            ?>
            <div id="add-answer-view">
                <div id="next-question-result">
                    <?php
                    foreach ($res as $row) {
                        $pr = new prime();
                        $q_type = $row['q_type'];
                        $q_type_name = $question->get_question_type($q_type);
                        $q_id = $row['ID'];
                        $st_id = $_SESSION['user_id'];
                        $answer = "";
                        $answer = $db->get_var_query("select answer from answer where q_id = $q_id and st_id = $st_id");
                        ?>
                        <input type="hidden" name="q_id" value="<?php echo $row['ID']; ?>">
                        <input type="hidden" name="st_id" value="<?php echo $_SESSION['user_id']; ?>">
                        <input type="hidden" name="a_date" value="<?php echo jdate("Y/m/d"); ?>">
                        <input type="hidden" name="a_time" value="<?php echo jdate('H:i:s'); ?>">
                        <input type="hidden" name="ip" value="11">
                        <input type="hidden" name="mac" value="22">
                        <div class="col-md-12">
                            <?php
                            $sql = "select e_timer from exam where ID = $ID ";
                            $res = $db->get_var_query($sql);
                            if ($res == 0) {
                                ?>
                                <span class="badge">فرصت برای پاسخ دادن به این سوال: <span
                                            id="timer"><?php echo $row['q_deadline']; ?></span> ثانیه</span>
                            <?php } ?>
                            <span class="badge"><?php echo $q_type_name; ?></span>
                            <span class="badge">سوال <?php echo $pr->per_number(($step + 1)); ?> از <?php echo $max; ?></span>
                            <span class="badge">بارم: <?php echo $pr->per_number($row['q_score']); ?></span>
                            <br><br>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $pr->per_number($row['q_content']); ?>
                                    <br>
                                    <?php $question->get_media($row['ID'], "large"); ?>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    if ($q_type == 0) {
                                        if ($row['q_opt1']) {
                                            ?>
                                            <input name="answer" type="radio"
                                                   value="<?php echo $row['q_opt1']; ?>" <?php if ($row['q_opt1'] == $answer) {
                                                echo 'checked';
                                            } ?>>
                                            <label><?php echo $pr->per_number($row['q_opt1']); ?></label>
                                            <?php
                                            $question->get_item_media($row['ID'], 1);
                                            ?>
                                            <hr>
                                            <?php
                                        }
                                        if ($row['q_opt2']) {
                                            ?>
                                            <input name="answer" type="radio"
                                                   value="<?php echo $row['q_opt2']; ?>" <?php if ($row['q_opt2'] == $answer) {
                                                echo 'checked';
                                            } ?>>
                                            <label><?php echo $pr->per_number($row['q_opt2']); ?></label>
                                            <?php
                                            $question->get_item_media($row['ID'], 2);
                                            ?>
                                            <hr>
                                            <?php
                                        }
                                        if ($row['q_opt3']) {
                                            ?>
                                            <input name="answer" type="radio"
                                                   value="<?php echo $row['q_opt3']; ?>" <?php if ($row['q_opt3'] == $answer) {
                                                echo 'checked';
                                            } ?>>
                                            <label><?php echo $pr->per_number($row['q_opt3']); ?></label>
                                            <?php
                                            $question->get_item_media($row['ID'], 3);
                                            ?>
                                            <hr>
                                            <?php
                                        }
                                        if ($row['q_opt4']) {
                                            ?>
                                            <input name="answer" type="radio"
                                                   value="<?php echo $row['q_opt4']; ?>" <?php if ($row['q_opt4'] == $answer) {
                                                echo 'checked';
                                            } ?>>
                                            <label><?php echo $pr->per_number($row['q_opt4']); ?></label>
                                            <?php
                                            $question->get_item_media($row['ID'], 4);
                                            ?>
                                            <hr>
                                            <?php
                                        }
                                    } else if ($q_type == 1) {
                                        ?>
                                        <label>پاسخ خود را در این قسمت بنویسید: </label>
                                        <textarea name="answer"
                                                  class="form-control answer_content"><?php echo $pr->per_number($answer); ?></textarea>
                                        <?php
                                    } else if ($q_type == 2) {
                                        ?>
                                        <label>پاسخ خود را در این قسمت بنویسید: </label>
                                        <textarea name="answer"
                                                  class="form-control answer_content"><?php echo $pr->per_number($answer); ?></textarea>
                                        <?php
                                    } else if ($q_type == 3) {
                                        if ($row['q_opt1']) {
                                            ?>
                                            <input name="answer" type="radio"
                                                   value="<?php echo $row['q_opt1']; ?>" <?php if ($row['q_opt1'] == $answer) {
                                                echo 'checked';
                                            } ?>>
                                            <label><?php echo $pr->per_number($row['q_opt1']); ?></label>
                                            <hr>
                                            <?php
                                        }
                                        if ($row['q_opt2']) {
                                            ?>
                                            <input name="answer" type="radio"
                                                   value="<?php echo $row['q_opt2']; ?>" <?php if ($row['q_opt2'] == $answer) {
                                                echo 'checked';
                                            } ?>>
                                            <label><?php echo $pr->per_number($row['q_opt2']); ?></label>
                                            <hr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="panel-footer text-center" style="display: block">
                                    <?php
                                    $prev = $step - 1;
                                    $e_id = $row['e_id'];
                                    $preview = $db->get_var_query("select e_preview from exam where ID = $e_id");
                                    if ($preview == 0) {
                                        $next = $step + 1;
                                        if ($step > 0) {
                                            ?>
                                            <button data-exam_id="<?php echo $ID; ?>"
                                                    data-next="<?php echo($prev); ?>" id="prev-question"
                                                    class="btn btn-warning btn-lg">سوال قبل
                                            </button>
                                            <?php
                                        }
                                    } ?>
                                    <?php
                                    $final_step = $max - 1;
                                    if ($final_step == $step) {
                                        echo '<button data-exam_id="' . $ID . '" data-next="' . $next . '" id="next-question" class="btn btn-primary btn-lg">پایان آزمون</button>';
                                    } else {
                                        echo '<button  data-exam_id="' . $ID . '" data-next="' . $next . '"  id="next-question" class="btn btn-success btn-lg">سوال بعد</button>';
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                    }
                    ?>
                </div>
            </div>
            <div id="answer-re">
            </div>
            <?php
        }

    }