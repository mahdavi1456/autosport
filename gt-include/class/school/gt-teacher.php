<?php
//معلم
class gt_teacher {
	
	public function add_teacher($array) {
		$db = new db();
		$t_name = $array['t_name'];
		$t_family = $array['t_family'];
		$t_username = $array['t_username'];
		$t_password = $array['t_password'];
		$t_password = md5($t_password);
		$g_id = $array['g_id'];
		$t_phone = $array['t_phone'];
		if($t_phone == ""){
			$t_phone = " ";
		}
		$t_national = $array['t_national'];
		if($t_national == ""){
			$t_national = " ";
		}
		$t_level = $array['t_level'];
		$t_address = $array['t_address'];
		$sql = "insert into teacher(t_name, t_family, t_username, t_password, g_id, t_phone, t_national, t_level, t_address) values('$t_name', '$t_family', '$t_username', '$t_password', $g_id, '$t_phone', '$t_national', '$t_level', '$t_address')";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	public function update_teacher($array) {
		$db = new db();
		$ID = $array['ID'];
		$t_name = $array['t_name'];
		$t_family = $array['t_family'];
		$t_username = $array['t_username'];
		$t_password = $array['t_password'];
		$g_id = $array['g_id'];
		$t_phone = $array['t_phone'];
		if($t_phone == ""){
			$t_phone = " ";
		}
		$t_national = $array['t_national'];
		if($t_national == ""){
			$t_national = " ";
		}
		$t_level = $array['t_level'];
		$t_address = $array['t_address'];
		$sql = "update teacher set t_name = '$t_name', t_family = '$t_family', t_username = '$t_username', g_id = $g_id, t_phone = '$t_phone', t_national = '$t_national', t_level = '$t_level', t_address = '$t_address' where ID = $ID";
		$db->ex_query($sql);
	}
	public function remove_teacher($ID) {
		$db = new db();
		$sql = "delete from teacher where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function check_login_teacher($username, $password) {
		$db = new db();
		$pass = md5($password);
		$res = $db->get_select_query("select * from teacher where t_username = '$username' and t_password = '$pass'");
		return count($res);
	}
	
	public function get_teacher_id($username) {
		$db = new db();
		$ID = $db->get_var_query("select ID from teacher where t_username = '$username'");
		return $ID;
	}
	public function get_teacher_name($ID) {
		$db = new db();
		$sql = "select t_name, t_family from teacher where ID = $ID";
		$res = $db->get_select_query($sql);
		if(count($res) > 0) {
			return $res[0]['t_name'] . " " . $res[0]['t_family'];
		} else {
			return "نامعتبر";
		}
	}
	
	public function get_teacher_level($ID) {
		$db = new db();
		$sql = "select t_level from teacher where ID = $ID";
		$t_level = $db->get_var_query($sql);
		if($t_level == 'manager') {
			return "مدیر";
		} else {
			return "معلم";
		}
	}
	
	public function change_password($ID, $new_password) {
		$db = new db();
		$sql = "update teacher set t_password = '$new_password' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function create_form($ID = null) {
		$gt_grade = new gt_grade();
		$db = new db();
		if($ID == null) {
			$t_name = "";
			$t_family = "";
			$t_username = "";
			$g_id = "";
			$t_phone = "";
			$t_national = "";
			$t_level = "";
			$t_address = "";
		} else {
			$asb = $db->get_select_query("select * from teacher where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$t_name = $roww['t_name'];
					$t_family = $roww['t_family'];
					$t_username = $roww['t_username'];
					$g_id = $roww['g_id'];
					$t_phone = $roww['t_phone'];
					$t_national = $roww['t_national'];
					$t_level = $roww['t_level'];
					$t_address = $roww['t_address'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-teacher-view">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>نام <span class="red">*</span></label>
								<input type="text" name="t_name" placeholder="نام" value="<?php echo $t_name; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نام خانوادگی <span class="red">*</span></label>
								<input type="text" name="t_family" placeholder="نام خانوادگی" value="<?php echo $t_family; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نام کاربری <span class="red">*</span></label>
								<input type="text" name="t_username" placeholder="نام کاربری" value="<?php echo $t_username; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>رمز عبور <span class="red">*</span></label>
								<input type="text" name="t_password" placeholder="رمز عبور" class="form-control" <?php if($ID != null) { ?> disabled  value="******" <?php } ?>>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>مقطع تحصیلی <span class="red">*</span></label>
								<select name="g_id" class="form-control select2">
									<?php
									$db = new db();
									$grade = $db->get_select_query("select * from grade order by ID");
									if(count ($grade > 0)) {
										foreach($grade as $row) {
											?>
											<option value="<?php echo $row['ID']; ?>" <?php if($row["ID"] == $g_id) { echo 'selected';} ?>><?php echo $row['g_name']; ?></option>
											<?php
										}
									} ?>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>تلفن</label>
								<input type="text" name="t_phone" placeholder="تلفن" value="<?php echo $t_phone; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>کد ملی</label>
								<input type="text" name="t_national" placeholder="کد ملی" value="<?php echo $t_national; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>سمت <span class="red">*</span></label>
								<select name="t_level" class="form-control select2">
									<option value="teacher" <?php if($t_level == 'teacher') { echo 'selected';} ?> >معلم</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>آدرس</label>
								<textarea name="t_address" placeholder="آدرس" class="form-control"><?php echo $t_address; ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_teacher" class="btn btn-sm btn-success add-teacher">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_teacher" class="btn btn-sm btn-warning update-teacher">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-teacher-view-result"><?php $this->list_teacher_view(); ?></div>
		<?php
	}
		
	public function list_teacher_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed table-bordered">
						<tr>
							<th>ردیف</th>
							<th>نام</th>
							<th>نام خانوادگی</th>
							<th>نام کاربری</th>
							<th>مقطع تحصیلی</th>
							<th>تلفن</th>
							<th>کد ملی</th>
							<th>سمت</th>
							<th>آدرس</th>
							<th>عملیات</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$db = new db();
						$gt_grade = new gt_grade();
						$list = $db->get_select_query("select * from teacher order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['t_name']); ?></td>
									<td><?php echo $pr->per_number($l['t_family']); ?></td>
									<td><?php echo $pr->per_number($l['t_username']); ?></td>
									<td><?php
										$g_id = $gt_grade->get_grade_name($l['g_id']);
									echo $pr->per_number($g_id); ?>
									</td>
									<td><?php echo $pr->per_number($l['t_phone']); ?></td>
									<td><?php echo $pr->per_number($l['t_national']); ?></td>
									<td><?php
										$level = $this->get_teacher_level($l['ID']);
									echo $pr->per_number($level); ?>
									</td>
									<td><?php echo $pr->per_number($l['t_address']); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-teacher-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<!--button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal<?php //echo $l['ID']; ?>">تغییر رمز</button-->
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-teacher">حذف</button>
										<div id="myModal<?php echo $l['ID']; ?>" class="modal fade" role="dialog">
											<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">
													<div>
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">تغییر رمز</h4>
														</div>
														<div class="modal-body">
															<div id="new-password<?php echo $l['ID']; ?>">
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label>رمز جدید <span class="red">*</span></label>
																			<input type="text" placeholder="رمز جدید" class="form-control" name="newpassword">
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<center>
																<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
																<button class="btn btn-primary btn-sm change-password" data-id="<?php echo $l['ID']; ?>" type="submit">ذخیره</button>
															</center>
														</div>
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else { ?>
							<tr>
								<td colspan="10">هیچ معلمی تاکنون ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}

}