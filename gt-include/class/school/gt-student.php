<?php
//دانش آموز
class gt_student {
	
	public function add_student($array) {
		$db = new db();
		$gt_date = new gt_date();
		$st_name = $array['st_name'];
		$st_family = $array['st_family'];
		$st_father_name = $array['st_father_name'];
		$st_username = $array['st_username'];
		$st_password = $array['st_password'];
		$st_password = md5($st_password);
		$st_status = $array['st_status'];
		$st_phone = $array['st_phone'];
		if($st_phone == ""){
			$st_phone = " ";
		}
		$st_national = $array['st_national'];
		if($st_national == ""){
			$st_national = " ";
		}
		$st_description = $array['st_description'];
		if($st_description == ""){
			$st_description = " ";
		}
		$st_lastupdate = $array['st_lastupdate'];
		$st_lastupdate = $gt_date->jgdate($st_lastupdate);
		$sql = "insert into student(st_name, st_family, st_father_name, st_username, st_password, st_status, st_phone, st_national, st_description, st_lastupdate) values('$st_name', '$st_family', '$st_father_name', '$st_username', '$st_password', $st_status, '$st_phone', '$st_national', '$st_description', '$st_lastupdate')";
		$last_id = $db->ex_query($sql);
		return $sql;
	}
	public function update_student($array) {
		$db = new db();
		$gt_date = new gt_date();
		$ID = $array['ID'];
		$st_name = $array['st_name'];
		$st_family = $array['st_family'];
		$st_father_name = $array['st_father_name'];
		$st_username = $array['st_username'];
		$st_password = $array['st_password'];
		$st_password = md5($st_password);
		$st_status = $array['st_status'];
		$st_phone = $array['st_phone'];
		if($st_phone == ""){
			$st_phone = " ";
		}
		$st_national = $array['st_national'];
		if($st_national == ""){
			$st_national = " ";
		}
		$st_description = $array['st_description'];
		$st_lastupdate = $array['st_lastupdate'];
		$st_lastupdate = $gt_date->jgdate($st_lastupdate);
		$sql = "update student set st_name = '$st_name', st_family = '$st_family', st_father_name = '$st_father_name', st_username = '$st_username', st_status = $st_status, st_phone = '$st_phone', st_national = '$st_national', st_description = '$st_description', st_lastupdate = '$st_lastupdate' where ID = $ID";
		$db->ex_query($sql);
	}
	public function remove_student($ID) {
		$db = new db();
		$sql = "delete from student where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function check_login_student($username, $password) {
		$db = new db();
		$pass = md5($password);
		$res = $db->get_select_query("select * from student where st_username = '$username' and st_password = '$pass'");
		return count($res);
	}
	
	public function get_student_id($username) {
		$db = new db();
		$ID = $db->get_var_query("select ID from student where st_username = '$username'");
		return $ID;
	}
	
	public function get_student_name($ID) {
		$db = new db();
		$sql = "select st_name, st_family from student where ID = $ID";
		$res = $db->get_select_query($sql);
		if(count($res) > 0) {
			return $res[0]['st_name'] . " " . $res[0]['st_family'];
		} else {
			return "نامعتبر";
		}
	}
	
	public function change_password($ID, $new_password) {
		$db = new db();
		$sql = "update student set st_password = '$new_password' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_student_status($ID) {
		$db = new db();
		$sql = "select st_status from student where ID = $ID";
		$st_status = $db->get_var_query($sql);
		if($st_status == 0) {
			return "مجاز";
		} else {
			return "غیرمجاز";
		}
	}
	
	public function create_form($ID = null) {
		$db = new db();
		$gt_date = new gt_date();
		if($ID == null) {
			$st_name = "";
			$st_family = "";
			$st_father_name = "";
			$st_username = "";
			$st_status = "";
			$st_phone = "";
			$st_national = "";
			$st_description = "";
			$st_lastupdate = "";
		} else {
			$asb = $db->get_select_query("select * from student where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$st_name = $roww['st_name'];
					$st_family = $roww['st_family'];
					$st_father_name = $roww['st_father_name'];
					$st_username = $roww['st_username'];
					$st_status = $roww['st_status'];
					$st_phone = $roww['st_phone'];
					$st_national = $roww['st_national'];
					$st_description = $roww['st_description'];
					$st_lastupdate = $roww['st_lastupdate'];
					$st_lastupdate = $gt_date->jgdate($st_lastupdate);
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-student-view">
					<input type="hidden" name="st_lastupdate" value="<?php echo jdate('Y/m/d'); ?>">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>نام <span class="red">*</span></label>
								<input type="text" name="st_name" placeholder="نام" value="<?php echo $st_name; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نام خانوادگی <span class="red">*</span></label>
								<input type="text" name="st_family" placeholder="نام خانوادگی" value="<?php echo $st_family; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>کد ملی</label>
								<input type="text" name="st_national" placeholder="کد ملی" value="<?php echo $st_national; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نام پدر <span class="red">*</span></label>
								<input type="text" name="st_father_name" placeholder="نام پدر" value="<?php echo $st_father_name; ?>" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>نام کاربری <span class="red">*</span></label>
								<input type="text" name="st_username" placeholder="نام کاربری" value="<?php echo $st_username; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>رمز عبور <span class="red">*</span></label>
								<input type="text" name="st_password" placeholder="رمز عبور" class="form-control" <?php if($ID != null) { ?> disabled  value="******" <?php } ?>>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>تلفن</label>
								<input type="text" name="st_phone" placeholder="تلفن" value="<?php echo $st_phone; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>وضعیت امتحان <span class="red">*</span></label>
								<select name="st_status" class="form-control select2">
									<option value="0" <?php if($st_status == 0) { echo 'selected';} ?> >مجاز</option>
									<option value="1" <?php if($st_status == 1) { echo 'selected';} ?> >غیرمجاز</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>توضیحات</label>
								<textarea name="st_description" placeholder="توضیحات" class="form-control"><?php echo $st_description; ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_student" class="btn btn-sm btn-success add-student">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_student" class="btn btn-sm btn-warning update-student">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-student-view-result"><?php $this->list_student_view(); ?></div>
		<?php
	}
		
	public function list_student_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام</th>
							<th>نام خانوادگی</th>
							<th>نام پدر</th>
							<th>نام کاربری</th>
							<th>تلفن</th>
							<th>کد ملی</th>
							<th>وضعیت</th>
							<th>توضیحات</th>
							<th>آخرین بروزرسانی</th>
							<th>عملیات</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$db = new db();
						$gt_date = new gt_date();
						$list = $db->get_select_query("select * from student order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['st_name']); ?></td>
									<td><?php echo $pr->per_number($l['st_family']); ?></td>
									<td><?php echo $pr->per_number($l['st_father_name']); ?></td>
									<td><?php echo $pr->per_number($l['st_username']); ?></td>
									<td><?php echo $pr->per_number($l['st_phone']); ?></td>
									<td><?php echo $pr->per_number($l['st_national']); ?></td>
									<td><?php
										$status = $this->get_student_status($l['ID']);
									echo $pr->per_number($status); ?>
									</td>
									<td><?php echo $pr->per_number($l['st_description']); ?></td>
									<td><?php
										$date = $gt_date->jgdate($l['st_lastupdate']);
									echo $pr->per_number($date); ?>
									</td>
									<td>
										<button class="btn btn-info btn-xs update-student-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<!--button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal<?php //echo $l['ID']; ?>">تغییر رمز</button-->
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-student">حذف</button>
										<div id="myModal<?php echo $l['ID']; ?>" class="modal fade" role="dialog">
											<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">
													<div>
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">تغییر رمز</h4>
														</div>
														<div class="modal-body">
															<div id="new-password<?php echo $l['ID']; ?>">
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label>رمز جدید <span class="red">*</span></label>
																			<input type="text" placeholder="رمز جدید" class="form-control" name="newpassword">
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<center>
																<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
																<button class="btn btn-primary btn-sm change-password" data-id="<?php echo $l['ID']; ?>" type="submit">ذخیره</button>
															</center>
														</div>
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else { ?>
							<tr>
								<td colspan="10">هیچ معلمی تاکنون ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	public function list_my_exam_report_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>عنوان آزمون</th>
							<th>معلم</th>
							<th>درس</th>
							<th>تاریخ</th>
							<th>ساعت</th>
							<th>نمره آزمون</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$db = new db();
						$gt_teacher = new gt_teacher();
						$gt_lesson = new gt_lesson();
						$list = $db->get_select_query("select * from exam order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $l['e_title']; ?></td>
									<td><?php echo $l['t_id']; ?></td>
									<td><?php echo $l['l_id']; ?></td>
									<td><?php echo $l['e_date']; ?></td>
									<td><?php echo $l['e_time']; ?></td>
									<td><?php echo $l['e_score']; ?></td>
									<td>
										<a href="do_exam?exam_id=<?php echo $l['ID']; ?>" class="btn btn-primary btn-xs">مشاهده کارنامه</a>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="9" class="text-center">تا کنون هیچ نمره ای ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
	public function edit_profile_student($ID = null) {
		if($ID == null){
			$ID = $_SESSION['user_id'];
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-student-view">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>نام کاربری جدید</label>
								<input type="text" name="new_username" placeholder="نام کاربری جدید" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>رمز جدید</label>
								<input type="text" name="new_pass" placeholder="رمز جدید" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="button" data-id="<?php echo $ID; ?>" class="btn btn-sm btn-success edit-student">ذخیره</button>
						</div>
					</div>
				</div>
			</div>
		</div><hr>
		<div class="row">
			<div class="col-md-12">	
				<div class="form-group">
					<label>تصویر پروفایل</label><br>	
					<form class="uploadForm" action="" method="post">
						<div id="targetLayer"><?php $this->get_profile($ID); ?></div>
						<div class="uploadFormLayer">
							<input name="userImage" type="file" class="inputFile" /><br/>
							<input type="submit" value="ذخیره تصویر" class="btnSubmit" />
							<input type="hidden" class="st_id" name="st_id" value="<?php echo $ID; ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
	}
	
	public function get_profile($st_id, $size = "small", $type = null) {
		$db = new db();
		$filename = $db->get_var_query("select st_file from student where ID = $st_id");
		if($filename != ""){
			?>
			<a href="http://gtserver.ir/gt-content/uploads/profile/<?php echo $filename; ?>" target="_blank">
				<img style="width: <?php if($size == "small") { echo '18%;'; } else { echo '100%;'; } ?> border-radius: 4px; padding: 1px; border: 2px solid #ddd;" src="http://gtserver.ir/gt-content/uploads/profile/<?php echo $filename; ?>" class="upload-preview image-preview img-responsive">
			</a>
			<?php
			if($type == null){ ?>
				<button type="button" class="btn btn-danger btn-xs remove-file" data-stid="<?php echo $st_id; ?>">حذف</button>
				<?php
			}
		}
	}
	
}