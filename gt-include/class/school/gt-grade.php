<?php
//مقطع
class gt_grade {
	
	public function add_grade($array) {
		$db = new db();
		$g_name = $array['g_name'];
		$sql = "insert into grade(g_name) values('$g_name')";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_grade($array) {
		$db = new db();
		$ID = $array['ID'];
		$g_name = $array['g_name'];
		$sql = "update grade set g_name = '$g_name' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_grade($ID) {
		$db = new db();
		$sql = "delete from grade where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_grade_name($ID) {
		$db = new db();
		$g_name = $db->get_var_query("select g_name from grade where ID = $ID");
		return $g_name;
	}
	
	public function create_form($ID = null) {
		$db = new db();
		if($ID == null) {
			$g_name = "";
		} else {
			$asb = $db->get_select_query("select * from grade where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$g_name = $roww['g_name']; 
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-grade-view">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>نام مقطع <span class="red">*</span></label>
								<input type="text" name="g_name" value="<?php echo $g_name; ?>" placeholder="نام مقطع" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_grade" class="btn btn-sm btn-success add-grade">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_grade" class="btn btn-sm btn-warning update-grade">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-grade-view-result"><?php $this->list_grade_view(); ?></div>
		<?php
	}

    public function list_grade_view() {
        $db = new db();
        ?>
        <div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام مقطع</th>
							<th>مدیریت</th>							
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$list = $db->get_select_query("select * from grade order by ID desc");
                        if(count($list) > 0) {
                            foreach($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $pr->per_number($row['g_name']); ?></td>     
									<td>
										<button class="btn btn-info btn-xs update-grade-form" data-id="<?php echo $row['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $row['ID']; ?>" class="btn btn-danger btn-xs remove-grade">حذف</button>
									</td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
							?>
							<tr>
								<td colspan="3">هیچ مقطعی تاکنون تعریف نشده است</td>
							</tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }
}