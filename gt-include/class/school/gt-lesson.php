<?php
//درس
class gt_lesson{
	
	public function add_lesson($array) {
		$db = new db();
		$l_name = $array['l_name'];
		$g_y_id = $array['g_y_id'];
		$sql = "insert into lesson(l_name, g_y_id) values('$l_name', $g_y_id)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_lesson($array) {
		$db = new db();
		$ID = $array['ID'];
		$l_name = $array['l_name'];
		$g_y_id = $array['g_y_id'];
		$sql = "update lesson set l_name = '$l_name', g_y_id = $g_y_id where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_lesson($ID) {
		$db = new db();
		$sql = "delete from lesson where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_lesson_name($ID) {
		$db = new db();
		$l_name = $db->get_var_query("select l_name from lesson where ID = $ID");
		return $l_name;
	}

    public function get_lesson_name_by_exam_id($ID) {
        $db = new db();
        $l_id = $db->get_var_query("select l_id from exam where ID=$ID order by exam.ID desc");
        $l_name = $db->get_var_query("select l_name from lesson where ID = $l_id");
        return $l_name;
    }


    public function create_form($ID = null) {
		$db = new db();
		$gt_grade_year = new gt_grade_year();
		if($ID == null) {
			$l_name = "";
			$g_y_id = "";
		} else {
			$asb = $db->get_select_query("select * from lesson where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$l_name = $roww['l_name'];
					$g_y_id = $roww['g_y_id'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-lesson-view">
					<div class="row">
						 <div class="col-md-3">
                            <div class="form-group">
                                <label>نام درس <span class="red">*</span></label>
                                <input type="text" name="l_name" placeholder="نام درس" value="<?php echo $l_name; ?>" class="form-control">
                            </div>
                        </div>
						<div class="col-md-3">
							<div class="form-group">
								<label>پایه تحصیلی <span class="red">*</span></label>
								<select name="g_y_id" class="form-control select2">
									<?php
									$db = new db();
									$res = $db->get_select_query("select * from grade_year order by ID desc");
									if(count($res) > 0) {
										foreach($res as $row) {
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $g_y_id) { echo 'selected';} ?>><?php echo $gt_grade_year->get_grade_year_name($row["ID"]); ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_lesson" class="btn btn-sm btn-success add-lesson">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_lesson" class="btn btn-sm btn-warning update-lesson">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-lesson-view-result"><?php $this->list_lesson_view(); ?></div>
		<?php
	}

    public function list_lesson_view() {
		$db = new db();
		$gt_grade_year = new gt_grade_year();
        ?>
        <div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام درس</th>
							<th>پایه تحصیلی</th>
							<th>عملیات</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$list = $db->get_select_query("select * from lesson order by ID desc");
                        if(count($list) > 0) {
                            foreach($list as $row) {
								$g_y_id = $gt_grade_year->get_grade_year_name($row['g_y_id']);
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $pr->per_number($row['l_name']); ?></td>
                                    <td><?php echo $pr->per_number($g_y_id); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-lesson-form" data-id="<?php echo $row['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $row['ID']; ?>" class="btn btn-danger btn-xs remove-lesson">حذف</button>
									</td>										
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
							?>
							<tr>
								<td colspan="4">هیچ درسی تاکنون ثبت نشده است</td>
							</tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }
}