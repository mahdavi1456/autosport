<?php
//آزمون
session_start();

class gt_exam
{

    public function add_exam($array)
    {
        $db = new db();
        $gt_date = new gt_date();
        $e_title = $array['e_title'];
        $t_id = $array['t_id'];
        $l_id = $array['l_id'];
        $c_id = $array['c_id'];
        $e_date = $array['e_date'];
        $e_date = $gt_date->jgdate($e_date);
        $e_time = $array['e_time'];
        $e_deadline = $array['e_deadline'];
        $e_score = $array['e_score'];
        $e_timer = $array['e_timer'];
        $e_preview = $array['e_preview'];
        $sql = "insert into exam(e_title, t_id, l_id, c_id, e_date, e_time, e_deadline, e_score, e_timer, e_preview) values('$e_title', $t_id, $l_id, $c_id, '$e_date', '$e_time', $e_deadline, $e_score, $e_timer, $e_preview)";
        $last_id = $db->ex_query($sql);
        return $last_id;
    }

    public function update_exam($array)
    {
        $db = new db();
        $gt_date = new gt_date();
        $ID = $array['ID'];
        $e_title = $array['e_title'];
        $t_id = $array['t_id'];
        $l_id = $array['l_id'];
        $c_id = $array['c_id'];
        $e_date = $array['e_date'];
        $e_date = $gt_date->jgdate($e_date);
        $e_time = $array['e_time'];
        $e_deadline = $array['e_deadline'];
        $e_score = $array['e_score'];
        $e_timer = $array['e_timer'];
        $e_preview = $array['e_preview'];
        $sql = "update exam set e_title = '$e_title', t_id = $t_id, l_id = $l_id, c_id = $c_id, e_date = '$e_date', e_time = '$e_time', e_deadline = $e_deadline, e_score = $e_score, e_timer = $e_timer, e_preview = $e_preview where ID = $ID";
        $db->ex_query($sql);
    }

    public function remove_exam($ID)
    {
        $db = new db();
        $sql = "delete from exam where ID = $ID";
        $db->ex_query($sql);
    }

    public function create_form($ID = null)
    {
        $db = new db();
        $gt_date = new gt_date();
        if ($ID == null) {
            $e_title = "";
            $t_id = "";
            $l_id = "";
            $c_id = "";
            $e_date = "";
            $e_time = "";
            $e_deadline = "";
            $e_score = "";
            $e_timer = "";
            $e_preview = "";
        } else {
            $asb = $db->get_select_query("select * from exam where ID = $ID");
            if (count($asb) > 0) {
                foreach ($asb as $roww) {
                    $e_title = $roww['e_title'];
                    $t_id = $roww['t_id'];
                    $l_id = $roww['l_id'];
                    $c_id = $roww['c_id'];
                    $e_date = $roww['e_date'];
                    $e_date = $gt_date->jgdate($e_date);
                    $e_time = $roww['e_time'];
                    $e_deadline = $roww['e_deadline'];
                    $e_score = $roww['e_score'];
                    $e_timer = $roww['e_timer'];
                    $e_preview = $roww['e_preview'];
                }
            }
        } ?>
        <div class="row" id="add-exam-view">
            <div class="col-md-12">
                <input type="hidden" name="t_id" value="<?php echo $_SESSION['user_id']; ?>">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>عنوان آزمون <span class="red">*</span></label>
                            <input type="text" name="e_title" value="<?php echo $e_title; ?>" placeholder="عنوان آزمون"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>درس <span class="red">*</span></label>
                            <select name="l_id" class="form-control select2">
                                <?php
                                $gt_grade_year = new gt_grade_year();
                                $lesson = $db->get_select_query("select * from lesson order by ID desc");
                                if (count($lesson > 0)) {
                                    foreach ($lesson as $row) {
                                        ?>
                                        <option value="<?php echo $row['ID']; ?>" <?php if ($row['ID'] == $l_id) {
                                            echo 'selected';
                                        } ?>><?php $g_y_id = $row['g_y_id'];
                                            echo $row['l_name'] . " " . $gt_grade_year->get_grade_year_name($g_y_id); ?></option>
                                        <?php
                                    }
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>کلاس <span class="red">*</span></label>
                            <select name="c_id" class="form-control select2">
                                <?php
                                $gt_classroom = new gt_classroom();
                                $classroom = $db->get_select_query("select * from classroom order by ID desc");
                                if (count($classroom > 0)) {
                                    foreach ($classroom as $row) {
                                        ?>
                                        <option value="<?php echo $row['ID']; ?>" <?php if ($row['ID'] == $c_id) {
                                            echo 'selected';
                                        } ?>><?php echo $gt_classroom->get_classroom_name($row['ID']); ?></option>
                                        <?php
                                    }
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>تاریخ <span class="red">*</span></label>
                            <input type="text" name="e_date" placeholder="تاریخ" value="<?php echo $e_date; ?>"
                                   class="date-picker form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>ساعت <span class="red">*</span></label>
                            <input id="timepicker" type="text" name="e_time" placeholder="ساعت"
                                   value="<?php echo $e_time; ?>"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>مهلت آزمون <span class="red">*</span> (به دقیقه)</label>
                            <input type="text" name="e_deadline" placeholder="مهلت آزمون"
                                   value="<?php echo $e_deadline; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>نمره آزمون <span class="red">*</span></label>
                            <input type="text" name="e_score" placeholder="نمره آزمون" value="<?php echo $e_score; ?>"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>آیا تمایل دارید زمان سنج برای آزمون فعال باشد؟ <span class="red">*</span></label>
                            <select name="e_timer" class="form-control select2">
                                <option value="0" <?php if ($e_timer == 0) {
                                    echo 'selected';
                                } ?>>بله
                                </option>
                                <option value="1" <?php if ($e_timer == 1) {
                                    echo 'selected';
                                } ?>>خیر
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>آیا تمایل دارید امکان بازگشت به سوال قبل وجود داشته باشد؟ <span class="red">*</span></label>
                            <select name="e_preview" class="form-control select2">
                                <option value="0" <?php if ($e_preview == 0) {
                                    echo 'selected';
                                } ?>>بله
                                </option>
                                <option value="1" <?php if ($e_preview == 1) {
                                    echo 'selected';
                                } ?>>خیر
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php
                        if ($ID == null) {
                            ?>
                            <button type="submit" name="add_exam" class="btn btn-sm btn-success add-exam">ذخیره</button>
                            <?php
                        } else {
                            ?>
                            <button type="button" name="update_exam" class="btn btn-sm btn-warning update-exam">ویرایش
                            </button>
                            <input type="hidden" name="ID" value="<?php echo $ID; ?>">
                            <?php
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div id="list-exam-view-result"><?php $this->list_exam_view(); ?></div>
        <?php
    }

    public function list_exam_view()
    { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>عنوان آزمون</th>
                            <th>معلم</th>
                            <th>درس</th>
                            <th>کلاس</th>
                            <th>تاریخ</th>
                            <th>ساعت</th>
                            <th>مهلت آزمون</th>
                            <th>نمره آزمون</th>
                            <th>زمان سنج</th>
                            <th>بازگشت به سوال قبل</th>
                            <th>مدیریت</th>
                        </tr>
                        <?php
                        $i = 1;
                        $pr = new prime();
                        $db = new db();
                        $gt_date = new gt_date();
                        $gt_teacher = new gt_teacher();
                        $gt_lesson = new gt_lesson();
                        $gt_grade_year = new gt_grade_year();
                        $t_id = $_SESSION['user_id'];
                        if($_SESSION["level"] == 'manager'){
                            $list = $db->get_select_query("select * from exam order by ID desc");
                        }
                        else{
                            $list = $db->get_select_query("select * from exam WHERE t_id = $t_id order by ID desc");
                        }
                        if (count($list) > 0) {
                            foreach ($list as $l) {
                                $date = $gt_date->jgdate($l['e_date']);
                                $current_date = $gt_date->jgdate(jdate("Y/n/j"));
                                $flag = 0;
                                if($l['e_date'] == $current_date){
                                    $flag =  1;
                                }
                                if($l['e_date'] < $current_date){
                                    $flag =  2;
                                }
                                ?>
                                <tr style="background-color: <?php if($flag ==1) {echo "#3ada59";}elseif ($flag==2){echo "#e31b31   ";} else{echo "#f8c427";}?>">
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $pr->per_number($l['e_title']); ?></td>
                                    <td><?php
                                        $t_name = $gt_teacher->get_teacher_name($l['t_id']);
                                        echo($t_name); ?>
                                    </td>
                                    <td><?php
                                        $l_id = $l['l_id'];
                                        $g_y_id = $db->get_var_query("select g_y_id from lesson where ID = $l_id");
                                        $lesson = $gt_lesson->get_lesson_name($l['l_id']) . " " . $gt_grade_year->get_grade_year_name($g_y_id);
                                        echo $pr->per_number($lesson);
                                        ?></td>
                                    <td><?php
                                        $c_id = $l['c_id'];
                                        $gt_classroom = new gt_classroom();
                                        $classroom = $gt_classroom->get_classroom_name($l['c_id']);
                                        echo $pr->per_number($classroom);
                                        ?></td>
                                    <td><?php
                                        echo $pr->per_number($date); ?></td>
                                    <td><?php echo $pr->per_number($l['e_time']); ?></td>
                                    <td><?php echo $pr->per_number($l['e_deadline']); ?></td>
                                    <td><?php echo $pr->per_number($l['e_score']); ?></td>
                                    <td><?php
                                        if ($l['e_timer'] == 0) {
                                            echo "بله";
                                        } else {
                                            echo "خیر";
                                        }
                                        ?></td>
                                    <td><?php
                                        if ($l['e_preview'] == 0) {
                                            echo "بله";
                                        } else {
                                            echo "خیر";
                                        }
                                        ?></td>
                                    <td class="td-btn">
                                        <a class="btn btn-info btn-xs update-exam-form"
                                           data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</a>
                                        <a href="question?exam_id=<?php echo $l['ID']; ?>"
                                           class="btn btn-warning btn-xs">مدیریت سوالات</a>
                                        <a href="do_exam?exam_id=<?php echo $l['ID']; ?>"
                                           class="btn btn-primary btn-xs">مشاهده آزمون</a>
                                        <button data-id="<?php echo $l['ID']; ?>"
                                                class="btn btn-danger btn-xs remove-exam">حذف
                                        </button>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="9" class="text-center">هیچ آزمونی تاکنون تعریف نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function get_exam_name($ID)
    {
        $db = new db();
        $sql = "select e_title from exam where ID = $ID";
        $e_title = $db->get_var_query($sql);
        return $e_title;
    }

    public function get_exam_name_by_q($q_id)
    {
        $db = new db();
        $e_id = $db->get_var_query("select e_id from question where ID = $q_id");
        $e_title = $db->get_var_query("select e_title from exam where ID = $e_id");
        return $e_title;
    }

    public function get_exam_date_by_q($q_id)
    {
        $db = new db();
        $pr = new prime();
        $gt_date = new gt_date();
        $e_id = $db->get_var_query("select e_id from question where ID = $q_id");
        $e_date = $db->get_var_query("select e_date from exam where ID = $e_id");
        $ej_date = $gt_date->jgdate($e_date);
        $eper_date = $pr->per_number($ej_date);
        return $eper_date;
    }

    public function get_exam_time_by_q($q_id)
    {
        $db = new db();
        $pr = new prime();
        $e_id = $db->get_var_query("select e_id from question where ID = $q_id");
        $e_time = $db->get_var_query("select e_time from exam where ID = $e_id");
        $eper_time = $pr->per_number($e_time);
        return $eper_time;
    }

    public function get_exam_score_by_q($q_id)
    {
        $db = new db();
        $pr = new prime();
        $e_id = $db->get_var_query("select e_id from question where ID = $q_id");
        $e_score = $db->get_var_query("select e_score from exam where ID = $e_id");
        $eper_score = $pr->per_number($e_score);
        return $eper_score;
    }

    public function get_teacher_name_by_q($q_id)
    {
        $db = new db();
        $e_id = $db->get_var_query("select e_id from question where ID = $q_id");
        $t_id = $db->get_var_query("select t_id from exam where ID = $e_id");
        $t_name = $db->get_select_query("select * from teacher where ID = $t_id");
        return $t_name[0]['t_name'] . " " . $t_name[0]['t_family'];
    }

    public function get_lesson_name_by_q($q_id)
    {
        $db = new db();
        $e_id = $db->get_var_query("select e_id from question where ID = $q_id");
        $t_id = $db->get_var_query("select l_id from exam where ID = $e_id");
        $lesson_name = $db->get_var_query("select l_name from lesson where ID = $t_id");
        return $lesson_name;
    }

    public function get_exam_course($ID)
    {
        $db = new db();
        $sql = "select l_name from exam inner join lesson on exam.l_id = lesson.ID where exam.ID = $ID";
        $l_name = $db->get_var_query($sql);
        return $l_name;
    }

    public function get_exam_field($ID, $field)
    {
        $db = new db();
        $sql = "select $field from exam where ID = $ID";
        return $db->get_var_query($sql);
    }

    function do_exam($ID, $step)
    {
        $db = new db();
        $question = new gt_question();
        $st_id = $_SESSION['user_id'];

        if($step == 0) {
            $sql = "select * from question where e_id = $ID order by ID desc limit 1";
//            $sql2 = "insert into do_exam(st_id, e_id) values($st_id, $ID)";
//            $db->ex_query($sql2);
        } else {
            $sql = "select * from question where e_id = $ID order by ID desc limit $step, 1";
        }
        $res = $db->get_select_query($sql);
        $max = $db->get_var_query("select count(ID) from question where e_id = $ID");
        if($step == -1) {
            $this->start_exam($ID);
            $_SESSION['start_time'] = date("H:i");
        } else if($max == $step) {
            $this->finish_exam();
        } else {
            if (count($res) > 0) {
                ?>
                <div id="add-answer-view">
                    <div id="next-question-result">
                        <?php
                        foreach ($res as $row) {
                            $pr = new prime();
                            $q_type = $row['q_type'];
                            $q_type_name = $question->get_question_type($q_type);
                            $q_id = $row['ID'];
                            $st_id = $_SESSION['user_id'];
                            $answer = "";
                            $answer = $db->get_var_query("select answer from answer where q_id = $q_id and st_id = $st_id");
                            ?>
                            <input type="hidden" name="q_id" value="<?php echo $row['ID']; ?>">
                            <input type="hidden" name="st_id" value="<?php echo $_SESSION['user_id']; ?>">
                            <input type="hidden" name="a_date" value="<?php echo jdate("Y/m/d"); ?>">
                            <input type="hidden" name="a_time" value="<?php echo jdate('H:i:s'); ?>">
                            <input type="hidden" name="ip" value="11">
                            <input type="hidden" name="mac" value="22">
                            <div class="col-md-12">
                                <?php
                                $sql = "select e_timer from exam where ID = $ID ";
                                $res = $db->get_var_query($sql);
                                if ($res == 0) {
                                    ?>
                                    <span class="badge">فرصت برای پاسخ دادن به این سوال: <span
                                                id="timer"><?php echo $row['q_deadline']; ?></span> ثانیه</span>
                                <?php } ?>
                                <span class="badge"><?php echo $q_type_name; ?></span>
                                <span class="badge">سوال <?php echo $pr->per_number(($step + 1)); ?> از <?php echo $max; ?></span>
                                <span  class="badge">بارم: <?php echo $pr->per_number($row['q_score']); ?></span>
                                <br><br>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <p dir="auto" style="white-space:pre-wrap;"><?php echo $pr->per_number($row['q_content']); ?></p>
                                        <br>
                                        <?php $question->get_media($row['ID'], "large"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                        if ($q_type == 0) {
                                            if ($row['q_opt1']) {
                                                ?>
                                                <input name="answer" type="radio"
                                                       value="<?php echo $row['q_opt1']; ?>" <?php if ($row['q_opt1'] == $answer) {
                                                    echo 'checked';
                                                } ?>>
                                                <label><?php echo $pr->per_number($row['q_opt1']); ?></label>
                                                <?php
                                                $question->get_item_media($row['ID'], 1);
                                                ?>
                                                <hr>
                                                <?php
                                            }
                                            if ($row['q_opt2']) {
                                                ?>
                                                <input name="answer" type="radio"
                                                       value="<?php echo $row['q_opt2']; ?>" <?php if ($row['q_opt2'] == $answer) {
                                                    echo 'checked';
                                                } ?>>
                                                <label><?php echo $pr->per_number($row['q_opt2']); ?></label>
                                                <?php
                                                $question->get_item_media($row['ID'], 2);
                                                ?>
                                                <hr>
                                                <?php
                                            }
                                            if ($row['q_opt3']) {
                                                ?>
                                                <input name="answer" type="radio"
                                                       value="<?php echo $row['q_opt3']; ?>" <?php if ($row['q_opt3'] == $answer) {
                                                    echo 'checked';
                                                } ?>>
                                                <label><?php echo $pr->per_number($row['q_opt3']); ?></label>
                                                <?php
                                                $question->get_item_media($row['ID'], 3);
                                                ?>
                                                <hr>
                                                <?php
                                            }
                                            if ($row['q_opt4']) {
                                                ?>
                                                <input name="answer" type="radio"
                                                       value="<?php echo $row['q_opt4']; ?>" <?php if ($row['q_opt4'] == $answer) {
                                                    echo 'checked';
                                                } ?>>
                                                <label><?php echo $pr->per_number($row['q_opt4']); ?></label>
                                                <?php
                                                $question->get_item_media($row['ID'], 4);
                                                ?>
                                                <hr>
                                                <?php
                                            }
                                        } else if ($q_type == 1) {
                                            ?>
                                            <label>پاسخ خود را در این قسمت بنویسید: </label>
                                            <textarea name="answer"
                                                      class="form-control answer_content"><?php echo $pr->per_number($answer); ?></textarea>
                                            <?php
                                        } else if ($q_type == 2) {
                                            ?>
                                            <label>پاسخ خود را در این قسمت بنویسید: </label>
                                            <textarea name="answer"
                                                      class="form-control answer_content"><?php echo $pr->per_number($answer); ?></textarea>
                                            <?php
                                        } else if ($q_type == 3) {
                                            if ($row['q_opt1']) {
                                                ?>
                                                <input name="answer" type="radio"
                                                       value="<?php echo $row['q_opt1']; ?>" <?php if ($row['q_opt1'] == $answer) {
                                                    echo 'checked';
                                                } ?>>
                                                <label><?php echo $pr->per_number($row['q_opt1']); ?></label>
                                                <hr>
                                                <?php
                                            }
                                            if ($row['q_opt2']) {
                                                ?>
                                                <input name="answer" type="radio"
                                                       value="<?php echo $row['q_opt2']; ?>" <?php if ($row['q_opt2'] == $answer) {
                                                    echo 'checked';
                                                } ?>>
                                                <label><?php echo $pr->per_number($row['q_opt2']); ?></label>
                                                <hr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="panel-footer text-center" style="display: block">
                                        <?php
                                        $e_id = $row['e_id'];
                                        $preview = $db->get_var_query("select e_preview from exam where ID = $e_id");
                                        $next = $step + 1;
                                        if ($preview == 0) {
                                            if ($step > 0) {
                                                $prev = $step - 1;
                                                ?>
                                                <button data-exam_id="<?php echo $ID; ?>"
                                                        data-next="<?php echo($prev); ?>" id="prev-question"
                                                        class="btn btn-warning btn-lg">سوال قبل
                                                </button>
                                                <?php
                                            }
                                        } ?>
                                        <?php
                                        $final_step = $max - 1;
                                        if ($final_step == $step) {
                                            echo '<button data-exam_id="' . $ID . '" data-next="' . $next . '" id="next-question" class="btn btn-primary btn-lg">پایان آزمون</button>';
                                        } else {
                                            echo '<button  data-exam_id="' . $ID . '" data-next="' . $next . '"  id="next-question" class="btn btn-success btn-lg">سوال بعد</button>';
                                        } ?>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $("#exam_timer").TimeCircles({
                                            time:{
                                                Days:{
                                                    show: false
                                                },
                                                Hours:{
                                                    show: true
                                                }
                                            }
                                        });

                                        setInterval(function(){
                                            var remaining_second = $("#exam_timer").TimeCircles().getTime();
                                            var time = 0;
                                            if(remaining_second < 59)
                                            {
                                                if(time!==1){
                                                    window.confirm('آزمون شما به پایان رسید');
                                                    time = 1;
                                                }
                                                window.location.href = "http://gtserver.ir/login";
                                            }
                                        }, 1000);

                                    });
                                </script>
                                <?php
                                $start_time = explode(":", $_SESSION['start_time']);
                                $system_h = $start_time[0];
                                $system_m = $start_time[1];
                                $e_time = $db->get_var_query("select e_time from exam where ID = $ID");
                                $e_deadline = $db->get_var_query("select e_deadline from exam where ID = $ID");
                                $exam_duration = $e_deadline;
                                $fulltime = explode(":", $e_time);
                                $e_time_h = $fulltime[0];
                                $e_time_m = $fulltime[1];
                                $e_final_time_h = floor($e_deadline / 60) + $e_time_h;
                                $e_deadline = $e_deadline - (60 *  floor($e_deadline / 60));
                                $e_final_time_m = $e_deadline + $e_time_m;
                                if($e_final_time_m>=60){
                                    $e_final_time_h += floor($e_final_time_m / 60);
                                    $e_final_time_m = $e_final_time_m - 60;
                                }
                                $minutes1 = (($system_h * 60.0) + $system_m);
                                $minutes2 = (($e_final_time_h * 60.0 )+ $e_final_time_m);

                                $exam_duration =  ($minutes2 - $minutes1);

                                ?>
                                <div align="center">
                                    <div id="exam_timer" data-timer="<?php echo $exam_duration * 60; ?>" style="max-width:400px; width: 100%; height: 200px;"></div>
                                </div>
                            </div>

                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                </div>
                <div id="answer-re">
                </div>
                <?php
            }
        }
    }


    public function list_my_exam_view()
    { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>عنوان آزمون</th>
                            <th>معلم</th>
                            <th>درس</th>
                            <th>تاریخ</th>
                            <th>ساعت</th>
                            <th>مهلت آزمون</th>
                            <th>نمره آزمون</th>
                            <th>مدیریت</th>
                        </tr>
                        <?php
                        $i = 1;
                        $pr = new prime();
                        $db = new db();
                        $gt_date = new gt_date();
                        $gt_teacher = new gt_teacher();
                        $gt_lesson = new gt_lesson();
                        $st_id = $_SESSION['user_id'];
                        $list = $db->get_select_query("select * , exam.ID as examID from exam inner join register on exam.c_id = register.c_id where register.st_id = $st_id order by exam.ID desc");
                        if (count($list) > 0) {
                            foreach ($list as $l) {
                                $date = $gt_date->jgdate($l['e_date']);
                                $current_date = $gt_date->jgdate(jdate("Y/n/j"));
                                $flag = 0;
                                if($l['e_date'] == $current_date){
                                    $flag =  1;
                                }
                                if($l['e_date'] < $current_date){
                                    $flag =  2;
                                }
                                 ?>
                                <tr style="background-color: <?php if($flag ==1) {echo "#3ada59";}elseif ($flag==2){echo "#e31b31   ";} else{echo "#f8c427";}?>">
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $pr->per_number($l['e_title']); ?></td>
                                    <td><?php
                                        $t_name = $gt_teacher->get_teacher_name($l['t_id']);
                                        echo $pr->per_number($t_name); ?>
                                    </td>
                                    <td><?php
                                        $l_name = $gt_lesson->get_lesson_name($l['l_id']);
                                        echo $pr->per_number($l_name); ?></td>
                                    <td><?php

                                        echo $pr->per_number($date); ?>
                                    </td>
                                    <td><?php echo $pr->per_number($l['e_time']); ?></td>
                                    <td><?php echo $pr->per_number($l['e_deadline']); ?></td>
                                    <td><?php echo $pr->per_number($l['e_score']); ?></td>
                                    <td>
                                        <?php
                                        $db = new db();
                                        $ex_id = $l['examID'];
                                        $st_id = $_SESSION['user_id'];
                                        $sql = "select * from do_exam where e_id = $ex_id and st_id = $st_id";
                                        $res = $db->get_select_query($sql);
                                        if (count($res) > 0) {
                                            ?>
                                            <a class="btn btn-danger btn-xs" disabled>آزمون انجام شده است</a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="do_exam?exam_id=<?php echo $l['examID']; ?>"
                                               class="btn btn-primary btn-xs">شرکت در آزمون</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="9" class="text-center">هیچ آزمونی تاکنون تعریف نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function start_exam($ID)
    {
        $pr = new prime();
        $db = new db();
        $gt_date = new gt_date();
        $mydate = jdate("Y/m/d");
        $mydate = $gt_date->jgdate($mydate);
        $mydate = $gt_date->PersianToEnglish($mydate);
        $system_h = date("H");
        $system_m = date("i");
        $e_date = $db->get_var_query("select e_date from exam where ID = $ID");
        $e_time = $db->get_var_query("select e_time from exam where ID = $ID");
        $e_deadline = $db->get_var_query("select e_deadline from exam where ID = $ID");
        $fulltime = explode(":", $e_time);
        $e_time_h = $fulltime[0];
        $e_time_m = $fulltime[1];
        $e_final_time_h = floor($e_deadline / 60) + $e_time_h;
        $e_deadline = $e_deadline - (60 * floor($e_deadline / 60));
        $e_final_time_m = $e_deadline + $e_time_m;
        if ($e_final_time_m >= 60) {
            $e_final_time_h += floor($e_final_time_m / 60);
            $e_final_time_m = $e_final_time_m - 60;
        }
        $flag = "0";
        if ($system_h < $e_final_time_h && $system_h > $e_time_h) {
            $flag = "1";
        }if ($system_h == $e_time_h) {
            if ($system_m >= $e_time_m && $system_h < $e_final_time_h) {
                $flag = "1";
            }
        } if ($system_h == $e_final_time_h) {
            if ($system_m <= $e_final_time_m) {
                $flag = "1";
            }
        }if($system_h == $e_final_time_h && $system_h==$e_time_h){
            if ($system_m <= $e_final_time_m&&$system_m>=$e_time_m) {
                $flag = "1";
            }
        }

        ?>
        <div id="next-question-result">
            <div class="col-md-12 text-center">
                <table class="table table-bordered">
                    <tr>
                        <th>عنوان آزمون:</th>
                        <td><?php
                            $e_name = $this->get_exam_name($ID);
                            echo $pr->per_number($e_name); ?></td>
                        <th>درس:</th>
                        <td><?php
                            $l_name = $this->get_exam_course($ID);
                            echo $pr->per_number($l_name); ?></td>
                    </tr>
                    <tr>
                        <th>تاریخ شروع:</th>
                        <td><?php
                            $date = $this->get_exam_field($ID, 'e_date');
                            $jdate = $date = $gt_date->jgdate($date);
                            echo $pr->per_number($jdate); ?></td>
                        <th>ساعت شروع:</th>
                        <td><?php
                            $time = $this->get_exam_field($ID, 'e_time');
                            echo $pr->per_number($time); ?></td>
                    </tr>
                    <tr>
                        <th>وقت آزمون(به دقیقه):</th>
                        <td><?php
                            $dead_line = $this->get_exam_field($ID, 'e_deadline');
                            echo $pr->per_number($dead_line); ?></td>
                        <th>نمره آزمون:</th>
                        <td><?php
                            $score = $this->get_exam_field($ID, 'e_score');
                            echo $pr->per_number($score); ?></td>
                    </tr>
                </table>
                <?php
                if ($_SESSION['level'] == "manager" || $_SESSION['level'] == "teacher") {
                    ?>
                    <button data-exam_id="<?php echo $ID; ?>" data-next="0" id="next-question"
                            class="btn btn-success btn-lg">شروع آزمون
                    </button>
                    <?php
                }else if ($mydate == $e_date && $flag == "1") {
                    ?>
                    <button data-exam_id="<?php echo $ID; ?>" data-next="0" id="next-question"
                            class="btn btn-success btn-lg">شروع آزمون
                    </button>
                    <?php
                } else { ?>
                    <button class="btn btn-danger btn-lg">مجاز به شرکت در آزمون نمی باشید</button>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }


    public function finish_exam()
    {
        ?>
        <div class="col-md-12">
            <div class="alert alert-info text-center">
                <h4>اتمام آزمون</h4>
                <hr>
                <p>آزمون شما با موفقیت به پایان رسید. با آرزوی بهترین ها برای شما</p>
            </div>
        </div>
        <?php
    }

    public function list_view_results_view()
    { ?>
        <div class="row" id="rest-print">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>ردیف</th>
                            <th>نام دانش آموز</th>
                            <th>تاریخ آزمون</th>
                            <th>ساعت آزمون</th>
                            <th>عملیات</th>
                        </tr>
                        <?php
                        $i = 1;
                        $db = new db();
                        $gt_student = new gt_student();
                        $pr = new prime();
                        $st_list = $db->get_select_query("select st_id, ID, q_id from answer");
                        foreach ($st_list as $st) {
                            $ID = $st['ID'];
                            $q_id = $st['q_id'];
                            $e_id = $db->get_var_query("select e_id from question where ID = $q_id");
                            $e_date = $db->get_var_query("select e_date from exam where ID = $e_id");
                            $gt_date = new gt_date();
                            $date = $gt_date->jgdate($e_date);
                            $e_time = $db->get_var_query("select e_time from exam where ID = $e_id");
                            ?>
                            <tr>
                                <td><?php echo $pr->per_number($i); ?></td>
                                <td><?php echo $pr->per_number($name = $gt_student->get_student_name($st['st_id'])); ?></td>
                                <td><?php echo $pr->per_number($date); ?></td>
                                <td><?php echo $pr->per_number($e_time); ?></td>
                                <td>
                                    <a href="exam_report?exam_id=<?php echo $l['ID']; ?>"
                                       class="btn btn-primary btn-xs">مشاهده کارنامه</a>
                                    <a href="exam_review?exam_id=<?php echo $l['ID']; ?>"
                                       class="btn btn-primary btn-xs">بازبینی آزمون</a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_done_exam_view()
    { ?>
        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>عنوان آزمون</th>
                            <th>معلم</th>
                            <th>درس</th>
                            <th>تاریخ</th>
                            <th>ساعت</th>
                            <th>نمره آزمون</th>
                            <th>مدیریت</th>
                        </tr>
                        <?php
                        $i = 1;
                        $db = new db();
                        $pr = new prime();
                        $gt_date = new gt_date();
                        $gt_teacher = new gt_teacher();
                        $gt_lesson = new gt_lesson();
                        if ($_SESSION['level'] == "manager" || $_SESSION['level'] == "teacher") {
                            $st_id = $_GET['st_id'];
                        } else {
                            $st_id = $_SESSION['user_id'];
                        }
                        $sql = "select * from answer where st_id = $st_id group by st_id";
                        $student = $db->get_select_query($sql);
                        if (count($student) > 0) {
                            foreach ($student as $s) {
                                $q_id = $s['q_id'];
                                $e_id = $db->get_var_query("select e_id from question where ID = $q_id");
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $this->get_exam_name_by_q($s['q_id']); ?></td>
                                    <td><?php echo $this->get_teacher_name_by_q($s['q_id']); ?></td>
                                    <td><?php echo $this->get_lesson_name_by_q($s['q_id']); ?></td>
                                    <td><?php echo $this->get_exam_date_by_q($s['q_id']); ?></td>
                                    <td><?php echo $this->get_exam_time_by_q($s['q_id']); ?></td>
                                    <td><?php echo $this->get_exam_score_by_q($s['q_id']); ?></td>
                                    <td>
                                        <a href="exam_report?exam_id=<?php echo $e_id; ?>&st_id=<?php echo $st_id; ?>"
                                           class="btn btn-primary btn-xs">مشاهده کارنامه</a>
                                        <a href="exam_review?exam_id=<?php echo $e_id; ?>&st_id=<?php echo $st_id; ?>"
                                           class="btn btn-primary btn-xs">بازبینی آزمون</a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                            ?>


                            <tr>
                                <td colspan="9" class="text-center">تا کنون هیچ آزمونی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>

            </div>
        </div>
        <?php
    }

    public function get_st_score($q_id, $st_id)
    {
        $db = new db();
        $pr = new prime();
        $q_type = $db->get_var_query("select q_type from question where ID = $q_id");
        if ($q_type == 0 || $q_type == 3) {
            $correct_answer = $db->get_var_query("select correct_answer from question where ID = $q_id");
            $st_answer = $db->get_var_query("select answer from answer where q_id = $q_id and st_id = $st_id");
            $score = $db->get_var_query("select q_score from question where ID = $q_id");
            $per_score = $pr->per_number($score);
            if ($correct_answer == $st_answer) {
                return $per_score;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function get_exam_score($exam_id, $st_id)
    {
        $db = new db();
        $score = 0;
        $question = $db->get_select_query("select * from question where e_id = $exam_id");
        if (count($question) > 0) {
            foreach ($question as $l) {
                $q_id = $l['ID'];
                $score += $db->get_var_query("select st_score from answer where q_id = $q_id and st_id = $st_id");
            }
        }
        return $score;
    }

    public function list_exam_review_view()
    { ?>
        <div class="row" id="rest-print">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>ردیف</th>
                            <th>سوال</th>
                            <th>پاسخ صحیح</th>
                            <th>پاسخ دانش آموز</th>
                            <th>بارم سوال</th>
                            <th>نمره دانش آموز</th>
                        </tr>
                        <?php
                        $i = 1;
                        $db = new db();
                        $u_id = $_SESSION['user_id'];
                        $exam_id = $_GET['exam_id'];
                        $st_id = $_GET['st_id'];
                        $gt_student = new gt_student();
                        $pr = new prime();

                        $list = $db->get_select_query("select *, answer.ID as asid from question inner join answer on question.ID = answer.q_id where question.e_id = $exam_id and answer.st_id = $st_id order by question.ID desc");
                        if (count($list) > 0) {
                            foreach ($list as $l) { ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $l['q_content']; ?></td>
                                    <td><?php echo $l['correct_answer']; ?></td>
                                    <td><?php echo $l['answer']; ?></td>
                                    <td><?php echo $pr->per_number($l['q_score']); ?></td>
                                    <td>
                                        <input name="score" type="text" class="score"
                                               data-id="<?php echo $l['asid']; ?>" data-eid="<?php echo $l['e_id']; ?>"
                                               data-stid="<?php echo $st_id; ?>"
                                               value="<?php echo $pr->per_number($l['st_score']); ?>">
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            } ?>
                            <tr>
                                <td colspan="4" class="text-center"><strong>نمره کل</strong></td>
                                <td><?php
                                    $e_id = $l['e_id'];
                                    $score = $db->get_var_query("select e_score from exam where ID = $e_id");
                                    echo $pr->per_number($score);
                                    ?></td>
                                <td>
                                    <div id="result-score"><?php $exam_score = $this->get_exam_score($e_id, $st_id);
                                        echo $pr->per_number($exam_score); ?></div>
                                </td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td colspan="9" class="text-center">تا کنون آزمونی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_exam_report_view()
    { ?>
            <div id="printarea" class="col-md-12">
                <div  class="table-responsive">
                    <table class="table">
                        <tr colspan="12">
                            <th colspan="4">نام و نام خانوادگی</th>
                            <th colspan="4">عنوان آزمون</th>
                            <th colspan="4">نمره</th>
                        </tr>
                        <?php
                        $db = new db();
                        $pr = new prime();
                        if ($_SESSION['level'] == "manager" || $_SESSION['level'] == "teacher") {
                            $st_id = $_GET['st_id'];
                        } else {
                            $st_id = $_SESSION['user_id'];
                        }
                        $exam_id = $_GET['exam_id'];
                        $gt_student = new gt_student(); ?>
                        <tr colspan="12">
                            <td colspan="4"><?php
                                $u_id = $gt_student->get_student_name($st_id);
                                echo $pr->per_number($u_id); ?>
                            </td>
                            <td colspan="4"><?php
                                $e_name = $this->get_exam_name($exam_id);
                                echo $pr->per_number($e_name); ?>
                            </td>
                            <td colspan="4"><?php
                                $st_score = $this->get_st_score($q_id, $st_id);
                                echo $pr->per_number($st_score); ?>
                            </td>
                        </tr>
                        <tr colspan="12">
                            <th colspan="4">سوال</th>
                            <th colspan="2">پاسخ صحیح</th>
                            <th colspan="2">پاسخ دانش آموز</th>
                            <th colspan="2">بارم سوال</th>
                            <th colspan="2">نمره دانش آموز</th>
                        </tr>
                        <?php
                        $i = 1;
                        $res = $db->get_select_query("select * from question where e_id = $exam_id order by ID desc");
                        if (count($res) > 0) {
                            foreach ($res as $row) { ?>
                                <tr colspan="12">
                                    <td colspan="4"><?php
                                        echo $row['q_content'];
                                        ?></td>
                                    <td colspan="2"><?php
                                        echo $row['correct_answer'];
                                        ?></td>
                                    <td colspan="2"><?php
                                        $q_id = $row['ID'];
                                        $answer = $db->get_var_query("select answer from answer where q_id = $q_id");
                                        echo $answer;
                                        ?></td>
                                    <td colspan="2"><?php
                                        echo $row['q_score'];
                                        ?></td>
                                    <td colspan="2"><?php
                                        $q_id = $row['ID'];
                                        $score = $db->get_var_query("select st_score from answer where q_id = $q_id");
                                        echo $score;
                                        ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } ?>
                    </table>
                </div>
            </div>
			<div class="row">
				<div class="col-md-12 no-print">
					<input type="button" value="چاپ کارنامه" onclick='printFunc();' class="btn btn-sm btn-default">
				</div>
			</div>
		
        <?php
    }

} ?>