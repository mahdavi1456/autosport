<?php
//کلاس
class gt_classroom {
	
	public function add_classroom($array) {
		$db = new db();
		$c_name = $array['c_name'];
		$s_id = $array['s_id'];
		$g_y_id = $array['g_y_id'];
		$sql = "insert into classroom(c_name, s_id, g_y_id) values('$c_name', $s_id, $g_y_id)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_classroom($array) {
		$db = new db();
		$ID = $array['ID'];
		$c_name = $array['c_name'];
		$s_id = $array['s_id'];
		$g_y_id = $array['g_y_id'];
		$sql = "update classroom set c_name = '$c_name', s_id = $s_id, g_y_id = $g_y_id where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_classroom($ID) {
		$db = new db();
		$sql = "delete from classroom where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_classroom_name($ID) {
		$db = new db();
		$c_name = $db->get_var_query("select c_name from classroom where ID = $ID");
		return $c_name;
	}
	
	public function create_form($ID = null) {
		$db = new db();
		$gt_grade_year = new gt_grade_year();
		if($ID == null) {
			$c_name = "";
			$s_id = "";
			$g_y_id = "";
		} else {
			$asb = $db->get_select_query("select * from classroom where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$c_name = $roww['c_name'];
					$s_id = $roww['s_id'];
					$g_y_id = $roww['g_y_id'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-classroom-view">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>نام کلاس <span class="red">*</span></label>
								<input type="text" name="c_name" placeholder="نام کلاس" value="<?php echo $c_name; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>مدرسه <span class="red">*</span></label>
								<select name="s_id" class="form-control select2">
									<?php
									$res = $db->get_select_query("select * from school order by ID desc");
									if(count($res) > 0) {
										foreach($res as $row) {
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $s_id) { echo 'selected';} ?>><?php echo $row["s_name"]; ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>پایه تحصیلی <span class="red">*</span></label>
								<select name="g_y_id" class="form-control select2">
									<?php
									$res = $db->get_select_query("select * from grade_year order by ID desc");
									if(count($res) > 0) {
										foreach($res as $row) {
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $g_y_id) { echo 'selected';} ?>><?php echo $gt_grade_year->get_grade_year_name($row["ID"]); ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_classroom" class="btn btn-sm btn-success add-classroom">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_classroom" class="btn btn-sm btn-warning update-classroom">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-classroom-view-result"><?php $this->list_classroom_view(); ?></div>
		<?php
	}

	public function list_classroom_view() {
		$gt_school = new gt_school();
		$gt_grade_year = new gt_grade_year();
		$db = new db();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام کلاس</th>
							<th>مدرسه</th>
							<th>پایه تحصیلی</th>
							<th>عملیات</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$list = $db->get_select_query("select * from classroom order by ID desc");
						if(count($list) > 0) {
							foreach($list as $row) { 
							$s_id = $gt_school->get_school_name($row['s_id']);
							$g_y_id = $gt_grade_year->get_grade_year_name($row['g_y_id']);
							?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($row['c_name']); ?></td>
									<td><?php echo $pr->per_number($s_id); ?></td>
									<td><?php echo $pr->per_number($g_y_id); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-classroom-form" data-id="<?php echo $row['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $row['ID']; ?>" class="btn btn-danger btn-xs remove-classroom">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else { ?>
							<tr>
								<td colspan="6">هیچ کلاسی تاکنون ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
	public function list_my_classroom_view() {
		$gt_school = new gt_school();
		$gt_grade_year = new gt_grade_year();
		$db = new db();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام کلاس</th>
							<th>مدرسه</th>
							<th>پایه تحصیلی</th>
							<th>عملیات</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$t_id = $_SESSION['user_id'];
						$list = $db->get_select_query("select c_id from teaching where t_id = $t_id order by ID desc");
						if(count($list) > 0) {
							foreach($list as $row) {
								$c_id = $row['c_id'];
								$s_id = $db->get_var_query("select s_id from classroom where ID = $c_id");
								$g_y_id = $db->get_var_query("select g_y_id from classroom where ID = $c_id"); ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php
										$c_id = $this->get_classroom_name($row['c_id']);
									echo $pr->per_number($c_id); ?>
									</td>
									<td><?php
										$s_id = $gt_school->get_school_name($s_id);
									echo $pr->per_number($s_id);  ?>
									</td>
									<td><?php
									$g_y_id = $gt_grade_year->get_grade_year_name($g_y_id);
									echo $pr->per_number($g_y_id);  ?></td>
									<td>
										<a href="student_list?c_id=<?php echo $row['c_id']; ?>" class="btn btn-primary btn-xs">مشاهده لیست دانش آموزان</a>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else { ?>
							<tr>
								<td colspan="6">هیچ کلاسی تاکنون ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
	public function list_student_list_view() {
		$gt_school = new gt_school();
		$gt_grade_year = new gt_grade_year();
		$db = new db();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام دانش آموز</th>
							<th>عملیات</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$gt_student = new gt_student();
						$c_id = $_GET['c_id'];
						$list = $db->get_select_query("select st_id from register where c_id = $c_id order by ID desc");
						if(count($list) > 0) {
							foreach($list as $row) {
								?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php
										$st_name = $gt_student->get_student_name($row['st_id']);
										echo $pr->per_number($st_name); ?>
									</td>
									<td>

										<a href="done_exam?st_id=<?php echo $row['st_id'] ; ?>" class="btn btn-primary btn-xs">مشاهده آزمون ها</a>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else { ?>
							<tr>
								<td colspan="6">تا کنون هیچ دانش آموزی ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
}