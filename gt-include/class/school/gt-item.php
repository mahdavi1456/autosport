<?php
//گزینه
class gt_item {
	
	public function add_item_view() { ?>
        <div class="row">
            <div class="col-md-12">
                <div id="add-option-form">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>کددرس <span class="red">*</span></label>
                                <input type="text" name="g_id" placeholder="کد درس" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>گزینه ها <span class="red">*</span></label>
                            <input type="text" name="option" placeholder="گزینه ها" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
					<div class="col-md-12">
						<button type="submit" name="add_option" id="add-option" class="btn btn-sm btn-success">ذخیره</button>
					</div>
                </div>
            </div>
        </div>
     <?php
    }
    public function list_item_view() {
        $sql = "select * from item order by ID dsec";
        $db = new db();
        $res = $db->get_select_query($sql);
        $i = 1;
        ?>
        <div class="row result">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
                    <tr>
							<th>ردیف</th>
							<th>کد سوال</th>
                            <th>گزینه ها</th>
						</tr>
                     <?php
						$i = 1;
						$pr = new prime();
						$db = new db();
                        if(count($res) > 0) {
                            foreach($res as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $pr->per_number($row['q_id']); ?></td>
                                    <td><?php echo $pr->per_number($row['option']); ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                        ?>
                        <tr>
                            <td colspan="10">هیچ گزینه ای ثبت نشده است</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }
}