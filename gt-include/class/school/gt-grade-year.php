<?php
//پایه تحصیلی
class gt_grade_year{
	
	public function add_grade_year($array) {
		$db = new db();
		$g_year = $array['g_year'];
		$g_id = $array['g_id'];
		$sql = "insert into grade_year(g_year, g_id) values('$g_year', $g_id)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_grade_year($array) {
		$db = new db();
		$ID = $array['ID'];
		$g_year = $array['g_year'];
		$g_id = $array['g_id'];
		$sql = "update grade_year set g_year = '$g_year', g_id = $g_id where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_grade_year($ID) {
		$db = new db();
		$sql = "delete from grade_year where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_grade_year_name($ID) {
		$db = new db();
		$gt_grade = new gt_grade();
		$g_year = $db->get_var_query("select g_year from grade_year where ID = $ID");
		$g_id = $db->get_var_query("select g_id from grade_year where ID = $ID");
		$grade_name = $gt_grade->get_grade_name($g_id);
		return $g_year . " " . $grade_name;
	}
	
	public function create_form($ID = null) {
		$db = new db();
		if($ID == null) {
			$g_year = "";
			$g_id = "";
		} else {
			$asb = $db->get_select_query("select * from grade_year where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$g_year = $roww['g_year'];
					$g_id = $roww['g_id'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-grade-year-view">
					<div class="row">
						<div class="col-md-3">
                            <div class="form-group">
                                <label>نام پایه تحصیلی <span class="red">*</span></label>
                                <input type="text" name="g_year" value="<?php echo $g_year; ?>" placeholder="نام پایه تحصیلی" class="form-control">
                            </div>
                        </div>
						<div class="col-md-3">
							<div class="form-group">
								<label>مقطع <span class="red">*</span></label>
								<select name="g_id" class="form-control select2">
									<?php
									$db = new db();
									$res = $db->get_select_query("select * from grade order by ID desc");
									if(count($res) > 0) {
										foreach($res as $row) {
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $g_id) { echo 'selected';} ?>><?php echo $row["g_name"]; ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_grade_year" class="btn btn-sm btn-success add-grade-year">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_grade_year" class="btn btn-sm btn-warning update-grade-year">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-grade-year-view-result"><?php $this->list_grade_year_view(); ?></div>
		<?php
	}
	
    public function list_grade_year_view() {
        ?>
        <div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام پایه تحصیلی</th>
							<th>نام مقطع</th>
							<th>عملیات</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$db = new db();
						$gt_grade = new gt_grade();
						$list = $db->get_select_query("select * from grade_year order by ID desc");
						if(count($list) > 0) {
							foreach($list as $row) {
								$g_id = $gt_grade->get_grade_name($row['g_id']);
								?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($row['g_year']); ?></td>       
									<td><?php echo $pr->per_number($g_id); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-grade-year-form" data-id="<?php echo $row['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $row['ID']; ?>" class="btn btn-danger btn-xs remove-grade-year">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else {
							?>
								<tr>
									<td colspan="4">هیچ پایه تحصیلی تاکنون ثبت نشده است</td>
								</tr>
						<?php
						}
						?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }
	
}