<?php
class gt_duties {
	
	public function update_duties($array) {
		$db = new db();
		$ID = $array['ID'];
		$p_id = $array['p_id'];
		$title = $array['title'];
		$details = $array['details'];
		$d_date = $array['d_date'];
		$t_id = $array['t_id'];
		$sql = "update duties set p_id = $p_id, title = '$title', details = '$details', d_date = '$d_date', t_id = $t_id where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_duties($ID) {
		$db = new db();
		$link = $db->get_var_query("select file from duties where ID = $ID");
		$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/sport/duties/" . $link;
		if(unlink($path)){
			$sql = "delete from duties where ID = $ID";
			$db->ex_query($sql);
		}
	}
	
	public function create_form($ID = null) {
		$db = new db();
		$gt_date = new gt_date();
		if($ID == null) {
			$p_id = "";
			$title = "";
			$details = "";
			$d_date = "";
		} else {
			$asb = $db->get_select_query("select * from duties where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$p_id = $roww['p_id'];
					$title = $roww['title'];
					$details = $roww['details'];
					$d_date = $roww['d_date'];
					$d_date = $gt_date->jgdate($d_date);
				}
			 }
		}?>
		<div class="row">
			<div class="col-md-12">
				<form id="<?php if($ID == null) { echo "uploadForm"; } else { echo "editForm";  } ?>" action="" method="post">
					<div id="add-duties-view">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label>عنوان <span class="red">*</span></label>
									<input type="text" name="title" placeholder="عنوان" value="<?php echo $title; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>بازیکن <span class="red">*</span></label>
									<select name="p_id" class="form-control select2">
										<?php
										$db = new db() ;
										$player = $db->get_select_query("select * from player order by ID desc");
										if(count ($player) > 0) {
											foreach($player as $row){
												?>
												<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $p_id){ echo 'selected'; } ?>><?php 
													$gt_player = new gt_player();
													$name = $gt_player->get_player_name($row['ID']);
													echo $name;
												?></option>
												<?php
											}
										} ?>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>تاریخ <span class="red">*</span></label>
									<input type="text" name="d_date" placeholder="تاریخ" value="<?php echo $d_date; ?>" class='date-picker form-control'>
								</div>
							</div>
							<div class="col-md-3">
								<label>انتخاب فایل <span class="red">*</span></label>
								<input name="d_file" type="file" class="inputFile" id="d_file"/><br/>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>توضیحات</label>
									<textarea type="text" name="details" placeholder="توضیحات" value="<?php echo $details; ?>" class="form-control"></textarea>
								</div>
							
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<?php
								if($ID == null) {
									?>
									<button type="submit" name="add_duties" class="btn btn-sm btn-success add-duties">ذخیره</button>
									<?php
								} else {
									?>
									<button type="submit" name="update_duties" class="btn btn-sm btn-warning update-duties">ویرایش</button>
									<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
									<?php
								} ?>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div id="list-duties-view-result"><?php $this->list_duties_view(); ?></div>
	<?php
	}
	
	public function list_duties_view() {
		?>
		<div class="row result">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>عنوان</th>
							<th>بازیکن</th>
							<th>تاریخ</th>
							<th>توضیحات</th>
							<th>سند</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$db = new db();
						$gt_date = new gt_date();
						$pr = new prime();
						$gt_player = new gt_player();
						$engine = new engine();
						$url = $engine->get_the_url();
						$list = $db->get_select_query("select * from duties order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['title']); ?></td>
									<td><?php 
										$name = $gt_player->get_player_name($l['p_id']);
										echo $pr->per_number($name);
									?></td>
									<td><?php
									$date = $gt_date->jgdate($l['d_date']);
									echo $pr->per_number($date); ?></td>
									<td><?php echo $pr->per_number($l['details']); ?></td>
									<td>
										<?php
										if($l['file'] != ""){ ?>
											<a href="<?php echo $url . "gt-content/uploads/sport/duties/" . $l['file']; ?>" target="_blank">
												<img style="width: 10%; border-radius: 4px; padding: 1px; border: 2px solid #ddd;" src="<?php echo $url . "gt-content/uploads/sport/duties/" . $l['file']; ?>">
											</a>
											<?php
										} ?>
									</td>
									<td>
										<button class="btn btn-info btn-xs update-duties-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-duties">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="6">تا کنون هیچ تمرینی ثبت نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}

}