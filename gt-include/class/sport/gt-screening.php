<?php
class gt_screening {
	
	public function create_form($agecategory = null , $p_id = null, $set_date = null) {
		$db = new db();
		$gt_date = new gt_date();
		$set_date2 = $gt_date->jgdate($set_date);
		?>
		<div id="add-screening-view">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>بازیکن </label>
						<select name="p_id" class="form-control p_id select2">
                            <option value="0" selected>انتخاب نام بازیکن</option>
                            <?php
							$db = new db();
							$gt_player = new gt_player();
							$res = $db->get_select_query("select * from player order by ID desc");
							if(count($res) > 0){
								foreach($res as $row){
									?>
									<option value="<?php echo $row["ID"]; ?>" <?php if($p_id == $row["ID"]){ echo 'selected'; } ?>><?php echo $gt_player->get_player_name($row["ID"]); ?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
				</div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>کد ملی </label>
                        <?php
                        $db = new db();
                        $pr = new prime();
                        $national_id = $db->get_var_query("select p_national_code from player where ID = $p_id");
                        ?>
                        <input type="text" name="national_id" placeholder="کد ملی" class="form-control national_id" value="<?php echo $pr->per_number($national_id); ?>">
                    </div>
                </div>
					<input type="hidden" name="agecategory" class="form-control agecategory">
					<input type="hidden" name="age" value="<?php echo $agecategory; ?>">
				<div class="col-md-3">
					<div class="form-group">
						<label>تاریخ <span class="red">*</span></label>
						<input type="text" name="set_date" placeholder="تاریخ" value="<?php if($set_date != null){ echo $set_date; } else{ echo jdate('Y/m/d'); } ?>" class="date-picker set_date form-control">
					</div>
				</div><br>
				<div class="col-md-3">
					<button type="submit" class="btn btn-sm btn-success view-screening">مشاهده</button>
				</div>
				<?php
				if($agecategory != null){ ?>
					<div class="col-md-12">
						<table class="table">
							<tr>
								<th>شاخص</th>
								<th>پارامتر</th>
								<th>معیار اندازه گیری 1 تا 5</th>
							</tr>
							<?php
							$sql = $db->get_select_query("select * from screening_index where ages_id = $agecategory");
							if(count($sql) > 0) { 
								foreach($sql as $row){
									$si_id = $row['ID'];
									$sql1 = $db->get_select_query("select * from parameter where pa_index = $si_id"); ?>
									<tr>
										<td rowspan="<?php if(count($sql1) > 0) { echo count($sql1); } else { echo 1; } ?>"><?php echo $row['si_name']; ?></td>
										<?php
										if(count($sql1) > 0) {
											$id1 = $sql1[0]['ID'];
											$score = $db->get_var_query("select score from screening where par_id = $id1 and p_id = $p_id and set_date = '$set_date2'"); ?>
											<td><?php echo $sql1[0]['pa_name']; ?></td>
											<td><input type="tex" name="score<?php echo $sql1[0]['ID']; ?>" value="<?php echo $score; ?>" class="form-control"></td>
											<input type="hidden" name="par_id" value="<?php echo $sql1[0]['ID']; ?>">
											<?php
										} else { ?>
											<td colspan="2">پارامتری وجود ندارد</td>
											<?php
										}?>
									</tr>
									<?php
									if(count($sql1) > 1) { 
										foreach($sql1 as $row1){
											if($row1['ID'] != $id1){
												$ID = $row1['ID'];
												$score = $db->get_var_query("select score from screening where par_id = $ID and p_id = $p_id and set_date = '$set_date2'"); ?>
												<tr>
													<td><?php echo $row1['pa_name']; ?></td>
													<td><input type="tex" name="score<?php echo $row1['ID']; ?>" value="<?php echo $score; ?>" class="form-control"></td>
													<input type="hidden" name="par_id" value="<?php echo $row1['ID']; ?>">
												</tr>
												<?php
											}
										}
									} 
								}
							} else { ?>
								<td colspan="3">شاخصی وجود ندارد</td>
								<?php
							}?>
						</table>
					</div>
					<div class="col-md-12">
						<button type="button" name="add_screening" class="btn btn-sm btn-success add-screening">ذخیره</button>
					</div>
					<?php
				} ?>
			</div>
		</div>
		<?php
	}

}