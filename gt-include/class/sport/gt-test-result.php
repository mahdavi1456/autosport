<?php
class gt_test_result {
	
	public function add_test_result($array) {
		$db = new db();
		$gt_date = new gt_date();
		$s_id = $array['s_id'];
		$p_id = $array['p_id'];
		$tr_score1 = $array['tr_score1'];
		$tr_score2 = $array['tr_score2'];
		$tr_score3 = $array['tr_score3'];
		$tr_score4 = $array['tr_score4'];
		$tr_score5 = $array['tr_score5'];
		$tr_time1 = $array['tr_time1'];
		$tr_time2 = $array['tr_time2'];
		$tr_time3 = $array['tr_time3'];
		$tr_time4 = $array['tr_time4'];
		$tr_time5 = $array['tr_time5'];
		$tr_turn = $array['tr_turn'];
		$tr_number = $array['tr_number'];
		$c_id = $array['c_id'];
		$sup_id = $array['sup_id'];
		$tr_date = $array['tr_date'];
		$tr_date = $gt_date->jgdate($tr_date);
		$res = $db->get_select_query("select * from test_result where p_id = $p_id and tr_turn = $tr_turn and tr_number = $tr_number");
		if(count($res) > 0){
			$sql = "update test_result set s_id = $s_id, c_id = $c_id, sup_id = $sup_id, tr_date = '$tr_date', tr_score1 = '$tr_score1', tr_score2 = '$tr_score2', tr_score3 = '$tr_score3', tr_score4 = '$tr_score4', tr_score5 = '$tr_score5', tr_time1 = $tr_time1, tr_time2 = $tr_time2, tr_time3 = $tr_time3, tr_time4 = $tr_time4, tr_time5 = $tr_time5 where p_id = $p_id and tr_turn = $tr_turn  and tr_number = $tr_number";
			$db->ex_query($sql);
		} else {
			$sql = "insert into test_result(s_id, p_id, tr_turn, tr_number, c_id, sup_id, tr_date, tr_score1, tr_score2, tr_score3, tr_score4, tr_score5, tr_time1, tr_time2, tr_time3, tr_time4, tr_time5) values($s_id, $p_id, '$tr_turn', $tr_number, $c_id, $sup_id, '$tr_date', '$tr_score1', '$tr_score2', '$tr_score3', '$tr_score4', '$tr_score5', $tr_time1, $tr_time2, $tr_time3, $tr_time4, $tr_time5)";
			$db->ex_query($sql);
		}
		return $sql;
	}

	public function remove_test_result($ID) {
		$db = new db();
		$sql = "delete from test_result where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function create_form($tr_turn = null, $s_id = null, $p_id = null, $c_id = null, $sup_id = null, $tr_date = null, $tr_number = null) { ?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-test-result-view">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>ایستگاه <span class="red">*</span></label>
								<select name="s_id" class="form-control select2">
									<?php
									$db = new db();
									$gt_date = new gt_date();
									$res = $db->get_select_query("select * from station");
									if(count($res) > 0){
										foreach($res as $row){
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $s_id){ echo 'selected'; } ?>><?php echo $row["s_name"]; ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>بازیکن <span class="red">*</span></label>
								<select name="p_id" class="form-control select2">
									<?php
									$gt_player = new gt_player();
									$res = $db->get_select_query("select * from player");
									if(count($res) > 0){
										foreach($res as $row){
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $p_id){ echo 'selected'; } ?>><?php echo $gt_player->get_player_name($row["ID"]); ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>ثبات<span class="red">*</span></label>
								<select name="c_id" class="form-control select2">
									<?php
									$gt_coach = new gt_coach();
									$res = $db->get_select_query("select * from coach");
									if(count($res) > 0){
										foreach($res as $row){
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $c_id){ echo 'selected'; } ?>><?php echo $gt_coach->get_coach_name($row["ID"]); ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>ناظر <span class="red">*</span></label>
                                <select name="sup_id" class="form-control select2">
                                    <?php
                                    $gt_coach = new gt_coach();
                                    $res = $db->get_select_query("select * from coach");
                                    if(count($res) > 0){
                                        foreach($res as $row){
                                            ?>
                                            <option value="<?php echo $row["ID"]; ?>" <?php if($row["ID"] == $sup_id){ echo 'selected'; } ?>><?php echo $gt_coach->get_coach_name($row["ID"]); ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
						<div class="col-md-4">
							<div class="form-group">
								<label>تاریخ <span class="red">*</span></label>
								<input type="text" name="tr_date" placeholder="تاریخ" value="<?php echo $tr_date; ?>" class="date-picker form-control">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>نوبت آزمون <span class="red">*</span></label>
								<select name="tr_turn" class="form-control select2">
									<option value="1" <?php if($tr_turn == 1){ echo 'selected'; } ?>>اول</option>
									<option value="2" <?php if($tr_turn == 2){ echo 'selected'; } ?>>دوم</option>
									<option value="3" <?php if($tr_turn == 3){ echo 'selected'; } ?>>سوم</option>
									<option value="4" <?php if($tr_turn == 4){ echo 'selected'; } ?>>چهارم</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>شماره آزمون <span class="red">*</span></label>
								<select name="tr_number" class="form-control select2">
									<option value="1" <?php if($tr_number == 1){ echo 'selected'; } ?>>1</option>
									<option value="2" <?php if($tr_number == 2){ echo 'selected'; } ?>>2</option>
									<option value="3" <?php if($tr_number == 3){ echo 'selected'; } ?>>3</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn btn-sm btn-success view-test-result">انتخاب</button>
							</div>
						</div>
					</div>
					<?php
					$tr_date = $gt_date->jgdate($tr_date);
					$res = $db->get_select_query("select * from test_result where p_id = $p_id and tr_turn = $tr_turn and tr_turn = $tr_turn");
					if(count($res) > 0){
						$tr_score1 = $res[0]['tr_score1'];
						$tr_score2 = $res[0]['tr_score2'];
						$tr_score3 = $res[0]['tr_score3'];
						$tr_score4 = $res[0]['tr_score4'];
						$tr_score5 = $res[0]['tr_score5'];
						$tr_time1 = $res[0]['tr_time1'];
						$tr_time2 = $res[0]['tr_time2'];
						$tr_time3 = $res[0]['tr_time3'];
						$tr_time4 = $res[0]['tr_time4'];
						$tr_time5 = $res[0]['tr_time5'];
					}
					if($tr_turn == 2 || $tr_turn == 4){ ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>نمره آزمون1 <span class="red">*</span></label>
									<input type="text" name="tr_score1" placeholder="نمره آزمون1" value="<?php echo $tr_score1; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>زمان آزمون1 <span class="red">*</span></label>
									<input type="number" name="tr_time1" placeholder="زمان آزمون 1 بر حسب ثانیه" value="<?php echo $tr_time1; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>نمره آزمون2 <span class="red">*</span></label>
									<input type="text" name="tr_score2" placeholder="نمره آزمون2" value="<?php echo $tr_score2; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>زمان آزمون2 <span class="red">*</span></label>
									<input type="number" name="tr_time2" placeholder="زمان آزمون 2 بر حسب ثانیه" value="<?php echo $tr_time2; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>نمره آزمون3 <span class="red">*</span></label>
									<input type="text" name="tr_score3" placeholder="نمره آزمون3" value="<?php echo $tr_score3; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>زمان آزمون3 <span class="red">*</span></label>
									<input type="number" name="tr_time3" placeholder="زمان آزمون 3 بر حسب ثانیه" value="<?php echo $tr_time3; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>نمره آزمون4 <span class="red">*</span></label>
									<input type="text" name="tr_score4" placeholder="نمره آزمون4" value="<?php echo $tr_score4; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>زمان آزمون4 <span class="red">*</span></label>
									<input type="number" name="tr_time4" placeholder="زمان آزمون 4 بر حسب ثانیه" value="<?php echo $tr_time4; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>نمره آزمون5 <span class="red">*</span></label>
									<input type="text" name="tr_score5" placeholder="نمره آزمون5" value="<?php echo $tr_score5; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>زمان آزمون5 <span class="red">*</span></label>
									<input type="number" name="tr_time5" placeholder="زمان آزمون 5 بر حسب ثانیه" value="<?php echo $tr_time5; ?>" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" name="add_test_result" class="btn btn-sm btn-success add-test-result">ذخیره</button>
							</div>
						</div>
						<?php
					} else if($tr_turn == 1 || $tr_turn == 3){ ?>
						<input type="hidden" name="tr_time1" value="1" class="form-control">
						<input type="hidden" name="tr_time2" value="1" class="form-control">
						<input type="hidden" name="tr_time3" value="1" class="form-control">
						<input type="hidden" name="tr_time4" value="1" class="form-control">
						<input type="hidden" name="tr_time5" value="1" class="form-control">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label>نمره آزمون1 <span class="red">*</span></label>
									<input type="text" name="tr_score1" placeholder="نمره آزمون1" value="<?php echo $tr_score1; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label>نمره آزمون2 <span class="red">*</span></label>
									<input type="text" name="tr_score2" placeholder="نمره آزمون2" value="<?php echo $tr_score2; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label>نمره آزمون3 <span class="red">*</span></label>
									<input type="text" name="tr_score3" placeholder="نمره آزمون3" value="<?php echo $tr_score3; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label>نمره آزمون4 <span class="red">*</span></label>
									<input type="text" name="tr_score4" placeholder="نمره آزمون4" value="<?php echo $tr_score4; ?>" class="form-control">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label>نمره آزمون5 <span class="red">*</span></label>
									<input type="text" name="tr_score5" placeholder="نمره آزمون5" value="<?php echo $tr_score5; ?>" class="form-control">
								</div>
							</div><br>
							<div class="col-md-2">
								<button type="submit" name="add_test_result" class="btn btn-sm btn-success add-test-result">ذخیره</button>
							</div>
						</div>
						<?php
					} ?>
				</div>
			</div>
		</div>
		<?php
	}

}