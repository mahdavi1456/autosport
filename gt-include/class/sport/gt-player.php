<?php
//بازیکنان
class gt_player {
	
	public function add_player($array) {
		$db = new db();
		$gt_date = new gt_date();
		$p_name = $array['p_name'];
		$p_family = $array['p_family'];
		$p_father_name = $array['p_father_name'];
		$p_national_code = $array['p_national_code'];
		$p_post = $array['p_post'];
		$p_foot_type = $array['p_foot_type'];
		$p_category = 0;
		$p_ages = 0;
		$p_shirt_number = $array['p_shirt_number'];
		$date_birth = $array['date_birth'];
		$date_birth = $gt_date->jgdate($date_birth);
		$insurance_number = $array['insurance_number'];
		$expiration_insurance = $array['expiration_insurance'];
		$expiration_insurance = $gt_date->jgdate($expiration_insurance);
		$personnel_id = 1;
		$p_username = $array['p_username'];
		$p_password = $array['p_password'];
		$p_password = md5($p_password);
		$sql = "insert into player(p_username, p_password, p_shirt_number, p_national_code, p_name, p_family, p_father_name, p_post, p_foot_type, p_category, p_ages, date_birth, insurance_number, expiration_insurance, personnel_id) values('$p_username', '$p_password', $p_shirt_number, '$p_national_code', '$p_name', '$p_family', '$p_father_name', '$p_post', '$p_foot_type', $p_category, $p_ages, '$date_birth', '$insurance_number', '$expiration_insurance', '$personnel_id')";
		$last_id = $db->ex_query($sql);
		return $sql;
	}
	
	public function update_player($array) {
		$db = new db();
		$gt_date = new gt_date();
		$ID = $array['ID'];
		$p_name = $array['p_name'];
		$p_family = $array['p_family'];
		$p_father_name = $array['p_father_name'];
		$p_national_code = $array['p_national_code'];
		$p_post = $array['p_post'];
		$p_foot_type = $array['p_foot_type'];
		$p_shirt_number = $array['p_shirt_number'];
		$date_birth = $array['date_birth'];
		$date_birth = $gt_date->jgdate($date_birth);
		$insurance_number = $array['insurance_number'];
		$expiration_insurance = $array['expiration_insurance'];
		$expiration_insurance = $gt_date->jgdate($expiration_insurance);
		$p_username = $array['p_username'];
		$p_password = md5($p_password);
		$sql = "update player set p_username = '$p_username', p_name = '$p_name', p_family = '$p_family', p_father_name = '$p_father_name', p_national_code = '$p_national_code', p_post = '$p_post', p_foot_type = '$p_foot_type', p_shirt_number = $p_shirt_number, date_birth = '$date_birth', insurance_number = '$insurance_number', expiration_insurance = '$expiration_insurance' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_player($ID) {
		$db = new db();
		$sql = "delete from player where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_player_name($ID) {
		$db = new db();
		$p_list = $db->get_select_query("select p_name, p_family from player where ID = $ID");
		if(count($p_list) > 0) {
			return $p_list[0]['p_name'] . " " . $p_list[0]['p_family'];
		} else {
			return "نامعتبر";
		}
	}
	
	public function get_player_category_name($ID) {
		$db = new db();
		$p_category = $db->get_var_query("select p_category from player where ID = $ID");
		if($p_category == 0) {
			return "مقدماتی";
		} else if($p_category == 1) {
			return "میانی";
		} else if($p_category == 2) {
			return "پیشرفته";
		} else if($p_category == 3) {
			return "ویژه";
		} else if($p_category == 4) {
			return "بزرگسالان";
		}
	}
	
	public function get_player_ages($ID) {
		$db = new db();
		$p_ages = $db->get_var_query("select p_ages from player where ID = $ID");
		return "U" . $p_ages;
	}
	
	public function check_login_player($username, $password) {
		$db = new db();
		$pass = md5($password);
		$res = $db->get_select_query("select * from player where p_username = '$username' and p_password = '$pass'");
		return count($res);
	}
	
	public function get_player_id($username) {
		$db = new db();
		$ID = $db->get_var_query("select ID from player where p_username = '$username'");
		return $ID;
	}
	
	public function create_form($ID = null) { 
		$db = new db();
		$gt_date = new gt_date();
		if($ID == null) {
			$p_name = "";
			$p_family = "";
			$p_father_name = "";
			$p_national_code = "";
			$p_post = "";
			$p_foot_type = "";
			$p_category = "";
			$p_shirt_number = "";
			$date_birth = "";
			$insurance_number = "";
			$expiration_insurance = "";
			$personnel_id = "";
			$p_username = "";
			$p_password = "";
		} else {
			$asb = $db->get_select_query("select * from player where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$p_name = $roww['p_name'];
					$p_family = $roww['p_family'];
					$p_father_name = $roww['p_father_name'];
					$p_national_code = $roww['p_national_code'];
					$p_post = $roww['p_post'];
					$p_foot_type = $roww['p_foot_type'];
					$p_category = $roww['p_category'];
					$p_shirt_number = $roww['p_shirt_number'];
					$date_birth = $roww['date_birth'];
					if($date_birth != ""){
						$date_birth = $gt_date->jgdate($date_birth);
					}
					$insurance_number = $roww['insurance_number'];
					$expiration_insurance = $roww['expiration_insurance'];
					if($expiration_insurance != ""){
						$expiration_insurance = $gt_date->jgdate($expiration_insurance);
					}
					$personnel_id = $roww['personnel_id'];
					$p_username = $roww['p_username'];
					$p_password = $roww['p_password'];
				}
			 }
		} ?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-player-view">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>نام <span class="red">*</span></label>
								<input type="text" name="p_name" placeholder="نام" value="<?php echo $p_name; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نام خانوادگی <span class="red">*</span></label>
								<input type="text" name="p_family" placeholder="نام خانوادگی" value="<?php echo $p_family; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نام پدر <span class="red">*</span></label>
								<input type="text" name="p_father_name" placeholder="نام پدر" value="<?php echo $p_father_name; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>کد ملی <span class="red">*</span></label>
								<input type="text" name="p_national_code" placeholder="کد ملی" value="<?php echo $p_national_code; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>پست <span class="red">*</span></label>
								<select name="p_post" class="form-control select2">
									<option value="دروازه بان" <?php if($p_post == "دروازه بان"){ echo 'selected';} ?>>دروازه بان</option>
									<option value="دفاع وسط" <?php if($p_post == "دفاع وسط"){ echo 'selected';} ?>>دفاع وسط</option>
									<option value="دفاع چپ" <?php if($p_post == "دفاع چپ"){ echo 'selected';} ?>>دفاع چپ</option>
									<option value="دفاع راست" <?php if($p_post == "دفاع راست"){ echo 'selected';} ?>>دفاع راست</option>
									<option value="هافبک وسط" <?php if($p_post == "هافبک وسط"){ echo 'selected';} ?>>هافبک وسط</option>
									<option value="هافبک چپ" <?php if($p_post == "هافبک چپ"){ echo 'selected';} ?>>هافبک چپ</option>
									<option value="هافبک راست" <?php if($p_post == "هافبک راست"){ echo 'selected';} ?>>هافبک راست</option>
									<option value="هافبک دفاعی" <?php if($p_post == "هافبک دفاعی"){ echo 'selected';} ?>>هافبک دفاعی</option>
									<option value="مهاجم" <?php if($p_post == "مهاجم"){ echo 'selected';} ?>>مهاجم</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نوع پا <span class="red">*</span></label>
								<select name="p_foot_type" class="form-control select2">
									<option value="راست" <?php if($p_foot_type == "راست"){ echo 'selected';} ?>>راست پا</option>
									<option value="چپ" <?php if($p_foot_type == "چپ"){ echo 'selected';} ?>>چپ پا</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>شماره پیراهن <span class="red">*</span></label>
								<input type="text" name="p_shirt_number" placeholder="شماره پیراهن" value="<?php echo $p_shirt_number; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>تاریخ تولد <span class="red">*</span></label>
								<input type="text" name="date_birth" placeholder="تاریخ تولد" value="<?php echo $date_birth; ?>" class="form-control date-picker">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>شماره بیمه <span class="red">*</span></label>
								<input type="text" name="insurance_number" placeholder="شماره بیمه" value="<?php echo $insurance_number; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>تاریخ انقضای بیمه <span class="red">*</span></label>
								<input type="text" name="expiration_insurance" placeholder="تاریخ انقضای بیمه" value="<?php echo $expiration_insurance; ?>" class="form-control date-picker">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>نام کاربری <span class="red">*</span></label>
								<input type="text" name="p_username" placeholder="نام کاربری" value="<?php echo $p_username; ?>" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>رمز عبور <span class="red">*</span></label>
								<input type="text" name="p_password" placeholder="رمز عبور" class="form-control" <?php if($ID != null) { ?> disabled  value="******" <?php } ?>>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_player" class="btn btn-sm btn-success add-player">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_player" class="btn btn-sm btn-warning update-player">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-player-view-result"><?php $this->list_player_view(); ?></div>
	<?php
	}
	
	public function list_player_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام بازیکن</th>
							<th>نام پدر</th>
							<th>کد ملی</th>
							<th>پست</th>
							<th>نوع پا</th>
							<th>دوره سنی</th>
							<th>رده سنی</th>
							<th>شماره پیراهن</th>
							<th>شماره پرسنلی</th>
							<th>تاریخ تولد</th>
							<th>شماره بیمه</th>
							<th>تاریخ انقضای بیمه</th>
							<th>نام کاربری</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$db = new db();
						$gt_date = new gt_date();
						$pr = new prime();
						$list = $db->get_select_query("select * from player order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php
									$p_name = $this->get_player_name($l['ID']);
									echo $pr->per_number($p_name);
									?></td>
									<td><?php echo $pr->per_number($l['p_father_name']); ?></td>
									<td><?php echo $pr->per_number($l['p_national_code']); ?></td>
									<td><?php echo $pr->per_number($l['p_post']); ?></td>
									<td><?php echo $pr->per_number($l['p_foot_type']); ?></td>
									<td><?php echo $this->get_player_category_name($l['ID']) ?></td>
									<td><?php echo "U" . $pr->per_number($l['p_ages']); ?></td>
									<td><?php echo $pr->per_number($l['p_shirt_number']); ?></td>
									<td><?php echo $pr->per_number($l['personnel_id']); ?></td>
									<td><?php if($l['date_birth'] != ""){ $date_birth = $gt_date->jgdate($l['date_birth']); echo $pr->per_number($date_birth); } ?></td>
									<td><?php echo $pr->per_number($l['insurance_number']); ?></td>
									<td><?php if($l['expiration_insurance'] != ""){  $expiration_insurance = $gt_date->jgdate($l['expiration_insurance']); echo $pr->per_number($expiration_insurance); } ?></td>
									<td><?php echo $pr->per_number($l['p_username']); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-player-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<a href="profile_player?p_id=<?php echo $l['ID']; ?>" class="btn btn-primary btn-xs">ویرایش پروفایل</a>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-player">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="13">هیچ بازیکنی تاکنون تعریف نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
	public function change_password($ID, $new_password) {
		$db = new db();
		$sql = "update player set p_password = '$new_password' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function edit_profile($ID = null) {
		if($ID == null){
			if(isset($_GET['p_id'])){
				$ID = $_GET['p_id'];
			} else {
				$ID = $_SESSION['user_id'];
			}
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-player-view">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>نام کاربری جدید</label>
								<input type="text" name="new_username" placeholder="نام کاربری جدید" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>رمز جدید</label>
								<input type="text" name="new_pass" placeholder="رمز جدید" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="button" data-id="<?php echo $ID; ?>" class="btn btn-sm btn-success edit-player">ذخیره</button>
						</div>
					</div>
				</div>
			</div>
		</div><hr>
		<div class="row">
			<div class="col-md-12">	
				<div class="form-group">
					<label>تصویر پروفایل</label><br>	
					<form class="uploadForm" action="" method="post">
						<div id="targetLayer"><?php $this->get_profile($ID); ?></div>
						<div class="uploadFormLayer">
							<input name="userImage" type="file" class="inputFile" /><br/>
							<input type="submit" value="ذخیره تصویر" class="btnSubmit" />
							<input type="hidden" class="p_id" name="p_id" value="<?php echo $ID; ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
	}
		
	public function get_profile($p_id, $size = "small", $type = null) {
		$db = new db();
		$engine = new engine();
		$url = $engine->get_the_url();
		$filename = $db->get_var_query("select p_file from player where ID = $p_id");
		if($filename != ""){
			?>
			<a href="<?php echo $url . "/gt-content/uploads/profile/" . $filename; ?>" target="_blank">
				<img style="width: <?php if($size == "small") { echo '18%;'; } else { echo '100%;'; } ?> border-radius: 4px; padding: 1px; border: 2px solid #ddd;" src="<?php echo $url . "/gt-content/uploads/profile/" . $filename; ?>" class="upload-preview image-preview img-responsive">
			</a>
			<?php
			if($type == null){ ?>
				<button type="button" class="btn btn-danger btn-xs remove-file" data-pid="<?php echo $p_id; ?>">حذف</button>
				<?php
			}
		}
	}

}