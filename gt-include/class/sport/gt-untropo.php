<?php

class gt_untropo
{

    public function add_untropo($array)
    {
        $db = new db();
        $gt_date = new gt_date();
        $u_season = $array['u_season'];
        $u_date = $array['u_date'];
        $u_date = $gt_date->jgdate($u_date);
        $p_id = $array['p_id'];
        $u_height = $array['u_height'];
        $u_wieght = $array['u_wieght'];
        $u_bfp = $array['u_bfp'];
        $u_bmi = $array['u_bmi'];
        $u_somatotype = $array['u_somatotype'];
        $u_pascher = $array['u_pascher'];
        $u_column_dislocation = $array['u_column_dislocation'];
        $u_pelivs_dislocation = $array['u_pelivs_dislocation'];
        $u_knee_dislocation = $array['u_knee_dislocation'];
        $u_tatsus_dislocation = $array['u_tatsus_dislocation'];
        $u_talus_dislocation = $array['u_talus_dislocation'];
        $sql = "insert into untropo(u_season, u_date, u_bfp, u_bmi, u_somatotype, u_pascher, u_column_dislocation, u_pelivs_dislocation, u_knee_dislocation, u_tatsus_dislocation, u_talus_dislocation, p_id, u_height, u_wieght) ";
        $sql .= " values('$u_season', '$u_date', '$u_bfp', '$u_bmi', '$u_somatotype', '$u_pascher', '$u_column_dislocation', '$u_pelivs_dislocation', '$u_knee_dislocation', '$u_tatsus_dislocation', '$u_talus_dislocation', $p_id, $u_height, $u_wieght)";
        $last_id = $db->ex_query($sql);
        return $last_id;
    }

    public function update_untropo($array)
    {
        $db = new db();
        $gt_date = new gt_date();
        $ID = $array['ID'];
        $u_season = $array['u_season'];
        $u_date = $array['u_date'];
        $u_date = $gt_date->jgdate($u_date);
        $p_id = $array['p_id'];
        $u_height = $array['u_height'];
        $u_wieght = $array['u_wieght'];
        $u_bfp = $array['u_bfp'];
        $u_bmi = $array['u_bmi'];
        $u_somatotype = $array['u_somatotype'];
        $u_pascher = $array['u_pascher'];
        $u_column_dislocation = $array['u_column_dislocation'];
        $u_pelivs_dislocation = $array['u_pelivs_dislocation'];
        $u_knee_dislocation = $array['u_knee_dislocation'];
        $u_tatsus_dislocation = $array['u_tatsus_dislocation'];
        $u_talus_dislocation = $array['u_talus_dislocation'];
        $sql = "update untropo set u_season = '$u_season', u_date = '$u_date', p_id = $p_id, u_height = $u_height, u_wieght = $u_wieght, u_bfp = '$u_bfp', u_bmi = '$u_bmi', u_somatotype = '$u_somatotype', u_pascher = '$u_pascher', u_column_dislocation = '$u_column_dislocation', u_pelivs_dislocation = '$u_pelivs_dislocation', u_knee_dislocation = '$u_knee_dislocation', u_tatsus_dislocation = '$u_tatsus_dislocation', u_talus_dislocation = '$u_talus_dislocation' where ID = $ID";
        $db->ex_query($sql);
    }

    public function remove_untropo($ID)
    {
        $db = new db();
        $sql = "delete from untropo where ID = $ID";
        $db->ex_query($sql);
    }

    public function create_form($ID = null)
    {
        $db = new db();
        $gt_date = new gt_date();
        if ($ID == null) {
            $u_season = "";
            $u_date = "";
            $p_id = "";
            $u_height = "";
            $u_wieght = "";
            $u_bfp = "";
            $u_bmi = "";
            $u_somatotype = "";
            $u_pascher = "";
            $u_column_dislocation = "";
            $u_pelivs_dislocation = "";
            $u_knee_dislocation = "";
            $u_tatsus_dislocation = "";
            $u_talus_dislocation = "";
        } else {
            $asb = $db->get_select_query("select * from untropo where ID = $ID");
            if (count($asb) > 0) {
                foreach ($asb as $roww) {
                    $u_season = $roww['u_season'];
                    $u_date = $roww['u_date'];
                    $u_date = $gt_date->jgdate($u_date);
                    $p_id = $roww['p_id'];
                    $u_height = $roww['u_height'];
                    $u_wieght = $roww['u_wieght'];
                    $u_bfp = $roww['u_bfp'];
                    $u_bmi = $roww['u_bmi'];
                    $u_somatotype = $roww['u_somatotype'];
                    $u_pascher = $roww['u_pascher'];
                    $u_column_dislocation = $roww['u_column_dislocation'];
                    $u_pelivs_dislocation = $roww['u_pelivs_dislocation'];
                    $u_knee_dislocation = $roww['u_knee_dislocation'];
                    $u_tatsus_dislocation = $roww['u_tatsus_dislocation'];
                    $u_talus_dislocation = $roww['u_talus_dislocation'];
                }
            }
        } ?>
        <div id="add-untropo-view">
            <div class="row">
                <div class="col-md-12">
                    <h3>فصل</h3>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>فصل</label>
                        <select name="u_season" class="form-control">
                            <option value="بهار" <?php if ($u_season == "بهار") {
                                echo 'selected';
                            } ?> >بهار
                            </option>
                            <option value="تابستان" <?php if ($u_season == "تابستان") {
                                echo 'selected';
                            } ?> >تابستان
                            </option>
                            <option value="پاییز" <?php if ($u_season == "پاییز") {
                                echo 'selected';
                            } ?> >پاییز
                            </option>
                            <option value="زمستان" <?php if ($u_season == "زمستان") {
                                echo 'selected';
                            } ?> >زمستان
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>تاریخ</label>
                        <input type="text" name="u_date" value="<?php echo $u_date; ?>" placeholder="تاریخ"
                               class="date-picker form-control">
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>مشخصات متقاضی</h3>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>بازیکن</label>
                        <select name="p_id" class="form-control select2">
                            <?php
                            $db = new db();
                            $gt_player = new gt_player();
                            $res = $db->get_select_query("select * from player order by ID desc");
                            if (count($res) > 0) {
                                foreach ($res as $row) {
                                    ?>
                                    <option value="<?php echo $row["ID"]; ?>" <?php if ($row["ID"] == $p_id) {
                                        echo 'selected';
                                    } ?>><?php echo $gt_player->get_player_name($row["ID"]); ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>شاخص های پیکری</h3>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>طول قامت (cm)</label>
                        <input type="text" name="u_height" id="u-height" placeholder="طول قامت (cm)"
                               value="<?php echo $u_height; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>وزن (kg)</label>
                        <input type="text" name="u_wieght" id="u-wieght" placeholder="وزن (kg)"
                               value="<?php echo $u_wieght; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>درصد چربی (BFP) </label>
                        <input type="text" name="u_bfp" placeholder="درصد چربی (BFP)" value="<?php echo $u_bfp; ?>"
                               class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>شاخص توده بدنی (BMI)</label>
                        <input type="text" name="u_bmi" id="u-bmi" placeholder="شاخص توده بدنی (BMI)"
                               value="<?php echo $u_bmi; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>تیپ بدنی</label>
                        <select name="u_somatotype" class="form-control">
                            <option value="لاغر پیکر" <?php if ($u_somatotype == "لاغر پیکر") {
                                echo 'selected';
                            } ?> >لاغر پیکر
                            </option>
                            <option value="عضلانی پیکر" <?php if ($u_somatotype == "عضلانی پیکر") {
                                echo 'selected';
                            } ?> >عضلانی پیکر
                            </option>
                            <option value="فربه پیکر" <?php if ($u_somatotype == "فربه پیکر") {
                                echo 'selected';
                            } ?> >فربه پیکر
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>رده BMI</label>
                        <input type="text" name="group_bmi" id="group_bmi" placeholder="رده BMI" class="form-control" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>ناهجاری های اسکلتی عضلانی</h2>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>پاسچر</label>
                        <input type="text" name="u_pascher" placeholder="پاسچر" value="<?php echo $u_pascher; ?>"
                               class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>ستون مهره ای</label>
                        <input type="text" name="u_column_dislocation" placeholder="ستون مهره ای"
                               value="<?php echo $u_column_dislocation; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>وضعیت لگن</label>
                        <input type="text" name="u_pelivs_dislocation" placeholder="وضعیت لگن"
                               value="<?php echo $u_pelivs_dislocation; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>وضعیت زانو</label>
                        <input type="text" name="u_knee_dislocation" placeholder="وضعیت زانو"
                               value="<?php echo $u_knee_dislocation; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>وضعیت مچ پا</label>
                        <input type="text" name="u_talus_dislocation" placeholder="وضیت مچ پا"
                               value="<?php echo $u_talus_dislocation; ?>" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>وضعیت کف پا</label>
                        <input type="text" name="u_tatsus_dislocation" placeholder="وضیت کف پا"
                               value="<?php echo $u_tatsus_dislocation; ?>" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    if ($ID == null) {
                        ?>
                        <button type="button" class="btn btn-sm btn-success add-untropo">ذخیره</button>
                        <?php
                    } else {
                        ?>
                        <button type="button" class="btn btn-sm btn-warning update-untropo">ویرایش</button>
                        <input type="hidden" name="ID" value="<?php echo $ID; ?>">
                        <?php
                    } ?>
                </div>
            </div>
        </div>
        <div id="list-untropo-view-result"><?php $this->list_untropo_view(); ?></div>
        <?php
    }

    public function list_untropo_view()
    { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>نام بازیکن</th>
                            <th>مدیریت</th>
                        </tr>
                        <?php
                        $i = 1;
                        $db = new db();
                        $pr = new prime();
                        $gt_player = new gt_player();
                        $list = $db->get_select_query("select * from untropo order by ID desc");
                        if (count($list) > 0) {
                            foreach ($list as $l) { ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $gt_player->get_player_name($l['p_id']); ?></td>
                                    <td>
                                        <button class="btn btn-info btn-xs update-untropo-form"
                                                data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش
                                        </button>
                                        <button data-id="<?php echo $l['ID']; ?>"
                                                class="btn btn-danger btn-xs remove-untropo">حذف
                                        </button>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="9">تاکنون هیچ پیکرسنجی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

}