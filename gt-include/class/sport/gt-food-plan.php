<?php
class gt_food_plan {
	
	public function add_food_plan($array) {
		$db = new db();
		$p_id = $array['p_id'];
		$fp_type = $array['fp_type'];
		$fp_name = $array['fp_name'];
		$fp_date = $array['fp_date'];
		$fp_time = $array['fp_time'];
		$sql = "insert into food_plan(p_id, fp_type, fp_name, fp_date, fp_time) values($p_id, '$fp_type', '$fp_name', '$fp_date', '$fp_time')";
		$last_id = $db->ex_query($sql);
		return $sql;
	}
	
	public function update_food_plan($array) {
		$db = new db();
		$ID = $array['ID'];
		$p_id = $array['p_id'];
		$fp_type = $array['fp_type'];
		$fp_name = $array['fp_name'];
		$fp_date = $array['fp_date'];
		$fp_time = $array['fp_time'];
		$sql = "update food_plan set p_id = $p_id, fp_type = '$fp_type', fp_name = '$fp_name', fp_date = '$fp_date', fp_time = '$fp_time' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_food_plan($ID) {
		$db = new db();
		$sql = "delete from food_plan where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function create_form() {
		$db = new db(); ?>
		<div class="row">
			<div class="col-md-12">
				<div id="addfood-plan">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>بازیکن <span class="red">*</span></label>
								<select name="p_id" id="p-id" class="form-control select2">
									<?php
									$db = new db();
									$gt_player = new gt_player();
									$res = $db->get_select_query("select * from player");
									if(count($res) > 0){
										foreach($res as $row){
											?>
											<option value="<?php echo $row["ID"]; ?>"><?php echo $gt_player->get_player_name($row["ID"]); ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>از تاریخ <span class="red">*</span></label>
								<input type="text" name="f_date" id="mydate" placeholder="از تاریخ" value="<?php echo jdate("Y/m/d"); ?>" class="date-picker form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>به مدت <span class="red">*</span></label>
								<select id="day" name="day" class="form-control">
									<option value="7">7 روز</option>
									<option value="15">15 روز</option>
									<option value="21">21 روز</option>
									<option value="30">30 روز</option>
								</select>
							</div>
						</div></br>
						<div class="col-md-3">
							<button type="submit" class="btn btn-sm btn-success set-date">انتخاب</button>
						</div>
					</div></br>
					<div class="result">
					</div>
				</div>
			</div>
		</div>
	<?php
	}

}