<?php

class gt_coach
{

    public function add_coach($array)
    {
        $db = new db();
        $gt_date = new gt_date();
        $c_name = $array['c_name'];
        $c_family = $array['c_family'];
        $c_birth = $array['c_birth'];
        $c_birth = $gt_date->jgdate($c_birth);
        $c_level = $array['c_level'];
        $c_mobile = $array['c_mobile'];
        $c_address = $array['c_address'];
        $c_former_club = $array['c_former_club'];
        $c_username = $array['c_username'];
        $c_password = $array['c_password'];
        $sql = "insert into coach(c_username, c_password, c_name, c_family, c_birth, c_level, c_mobile, c_address, c_former_club,c_access_level) values('$c_username', '$c_password', '$c_name', '$c_family', '$c_birth', '$c_level', '$c_mobile', '$c_address', '$c_former_club','coach')";
        $last_id = $db->ex_query($sql);
        return $last_id;
    }

    public function update_coach($array)
    {
        $db = new db();
        $gt_date = new gt_date();
        $ID = $array['ID'];
        $c_name = $array['c_name'];
        $c_family = $array['c_family'];
        $c_birth = $array['c_birth'];
        $c_birth = $gt_date->jgdate($c_birth);
        $c_level = $array['c_level'];
        $c_mobile = $array['c_mobile'];
        $c_address = $array['c_address'];
        $c_former_club = $array['c_former_club'];
        $c_username = $array['c_username'];
        $sql = "update coach set c_username = '$c_username', c_name = '$c_name', c_family = '$c_family', c_birth = '$c_birth', c_level = '$c_level', c_mobile = '$c_mobile', c_address = '$c_address', c_former_club = '$c_former_club' where ID = $ID";
        $db->ex_query($sql);
    }

    public function remove_coach($ID)
    {
        $db = new db();
        $sql = "delete from coach where ID = $ID";
        $db->ex_query($sql);
    }

    public function get_coach_name($ID)
    {
        $db = new db();
        $c_list = $db->get_select_query("select c_name, c_family from coach where ID = $ID");
        if (count($c_list) > 0) {
            return $c_list[0]['c_name'] . " " . $c_list[0]['c_family'];
        } else {
            return "نامعتبر";
        }
    }

    public function check_login_coach($username, $password)
    {
        $db = new db();
        $pass = md5($password);
        $res = $db->get_select_query("select * from coach where c_username = '$username' and c_password = '$pass' and c_access_level = 'coach'");
        return count($res);
    }
	
	public function check_login_manager($username, $password)
    {
        $db = new db();
        $pass = md5($password);
        $res = $db->get_select_query("select * from coach where c_username = '$username' and c_password = '$pass' and c_access_level = 'manager'");
        return count($res);
    }

    public function get_coach_id($username)
    {
        $db = new db();
        $ID = $db->get_var_query("select ID from coach where c_username = '$username'");
        return $ID;
    }

    public function create_form($ID = null)
    {
        $db = new db();
        $gt_date = new gt_date();
        if ($ID == null) {
            $c_name = "";
            $c_family = "";
            $c_birth = "";
            $c_level = "";
            $c_mobile = "";
            $c_address = "";
            $c_former_club = "";
            $c_username = "";
        } else {
            $asb = $db->get_select_query("select * from coach where ID = $ID");
            if (count($asb) > 0) {
                foreach ($asb as $roww) {
                    $c_name = $roww['c_name'];
                    $c_family = $roww['c_family'];
                    $c_birth = $roww['c_birth'];
                    $c_birth = $gt_date->jgdate($c_birth);
                    $c_level = $roww['c_level'];
                    $c_mobile = $roww['c_mobile'];
                    $c_address = $roww['c_address'];
                    $c_former_club = $roww['c_former_club'];
                    $c_username = $roww['c_username'];
                }
            }
        }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div id="add-coach-view">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>نام <span class="red">*</span></label>
                                <input type="text" name="c_name" placeholder="نام" value="<?php echo $c_name; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>نام خانوادگی <span class="red">*</span></label>
                                <input type="text" name="c_family" placeholder="نام خانوادگی"
                                       value="<?php echo $c_family; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>تاریخ تولد <span class="red">*</span></label>
                                <input type="text" name="c_birth" placeholder="تاریخ تولد"
                                       value="<?php echo $c_birth; ?>" class="date-picker form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>سطح <span class="red">*</span></label>
                                <select name="c_level" class="form-control select2">
                                    <option value="aآسیا" <?php if ($c_level == "aآسیا") {
                                        echo 'selected';
                                    } ?>>aآسیا
                                    </option>
                                    <option value="bآسیا" <?php if ($c_level == "bآسیا") {
                                        echo 'selected';
                                    } ?>>bآسیا
                                    </option>
                                    <option value="cآسیا" <?php if ($c_level == "cآسیا") {
                                        echo 'selected';
                                    } ?>>cآسیا
                                    </option>
                                    <option value="aملی" <?php if ($c_level == "aملی") {
                                        echo 'selected';
                                    } ?>>aملی
                                    </option>
                                    <option value="bملی" <?php if ($c_level == "bملی") {
                                        echo 'selected';
                                    } ?>>bملی
                                    </option>
                                    <option value="cملی" <?php if ($c_level == "cملی") {
                                        echo 'selected';
                                    } ?>>cملی
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>موبایل <span class="red">*</span></label>
                                <input type="text" name="c_mobile" placeholder="موبایل" value="<?php echo $c_mobile; ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>باشگاه سابق</label>
                                <input type="text" name="c_former_club" placeholder="باشگاه سابق"
                                       value="<?php echo $c_former_club; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>نام کاربری <span class="red">*</span></label>
                                <input type="text" name="c_username" placeholder="نام کاربری"
                                       value="<?php echo $c_username; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>رمز عبور <span class="red">*</span></label>
                                <input type="text" name="c_password" placeholder="رمز عبور"
                                       class="form-control" <?php if ($ID != null) { ?> disabled  value="******" <?php } ?>>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>آدرس <span class="red">*</span></label>
                                <input type="text" name="c_address" placeholder="آدرس" value="<?php echo $c_address; ?>"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            if ($ID == null) {
                                ?>
                                <button type="button" name="add_coach" class="btn btn-sm btn-success add-coach">ذخیره
                                </button>
                                <?php
                            } else {
                                ?>
                                <button type="button" name="update_coach" class="btn btn-sm btn-warning update-coach">
                                    ویرایش
                                </button>
                                <input type="hidden" name="ID" value="<?php echo $ID; ?>">
                                <?php
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="list-coach-view-result"><?php $this->list_coach_view(); ?></div>
        <?php
    }

    public function list_coach_view()
    { ?>
        <div class="row result">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>نام مربی</th>
                            <th>تاریخ تولد</th>
                            <th>سطح</th>
                            <th>باشگاه سابق</th>
                            <th>موبایل</th>
                            <th>آدرس</th>
                            <th>نام کاربری</th>
                            <th>مدیریت</th>
                        </tr>
                        <?php
                        $i = 1;
                        $db = new db();
                        $pr = new prime();
                        $gt_date = new gt_date();
                        $list = $db->get_select_query("select * from coach order by ID desc");
                        if (count($list) > 0) {
                            foreach ($list as $l) { ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php
                                        $c_name = $this->get_coach_name($l['ID']);
                                        echo $pr->per_number($c_name);
                                        ?></td>
                                    <td><?php $c_birth = $gt_date->jgdate($l['c_birth']);
                                        echo $pr->per_number($c_birth); ?></td>
                                    <td><?php echo $pr->per_number($l['c_level']); ?></td>
                                    <td><?php echo $pr->per_number($l['c_former_club']); ?></td>
                                    <td><?php echo $pr->per_number($l['c_mobile']); ?></td>
                                    <td><?php echo $pr->per_number($l['c_address']); ?></td>
                                    <td><?php echo $pr->per_number($l['c_username']); ?></td>
                                    <td>
                                        <button class="btn btn-info btn-xs update-coach-form"
                                                data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش
                                        </button>
                                        <button data-id="<?php echo $l['ID']; ?>"
                                                class="btn btn-danger btn-xs remove-coach">حذف
                                        </button>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="8">هیچ مربی تاکنون ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_player_list_view()
    {
        $gt_player = new gt_player();
        $db = new db();
        ?>
        <div class="row">
            <form method="get" action="">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>بازیکن</label>
                        <input type="text" name="name" placeholder="نام بازیکن یا کد ملی"
                               value="<?php if (isset($_GET['name'])) {
                                   echo $_GET['name'];
                               } ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-sm btn-success">جستجو</button>
                </div>
            </form>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>بازیکن</th>
							<th>رده سنی</th>
                            <th>شماره پیراهن</th>
                            <th>عملیات</th>
                        </tr>
                        <?php
                        $i = 1;
                        $pr = new prime();
                        $c_id = $_SESSION['user_id'];
                        $name = $_GET['name'];
                        $level = $_SESSION["level"];

                        if ($name == "") {
                            if ($level == 'manager') {
                                $list = $db->get_select_query("select *, ID as p_id from player order by ID desc");
                            } else {
                                $list = $db->get_select_query("select p_id from sport_register where c_id = $c_id order by ID desc");
                            }
                        } else {
                            if ($level == 'manager') {
                                $list = $db->get_select_query("select *, ID as p_id from player where (p_name LIKE '%$name%' or p_family LIKE '%$name%' or p_national_code LIKE '%$name%') order by ID desc");
                            } else {
                                $list = $db->get_select_query("select sport_register.p_id from sport_register inner join player on sport_register.p_id = player.ID where sport_register.c_id = $c_id and (player.p_name LIKE '%$name%' or player.p_family LIKE '%$name%' or player.p_national_code LIKE '%$name%') order by sport_register.ID desc");
                            }
                        }
                        if (count($list) > 0) {
                            foreach ($list as $row) {

                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <?php
                                    if ($level == 'manager') { ?>
                                        <td><?php
                                            $player_name = $gt_player->get_player_name($row['ID']);
                                            echo $pr->per_number($player_name); ?>
                                        </td>
										<td><?php
                                            $player_ages = $gt_player->get_player_ages($row['ID']);
                                            echo $pr->per_number($player_ages); ?>
                                        </td>
                                        <?php
                                    } else { ?>
                                        <td><?php
                                            $player_name = $gt_player->get_player_name($row['p_id']);
                                            echo $pr->per_number($player_name); ?>
                                        </td>
										<td><?php
                                            $player_ages = $gt_player->get_player_ages($row['p_id']);
                                            echo $pr->per_number($player_ages); ?>
                                        </td>
                                        <?php
                                    } ?>
                                    <td><?php echo $pr->per_number($row['p_shirt_number']); ?></td>
                                    <td>
                                        <a href="station_list?p_id=<?php echo $row['p_id']; ?>"
                                           class="btn btn-primary btn-xs">مشاهده ایستگاه ها</a>
                                        <a href="screening_list?p_id=<?php echo $row['p_id']; ?>"
                                           class="btn btn-warning btn-xs">مشاهده امتیازات غربالگری</a>
                                        <a href="untropo_result?p_id=<?php echo $row['p_id']; ?>"
                                           class="btn btn-success btn-xs">مشاهده نتایج پیکرسنجی</a>
                                        <a href="food_list?p_id=<?php echo $row['p_id']; ?>"
                                           class="btn btn-danger btn-xs">مشاهده برنامه غذایی</a>
										<a href="rollcall_list?p_id=<?php echo $row['p_id']; ?>"
                                           class="btn btn-info btn-xs">گزارش حضور و غیاب</a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="6">تا کنون هیچ بازیکنی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_coach_list_view()
    {
        $db = new db();
        ?>
        <div class="row">
            <form method="get" action="">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>مربی</label>
                        <input type="text" name="name" placeholder="نام مربی" value="<?php if (isset($_GET['name'])) {
                            echo $_GET['name'];
                        } ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-sm btn-success">جستجو</button>
                </div>
            </form>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>مربی</th>
                            <th>عملیات</th>
                        </tr>
                        <?php
                        $i = 1;
                        $pr = new prime();
                        $c_id = $_SESSION['user_id'];
                        $name = $_GET['name'];
                        if ($name == "") {
                            $list = $db->get_select_query("select ID from coach order by ID desc");
                        } else {
                            $list = $db->get_select_query("select ID from coach where (c_name LIKE '%$name%' or c_family LIKE '%$name%') order by ID desc");
                        }
                        if (count($list) > 0) {
                            foreach ($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php
                                        $coach_name = $this->get_coach_name($row['ID']);
                                        echo $pr->per_number($coach_name); ?>
                                    </td>
                                    <td>
                                        <a href="list_players_coach?ID=<?php echo $row['ID']; ?>"
                                           class="btn btn-primary btn-xs">مشاهده بازیکنان</a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="6">تا کنون هیچ مربی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_players_coach_view()
    {
        $gt_player = new gt_player();
        $db = new db();
        $c_id = $_GET['ID'];
        ?>
        <div class="row">
            <form method="get" action="">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>بازیکن</label>
                        <input type="hidden" name="ID" value="<?php echo $c_id; ?>">
                        <input type="text" name="name" placeholder="نام بازیکن یا کد ملی"
                               value="<?php if (isset($_GET['name'])) {
                                   echo $_GET['name'];
                               } ?>" class="form-control">
                    </div>
                </div>
                <br>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-sm btn-success">جستجو</button>
                </div>
            </form>
            <div class="col-md-12">
                <div class="table-responsive" id="printarea">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>بازیکن</th>
							<th>رده سنی</th>
                        </tr>
                        <?php
                        $i = 1;
                        $pr = new prime();
                        $name = $_GET['name'];
                        if ($name == "") {
                            $list = $db->get_select_query("select p_id from sport_register where c_id = $c_id order by ID desc");
                        } else {
                            $list = $db->get_select_query("select sport_register.p_id from sport_register inner join player on sport_register.p_id = player.ID where sport_register.c_id = $c_id and (player.p_name LIKE '%$name%' or player.p_family LIKE '%$name%' or player.p_national_code LIKE '%$name%') order by sport_register.ID desc");
                        }
                        if (count($list) > 0) {
                            foreach ($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php
                                        $player_name = $gt_player->get_player_name($row['p_id']);
                                        echo $pr->per_number($player_name); ?>
                                    </td>
									 <td><?php
                                        $player_ages = $gt_player->get_player_ages($row['p_id']);
                                        echo $pr->per_number($player_ages); ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="6">تا کنون هیچ بازیکنی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12 no-print">
                        <input type="button" value="چاپ" onclick="printFunc();" class="btn btn-sm btn-default">
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_station_list_view()
    { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>ایستگاه</th>
                            <th>مدیریت</th>
                        </tr>
                        <?php
                        $i = 1;
                        $db = new db();
                        $pr = new prime();
                        $gt_date = new gt_date();
                        $gt_player = new gt_player();
                        $gt_station = new gt_station();
                        /*if($_SESSION['level'] == "teacher") {
                            $p_id = $_GET['p_id'];
                        } else {
                            $p_id = $_SESSION['user_id'];
                        } */
                        $p_id = $_GET['p_id'];
                        $sql = "select * from test_result where p_id = $p_id group by s_id";
                        $station = $db->get_select_query($sql);
                        if (count($station) > 0) {
                            foreach ($station as $s) {
                                $s_id = $s['s_id'];
                                $s_name = $gt_station->get_station_name($s_id);
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $pr->per_number($s_name); ?></td>
                                    <td>
                                        <a href="score_list?s_id=<?php echo $s_id; ?>&p_id=<?php echo $p_id; ?>"
                                           class="btn btn-primary btn-xs">مشاهده امتیازات</a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9" class="text-center">تا کنون هیج ایستگاهی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_food_list_view()
    { ?>
        <div class="row">
            <div class="col-md-12">
                <div id="addfood-plan">
                    <div class="row">
                        <input type="hidden" name="p_id" id="p-id" value="<?php echo $_GET['p_id']; ?>">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>از تاریخ <span class="red">*</span></label>
                                <input type="text" name="date" id="mydate" placeholder="از تاریخ"
                                       value="<?php echo jdate("Y/m/d"); ?>" class="date-picker form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>به مدت <span class="red">*</span></label>
                                <select id="day" name="day" class="form-control">
                                    <option value="7">7 روز</option>
                                    <option value="14">14 روز</option>
                                    <option value="21">21 روز</option>
                                    <option value="28">28 روز</option>
                                </select>
                            </div>
                        </div>
                        </br>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-sm btn-success view-food-plan">نمایش</button>
                        </div>
                    </div>
                    </br>
                    <div class="result" id="printarea">
                    </div>
                    <div class="row">
                        <div class="col-md-12 no-print">
                            <input type="button" value="چاپ" onclick="printFunc();" class="btn btn-sm btn-default">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_score_list_view()
    { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>نام ایستگاه</th>
                            <th>تاریخ</th>
                            <th>نوبت آزمون</th>
                            <th>مجموع نمرات</th>
                        </tr>
                        <?php
                        $i = 1;
                        $db = new db();
                        $pr = new prime();
                        $gt_date = new gt_date();
                        $gt_station = new gt_station();
                        $s_id = $_GET['s_id'];
                        $p_id = $_GET['p_id'];
                        $sql = "select * from test_result where s_id = $s_id and p_id = $p_id";
                        $station = $db->get_select_query($sql);
                        if (count($station) > 0) {
                            foreach ($station as $s) {
                                $s1 = $s['tr_score1'];
                                $s2 = $s['tr_score2'];
                                $s3 = $s['tr_score3'];
                                $s4 = $s['tr_score4'];
                                $s5 = $s['tr_score5'];
                                $score = $s1 + $s2 + $s3 + $s4 + $s5;
                                $s_name = $gt_station->get_station_name($s_id);
                                //$score = $db->get_select_query("select sum(tr_score1, tr_score2, tr_score3, tr_score4, tr_score5) from test_result where s_id = $s_id");
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php echo $pr->per_number($s_name); ?></td>
                                    <td><?php $tr_date = $gt_date->jgdate($s['tr_date']);
                                        echo $pr->per_number($tr_date); ?></td>
                                    <td><?php echo $pr->per_number($s['tr_turn']); ?></td>
                                    <td><?php echo $pr->per_number($score); ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9" class="text-center">تا کنون هیچ امتیازی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_screening_list_view()
    { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>تاریخ</th>
                            <th>مجموع امتیازات</th>
                        </tr>
                        <?php
                        $i = 1;
                        $db = new db();
                        $pr = new prime();
                        $gt_date = new gt_date();
                        $p_id = $_GET['p_id'];
                        $sql = "select *, sum(score) as sumscore from screening where p_id = $p_id group by set_date";
                        $screening = $db->get_select_query($sql);
                        if (count($screening) > 0) {
                            foreach ($screening as $s) {
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php $set_date = $gt_date->jgdate($s['set_date']);
                                        echo $pr->per_number($set_date); ?></td>
                                    <td><?php echo $pr->per_number($s['sumscore']); ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9" class="text-center">تا کنون هیج امتیازی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function list_untropo_result()
    { ?>
        <div id="add-untropo-view">
            <form action="" method="get">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>فصل</label>
                            <select name="u_season" class="form-control">
                                <option value="بهار" <?php if (isset($_GET['u_season'])) {
                                    if ($_GET['u_season'] == "بهار") {
                                        echo 'selected';
                                    }
                                } ?> >بهار
                                </option>
                                <option value="تابستان" <?php if (isset($_GET['u_season'])) {
                                    if ($_GET['u_season'] == "تابستان") {
                                        echo 'selected';
                                    }
                                } ?> >تابستان
                                </option>
                                <option value="پاییز" <?php if (isset($_GET['u_season'])) {
                                    if ($_GET['u_season'] == "پاییز") {
                                        echo 'selected';
                                    }
                                } ?> >پاییز
                                </option>
                                <option value="زمستان" <?php if (isset($_GET['u_season'])) {
                                    if ($_GET['u_season'] == "زمستان") {
                                        echo 'selected';
                                    }
                                } ?> >زمستان
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>تاریخ</label>
                            <input type="text" name="u_date" value="<?php if (isset($_GET['u_season'])) {
                                echo $_GET['u_date'];
                            } ?>" placeholder="تاریخ" class="date-picker form-control">
                        </div>
                    </div>
                    <br>
                    <div class="col-md-4">
                        <input type="hidden" value="<?php echo $_GET['p_id']; ?>" name="p_id">
                        <button type="submit" class="btn btn-sm btn-success">مشاهده</button>
                    </div>
                </div>
            </form>
            <?php
            if (isset($_GET['u_season'])) {
                $db = new db();
                $pr = new prime();
                $gt_date = new gt_date();
                $p_id = $_GET['p_id'];
                $u_date = $_GET['u_date'];
                $u_date = $gt_date->jgdate($u_date);
                $u_season = $_GET['u_season'];
                $sql = "select * from untropo where p_id = $p_id and u_date = '$u_date' and u_season = '$u_season'";
                $untropo = $db->get_select_query($sql);
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>شاخص های پیکری</h3>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>طول قامت (cm): <?php echo $pr->per_number($untropo[0]['u_height']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>وزن (kg): <?php echo $pr->per_number($untropo[0]['u_wieght']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>درصد چربی (BFP): <?php echo $pr->per_number($untropo[0]['u_bfp']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>شاخص توده بدنی (BMI): <?php echo $pr->per_number($untropo[0]['u_bmi']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>تیپ بدنی: <?php echo $pr->per_number($untropo[0]['u_somatotype']); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>ناهجاری های اسکلتی عضلانی</h2>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>پاسچر: <?php echo $pr->per_number($untropo[0]['u_pascher']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>ستون مهره ای: <?php echo $pr->per_number($untropo[0]['u_column_dislocation']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>وضعیت لگن: <?php echo $pr->per_number($untropo[0]['u_pelivs_dislocation']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>وضعیت زانو: <?php echo $pr->per_number($untropo[0]['u_knee_dislocation']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>وضعیت مچ پا: <?php echo $pr->per_number($untropo[0]['u_talus_dislocation']); ?></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <p>وضعیت کف پا: <?php echo $pr->per_number($untropo[0]['u_tatsus_dislocation']); ?></p>
                        </div>
                    </div>
                </div>
                <?php
            } ?>
        </div>
        <?php
    }
	
	public function rollcall_list_view()
    {
        $db = new db();
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tr>
                            <th>ردیف</th>
                            <th>تاریخ</th>
                            <th>وضعیت</th>
							<th>توضیحات</th>
                        </tr>
                        <?php
                        $i = 1;
                        $pr = new prime();
						$gt_date = new gt_date();$c_birth = $gt_date->jgdate($c_birth);
                        $p_id = $_GET['p_id'];
                        $list = $db->get_select_query("select * from rollcall where p_id = $p_id order by ID asc");
                        if (count($list) > 0) {
                            foreach ($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $pr->per_number($i); ?></td>
                                    <td><?php $rc_date = $gt_date->jgdate($row['rc_date']); echo $pr->per_number($rc_date);?></td>
									<td><?php if($row['rc_val'] == 1){ ?> <i class="icofont-verification-check"></i> <?php } else { ?> <i class="icofont-close"></i><?php } ?></td>
									<td><?php echo $pr->per_number($row['rc_details']); ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="6">تا کنون هیچ حضور غیابی ثبت نشده است</td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

    public function edit_profile_coach($ID = null)
    {
        if ($ID == null) {
            $ID = $_SESSION['user_id'];
        }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div id="add-coach-view">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>نام کاربری جدید</label>
                                <input type="text" name="new_username" placeholder="نام کاربری جدید"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>رمز جدید</label>
                                <input type="text" name="new_pass" placeholder="رمز جدید" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" data-id="<?php echo $ID; ?>"
                                    class="btn btn-sm btn-success edit-coach">ذخیره
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>تصویر پروفایل</label><br>
                    <form class="uploadForm" action="" method="post">
                        <div id="targetLayer"><?php $this->get_profile($ID); ?></div>
                        <div class="uploadFormLayer">
                            <input name="userImage" type="file" class="inputFile"/><br/>
                            <input type="submit" value="ذخیره تصویر" class="btnSubmit"/>
                            <input type="hidden" class="c_id" name="c_id" value="<?php echo $ID; ?>">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
    }

    public function get_profile($c_id, $size = "small", $type = null)
    {
        $db = new db();
        $engine = new engine();
        $url = $engine->get_the_url();
        $filename = $db->get_var_query("select c_file from coach where ID = $c_id");
        if ($filename != "") {
            ?>
            <a href="<?php echo $url . "/gt-content/uploads/profile/" . $filename; ?>" target="_blank">
                <img style="width: <?php if ($size == "small") {
                    echo '18%;';
                } else {
                    echo '100%;';
                } ?> border-radius: 4px; padding: 1px; border: 2px solid #ddd;"
                     src="<?php echo $url . "/gt-content/uploads/profile/" . $filename; ?>"
                     class="upload-preview image-preview img-responsive">
            </a>
            <?php
            if ($type == null) { ?>
                <button type="button" class="btn btn-danger btn-xs remove-file" data-cid="<?php echo $c_id; ?>">حذف
                </button>
                <?php
            }
        }
    }

}