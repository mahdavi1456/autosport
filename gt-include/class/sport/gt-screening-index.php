 <?php
class gt_screening_index {
	
	public function add_screening_index($array) {
		$db = new db();
		$si_name = $array['si_name'];
		$si_coefficient = $array['si_coefficient'];
		$ages_id = $array['ages_id'];
		$sql = "insert into screening_index(si_name,si_coefficient, ages_id) values('$si_name','$si_coefficient', $ages_id)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_screening_index($array) {
		$db = new db();
		$ID = $array['ID'];
		$si_name = $array['si_name'];
		$si_coefficient = $array['si_coefficient'];
		$ages_id = $array['ages_id'];
		$sql = "update screening_index set si_name = '$si_name', ages_id = '$ages_id', si_coefficient = '$si_coefficient' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_screening_index($ID) {
		$db = new db();
		$sql = "delete from screening_index where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_screening_index_name($ID) {
		$db = new db();
		$si_name = $db->get_var_query("select si_name from screening_index where ID = $ID");
		return $si_name;
	}
	public function get_screening_index_coefficient($ID) {
		$db = new db();
		$si_coefficient = $db->get_var_query("select si_coefficient from screening_index where ID = $ID");
		return $si_coefficient;
	}
	
	public function get_screening_index_ages($ID) {
		$db = new db();
		$si_name = $db->get_var_query("select si_name from screening_index where ID = $ID");
		$ages_id = $db->get_var_query("select ages_id from screening_index where ID = $ID");
		return $si_name . " U" . $ages_id;
	}

	public function create_form($ID = null) {
		$db = new db();
		if($ID == null) {
			$si_name = "";
            $si_coefficient = "";
			$ages_id = "";
		} else {
			$asb = $db->get_select_query("select * from screening_index where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$si_name = $roww['si_name'];
					$si_coefficient = $roww['si_coefficient'];
					$ages_id = $roww['ages_id'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-screening-index-view">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>نام شاخص <span class="red">*</span></label>
								<input type="text" name="si_name" placeholder="نام شاخص" value="<?php echo $si_name; ?>" class="form-control" required>
							</div>
						</div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>ضریب شاخص <span class="red">*</span></label>
                                <input type="number" name="si_coefficient" placeholder="ضریب شاخص" value="<?php echo $si_coefficient; ?>" class="form-control" required>
                            </div>
                        </div>
						<div class="col-md-2">
							<div class="form-group">
								<label>رده سنی <span class="red">*</span></label>
								<select name="ages_id" class="form-control select2">
									<option value="7" <?php if($ages_id == 7){ echo 'selected'; } ?>>U7</option>
									<option value="8" <?php if($ages_id == 8){ echo 'selected'; } ?>>U8</option>
									<option value="9" <?php if($ages_id == 9){ echo 'selected'; } ?>>U9</option>
									<option value="10" <?php if($ages_id == 10){ echo 'selected'; } ?>>U10</option>
									<option value="11" <?php if($ages_id == 11){ echo 'selected'; } ?>>U11</option>
									<option value="12" <?php if($ages_id == 12){ echo 'selected'; } ?>>U12</option>
									<option value="13" <?php if($ages_id == 13){ echo 'selected'; } ?>>U13</option>
									<option value="14" <?php if($ages_id == 14){ echo 'selected'; } ?>>U14</option>
									<option value="15" <?php if($ages_id == 15){ echo 'selected'; } ?>>U15</option>
									<option value="16" <?php if($ages_id == 16){ echo 'selected'; } ?>>U16</option>
									<option value="19" <?php if($ages_id == 19){ echo 'selected'; } ?>>U19</option>
									<option value="21" <?php if($ages_id == 21){ echo 'selected'; } ?>>U21</option>
								</select>
							</div>
						</div>
						<br>
						<div class="col-md-2">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_screening_index" class="btn btn-sm btn-success add-screening-index">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_screening_index" class="btn btn-sm btn-warning update-screening-index">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-screening-index-view-result"><?php $this->list_screening_index_view(); ?></div>
		<?php
	}
	
	public function list_screening_index_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام شاخص</th>
							<th>ضریب شاخص</th>
							<th>رده سنی</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$db = new db();
						$pr = new prime();
						$list = $db->get_select_query("select * from screening_index order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['si_name']); ?></td>
									<td><?php echo $pr->per_number($l['si_coefficient']); ?></td>
									<td><?php echo "U" . $pr->per_number($l['ages_id']); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-screening-index-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-screening-index">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="5">هیچ شاخصی تاکنون تعریف نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
}