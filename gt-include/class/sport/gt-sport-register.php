<?php
//ثبت نام
class gt_sport_register {
	
	public function add_sport_register($array) {
		$db = new db();
		$c_id = $array['c_id'];
		$p_id = $array['p_id'];
		$sql = "insert into sport_register(c_id, p_id) values($c_id, $p_id)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_sport_register($array) {
		$db = new db();
		$c_id = $array['c_id'];
		$p_id = $array['p_id'];
		$sql = "update sport_register set c_id = $c_id, p_id = $p_id where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_sport_register($ID) {
		$db = new db();
		$sql = "delete from sport_register where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function create_form($ID = null) {
		$gt_coach = new gt_coach();
		$gt_player = new gt_player();
		$db = new db();
		if($ID == null) {
			$c_id = "";
			$p_id = "";
		} else {
			$asb = $db->get_select_query("select * from sport_register where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$c_id = $roww['c_id'];
					$p_id = $roww['p_id']; 
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-sport-register-view">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>بازیکن <span class="red">*</span></label>
								<select name="p_id" class="form-control select2">
									<?php		
									$player = $db->get_select_query("select * from player order by ID desc");
									if(count ($player > 0)) {
										foreach($player as $row) {
										?>
										<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $p_id) { echo 'selected';} ?>><?php echo $gt_player->get_player_name($row["ID"]); ?></option>
										<?php
										}
									} ?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>مربی <span class="red">*</span></label>
								<select name="c_id" class="form-control select2">
									<?php
									$coach = $db->get_select_query("select * from coach order by ID desc");
									if(count($coach > 0)) {
										foreach($coach as $row) {
											?>
											<option value="<?php echo $row['ID']; ?>" <?php if($row['ID'] == $c_id) { echo 'selected';} ?>><?php echo $gt_coach->get_coach_name($row["ID"]); ?></option>
											<?php
										}
									} ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_sport_register" class="btn btn-sm btn-success add-sport-register">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_sport_register" class="btn btn-sm btn-warning update-sport-register">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-sport-register-view-result"><?php $this->list_sport_register_view(); ?></div>
		<?php
	}
		
	public function list_sport_register_view() {
		$db = new db();
		$gt_player = new gt_player();
		$gt_coach = new gt_coach();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>بازیکن</th>
							<th>مربی</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$pr = new prime();
						$list = $db->get_select_query("select * from sport_register order by ID desc");
						if(count($list) > 0) {
							foreach($list as $l) {
								$p_name = $gt_player->get_player_name($l['p_id']);
								?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($p_name); ?></td>
									<td><?php
										$c_id = $l['c_id'];
										$coach_name = $gt_coach->get_coach_name($c_id);
										echo $pr->per_number($coach_name);
										?>
									</td>
									<td>
										<button class="btn btn-info btn-xs update-sport-register-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-sport-register">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						} else { ?>
							<tr>
								<td colspan="8">تا کنون هیچ ثبت نامی انجام نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}

}