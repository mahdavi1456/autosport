<?php
class gt_report
{
    public function create_form($p_id = null, $test_number = null,$stations_score_array = null){ ?>
        <div class="row">
            <div class="col-md-12">
                <div id="player-report-view">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>نام بازیکن / کد ملی</label>
                                <select name="p_id" class="form-control select2">
                                    <?php
                                    $db = new db();
                                    $pr = new prime();
                                    $gt_player = new gt_player();
                                    $res = $db->get_select_query("select * from player");
                                    if (count($res) > 0) {
                                        foreach ($res as $row) {
                                            ?>
                                            <option value="<?php echo $row["ID"]; ?>" <?php if ($row["ID"] == $p_id) {
                                                echo 'selected';
                                            } ?>><?php echo $gt_player->get_player_name($row['ID']) . '#' . $row['p_national_code']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>شماره آزمون</label>
                                <select name="test_number" class="form-control">
                                    <option value="1"<?php if ($test_number == 1) echo 'selected'?>>آزمون اول</option>
                                    <option value="2"<?php if ($test_number == 2) echo 'selected'?>>آزمون دوم</option>
                                    <option value="3"<?php if ($test_number == 3) echo 'selected'?>>آزمون سوم</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-sm btn-success view-player-report">مشاهده</button>
                            </div>
                        </div>
                        <?php if ($p_id != null && $test_number != null) { ?>
                            <div class="row">
                                <?php
                                $s_name = array("ایستگاه اول", "ایستگاه دوم", "ایستگاه سوم", "ایستگاه چهارم")
                                ?>
                                <script>
                                    var ctx = document.getElementById('myChart').getContext('2d');
                                    var myChart = new Chart(ctx, {
                                        type: 'bar',
                                        data: {
                                            labels:<?php echo json_encode($s_name, JSON_NUMERIC_CHECK); ?>,
                                            datasets: [{
                                                label: 'امتیاز کسب شده در هر ایستکاه در مرحله انتخابی',
                                                data: [10,0,8,14],
                                                backgroundColor: [
                                                    'rgba(255, 99, 132, 0.2)',
                                                    'rgba(54, 162, 235, 0.2)',
                                                    'rgba(255, 206, 86, 0.2)',
                                                    'rgba(75, 192, 192, 0.2)'
                                                ],
                                                borderColor: [
                                                    'rgba(255, 99, 132, 1)',
                                                    'rgba(54, 162, 235, 1)',
                                                    'rgba(255, 206, 86, 1)',
                                                    'rgba(75, 192, 192, 1)'
                                                ],
                                                borderWidth: 1
                                            }]
                                        },
                                        options: {
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true
                                                    }
                                                }]
                                            }
                                        }
                                    });

                                    var ctx = document.getElementById('myChart2').getContext('2d');
                                    var myChart = new Chart(ctx, {
                                        type: 'bar',
                                        data: {
                                            labels:<?php echo json_encode($s_name, JSON_NUMERIC_CHECK); ?>,
                                            datasets: [{
                                                label: 'میانگین امتیاز کسب شده در هر ایستگاه در تمامی مراحل',
                                                data: [1,0,6,14],
                                                backgroundColor: [
                                                    'rgba(255, 99, 132, 0.2)',
                                                    'rgba(54, 162, 235, 0.2)',
                                                    'rgba(255, 206, 86, 0.2)',
                                                    'rgba(75, 192, 192, 0.2)'
                                                ],
                                                borderColor: [
                                                    'rgba(255, 99, 132, 1)',
                                                    'rgba(54, 162, 235, 1)',
                                                    'rgba(255, 206, 86, 1)',
                                                    'rgba(75, 192, 192, 1)'
                                                ],
                                                borderWidth: 1
                                            }]
                                        },
                                        options: {
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true
                                                    }
                                                }]
                                            }
                                        }
                                    });


                                </script>
                                <div class="col-md-6">
                                    <canvas id="myChart"></canvas>
                                </div>
                                <div class="col-md-6">
                                    <canvas id="myChart2"></canvas>
                                </div>
                            </div>
                            <div class="row">
                                <div style="padding: 25px;" class="col-md-12">
                                    <h4>گزارش جدولی (ریز جدول)</h4>
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tr>
                                                <th>ایستگاه</th>
                                                <th>آزمون1</th>
                                                <th>آزمون2</th>
                                                <th>آزمون3</th>
                                                <th>آزمون4</th>
                                                <th>آزمون5</th>
                                                <th>میانگین</th>
                                                <th>ثبات</th>
                                                <th>ناظر</th>
                                            </tr>
                                            <?php
                                            $j = 0;
                                            $i = 1;
                                            $db = new db();
                                            $pr = new prime();
                                            $coach = new gt_coach();
                                            $list = $db->get_select_query("select * from station order by ID asc");
                                            if (count($list) > 0) {
                                                foreach ($list as $l) {

                                                    $res = $db->get_select_query("select * from test_result where p_id = $p_id and tr_number = $test_number and tr_turn = $i");
                                                    if (count($res) > 0) {
                                                        $j = 1;
                                                        $total = 0;
                                                        $t1 = $res[0]['tr_score1'] / $res[0]['tr_time1'];
                                                        $t1 = round($t1);
                                                        $t2 = $res[0]['tr_score2'] / $res[0]['tr_time2'];
                                                        $t2 = round($t2);
                                                        $t3 = $res[0]['tr_score3'] / $res[0]['tr_time3'];
                                                        $t3 = round($t3);
                                                        $t4 = $res[0]['tr_score4'] / $res[0]['tr_time4'];
                                                        $t4 = round($t4);
                                                        $t5 = $res[0]['tr_score5'] / $res[0]['tr_time5'];
                                                        $t5 = round($t5);
                                                        $total = ($t1) + ($t2) + ($t3) + ($t4) + ($t5); ?>
                                                        <tr>
                                                            <td><?php echo $pr->per_number($l['s_name']); ?></td>
                                                            <td><?php echo $pr->per_number($t1); ?></td>
                                                            <td><?php echo $pr->per_number($t2); ?></td>
                                                            <td><?php echo $pr->per_number($t3); ?></td>
                                                            <td><?php echo $pr->per_number($t4); ?></td>
                                                            <td><?php echo $pr->per_number($t5); ?></td>
                                                            <td><?php $total = $total / 5;
                                                                echo $pr->per_number(round($total)); ?></td>
                                                            <td><?php echo $coach->get_coach_name($res[0]['c_id']); ?></td>
                                                            <td><?php echo $coach->get_coach_name($res[0]['sup_id']); ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    $i++;
                                                }
                                            }
                                            if ($j == 0) { ?>
                                                <tr>
                                                    <td colspan="10">هیچ امتیازی تاکنون ثبت نشده است</td>
                                                </tr>
                                                <?php
                                            } ?>
                                        </table>
                                    </div>
                                </div>
                                <div style="padding: 25px;" class="col-md-12">
                                    <h4>گزارش جدولی(میانگینی)</h4>
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tr>
                                                <th>شماره پرسنلی</th>
                                                <th>نام و نام خانوادگی</th>
                                                <th>ایستگاه 1</th>
                                                <th>ایستگاه 2</th>
                                                <th>ایستگاه 3</th>
                                                <th>ایستگاه 4</th>
                                            </tr>
                                            <?php
                                            $j = 0;
                                            $i = 1;
                                            $db = new db();
                                            $pr = new prime();
                                            $coach = new gt_coach();
                                            $player = new gt_player();
                                            $list = $db->get_select_query("select * from player order by ID asc");
                                            if (count($list) > 0) {
                                                foreach ($list as $l) {
                                                    $p_id = $l['ID'];
                                                    $total1 = 0;
                                                    $total2 = 0;
                                                    $total3 = 0;
                                                    $total4 = 0;
                                                    $res = $db->get_select_query("select * from test_result where p_id = $p_id and tr_number = $test_number and tr_turn = 1");
                                                    if (count($res) > 0) {
                                                        foreach ($res as $r) {
                                                            $j = 1;
                                                            $total = 0;
                                                            $t1 = $r['tr_score1'] / $r['tr_time1'];
                                                            $t1 = round($t1);
                                                            $t2 = $r['tr_score2'] / $r['tr_time2'];
                                                            $t2 = round($t2);
                                                            $t3 = $r['tr_score3'] / $r['tr_time3'];
                                                            $t3 = round($t3);
                                                            $t4 = $r['tr_score4'] / $r['tr_time4'];
                                                            $t4 = round($t4);
                                                            $t5 = $r['tr_score5'] / $r['tr_time5'];
                                                            $t5 = round($t5);
                                                            $total = ($t1) + ($t2) + ($t3) + ($t4) + ($t5);
                                                            $total1 += $total / 5;
                                                        }
                                                    }
                                                    $res = $db->get_select_query("select * from test_result where p_id = $p_id and tr_number = $test_number and tr_turn = 2");
                                                    if (count($res) > 0) {
                                                        foreach ($res as $r) {
                                                            $j = 1;
                                                            $total = 0;
                                                            $t1 = $r['tr_score1'] / $r['tr_time1'];
                                                            $t1 = round($t1);
                                                            $t2 = $r['tr_score2'] / $r['tr_time2'];
                                                            $t2 = round($t2);
                                                            $t3 = $r['tr_score3'] / $r['tr_time3'];
                                                            $t3 = round($t3);
                                                            $t4 = $r['tr_score4'] / $r['tr_time4'];
                                                            $t4 = round($t4);
                                                            $t5 = $r['tr_score5'] / $r['tr_time5'];
                                                            $t5 = round($t5);
                                                            $total = ($t1) + ($t2) + ($t3) + ($t4) + ($t5);
                                                            $total2 += $total / 5;
                                                        }
                                                    }
                                                    $res = $db->get_select_query("select * from test_result where p_id = $p_id and tr_number = $test_number and tr_turn = 3");
                                                    if (count($res) > 0) {
                                                        foreach ($res as $r) {
                                                            $j = 1;
                                                            $total = 0;
                                                            $t1 = $r['tr_score1'] / $r['tr_time1'];
                                                            $t1 = round($t1);
                                                            $t2 = $r['tr_score2'] / $r['tr_time2'];
                                                            $t2 = round($t2);
                                                            $t3 = $r['tr_score3'] / $r['tr_time3'];
                                                            $t3 = round($t3);
                                                            $t4 = $r['tr_score4'] / $r['tr_time4'];
                                                            $t4 = round($t4);
                                                            $t5 = $r['tr_score5'] / $r['tr_time5'];
                                                            $t5 = round($t5);
                                                            $total = ($t1) + ($t2) + ($t3) + ($t4) + ($t5);
                                                            $total3 += $total / 5;
                                                        }
                                                    }
                                                    $res = $db->get_select_query("select * from test_result where p_id = $p_id and tr_number = $test_number and tr_turn = 4");
                                                    if (count($res) > 0) {
                                                        foreach ($res as $r) {
                                                            $j = 1;
                                                            $total = 0;
                                                            $t1 = $r['tr_score1'] / $r['tr_time1'];
                                                            $t1 = round($t1);
                                                            $t2 = $r['tr_score2'] / $r['tr_time2'];
                                                            $t2 = round($t2);
                                                            $t3 = $r['tr_score3'] / $r['tr_time3'];
                                                            $t3 = round($t3);
                                                            $t4 = $r['tr_score4'] / $r['tr_time4'];
                                                            $t4 = round($t4);
                                                            $t5 = $r['tr_score5'] / $r['tr_time5'];
                                                            $t5 = round($t5);
                                                            $total = ($t1) + ($t2) + ($t3) + ($t4) + ($t5);
                                                            $total4 += $total / 5;
                                                        }
                                                    } ?>
                                                    <tr>
                                                        <td><?php echo $pr->per_number($l['personnel_id']) ?></td>
                                                        <td><?php echo $player->get_player_name($l['ID']); ?></td>
                                                        <td><?php $total1 = $total1;
                                                            echo $pr->per_number(round($total1)); ?></td>
                                                        <td><?php $total2 = $total2;
                                                            echo $pr->per_number(round($total2)); ?></td>
                                                        <td><?php $total3 = $total3;
                                                            echo $pr->per_number(round($total3)); ?></td>
                                                        <td><?php $total4 = $total4;
                                                            echo $pr->per_number(round($total4)); ?></td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            if ($j == 0) { ?>
                                                <tr>
                                                    <td colspan="10">هیچ امتیازی تاکنون ثبت نشده است</td>
                                                </tr>
                                                <?php
                                            } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <?php
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}