 <?php
class gt_parameter {
	
	public function add_parameter($array) {
		$db = new db();
		$pa_name = $array['pa_name'];
		$pa_index = $array['pa_index'];
		$sql = "insert into parameter(pa_name, pa_index) values('$pa_name', $pa_index)";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_parameter($array) {
		$db = new db();
		$ID = $array['ID'];
		$pa_name = $array['pa_name'];
		$pa_index = $array['pa_index'];
		$sql = "update parameter set pa_name = '$pa_name', pa_index = $pa_index where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_parameter($ID) {
		$db = new db();
		$sql = "delete from parameter where ID = $ID";
		$db->ex_query($sql);
	}

	public function create_form($ID = null) {
		$db = new db();
		if($ID == null) {
			$pa_name = "";
			$pa_index = "";
		} else {
			$asb = $db->get_select_query("select * from parameter where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$pa_name = $roww['pa_name'];
					$pa_index = $roww['pa_index'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-parameter-view">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>نام پارامتر <span class="red">*</span></label>
								<input type="tex" name="pa_name" placeholder="نام پارامتر" value="<?php echo $pa_name; ?>" class="form-control" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>شاخص <span class="red">*</span></label>
								<select name="pa_index" class="form-control select2">
									<?php
									$db = new db();
									$gt_screening_index = new gt_screening_index();
									$res = $db->get_select_query("select * from screening_index order by ID desc");
									if(count($res) > 0){
										foreach($res as $row){
											?>
											<option value="<?php echo $row["ID"]; ?>" <?php if($pa_index == $row["ID"]){ echo 'selected'; } ?>><?php echo $gt_screening_index->get_screening_index_ages($row["ID"]); ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_parameter" class="btn btn-sm btn-success add-parameter">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_parameter" class="btn btn-sm btn-warning update-parameter">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-parameter-view-result"><?php $this->list_parameter_view(); ?></div>
		<?php
	}
	
	public function list_parameter_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام پارامتر</th>
							<th>شاخص</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$db = new db();
						$pr = new prime();
						$gt_screening_index = new gt_screening_index();
						$list = $db->get_select_query("select * from parameter order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['pa_name']); ?></td>
									<td><?php $index_name = $gt_screening_index->get_screening_index_ages($l["pa_index"]); echo $pr->per_number($index_name); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-parameter-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-parameter">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="5">هیچ پارامتری تاکنون تعریف نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
}