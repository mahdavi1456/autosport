<?php
class gt_station {
	
	public function add_station($array) {
		$db = new db();
		$s_name = $array['s_name'];
		$sql = "insert into station(s_name) values('$s_name')";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_station($array) {
		$db = new db();
		$ID = $array['ID'];
		$s_name = $array['s_name'];
		$sql = "update station set s_name = '$s_name' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_station($ID) {
		$db = new db();
		$sql = "delete from station where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_station_name($ID) {
		$db = new db();
		$s_name = $db->get_var_query("select s_name from station where ID = $ID");
		return $s_name;
	}
	
	public function create_form($ID = null) {
		$db = new db();
		if($ID == null) {
			$s_name = "";
		} else {
			$asb = $db->get_select_query("select * from station where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$s_name = $roww['s_name'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-station-view">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>عنوان <span class="red">*</span></label>
								<input type="tex" name="s_name" placeholder="عنوان" class="form-control" value="<?php echo $s_name; ?>" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_station" class="btn btn-sm btn-success add-station">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_station" class="btn btn-sm btn-warning update-station">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-station-view-result"><?php $this->list_station_view(); ?></div>
		<?php
	}
	
	public function list_station_view() { ?>
		<div class="row result">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام ایستگاه</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$db = new db();
						$pr = new prime();
						$list = $db->get_select_query("select * from station order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['s_name']); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-station-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-station">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="4">هیچ ایستگاهی تاکنون تعریف نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
}