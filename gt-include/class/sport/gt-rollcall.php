 <?php
class gt_rollcall {
	
	public function add_rollcall($array) {
		$db = new db();
		$p_id = $array['p_id'];
		$rc_val = $array['rc_val'];
		$rc_details = $array['rc_details'];
		$rc_date = $array['rc_date'];
		$sql1 = $db->get_select_query("select * from rollcall where rc_date = '$rc_date' and p_id = $p_id");
		if(count($sql1) > 0) {
			$sql = "update rollcall set rc_val = $rc_val, rc_details = '$rc_details' where rc_date = '$rc_date' and p_id = $p_id";
		} else {
			$sql = "insert into rollcall(p_id, rc_val, rc_details, rc_date) values($p_id, $rc_val, '$rc_details', '$rc_date')";
		}
		$last_id = $db->ex_query($sql);
		return $last_id;
	}

	public function create_form() {
		$db = new db(); ?>
		<div class="row">
			<div class="col-md-12">
				<div id="addrollcall">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
                                <label>نام بازیکن / کد ملی</label>
                                <select name="p_id" id="p_id" class="form-control select2">
									<option value="">همه ی بازیکنان</option>
                                    <?php
                                    $db = new db();
                                    $pr = new prime();
                                    $gt_player = new gt_player();
                                    $res = $db->get_select_query("select * from player");
                                    if (count($res) > 0) {
                                        foreach ($res as $row) {
                                            ?>
                                            <option value="<?php echo $row["ID"]; ?>" <?php if ($row["ID"] == $p_id) {
                                                echo 'selected';
                                            } ?>><?php echo $gt_player->get_player_name($row['ID']) . '#' . $row['p_national_code']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>تاریخ <span class="red">*</span></label>
								<input type="text" name="date" id="mydate" placeholder="تاریخ" value="<?php echo jdate("Y/m/d"); ?>" class="date-picker form-control">
							</div>
						</div></br>
						<div class="col-md-3">
							<button type="submit" class="btn btn-sm btn-success set-date">انتخاب</button>
						</div>
					</div></br>
					<div class="result">
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	
}