 <?php
class gt_ages {
	
	public function add_ages($array) {
		$db = new db();
		$ag_name = $array['ag_name'];
		$sql = "insert into ages(ag_name) values('$ag_name')";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_ages($array) {
		$db = new db();
		$ID = $array['ID'];
		$ag_name = $array['ag_name'];
		$sql = "update ages set ag_name = '$ag_name' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_ages($ID) {
		$db = new db();
		$sql = "delete from ages where ID = $ID";
		$db->ex_query($sql);
	}

	public function get_ages_name($ID) {
		$db = new db();
		$ag_name = $db->get_var_query("select ag_name from ages where ID = $ID");
		return $ag_name;
	}

	public function create_form($ID = null) {
		$db = new db();
		if($ID == null) {
			$ag_name = "";
		} else {
			$asb = $db->get_select_query("select * from ages where ID = $ID");
			 if(count($asb) > 0) {
				foreach($asb as $roww) {
					$ag_name = $roww['ag_name'];
				}
			 }
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-ages-view">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label>رده سنی <span class="red">*</span></label>
								<input type="tex" name="ag_name" placeholder="رده سنی" value="<?php echo $ag_name; ?>" class="form-control" required>
							</div>
						</div><br>
						<div class="col-md-4">
							<?php
							if($ID == null) {
								?>
								<button type="button" name="add_ages" class="btn btn-sm btn-success add-ages">ذخیره</button>
								<?php
							} else {
								?>
								<button type="button" name="update_ages" class="btn btn-sm btn-warning update-ages">ویرایش</button>
								<input type="hidden" name="ID" value="<?php  echo $ID; ?>">
								<?php
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="list-ages-view-result"><?php $this->list_ages_view(); ?></div>
		<?php
	}
	
	public function list_ages_view() { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-condensed">
						<tr>
							<th>ردیف</th>
							<th>نام شاخص</th>
							<th>مدیریت</th>
						</tr>
						<?php
						$i = 1;
						$db = new db();
						$pr = new prime();
						$list = $db->get_select_query("select * from ages order by ID desc");
						if(count($list)>0){
							foreach($list as $l){ ?>
								<tr>
									<td><?php echo $pr->per_number($i); ?></td>
									<td><?php echo $pr->per_number($l['ag_name']); ?></td>
									<td>
										<button class="btn btn-info btn-xs update-ages-form" data-id="<?php echo $l['ID']; ?>">مشاهده و ویرایش</button>
										<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove-ages">حذف</button>
									</td>
								</tr>
								<?php
								$i++;
							}
						}else{ ?>
							<tr>
								<td colspan="3">هیچ شاخصی تاکنون تعریف نشده است</td>
							</tr>
						<?php
						} ?>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
}