<?php
class engine{
	
	public function load_view($view, $form) {
		$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-include/view/" . $form . "/" . $view . "-" . $form . ".php";
		$c = file_get_contents($path);
		return $c;
	}
	
	public function get_theme_url() {
		return "http://localhost/gt-content/themes/sport/";
	}
	
	public function get_the_url() {
		return "http://localhost/";
	}
	
	public function get_home_url() {
		return "http://localhost/";
	}
	
}