<?php
class gt_customer {
	
	public function add_customer($array) {
		$c_name = $array['c_name'];
		$c_family = $array['c_family'];
		$c_company = $array['c_company'];
		$c_national = $array['c_national'];
		$c_idnumber = $array['c_idnumber'];
		$c_national_id = $array['c_national_id'];
		$c_regnumber = $array['c_regnumber'];
		$c_economic = $array['c_economic'];
		$c_phone = $array['c_phone'];
		$c_fax = $array['c_fax'];
		$c_mobile = $array['c_mobile'];
		$c_address = $array['c_address'];
		$c_c_address = $array['c_c_address'];
		$c_c_phone = $array['c_c_phone'];
		$c_vat = $array['c_vat'];
		$c_activity = $array['c_activity'];
		$c_email = $array['c_email'];
		$c_postalcode = $array['c_postalcode'];
		$c_type = $array['c_type'];
		$sql = "insert into customer (c_name, c_family, c_company, c_national, c_idnumber, c_national_id, c_regnumber, c_economic, c_phone, c_fax, c_mobile, c_address, c_c_address, c_c_phone, c_vat, c_activity, c_email, c_postalcode, c_type) values('$c_name', '$c_family', '$c_company', '$c_national', $c_idnumber, '$c_national_id', $c_regnumber '$c_economic', '$c_phone', '$c_fax', '$c_mobile', '$c_address', '$c_c_address', '$c_c_phone', '$c_vat', '$c_activity', '$c_email', '$c_postalcode', '$c_type')";
		$db = new db();
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_customer($array) {
		$db = new db();
		$db = $array['ID'];
		$c_name = $array['c_name'];
		$c_family = $array['c_family'];
		$c_company = $array['c_company'];
		$c_national = $array['c_national'];
		$c_idnumber = $array['c_idnumber'];
		$c_national_id = $array['c_national_id'];
		$c_regnumber = $array['c_regnumber'];
		$c_economic = $array['c_economic'];
		$c_phone = $array['c_phone'];
		$c_fax = $array['c_fax'];
		$c_mobile = $array['c_mobile'];
		$c_address = $array['c_address'];
		$c_c_address = $array['c_c_address'];
		$c_c_phone = $array['c_c_phone'];
		$c_vat = $array['c_vat'];
		$c_activity = $array['c_activity'];
		$c_email = $array['c_email'];
		$c_postalcode = $array['c_postalcode'];
		$c_type = $array['c_type'];
		$sql = "update customer set c_name = '$c_name', c_family = '$c_family', c_company = '$c_company', c_national = '$c_national', c_idnumber = '$c_c_idnumber', c_national_id = '$c_national_id', c_regnumber = '$c_regnumber', c_economic = '$c_economic', c_phone = '$c_phone', c_fax = '$c_fax', c_mobile = '$c_mobile', c_address = '$c_address', c_c_address = '$c_c_address', c_c_phone = '$c_c_phone', c_vat = '$c_vat', c_activity = '$c_activity', c_email = '$c_email', c_postalcode = '$c_postalcode' c_type = '$c_type' where ID = $ID";
		$last_id = ex_query($sql);
		return $last_id;
	}
	
	public function remove_customer($ID) {
		$sql = "delete from customer where ID = $ID";
		$db = new db();
		$db->ex_query($sql);
	}
	
	public function get_customer_name($ID) {
		$sql = "select c_name, c_family from customer where ID = $ID";
		$db = new db();
		$res = $db->get_select_query($sql);
		if (count($res) > 0) {
			return $res[0]['c_name'] . " " . $res[0]['c_family'];
		} else {
			return "معتبر نیست";
		}
	}
	
	public function get_customer_type($ID) {
		$sql = "select c_type from customer where ID = $ID";
		$db = new db();
		$res = $db->get_var_query($sql);	
		if($res == 0) {
			return "شخص حقوقی است";
		} else {
			return "شخص حقیقی است";
		}
	}
	
}