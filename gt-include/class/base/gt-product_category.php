<?php
class gt_product_category {
	
	public function add_product_category($array) {
		$c_name = $array['c_name'];
		$c_parent = $array['c_parent'];
		$sql = "insert into product_category (c_name, c_parent) values('$c_name', $c_parent)";
		$db = new db();
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_product_category($array) {
		$db = new db();
		$ID = $array['ID'];
		$c_name = $array['c_name'];
		$c_parent = $array['c_parent'];
		$sql = "update product_category set c_name = '$c_name', c_parent = $c_parent where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_product_category($ID) {
		$sql = "delete from product_category where ID = $ID";
		$db = new db();
		$db->ex_query($sql);
	}
	
	public function get_product_category_name($ID) {
		$sql = "select c_name from product_category where ID = $ID";
		$db = new db();
		$c_name = $db->get_var_query($sql);
		return $c_name;
	}
	
	public function get_product_category() {
		$sql = "select * from product_category";
		$db = new db();
		$res = $db->get_select_query($sql);
		return $res;
	}
	
}
