<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";

class gt_user {
	
	public function add_user($array) {
		$db = new db();
		$u_name = $array['u_name'];
		$u_family = $array['u_family'];
		$u_birth = $array['u_birth'];
		$u_marital = $array['u_marital'];
		$u_email = $array['u_email'];
		$u_national = $array['u_national'];
		$pr_id = $array['pr_id'];
		$u_postal = $array['u_postal'];
		$u_address = $array['u_address'];
		$u_phone = $array['u_phone'];
		$u_mobile = $array['u_mobile'];
		$u_child_count = $array['u_child_count'];
		$u_username = $array['u_username'];
		$u_password = $array['u_password'];
		$u_regdate = $array['u_regdate'];
		$u_level = $array['u_level'];
		$u_link = $array['u_link'];
		$u_evidence = $array['u_evidence'];
		$u_cart = $array['u_cart'];
		$sql = "insert into user(u_name, u_family, u_birth, u_evidence, u_cart, u_marital, u_email, u_national, pr_id, u_postal, u_address, u_phone, u_mobile, u_child_count, u_username, u_password, u_regdate, u_level, u_link) values('$u_name', '$u_family', '$u_birth', '$u_evidence', '$u_cart', '$u_marital', '$u_email', '$u_national', $pr_id, '$u_postal', '$u_address', '$u_phone', '$u_mobile', $u_child_count, '$u_username', '$u_password', '$u_regdate', '$u_level', '$u_link')";
		$last_id = $db->ex_query($sql);
		return $last_id;
	}
	
	public function update_user($array) {
		$db = new db();
		$ID = $array['ID'];
		$u_name = $array['u_name'];
		$u_family = $array['u_family'];
		$u_birth = $array['u_birth'];
		$u_marital = $array['u_marital'];
		$u_email = $array['u_email'];
		$u_national = $array['u_national'];
		$pr_id = $array['pr_id'];
		$u_postal = $array['u_postal'];
		$u_address = $array['u_address'];
		$u_phone = $array['u_phone'];
		$u_mobile = $array['u_mobile'];
		$u_child_count = $array['u_child_count'];
		$u_username = $array['u_username'];
		$u_password = $array['u_password'];
		$u_regdate = $array['u_regdate'];
		$u_level = $array['u_level'];
		$u_link = $array['u_link'];
		$u_evidence = $array['u_evidence'];
		$u_cart = $array['u_cart'];
		$sql = "update user set u_name = '$u_name', u_family = '$u_family', u_birth = '$u_birth', u_cart = '$u_cart', u_evidence = '$u_evidence', u_marital = '$u_marital', u_email = '$u_email', u_national = '$u_national', pr_id = $pr_id, u_postal = '$u_postal', u_address = '$u_address', u_phone = '$u_phone', u_mobile = '$u_mobile', u_child_count = $u_child_count, u_username = '$u_username', u_password = '$u_password', u_regdate = '$u_regdate', u_level = '$u_level', u_link = '$u_link' where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function remove_user($ID) {
		$db = new db();
		$sql = "delete from user where ID = $ID";
		$db->ex_query($sql);
	}
	
	public function get_user_name($ID) {
		$db = new db();
		$u_list = $db->get_select_query("select u_name, u_family from user where ID = $ID");
		if(count($u_list) > 0) {
			return $u_list[0]['u_name'] . " " . $u_list[0]['u_family'];
		} else {
			return "نامعتبر";
		}
	}
	
	public function get_user_level($ID) {
		$db = new db();
		$u_level = $db->get_var_query("select u_level from user where ID = $ID");
		return $u_level;
	}
	
	public function get_user_id($username) {
		$db = new db();
		$u_id = $db->get_var_query("select u_id from user where u_username = '$username'");
		return $u_id;
	}
	
	public function check_login($username, $password) {
		$db = new db();
		$pass = md5($password);
		$res = $db->get_select_query("select * from user where u_username = '$username' and u_password = '$pass'");
		return count($res);
	}
	
	public function add_user_view() { 
		$str = "";
		$str .= "<div class='row'>";
		$str .= "<div class='col-md-12'>";
		$str .= "<form action='' method='post' id='adduser'>";
		$str .= "<input type='hidden' name='u_regdate' value=''>";
		$str .= "<div class='row'>";
		$str .= "<div class='col-md-3'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>نام <span class='red'>*</span></label>";
		$str .= "<input type='text' name='u_name' placeholder='نام' class='form-control' required>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-3'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>نام خانوادگی <span class='red'>*</span></label>";
		$str .= "<input type='text' name='u_family' placeholder='نام خانوادگی' class='form-control' required>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-3'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>تاریخ تولد <span class='red'>*</span></label>";
		$str .= "<input type='text' name='u_birth' placeholder='تاریخ تولد' class='date-picker form-control' required>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-3'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>کد ملی <span class='red'>*</span></label>";
		$str .= "<input type='text' name='u_national' placeholder='کد ملی' class='form-control' required>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='row'>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>مدرک تحصیلی <span class='red'>*</span></label>";
		$str .= "<select name='u_evidence' class='form-control select2'>";
		$str .= "<option value='دیپلم'>دیپلم</option>";
		$str .= "<option value='فوق دیپلم'>فوق دیپلم</option>";
		$str .= "<option value='لیسانس'>لیسانس</option>";
		$str .= "<option value='فوق لیسانس'>فوق لیسانس</option>";
		$str .= "<option value='دکتری'>دکتری</option>";
		$str .= "</select>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>وضعیت تاهل <span class='red'>*</span></label>";
		$str .= "<select name='u_marital' class='form-control select2'>";
		$str .= "<option value='مجرد'>مجرد</option>";
		$str .= "<option value='متاهل'>متاهل</option>";
		$str .= "</select>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>تعداد فرزند</label>";
		$str .= "<input type='text' name='u_child_count' placeholder='تعداد فرزند' class='form-control'>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='row'>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>تلفن</label>";
		$str .= "<input type='text' name='u_phone' placeholder='تلفن' class='form-control'>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>موبایل <span class='red'>*</span></label>";
		$str .= "<input type='text' name='u_mobile' placeholder='موبایل' class='form-control' required>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>ایمیل</label>";
		$str .= "<input type='text' name='u_email' placeholder='ایمیل' class='form-control'>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='row'>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>کد پستی</label>";
		$str .= "<input type='text' name='u_postal' placeholder='کد پستی' class='form-control'>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>استان <span class='red'>*</span></label>";
		$str .= "<select name='province_id' class='form-control province_id select2'>";
		/*$db = new db();
		$res = $db->get_select_query("select * from province where p_parent = 0 and p_status = 1");
		if(count($res) > 0) {
			foreach($res as $row) {
				$str .= "<option value=" . $row['ID'] . ">" . $row['p_name'] . "</option>";
			}
		}*/
		$str .= "</select>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>شهرستان <span class='red'>*</span></label>";
		$str .= "<div id='result_province'>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='row'>";
		$str .= "<div class='col-md-12'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>آدرس <span class='red'>*</span></label>";
		$str .= "<textarea rows='3' type='text' name='u_address' placeholder='آدرس' class='form-control' required></textarea>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-12'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>شماره کارت</label>";
		$str .= "<input type='text' name='u_cart' placeholder='شماره کارت' class='form-control'>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='row'>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>نام کاربری</label>";
		$str .= "<input type='text' name='u_username' placeholder='نام کاربری' class='form-control'>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>رمز ورود</label>";
		$str .= "<input type='text' name='u_password' placeholder='رمز ورود' class='form-control'>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='col-md-4'>";
		$str .= "<div class='form-group'>";
		$str .= "<label>سطح دسترسی <span class='red'>*</span></label>";
		$str .= "<select name='u_level' class='form-control select2'>";
		$str .= "<option value='مدیر'>مدیر</option>";
		$str .= "<option value='حسابدار'>حسابدار</option>";
		$str .= "</select>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "<div class='row'>";
		$str .= "<div class='col-md-12'>";
		$str .= "<button type='submit' name='add_user' id='add-user' class='btn btn-sm btn-success'>ذخیره</button>";
		$str .= "</div>";
		$str .= "</div>";
		$str .= "</form>";
		$str .= "</div>";
		$str .= "</div>";
		return $str;
	}
	
}