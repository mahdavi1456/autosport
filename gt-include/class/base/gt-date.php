<?php
class gt_date{
	
	public function jgdate($date) {
		
		if(strpos($date, '/') != false) {
			$arry = explode("/", $date);
			$year = $arry[0];
			$month = $arry[1];
			$day = $arry[2];
			$new_date = jalali_to_gregorian($year, $month, $day,'-');
			$jarry = explode("-", $new_date);
			$jyear = $jarry[0];
			$jmonth = $jarry[1];
			$jday = $jarry[2];
			if($jmonth < 10){
				$jmonth = "0" . $jmonth;
			}
			if($jday < 10){
				$jday = "0" . $jday;
			}
			$new_date = $jyear . "-" . $jmonth . "-" . $jday;
			
		} else {
			$arry = explode("-", $date);
			$year = $arry[0];
			$month = $arry[1];
			$day = $arry[2];
			$new_date = gregorian_to_jalali($year, $month, $day,'/');
			$jarry = explode("/", $new_date);
			$jyear = $jarry[0];
			$jmonth = $jarry[1];
			$jday = $jarry[2];
			if($jmonth < 10){
				$jmonth = "0" . $jmonth;
			}
			if($jday < 10){
				$jday = "0" . $jday;
			}
			$new_date = $jyear . "/" . $jmonth . "/" . $jday;
		}
		return $new_date;
	}
	
	public function PersianToEnglish($string) {
		
		$persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
		$english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
		 
		$output= str_replace($persian, $english, $string);
		return $output;
		
	}
	
}