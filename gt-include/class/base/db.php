<?php
class db {
	
	public function get_connection_string() {
		$pdo_conn = new PDO("mysql:host=localhost;dbname=sport;charset=utf8", 'root', '',
						array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
		return $pdo_conn;
	}

	public function ex_query($sql) {
		$pdo_conn = $this->get_connection_string();
		$pdo_statement = $pdo_conn->prepare($sql);
		$pdo_statement->execute();
		$last_id = $pdo_conn->lastInsertId();
		return $last_id;
	}

	public function get_select_query($sql) {
		$pdo_conn = $this->get_connection_string();
		$pdo_statement = $pdo_conn->prepare($sql);
		$pdo_statement->execute();
		$result = $pdo_statement->fetchAll();
		return $result;
	}

	public function get_var_query($sql) {
		$pdo_conn = $this->get_connection_string();
		$pdo_statement = $pdo_conn->prepare($sql);
		$pdo_statement->execute();
		$result = $pdo_statement->fetchAll();
		if($result)
			return $result[0][0];
		else
			return;
	}

}