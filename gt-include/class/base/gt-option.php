<?php
class gt_option {
	
	public function save_option($key, $value) {
		$db = new db();
		$check = $db->get_select_query("select * from option where o_key = '$key'");
		if(count($check) > 0) {
			$last_id = $db->ex_query("update option set o_value = '$value' where o_key = '$key'");
		}else{
			$last_id = $db->ex_query("insert into option(o_key, o_value) values('$key', '$value')");
		}
		return $last_id;
	}
	
	public function get_option($key) {
		$db = new db();
		$value = $db->get_var_query("select o_value from option where o_key = '$key'");
		return $value;
	}
	
	public function create_form() {
		$o_type = $this->get_option("o_type");
		$o_exam_create = $this->get_option("o_exam_create");
		$o_exam_delete = $this->get_option("o_exam_delete"); ?>
		<div class="row">
			<div class="col-md-12">
				<div id="add-option-view">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>نوع آموزشگاه <span class="red">*</span></label>
								<select name="o_type" class="form-control">
									<option value="0" <?php if($o_type == "0") { echo 'selected'; } ?>>مدرسه</option>
									<option value="1" <?php if($o_type == "1") { echo 'selected'; } ?>>دانشگاه</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>استاد بتواند آزمون بسازد؟ <span class="red">*</span></label>
								<select name="o_exam_create" class="form-control">
									<option value="0" <?php if($o_exam_create == "0") { echo 'selected'; } ?>>بله</option>
									<option value="1" <?php if($o_exam_create == "1") { echo 'selected'; } ?>>خیر</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>استاد بتواند آزمون حذف کند؟ <span class="red">*</span></label>
								<select name="o_exam_delete" class="form-control">
									<option value="0" <?php if($o_exam_delete == "0") { echo 'selected'; } ?>>بله</option>
									<option value="1" <?php if($o_exam_delete == "1") { echo 'selected'; } ?>>خیر</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="button" name="add_option" class="btn btn-sm btn-success add-option">ذخیره</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

}