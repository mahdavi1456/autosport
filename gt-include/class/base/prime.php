<?php
class prime {
	
	public function per_number($number){
		return str_replace(
			range(0, 9),
			array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'),
			$number
		);
	}

	public function eng_number($number){
		return str_replace(
			array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'),
			range(0, 9),
			$number
		);
	}

}