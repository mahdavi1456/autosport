$(document).ready(function(){
	$("#add_payment").click(function(){
		var addpayment = $("").text($("#addpayment").serialize());  
		$.post("payment.php", {add_payment:1, addpayment:addpayment}, function(data){
			$('').html(data);
		});
	});
	
	$("#update_payment").click(function(){
		var id = $(this).data('id');
		var updatepayment = $("").text($("#updatepayment").serialize());  
		$.post("payment.php", {update_payment:1, updatepayment:updatepayment, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$("#remove_payment").click(function(){
		var id = $(this).data('id');
		$.post("payment.php", {remove_payment:1, id:id}, function(data){
			$('').html(data);
		});
	});
});