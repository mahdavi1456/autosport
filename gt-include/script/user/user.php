<?php
if(isset($_POST['add_user'])) {
	$user = new gt_user();
	$adduser = $_POST['adduser'];
	$arry = explode("&", $adduser);
	$array = array();
	$arrlength=count($arry);
	for($x=0; $x<$arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$id = $user->add_user($array);
	exit();
}

if(isset($_POST['update_user'])) {
	$user = new gt_user();
	$array = array();
	$updateuser = $_POST['updateuser'];
	$id = $_POST['id'];
	$array[id] = $id;
	$arry = explode("&", $updateuser);
	$arrlength=count($array);

	for($x=0; $x<$arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$user->update_user($array);
	exit();
}

if(isset($_POST['remove_user'])) {
	$user = new gt_user();
	$id = $_POST['id'];
	$user->remove_user($id);
	exit();
}