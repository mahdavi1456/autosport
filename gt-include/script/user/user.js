$(document).ready(function(){
	$("#add_user").click(function() {
		var adduser = $("").text($("#adduser").serialize());  
		$.post("user.php", {add_user:1, adduser:adduser}, function(data){
			$('').html(data);
		});
	});
	
	$("#update_user").click(function() {
		var id = $(this).data('id');
		var updateuser = $("").text($("#updateuser").serialize());  
		$.post("user.php", {update_user:1, updateuser:updateuser, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$("#remove_user").click(function() {
		var id = $(this).data('id');
		$.post("user.php", {remove_user:1, id:id}, function(data){
			$('').html(data);
		});
	});
});