$(document).ready(function(){
	
	$(document.body).on('click', '.add-coach' ,function(){
		$('#list-coach-view-result').html("Loading...");
		var list = $("#add-coach-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-coach.php";
		$.post(full_url, {add_coach:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-coach-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-coach.php";
		$.post(full_url, {update_coach_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-coach' ,function(){
		$('#list-coach-view-result').html("Loading...");
		var list = $("#add-coach-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-coach.php";
		$.post(full_url, {update_coach:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.remove-coach' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-coach.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_coach:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
			});
		}
	});
	
	$(document.body).on('click', '.edit-coach' ,function(){
		var list = $("#add-coach-view *").serialize();
		var home_url = $('#home-url').val();
		var ID = $(this).data('id');
		var full_url = home_url + "gt-include/script/sport/ba-coach.php";
		$.post(full_url, {edit_coach:1, list:list, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
		alert("تغییرات با موفقیت ذخیره شد");
	});
	
	$(document.body).on('click', '.remove-file' ,function(){
		var c_id = $(this).data('cid');
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$("#targetLayer").html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-coach.php";
			$.post(full_url, {remove_file:1, c_id:c_id}, function(data) {
				$("#targetLayer").html(data);
			});
		}
	});
	
	$(".uploadForm").on('submit',(function(e) {
		$("#targetLayer").html("Loading...");
		e.preventDefault();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-coach.php";
		$.ajax(
			{
				url: full_url,
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				{
					$("#targetLayer").html(data);
				},
				error: function() 
				{
					
				} 	        
			});
		})
	);
	
});