$(document).ready(function(){
    $(document.body).on('click', '.view-player-report' ,function(){
        var list = $("#player-report-view *").serialize();
        var home_url = $('#home-url').val();
        var full_url = home_url + "gt-include/script/sport/ba-report.php";

        $.post(full_url, {player_report:1, list:list}, function(data) {
            $('.create-form').html(data);
            $('.select2').select2();
            $('.date-picker').persianDatepicker();
        });
    });

});