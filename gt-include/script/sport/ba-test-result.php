<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-test-result.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-coach.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";

if(isset($_POST['view_test_result'])) {
	$gt_test_result = new gt_test_result();
	$list = $_POST['list'];
	parse_str($list, $new);
	$tr_turn = $new['tr_turn'];
	$s_id = $new['s_id'];
	$p_id = $new['p_id'];
	$c_id = $new['c_id'];
	$sup_id = $new['sup_id'];
	$tr_date = $new['tr_date'];
	$tr_number = $new['tr_number'];
	$gt_test_result->create_form($tr_turn, $s_id, $p_id, $c_id, $sup_id, $tr_date, $tr_number);
	exit();
}

if(isset($_POST['add_test_result'])) {
	$gt_test_result = new gt_test_result();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_test_result->add_test_result($new);
	$gt_test_result->create_form();
	exit();
}

if(isset($_POST['update_test_result_form'])){
	$gt_test_result = new gt_test_result();
	$ID = $_POST['ID'];
	$gt_test_result->create_form($ID);
	exit();
}

if(isset($_POST['update_test_result'])) {
	$gt_test_result = new gt_test_result();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_test_result->update_test_result($new);
	$gt_test_result->create_form();
	exit();
}


if(isset($_POST['remove_test_result'])){
	$gt_test_result = new gt_test_result();
	$ID = $_POST['ID'];
	$gt_test_result->remove_test_result($ID);
	$gt_test_result->create_form();
	exit();
}
