<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-station.php";

if(isset($_POST['add_station'])) {
	$gt_station = new gt_station();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_station->add_station($new);
	$gt_station->create_form();
	exit();
}

if(isset($_POST['update_station_form'])){
	$gt_station = new gt_station();
	$ID = $_POST['ID'];
	$gt_station->create_form($ID);
	exit();
}

if(isset($_POST['update_station'])) {
	$gt_station = new gt_station();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_station->update_station($new);
	$gt_station->create_form();
	exit();
}


if(isset($_POST['remove_station'])){
	$gt_station = new gt_station();
	$ID = $_POST['ID'];
	$gt_station->remove_station($ID);
	$gt_station->create_form();
	exit();
}
