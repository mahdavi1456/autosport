<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-sport-register.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-coach.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";

if(isset($_POST['add_sport_register'])) {
	$gt_sport_register = new gt_sport_register();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_sport_register->add_sport_register($new);
	$gt_sport_register->create_form();
	exit();
}

if(isset($_POST['update_sport_register_form'])){
	$gt_sport_register = new gt_sport_register();
	$ID = $_POST['ID'];
	$gt_sport_register->create_form($ID);
	exit();
}

if(isset($_POST['update_sport_register'])) {
	$gt_sport_register = new gt_sport_register();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_sport_register->update_sport_register($new);
	$gt_sport_register->create_form();
	exit();
}


if(isset($_POST['remove_sport_register'])){
	$gt_sport_register = new gt_sport_register();
	$ID = $_POST['ID'];
	$gt_sport_register->remove_sport_register($ID);
	$gt_sport_register->create_form();
	exit();
}
