<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-food-plan.php";

if(isset($_POST['set_food_plan'])) {
	$db = new db();
	$gt_date = new gt_date();
	$mydate = $_POST['mydate'];
	$day = $_POST['day'];
	$p_id = $_POST['p_id'];
	$newdate = $gt_date->jgdate($mydate);
    $pr = new prime();

    ?>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr colspan="18">
					<th colspan="1">#</th>
					<th colspan="2">تاریخ</th>
					<th colspan="2">صبحانه</th>
					<th colspan="1">ساعت صبحانه</th>
					<th colspan="2">میان وعده</th>
					<th colspan="1">ساعت میان وعده</th>
					<th colspan="2">ناهار</th>
					<th colspan="1">ساعت ناهار</th>
					<th colspan="2">عصرانه</th>
					<th colspan="1">ساعت عصرانه</th>
					<th colspan="2">شام</th>
					<th colspan="1">ساعت شام</th>
				</tr>
				<?php
				for($i=1 ; $i <= $day ; $i++) { ?>
					<tr colspan="18">
						<td colspan="1"><?php echo $pr->per_number($i); ?></td>
						<td colspan="2"><?php echo $pr->per_number($gt_date->jgdate($newdate)); ?></td>
						<td colspan="2">
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'صبحانه' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp1_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td colspan="1">
							<input type="text" name="fp1_time<?php echo $i; ?>" value="<?php echo $time; ?>"  style="width: 50px !important;">
						</td>
						<td colspan="2">
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'میان وعده' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp2_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td colspan="1">
							<input type="text" name="fp2_time<?php echo $i; ?>" value="<?php echo $time; ?>" style="width: 50px !important;">
						</td>
						<td colspan="2">
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'ناهار' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp3_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td colspan="1">
							<input type="text" name="fp3_time<?php echo $i; ?>" value="<?php echo $time; ?>" style="width: 50px !important;">
						</td>
						<td colspan="2">
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'عصرانه' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp4_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td colspan="1">
							<input type="text" name="fp4_time<?php echo $i; ?>" value="<?php echo $time; ?>" style="width:50px !important;">
						</td>
						<td colspan="2">
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'شام' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp5_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td colspan="1">
							<input type="text" name="fp5_time<?php echo $i; ?>" value="<?php echo $time; ?>" style="width: 50px !important;">
						</td>
					</tr>
					<input type="hidden" name="fp_date<?php echo $i; ?>" value="<?php echo $newdate; ?>">
					<?php
					$newdate1 = strtotime ('+1 day', strtotime ($newdate)) ;
					$newdate = date ('Y-m-d' , $newdate1); 
				} ?>
				<input type="hidden" id="fp_i" value="<?php $i = $i - 1; echo $i; ?>">
			</table>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-sm btn-success add-food-plan">ذخیره</button>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['view_food_plan'])) {
	$db = new db();
	$gt_date = new gt_date();
	$mydate = $_POST['mydate'];
	$p_id = $_POST['p_id'];
	$day = $_POST['day'];
	$newdate = $gt_date->jgdate($mydate);
	?>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>#</th>
					<th>تاریخ</th>
					<th>صبحانه</th>
					<th>ساعت صبحانه</th>
					<th>میان وعده</th>
					<th>ساعت میان وعده</th>
					<th>ناهار</th>
					<th>ساعت ناهار</th>
					<th>عصرانه</th>
					<th>ساعت عصرانه</th>
					<th>شام</th>
					<th>ساعت شام</th>
				</tr>
				<?php
				for($i=1 ; $i <= $day ; $i++) { ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $gt_date->jgdate($newdate);; ?></td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'صبحانه' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							}
							 echo $name; ?>
						</td>
						<td>
							<?php echo $time; ?>
						</td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'میان وعده' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							}
							 echo $name; ?>
						</td>
						<td>
							<?php echo $time; ?>
						</td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'ناهار' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							}
							echo $name; ?>
						</td>
						<td>
							<?php echo $time; ?>
						</td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'عصرانه' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							}
							echo $name; ?>
						</td>
						<td>
							<?php echo $time; ?>
						</td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'شام' and p_id = $p_id");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							}
							echo $name; ?>
						</td>
						<td>
							<?php echo $time; ?>
						</td>
					</tr>
					<input type="hidden" name="fp_date<?php echo $i; ?>" value="<?php echo $newdate; ?>">
					<?php
					$newdate1 = strtotime ('+1 day', strtotime ($newdate)) ;
					$newdate = date ('Y-m-d' , $newdate1); 
				} ?>
				<input type="hidden" id="fp_i" value="<?php $i = $i - 1; echo $i; ?>">
			</table>
		</div>
	</div>
	<?php
	exit();
}


if(isset($_POST['add_food_plan'])) {
	$db = new db();
	$gt_food_plan = new gt_food_plan();
	$fp_i = $_POST['fp_i'];
	parse_str($_POST['addfood_plan'], $list);
	for($i = 1 ; $i <= $fp_i ; $i++) {
		$fp1_name = $list['fp1_name' . $i];
		$fp2_name = $list['fp2_name' . $i];
		$fp3_name = $list['fp3_name' . $i];
		$fp4_name = $list['fp4_name' . $i];
		$fp5_name = $list['fp5_name' . $i];
		$fp_date = $list['fp_date' . $i];
		$fp1_time = $list['fp1_time' . $i];
		$fp2_time = $list['fp2_time' . $i];
		$fp3_time = $list['fp3_time' . $i];
		$fp4_time = $list['fp4_time' . $i];
		$fp5_time = $list['fp5_time' . $i];
		$fp_date = $list['fp_date' . $i];
		$p_id = $list['p_id'];
		$res3 = $db->get_select_query("select * from food_plan where fp_date = '$fp_date'");
		if(count($res3) > 0) {
			$db->ex_query("delete from food_plan where fp_date = '$fp_date'");
		}
		if($fp1_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "صبحانه";
			$array['fp_name'] = $fp1_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp1_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
		if($fp2_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "میان وعده";
			$array['fp_name'] = $fp2_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp2_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
		if($fp3_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "ناهار";
			$array['fp_name'] = $fp3_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp3_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
		if($fp4_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "عصرانه";
			$array['fp_name'] = $fp4_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp4_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
		if($fp5_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "شام";
			$array['fp_name'] = $fp5_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp5_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
	}
	exit();
}