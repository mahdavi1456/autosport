<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/engine.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";

if(isset($_POST['add_player'])) {
	$gt_player = new gt_player();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_player->add_player($new);
	$gt_player->create_form();
	exit();
}

if(isset($_POST['update_player_form'])){
	$gt_player = new gt_player();
	$ID = $_POST['ID'];
	$gt_player->create_form($ID);
	exit();
}

if(isset($_POST['update_player'])) {
	$gt_player = new gt_player();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_player->update_player($new);
	$gt_player->create_form();
	exit();
}


if(isset($_POST['remove_player'])){
	$gt_player = new gt_player();
	$ID = $_POST['ID'];
	$gt_player->remove_player($ID);
	$gt_player->create_form();
	exit();
}

if(isset($_POST['edit_player'])) {
	$gt_player = new gt_player();
	$db = new db();
	$ID = $_POST['ID'];
	$list = $_POST['list'];
	parse_str($list, $new);
	$new_username = $new['new_username'];
	$new_pass = $new['new_pass'];
	if($new_pass == ""){
		$sql = "update player set p_username = '$new_username' where ID = $ID";
		$db->ex_query($sql);
	}
	else if($new_username == ""){
		$new_pass = md5($new_pass);
		$sql = "update player set p_password = '$new_pass' where ID = $ID";
		$db->ex_query($sql);
	} else {
		$new_pass = md5($new_pass);
		$sql = "update player set p_username = '$new_username', p_password = '$new_pass' where ID = $ID";
		$db->ex_query($sql);
	}
	$gt_player->edit_profile_player($ID);
	exit();
}

if(isset($_POST['remove_file'])){
	$gt_player = new gt_player();
	$db = new db();
	$p_id = $_POST['p_id'];
	$link = $db->get_var_query("select p_file from player where ID = $p_id");
	$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $link;
	if(unlink($path)){
		$sql = "update player set p_file = '' where ID = $p_id";
		$db->ex_query($sql);
	}
	$gt_player->get_profile($st_id);
}

if(is_array($_FILES)) {
	if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
		$sourcePath = $_FILES['userImage']['tmp_name'];
		$filename = $_FILES['userImage']['name'];
		$filename_list = explode(".", $filename);
		$new_filename = time() . "." . $filename_list[1]; 
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $new_filename;
		if(move_uploaded_file($sourcePath, $targetPath)) {
			$db = new db();
			$p_id = $_POST['p_id'];
			$p_file = $db->get_var_query("select p_file from player where ID = $p_id");
			if($p_file != ""){
				$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $p_file;
				if(unlink($path)){
				}
			}
			$sql = "update player set p_file = '$new_filename' where ID = $p_id";
			$db->ex_query($sql);
			$gt_player = new gt_player();
			$gt_player->get_profile($p_id);
		}
	}
}
