<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-parameter.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-screening-index.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";

if(isset($_POST['add_parameter'])) {
	$gt_parameter = new gt_parameter();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_parameter->add_parameter($new);
	$gt_parameter->create_form();
	exit();
}

if(isset($_POST['update_parameter_form'])){
	$gt_parameter = new gt_parameter();
	$ID = $_POST['ID'];
	$gt_parameter->create_form($ID);
	exit();
}

if(isset($_POST['update_parameter'])) {
	$gt_parameter = new gt_parameter();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_parameter->update_parameter($new);
	$gt_parameter->create_form();
	exit();
}


if(isset($_POST['remove_parameter'])){
	$gt_parameter = new gt_parameter();
	$ID = $_POST['ID'];
	$gt_parameter->remove_parameter($ID);
	$gt_parameter->create_form();
	exit();
}
