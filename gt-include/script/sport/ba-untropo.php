<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-untropo.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";

if(isset($_POST['add_untropo'])) {
	$gt_untropo = new gt_untropo();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_untropo->add_untropo($new);
	$gt_untropo->create_form();
	exit();
}

if(isset($_POST['update_untropo_form'])){
	$gt_untropo = new gt_untropo();
	$ID = $_POST['ID'];
	$gt_untropo->create_form($ID);
	exit();
}

if(isset($_POST['update_untropo'])) {
	$gt_untropo = new gt_untropo();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_untropo->update_untropo($new);
	$gt_untropo->create_form();
	exit();
}


if(isset($_POST['remove_untropo'])){
	$gt_untropo = new gt_untropo();
	$ID = $_POST['ID'];
	$gt_untropo->remove_untropo($ID);
	$gt_untropo->create_form();
	exit();
}

if(isset($_POST['update_bmi'])) {
	$u_height = $_POST['u_height'];
	$u_wieght = $_POST['u_wieght'];
	$u_height = $u_height / 100;
	$u_height = $u_height * $u_height;
	$u_wieght = $_POST['u_wieght'];
	$bmi = $u_wieght / $u_height;
	$bmi = floor($bmi * 100) / 100;
	echo $bmi;
	exit();
}
