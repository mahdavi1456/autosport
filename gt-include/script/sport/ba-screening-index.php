<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-screening-index.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-ages.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";

if(isset($_POST['add_screening_index'])) {
	$gt_screening_index = new gt_screening_index();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_screening_index->add_screening_index($new);
	$gt_screening_index->create_form();
	exit();
}

if(isset($_POST['update_screening_index_form'])){
	$gt_screening_index = new gt_screening_index();
	$ID = $_POST['ID'];
	$gt_screening_index->create_form($ID);
	exit();
}

if(isset($_POST['update_screening_index'])) {
	$gt_screening_index = new gt_screening_index();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_screening_index->update_screening_index($new);
	$gt_screening_index->create_form();
	exit();
}


if(isset($_POST['remove_screening_index'])){
	$gt_screening_index = new gt_screening_index();
	$ID = $_POST['ID'];
	$gt_screening_index->remove_screening_index($ID);
	$gt_screening_index->create_form();
	exit();
}
