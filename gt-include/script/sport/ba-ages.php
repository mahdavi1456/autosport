<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-ages.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";

if(isset($_POST['add_ages'])) {
	$gt_ages = new gt_ages();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_ages->add_ages($new);
	$gt_ages->create_form();
	exit();
}

if(isset($_POST['update_ages_form'])){
	$gt_ages = new gt_ages();
	$ID = $_POST['ID'];
	$gt_ages->create_form($ID);
	exit();
}

if(isset($_POST['update_ages'])) {
	$gt_ages = new gt_ages();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_ages->update_ages($new);
	$gt_ages->create_form();
	exit();
}


if(isset($_POST['remove_ages'])){
	$gt_ages = new gt_ages();
	$ID = $_POST['ID'];
	$gt_ages->remove_ages($ID);
	$gt_ages->create_form();
	exit();
}
