$(document).ready(function(){
	
	$(document.body).on('click', '.set-date' ,function(){
		var mydate = $('#mydate').val();
		var day = $('#day').val();
		var p_id = $('#p-id').find(":selected").val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-food-plan.php";
		$.post(full_url, {set_food_plan:1, mydate:mydate, day:day, p_id:p_id}, function(data){
			$('.result').html(data);
			$('.timepicker').timepicki({
			show_meridian:false,
			min_hour_value:0,
			max_hour_value:23,
			overflow_minutes:true,
			increase_direction:'up',
			disable_keyboard_mobile: true});
			$(".date-picker").persianDatepicker();
			$('.select2').select2();
			$( '#loading').click(function() {
				//showloader();
			});
		});
	});
	
	$(document.body).on('click', '.view-food-plan' ,function(){
		var mydate = $('#mydate').val();
		var day = $('#day').val();
		var p_id = $('#p-id').val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-food-plan.php";
		$.post(full_url, {view_food_plan:1, mydate:mydate, day:day, p_id:p_id}, function(data){
			$('.result').html(data);
			$('.timepicker').timepicki({
			show_meridian:false,
			min_hour_value:0,
			max_hour_value:23,
			overflow_minutes:true,
			increase_direction:'up',
			disable_keyboard_mobile: true});
			$(".date-picker").persianDatepicker();
			$('.select2').select2();
			$( '#loading').click(function() {
				//showloader();
			});
		});
	});
	
	$(document.body).on('click', '.add-food-plan' ,function(){
		var addfood_plan = $("#addfood-plan *").serialize();
		var fp_i = $('#fp_i').val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-food-plan.php";
		$.post(full_url, {add_food_plan:1, addfood_plan:addfood_plan, fp_i:fp_i}, function(data){
			$('.result').html(data);
			$('.timepicker').timepicki({
			show_meridian:false,
			min_hour_value:0,
			max_hour_value:23,
			overflow_minutes:true,
			increase_direction:'up',
			disable_keyboard_mobile: true});
			$(".date-picker").persianDatepicker();
			$('.select2').select2();
			$( '#loading').click(function() {
				//showloader();
			});
		});
		alert("اطلاعات با موفقیت ثبت شد");
	});
	
});