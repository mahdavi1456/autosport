<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/engine.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-duties.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";
if(isset($_POST['update_duties_form'])){
	$gt_duties = new gt_duties();
	$ID = $_POST['ID'];
	$gt_duties->create_form($ID);
	exit();
}

if(isset($_POST['remove_duties'])){
	$gt_duties = new gt_duties();
	$ID = $_POST['ID'];
	$gt_duties->remove_duties($ID);
	$gt_duties->create_form();
	exit();
}
