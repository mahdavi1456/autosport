$(document).ready(function(){
	
	$(document.body).on('click', '.view-screening' ,function(){
		var list = $("#add-screening-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-screening.php";
		$.post(full_url, {agecategory:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('change', '.p_id' ,function(){
		var p_id = $('.p_id').find(":selected").val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-screening.php";
		$.post(full_url, {set_agecategory:1, p_id:p_id}, function(data) {
			$('.agecategory').val(data);
		});
	});
	
	$(document.body).on('keyup', '.national_id' ,function(){
		var national_id = $('.national_id').val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-screening.php";
		$.post(full_url, {set_agecategory2:1, national_id:national_id}, function(data) {
			$('.agecategory').val(data);
		});
	});
	
	$(document.body).on('click', '.add-screening' ,function(){
		var list = $("#add-screening-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-screening.php";
		$.post(full_url, {add_screening:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
		alert("امتیازات با موفقیت ثبت شد");
	});
	
});