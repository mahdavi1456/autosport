<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-screening.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-coach.php";

if(isset($_POST['agecategory'])) {
	$gt_screening = new gt_screening();
	$list = $_POST['list'];
	parse_str($list, $new);
	$agecategory = $new['agecategory'];
	$set_date = $new['set_date'];
	$p_id = $new['p_id'];
    $national_id = $new['national_id'];
    if ($p_id == 0 && $national_id != "")
    {
        $db = new db();
        $p_id = $db->get_var_query("select ID from player where p_national_code = $national_id");
        $gt_screening->create_form($agecategory, $p_id, $set_date);
    }
    else if (($p_id != 0 && $national_id == "") || ($p_id != 0 && $national_id != ""))
    {
        $gt_screening->create_form($agecategory, $p_id, $set_date);
    }
    else{
        $gt_screening->create_form(null, $p_id, $set_date);
    }
	exit();
}

if(isset($_POST['add_screening'])) {
	$db = new db();
	$gt_screening = new gt_screening();
	$gt_date = new gt_date();
	$list = $_POST['list'];
	parse_str($list, $new);
	$p_id = $new['p_id'];
	$set_date1 = $new['set_date'];
	$set_date = $gt_date->jgdate($set_date1);
	$agecategory = $new['age'];
	$sql = $db->get_select_query("select * from screening_index where ages_id = $agecategory");
	if(count($sql) > 0) {
		foreach($sql as $row){
			$si_id = $row['ID'];
			$sql1 = $db->get_select_query("select * from parameter where pa_index = $si_id");
			if(count($sql1) > 0){
				foreach($sql1 as $row1){
					$id = $row1['ID'];
					$score = $new['score' . $id];
					$sql2 = $db->get_select_query("select * from screening where par_id = $id and p_id = $p_id and set_date = '$set_date'");
					if(count($sql2) > 0){
						if($score != ""){
                            $score = $score * $row['si_coefficient'];
							$db->ex_query("update screening set score = $score where par_id = $id and p_id = $p_id and set_date = '$set_date'");
						} else{
							$db->ex_query("delete from screening where par_id = $id and p_id = $p_id and set_date = '$set_date'");
						}
					} else{
						if($score != ""){
                            $score = $score * $row['si_coefficient'];
                            $db->ex_query("insert into screening(par_id, score, p_id, set_date) values($id, $score, $p_id, '$set_date')");
						}
					}
				}
			}
		}
	}
	$gt_screening->create_form($agecategory, $p_id, $set_date1);
	exit();
}

if(isset($_POST['set_agecategory'])) {
	$db = new db();
	$p_id = $_POST['p_id'];
	$p_ages = $db->get_var_query("select p_ages from player where ID = $p_id");
	echo $p_ages;
	exit();
}

if(isset($_POST['set_agecategory2'])) {
	$db = new db();
	$national_id = $_POST['national_id'];
	$p_ages = $db->get_var_query("select p_ages from player where p_national_code = $national_id");
	echo $p_ages;
	exit();
}