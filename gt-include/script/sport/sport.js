$(document).ready(function(){
	
	$(".add_player").click(function() {
		var addplayer = $("#addplayer *").serialize();
		var address = $('#address').val();
		$.post(address, {add_player:1, addplayer:addplayer}, function(data){
			$('.result').html(data);
		});
	});
	
	$("#update_player").click(function() {
		var id = $(this).data('id');
		var updateplayer = $("#updateplayer *").serialize();
		$.post("sport.php", {update_player:1, updateplayer:updateplayer, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$(document.body).on('click', '.remove_player' ,function(){
		var ID = $(this).data('id');
		var address = $('#address').val();
		$.post(address, {remove_player:1, ID:ID}, function(data){
			$('.result').html(data);
		});
	});
	
	$("#add_teacher").click(function() {
		var addteacher = $("#addteacher *").serialize();
		var address = $('#address').val();
		$.post(address, {add_teacher:1, addteacher:addteacher}, function(data){
			$('.result').html(data);
		});
	});
	
	$("#update_teacher").click(function() {
		var id = $(this).data('id');
		var updateteacher = $("#updateteacher").serialize();  
		$.post("sport.php", {update_teacher:1, updateteacher:updateteacher, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$(document.body).on('click', '.remove_teacher' ,function(){
		var ID = $(this).data('id');
		var address = $('#address').val();
		$.post(address, {remove_teacher:1, ID:ID}, function(data){
			$('.result').html(data);
		});
	});
	
	$("#add_station").click(function() {
		var addstation = $("#addstation *").serialize();
		var address = $('#address').val();
		$.post(address, {add_station:1, addstation:addstation}, function(data){
			$('.result').html(data);
		});
	});
	
	$("#update_station").click(function() {
		var id = $(this).data('id');
		var updatestation = $("#updatestation").serialize();  
		$.post("sport.php", {update_station:1, updatestation:updatestation, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$(document.body).on('click', '.remove_station' ,function(){
		var ID = $(this).data('id');
		var address = $('#address').val();
		$.post(address, {remove_station:1, ID:ID}, function(data){
			$('.result').html(data);
		});
	});
	
	$("#add_test_result").click(function() {
		var addtest_result = $("#addtest_result *").serialize();
		var address = $('#address').val();
		$.post(address, {add_test_result:1, addtest_result:addtest_result}, function(data){
			$('.result').html(data);
		});
	});
	
	$("#update_test_result").click(function() {
		var id = $(this).data('id');
		var updatetest_result = $("#updatetest_result").serialize();  
		$.post("sport.php", {update_test_result:1, updatetest_result:updatetest_result, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$("#remove_test_result").click(function() {
		var id = $(this).data('id');
		$.post("sport.php", {remove_test_result:1, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$(".set_date").click(function() {
		var mydate = $('#mydate').val();
		var day = $('#day').val();
		var address = $('#address').val();
		$.post(address, {set_food_plan:1, mydate:mydate, day:day}, function(data){
			$('.result').html(data);
		});
	});
	
	$(document.body).on('click', '.add_food_plan' ,function(){
		var addfood_plan = $("#addfood_plan *").serialize();
		var address = $('#address').val();
		var fp_i = $('#fp_i').val();
		$.post(address, {add_food_plan:1, addfood_plan:addfood_plan, fp_i:fp_i}, function(data){
			$('.result').html(data);
		});
		alert(data);
	});
});