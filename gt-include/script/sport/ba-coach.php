<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/engine.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-coach.php";

if(isset($_POST['add_coach'])) {
	$gt_coach = new gt_coach();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_coach->add_coach($new);
	$gt_coach->create_form();
	exit();
}

if(isset($_POST['update_coach_form'])){
	$gt_coach = new gt_coach();
	$ID = $_POST['ID'];
	$gt_coach->create_form($ID);
	exit();
}

if(isset($_POST['update_coach'])) {
	$gt_coach = new gt_coach();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_coach->update_coach($new);
	$gt_coach->create_form();
	exit();
}


if(isset($_POST['remove_coach'])){
	$gt_coach = new gt_coach();
	$ID = $_POST['ID'];
	$gt_coach->remove_coach($ID);
	$gt_coach->create_form();
	exit();
}

if(isset($_POST['edit_coach'])) {
	$gt_coach = new gt_coach();
	$db = new db();
	$ID = $_POST['ID'];
	$list = $_POST['list'];
	parse_str($list, $new);
	$new_username = $new['new_username'];
	$new_pass = $new['new_pass'];
	if($new_pass == ""){
		$sql = "update coach set c_username = '$new_username' where ID = $ID";
		$db->ex_query($sql);
	}
	else if($new_username == ""){
		$new_pass = md5($new_pass);
		$sql = "update coach set c_password = '$new_pass' where ID = $ID";
		$db->ex_query($sql);
	} else {
		$new_pass = md5($new_pass);
		$sql = "update coach set c_username = '$new_username', c_password = '$new_pass' where ID = $ID";
		$db->ex_query($sql);
	}
	$gt_coach->edit_profile_coach($ID);
	exit();
}

if(isset($_POST['remove_file'])){
	$gt_coach = new gt_coach();
	$db = new db();
	$c_id = $_POST['c_id'];
	$link = $db->get_var_query("select c_file from coach where ID = $c_id");
	$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $link;
	if(unlink($path)){
		$sql = "update coach set c_file = '' where ID = $c_id";
		$db->ex_query($sql);
	}
	$gt_coach->get_profile($c_id);
}

if(is_array($_FILES)) {
	if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
		$sourcePath = $_FILES['userImage']['tmp_name'];
		$filename = $_FILES['userImage']['name'];
		$filename_list = explode(".", $filename);
		$new_filename = time() . "." . $filename_list[1]; 
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $new_filename;
		if(move_uploaded_file($sourcePath, $targetPath)) {
			$db = new db();
			$c_id = $_POST['c_id'];
			$c_file = $db->get_var_query("select c_file from coach where ID = $c_id");
			if($c_file != ""){
				$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $c_file;
				if(unlink($path)){
				}
			}
			$sql = "update coach set c_file = '$new_filename' where ID = $c_id";
			$db->ex_query($sql);
			$gt_coach = new gt_coach();
			$gt_coach->get_profile($c_id);
		}
	}
}
