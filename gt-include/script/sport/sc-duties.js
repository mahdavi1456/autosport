$(document).ready(function(){
	
	$(document.body).on('click', '.update-duties-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-duties.php";
		$.post(full_url, {update_duties_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-duties' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-duties.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_duties:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
				$('.date-picker').persianDatepicker();
			});
		}
	});
	
	$(document.body).on('submit', '#uploadForm' ,function(e){
		e.preventDefault();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-duties-add.php";
		$.ajax({
			url: full_url,
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				$(".create-form").html(data);
			},
			error: function() 
			{
			}
	   });
	});
	
	$(document.body).on('submit', '#editForm' ,function(e){
		e.preventDefault();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-duties-update.php";
		$.ajax({
			url: full_url,
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				$(".create-form").html(data);
			},
			error: function() 
			{
			}
	   });
	});
	
});