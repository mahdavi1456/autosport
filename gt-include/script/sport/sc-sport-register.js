$(document).ready(function(){
	
	$(document.body).on('click', '.add-sport-register' ,function(){
		$('#list-sport-register-view-result').html("Loading...");
		var list = $("#add-sport-register-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-sport-register.php";
		$.post(full_url, {add_sport_register:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-sport-register-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-sport-register.php";
		$.post(full_url, {update_sport_register_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-sport-register' ,function(){
		$('#list-sport-register-view-result').html("Loading...");
		var list = $("#add-sport-register-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-sport-register.php";
		$.post(full_url, {update_sport_register:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.remove-sport-register' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-sport-register.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_sport_register:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
			});
		}
	});
	
});