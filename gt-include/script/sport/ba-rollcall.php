<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-rollcall.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";

if(isset($_POST['set_rollcall'])) {
	$db = new db();
	$gt_date = new gt_date();
	$gt_player = new gt_player();
	$mydate = $_POST['mydate'];
	$id = $_POST['id'];
	$newdate = $gt_date->jgdate($mydate);
    $pr = new prime();

    ?>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr colspan="18">
					<th colspan="1">#</th>
					<th colspan="2">نام بازیکن</th>
					<th colspan="1">رده سنی</th>
					<th colspan="1">حاضر</th>
					
					<th colspan="1">توضیحات</th>
				</tr>
				<?php
				if($id == ""){
					$list = $db->get_select_query("select * from player");
				} else {
					$list = $db->get_select_query("select * from player where ID = $id");
				}
				if(count($list)>0){
					foreach($list as $l){
						$p_id = $l['ID'];
						$sql = $db->get_select_query("select * from rollcall where rc_date = '$newdate' and p_id = $p_id");
						if(count($sql) > 0) {
							$rc_working_hour = $sql[0]['rc_working_hour'];
							$rc_details = $sql[0]['rc_details'];
						} else {
							$rc_working_hour = "";
							$rc_details = "";
						} ?>
						<tr colspan="18">
							<td colspan="1"><?php echo $pr->per_number($i); ?></td>
							<td colspan="2"><?php echo $gt_player->get_player_name($l['ID']); ?></td>
							<td colspan="1"><?php echo $gt_player->get_player_ages($l['ID']); ?></td>
							<td colspan="1">
								<input type="checkbox" name="rc_val<?php echo $p_id; ?>" class="rc-val" id="rc-val<?php echo $p_id; ?>" data-id="<?php echo $p_id; ?>" value="0">
							</td>
							
							<td colspan="1">
								<input type="text" name="rc_details<?php echo $p_id; ?>" style="width: 300px !important;">
							</td>
						</tr>
						<input type="hidden" name="rc_date" value="<?php echo $newdate; ?>">
						<?php
					}
				}?>
			</table>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-sm btn-success add-rollcall">ذخیره</button>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['add_rollcall'])) {
	$db = new db();
	$gt_rollcall = new gt_rollcall();
	parse_str($_POST['addrollcall'], $list);
	$list2 = $db->get_select_query("select * from player order by ID asc");
	if(count($list2)>0){
		foreach($list2 as $l){
			$p_id = $l['ID'];
			$rc_val = $list['rc_val' . $p_id];
			$rc_details = $list['rc_details' . $p_id];
			$rc_date = $list['rc_date'];
			//if($rc_working_hour != "") {
				$array = array();
				$array['p_id'] = $p_id;
				$array['rc_val'] = $rc_val;
				$array['rc_details'] = $rc_details;
				$array['rc_date'] = $rc_date;
				$gt_rollcall->add_rollcall($array);
			/*} else {
				$sql = $db->get_select_query("select * from rollcall where rc_date = '$rc_date' and p_id = $p_id");
				if(count($sql) > 0) {
					$db->ex_query("delete from rollcall where rc_date = '$rc_date' and p_id = $p_id");
				}
			}*/
		}
	}
	exit();
}