<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/engine.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-report.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-coach.php";

if (isset($_POST['player_report'])) {
    $gt_report = new gt_report();
    $list = $_POST['list'];
    parse_str($list, $new);
    $test_number = $new['test_number'];
    $p_id = $new['p_id'];
    $j = 0;
    $i = 1;
    $db = new db();
    $pr = new prime();
    $list = $db->get_select_query("select * from station order by ID asc");
    $stations_score_array = array();
    if (count($list) > 0) {
        foreach ($list as $l) {
            $res = $db->get_select_query("select * from test_result where p_id = $p_id and tr_number = $test_number and tr_turn = $i");

            if (count($res) > 0) {
                $j = 1;
                $total = 0;
                $t1 = $res[0]['tr_score1'] / $res[0]['tr_time1'];
                $t1 = round($t1);
                $t2 = $res[0]['tr_score2'] / $res[0]['tr_time2'];
                $t2 = round($t2);
                $t3 = $res[0]['tr_score3'] / $res[0]['tr_time3'];
                $t3 = round($t3);
                $t4 = $res[0]['tr_score4'] / $res[0]['tr_time4'];
                $t4 = round($t4);
                $t5 = $res[0]['tr_score5'] / $res[0]['tr_time5'];
                $t5 = round($t5);
                $total = ($t1) + ($t2) + ($t3) + ($t4) + ($t5);

                array_push($stations_score_array,$total);
            }
        }
    }

    $gt_report->create_form($p_id,$test_number,$stations_score_array);
    exit();
}