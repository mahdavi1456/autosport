$(document).ready(function(){
	
	$(document.body).on('click', '.add-screening-index' ,function(){
		$('#list-screening-index-view-result').html("Loading...");
		var list = $("#add-screening-index-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-screening-index.php";
		$.post(full_url, {add_screening_index:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-screening-index-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-screening-index.php";
		$.post(full_url, {update_screening_index_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-screening-index' ,function(){
		$('#list-screening-index-view-result').html("Loading...");
		var list = $("#add-screening-index-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-screening-index.php";
		$.post(full_url, {update_screening_index:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.remove-screening-index' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-screening-index.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_screening_index:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
			});
		}
	});
	
});