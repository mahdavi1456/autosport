$(document).ready(function(){
	
	$(document.body).on('click', '.set-date' ,function(){
		var mydate = $('#mydate').val();
		var id = $('#p_id').find(":selected").val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-rollcall.php";
		$.post(full_url, {set_rollcall:1, mydate:mydate, id:id}, function(data){
			$('.result').html(data);
			$(".date-picker").persianDatepicker();
			$('.select2').select2();
		});
	});

	$(document.body).on('click', '.add-rollcall' ,function(){
		var addrollcall = $("#addrollcall *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-rollcall.php";
		$.post(full_url, {add_rollcall:1, addrollcall:addrollcall}, function(data){
			$('.result').html(data);
			$(".date-picker").persianDatepicker();
			$('.select2').select2();
		});
		alert("اطلاعات با موفقیت ثبت شد");
	});
	
	$(document.body).on('click', '.rc-val' ,function(){
		var ID = $(this).data('id');
		var rcval = $('#rc-val' + ID).val();
		if(rcval == 0){
			$('#rc-val' + ID).val(1);
		} else{
			$('#rc-val' + ID).val(0);
		}
	});
	
});