$(document).ready(function(){
	
	$(document.body).on('click', '.add-player' ,function(){
		$('#list-player-view-result').html("Loading...");
		var list = $("#add-player-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-player.php";
		$.post(full_url, {add_player:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-player-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-player.php";
		$.post(full_url, {update_player_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-player' ,function(){
		$('#list-player-view-result').html("Loading...");
		var list = $("#add-player-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-player.php";
		$.post(full_url, {update_player:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-player' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-player.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_player:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
				$('.date-picker').persianDatepicker();
			});
		}
	});
	
	$(document.body).on('click', '.change-password' ,function(){
		$('#list-player-view-result').html("Loading...");
		var home_url = $('#home-url').val();
		var ID = $(this).data('id');
		var new_password = $('#new-password' + ID + ' *').serialize();
		alert(new_password);
		var full_url = home_url + "gt-include/script/sport/ba-player.php";
		$.post(full_url, {change_password:1, new_password:new_password, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('#myModal' + ID).modal('toggle');
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.edit-player' ,function(){
		var list = $("#add-player-view *").serialize();
		var home_url = $('#home-url').val();
		var ID = $(this).data('id');
		var full_url = home_url + "gt-include/script/sport/ba-player.php";
		$.post(full_url, {edit_player:1, list:list, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
		alert("تغییرات با موفقیت ذخیره شد");
	});
	
	$(document.body).on('click', '.remove-file' ,function(){
		var p_id = $(this).data('pid');
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$("#targetLayer").html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-player.php";
			$.post(full_url, {remove_file:1, p_id:p_id}, function(data) {
				$("#targetLayer").html(data);
			});
		}
	});
	
	$(".uploadForm").on('submit',(function(e) {
		$("#targetLayer").html("Loading...");
		e.preventDefault();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-player.php";
		$.ajax(
			{
				url: full_url,
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				{
					$("#targetLayer").html(data);
				},
				error: function() 
				{
					
				} 	        
			});
		})
	);
	
});