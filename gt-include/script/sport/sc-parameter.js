$(document).ready(function(){
	
	$(document.body).on('click', '.add-parameter' ,function(){
		$('#list-parameter-view-result').html("Loading...");
		var list = $("#add-parameter-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-parameter.php";
		$.post(full_url, {add_parameter:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-parameter-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-parameter.php";
		$.post(full_url, {update_parameter_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-parameter' ,function(){
		$('#list-parameter-view-result').html("Loading...");
		var list = $("#add-parameter-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-parameter.php";
		$.post(full_url, {update_parameter:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.remove-parameter' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-parameter.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_parameter:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
			});
		}
	});
	
});