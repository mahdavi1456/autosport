$(document).ready(function(){
	
	$(document.body).on('click', '.add-station' ,function(){
		$('#list-station-view-result').html("Loading...");
		var list = $("#add-station-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-station.php";
		$.post(full_url, {add_station:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-station-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-station.php";
		$.post(full_url, {update_station_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-station' ,function(){
		$('#list-station-view-result').html("Loading...");
		var list = $("#add-station-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-station.php";
		$.post(full_url, {update_station:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.remove-station' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-station.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_station:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
			});
		}
	});
	
});