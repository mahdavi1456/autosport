<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-player.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-teacher.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-station.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-test-result.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/sport/gt-food-plan.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";

if(isset($_POST['add_player'])) {
	$player = new gt_player();
	parse_str($_POST['addplayer'], $list);
	$id = $player->add_player($list);
	?>
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-condensed">
				<tr>
					<th>ردیف</th>
					<th>نام بازیکن</th>
					<th>نام پدر</th>
					<th>کد ملی</th>
					<th>پست</th>
					<th>نوع پا</th>
					<th>رده</th>
					<th>شماره پیراهن</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_player = new gt_player();
				$list = $db->get_select_query("select * from player order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $gt_player->get_player_name($l['ID']); ?></td>
							<td><?php echo $l['p_father_name']; ?></td>
							<td><?php echo $l['p_national_code']; ?></td>
							<td><?php echo $l['p_post']; ?></td>
							<td><?php echo $l['p_foot_type']; ?></td>
							<td><?php echo $l['p_category']; ?></td>
							<td><?php echo $l['p_shirt_number']; ?></td>
							<td>
								<a href="<?php ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</a>
								<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove_player" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="9">بدون بازیکن</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['update_player'])) {
	$player = new gt_player();
	$array = array();
	$updateplayer = $_POST['updateplayer'];
	$ID = $_POST['id'];
	$array[ID] = $ID;
	$arry = explode("&", $updateplayer);
	$arrlength = count($array);
	for($x = 0; $x < $arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$player->update_player($array);
	exit();
}

if(isset($_POST['remove_player'])) {
	$player = new gt_player();
	$ID = $_POST['ID'];
	$player->remove_player($ID);
	?>
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-condensed">
				<tr>
					<th>ردیف</th>
					<th>نام بازیکن</th>
					<th>نام پدر</th>
					<th>کد ملی</th>
					<th>پست</th>
					<th>نوع پا</th>
					<th>رده</th>
					<th>شماره پیراهن</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_player = new gt_player();
				$list = $db->get_select_query("select * from player order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $gt_player->get_player_name($l['ID']); ?></td>
							<td><?php echo $l['p_father_name']; ?></td>
							<td><?php echo $l['p_national_code']; ?></td>
							<td><?php echo $l['p_post']; ?></td>
							<td><?php echo $l['p_foot_type']; ?></td>
							<td><?php echo $l['p_category']; ?></td>
							<td><?php echo $l['p_shirt_number']; ?></td>
							<td>
								<a href="<?php ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</a>
								<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove_player" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="9">بدون بازیکن</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['add_teacher'])) {
	$teacher = new gt_teacher();
	parse_str($_POST['addteacher'], $list);
	$id = $teacher->add_teacher($list);
	?>
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-condensed">
				<tr>
					<th>ردیف</th>
					<th>نام مربی</th>
					<th>تاریخ تولد</th>
					<th>سطح</th>
					<th>باشگاه سابق</th>
					<th>موبایل</th>
					<th>آدرس</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_teacher = new gt_teacher();
				$list = $db->get_select_query("select * from teacher order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $gt_teacher->get_teacher_name($l['ID']); ?></td>
							<td><?php echo $l['t_birth']; ?></td>
							<td><?php echo $l['t_level']; ?></td>
							<td><?php echo $l['t_former_club']; ?></td>
							<td><?php echo $l['t_mobile']; ?></td>
							<td><?php echo $l['t_address']; ?></td>
							<td>
								<a href="<?php ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</a>
								<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove_teacher" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="8">بدون مربی</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['update_teacher'])) {
	$teacher = new gt_teacher();
	$array = array();
	$updateteacher = $_POST['updateteacher'];
	$ID = $_POST['id'];
	$array[ID] = $ID;
	$arry = explode("&", $updateteacher);
	$arrlength = count($array);
	for($x = 0; $x < $arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$teacher->update_teacher($array);
	exit();
}

if(isset($_POST['remove_teacher'])) {
	$teacher = new gt_teacher();
	$ID = $_POST['ID'];
	$teacher->remove_teacher($ID);
	?>
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-condensed">
				<tr>
					<th>ردیف</th>
					<th>نام مربی</th>
					<th>تاریخ تولد</th>
					<th>سطح</th>
					<th>باشگاه سابق</th>
					<th>موبایل</th>
					<th>آدرس</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_teacher = new gt_teacher();
				$list = $db->get_select_query("select * from teacher order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $gt_teacher->get_teacher_name($l['ID']); ?></td>
							<td><?php echo $l['t_birth']; ?></td>
							<td><?php echo $l['t_level']; ?></td>
							<td><?php echo $l['t_former_club']; ?></td>
							<td><?php echo $l['t_mobile']; ?></td>
							<td><?php echo $l['t_address']; ?></td>
							<td>
								<a href="<?php ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</a>
								<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove_teacher" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="8">بدون مربی</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['add_station'])) {
	$station = new gt_station();
	parse_str($_POST['addstation'], $list);
	$id = $station->add_station($list);
	?>
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-condensed">
				<tr>
					<th>ردیف</th>
					<th>نام ایستگاه</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$list = $db->get_select_query("select * from station order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $l['s_name']; ?></td>
							<td>
								<a href="<?php ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</a>
								<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove_station" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="4">بدون ایستگاه</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['update_station'])) {
	$station = new gt_station();
	$array = array();
	$updatestation = $_POST['updatestation'];
	$ID = $_POST['id'];
	$array[ID] = $ID;
	$arry = explode("&", $updatestation);
	$arrlength = count($array);
	for($x = 0; $x < $arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$station->update_station($array);
	exit();
}

if(isset($_POST['remove_station'])) {
	$station = new gt_station();
	$ID = $_POST['ID'];
	$station->remove_station($ID);
	?>
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-condensed">
				<tr>
					<th>ردیف</th>
					<th>نام ایستگاه</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$list = $db->get_select_query("select * from station order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $l['s_name']; ?></td>
							<td>
								<a href="<?php ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</a>
								<button data-id="<?php echo $l['ID']; ?>" class="btn btn-danger btn-xs remove_station" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="4">بدون ایستگاه</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['add_test_result'])) {
	$test_result = new gt_test_result();
	parse_str($_POST['addtest_result'], $list);
	$id = $test_result->add_test_result($list);
	echo "آزمون با موفقیت ثبت شد";
	exit();
}

if(isset($_POST['update_test_result'])) {
	$test_result = new gt_test_result();
	$array = array();
	$updatetest_result = $_POST['updatetest_result'];
	$ID = $_POST['id'];
	$array[ID] = $ID;
	$arry = explode("&", $updatetest_result);
	$arrlength = count($array);
	for($x = 0; $x < $arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$test_result->update_test_result($array);
	exit();
}

if(isset($_POST['remove_test_result'])) {
	$test_result = new gt_test_result();
	$ID = $_POST['ID'];
	$test_result->remove_test_result($ID);
	exit();
}

if(isset($_POST['set_food_plan'])) {
	$db = new db();
	$gt_date = new gt_date();
	$mydate = $_POST['mydate'];
	$day = $_POST['day'];
	$newdate = $gt_date->jgdate($mydate);
	?>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>#</th>
					<th>تاریخ</th>
					<th>صبحانه</th>
					<th>ساعت صبحانه</th>
					<th>میان وعده</th>
					<th>ساعت میان وعده</th>
					<th>ناهار</th>
					<th>ساعت ناهار</th>
					<th>عصرانه</th>
					<th>ساعت عصرانه</th>
					<th>شام</th>
					<th>ساعت شام</th>
				</tr>
				<?php
				for($i=1 ; $i <= $day ; $i++) { ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $gt_date->jgdate($newdate);; ?></td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'صبحانه'");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp1_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td>
							<input type="text" name="fp1_time<?php echo $i; ?>" value="<?php echo $time; ?>">
						</td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'میان وعده'");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp2_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td>
							<input type="text" name="fp2_time<?php echo $i; ?>" value="<?php echo $time; ?>">
						</td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'ناهار'");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp3_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td>
							<input type="text" name="fp3_time<?php echo $i; ?>" value="<?php echo $time; ?>">
						</td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'عصرانه'");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp4_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td>
							<input type="text" name="fp4_time<?php echo $i; ?>" value="<?php echo $time; ?>">
						</td>
						<td>
							<?php 
							$sql = $db->get_select_query("select * from food_plan where fp_date = '$newdate' and fp_type= 'شام'");
							if(count($sql) > 0) {
								$name = $sql[0]['fp_name'];
								$time = $sql[0]['fp_time'];
							} else {
								$name = "";
								$time = "";
							} ?>
							<input type="text" name="fp5_name<?php echo $i; ?>" value="<?php echo $name; ?>">
						</td>
						<td>
							<input type="text" name="fp5_time<?php echo $i; ?>" value="<?php echo $time; ?>">
						</td>
					</tr>
					<input type="hidden" name="fp_date<?php echo $i; ?>" value="<?php echo $newdate; ?>">
					<?php
					$newdate1 = strtotime ('+1 day', strtotime ($newdate)) ;
					$newdate = date ('Y-m-d' , $newdate1); 
				} ?>
				<input type="hidden" id="fp_i" value="<?php $i = $i - 1; echo $i; ?>">
			</table>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-sm btn-success add_food_plan">ذخیره</button>
		</div>
	</div>
	<?php
	exit();
}

if(isset($_POST['add_food_plan'])) {
	$db = new db();
	$gt_food_plan = new gt_food_plan();
	$fp_i = $_POST['fp_i'];
	parse_str($_POST['addfood_plan'], $list);
	for($i = 1 ; $i <= $fp_i ; $i++) {
		$fp1_name = $list['fp1_name' . $i];
		$fp2_name = $list['fp2_name' . $i];
		$fp3_name = $list['fp3_name' . $i];
		$fp4_name = $list['fp4_name' . $i];
		$fp5_name = $list['fp5_name' . $i];
		$fp_date = $list['fp_date' . $i];
		$fp1_time = $list['fp1_time' . $i];
		$fp2_time = $list['fp2_time' . $i];
		$fp3_time = $list['fp3_time' . $i];
		$fp4_time = $list['fp4_time' . $i];
		$fp5_time = $list['fp5_time' . $i];
		$fp_date = $list['fp_date' . $i];
		$p_id = $list['p_id'];
		$res3 = $db->get_select_query("select * from food_plan where fp_date = '$fp_date'");
		if(count($res3) > 0) {
			$db->ex_query("delete from food_plan where fp_date = '$fp_date'");
		}
		if($fp1_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "صبحانه";
			$array['fp_name'] = $fp1_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp1_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
		if($fp2_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "میان وعده";
			$array['fp_name'] = $fp2_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp2_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
		if($fp3_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "ناهار";
			$array['fp_name'] = $fp3_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp3_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
		if($fp4_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "عصرانه";
			$array['fp_name'] = $fp4_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp4_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
		if($fp5_name != "") {
			$array = array();
			$array['p_id'] = $p_id;
			$array['fp_type'] = "شام";
			$array['fp_name'] = $fp5_name;
			$array['fp_date'] = $fp_date;
			$array['fp_time'] = $fp5_time;
			$sql = $gt_food_plan->add_food_plan($array);
		}
	}
	exit();
}