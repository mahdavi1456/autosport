$(document).ready(function () {

    $(document.body).on('click', '.add-untropo', function () {
        $('#list-untropo-view-result').html("Loading...");
        var list = $("#add-untropo-view *").serialize();
        var home_url = $('#home-url').val();
        var full_url = home_url + "gt-include/script/sport/ba-untropo.php";
        $.post(full_url, {add_untropo: 1, list: list}, function (data) {
            $('.create-form').html(data);
            $('.select2').select2();
        });
    });

    $(document.body).on('click', '.update-untropo-form', function () {
        $('.create-form').html("Loading...");
        var ID = $(this).data('id');
        var home_url = $('#home-url').val();
        var full_url = home_url + "gt-include/script/sport/ba-untropo.php";
        $.post(full_url, {update_untropo_form: 1, ID: ID}, function (data) {
            $('.create-form').html(data);
            $('.select2').select2();
        });
    });

    $(document.body).on('click', '.update-untropo', function () {
        $('#list-untropo-view-result').html("Loading...");
        var list = $("#add-untropo-view *").serialize();
        var home_url = $('#home-url').val();
        var full_url = home_url + "gt-include/script/sport/ba-untropo.php";
        $.post(full_url, {update_untropo: 1, list: list}, function (data) {
            $('.create-form').html(data);
            $('.select2').select2();
        });
    });

    $(document.body).on('click', '.remove-untropo', function () {
        var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
        if (h == true) {
            $('.create-form').html("Loading...");
            var home_url = $('#home-url').val();
            var full_url = home_url + "gt-include/script/sport/ba-untropo.php";
            var ID = $(this).data('id');
            $.post(full_url, {remove_untropo: 1, ID: ID}, function (data) {
                $('.create-form').html(data);
                $('.select2').select2();
            });
        }
    });

	function getBmiType(number) {
		if (number < 18) {
			document.getElementById("group_bmi").style.backgroundColor = "yellow";
			document.getElementById("group_bmi").style.color =  "black";
			return 'لاغر پیکر';
		} else if (number <= 22 && number >= 18) {
			document.getElementById("group_bmi").style.backgroundColor =  "green";
			document.getElementById("group_bmi").style.color =  "white";
			return 'استاندارد';
		} else if (number <= 25 && number >= 22) {
			document.getElementById("group_bmi").style.backgroundColor =  "orange";
			document.getElementById("group_bmi").style.color =  "white";
			return 'اضافه وزن';
		} else if (number > 25) {
			document.getElementById("group_bmi").style.backgroundColor =  "red";
			document.getElementById("group_bmi").style.color =  "white";
			return 'چاق';
		}
	}

    $(document.body).on('keyup', '#u-height', function () {
        var u_height = $('#u-height').val();
        var u_wieght = $('#u-wieght').val();
        var home_url = $('#home-url').val();
        var full_url = home_url + "gt-include/script/sport/ba-untropo.php";
        $.post(full_url, {update_bmi: 1, u_height: u_height, u_wieght: u_wieght}, function (data) {
            $('#u-bmi').val(data);
            $('#group_bmi').val(getBmiType(data));

        });
    });


    $(document.body).on('keyup', '#u-wieght', function () {
        var u_height = $('#u-height').val();
        var u_wieght = $('#u-wieght').val();
        var home_url = $('#home-url').val();
        var full_url = home_url + "gt-include/script/sport/ba-untropo.php";
        $.post(full_url, {update_bmi: 1, u_height: u_height, u_wieght: u_wieght}, function (data) {
            $('#u-bmi').val(data);
            $('#group_bmi').val(getBmiType(data));
		});

	});
});

