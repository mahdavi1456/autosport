$(document).ready(function(){
	
	$(document.body).on('click', '.add-ages' ,function(){
		$('#list-ages-view-result').html("Loading...");
		var list = $("#add-ages-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-ages.php";
		$.post(full_url, {add_ages:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-ages-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-ages.php";
		$.post(full_url, {update_ages_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-ages' ,function(){
		$('#list-ages-view-result').html("Loading...");
		var list = $("#add-ages-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/sport/ba-ages.php";
		$.post(full_url, {update_ages:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.remove-ages' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/sport/ba-ages.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_ages:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
			});
		}
	});
	
});