$(document).ready(function(){
	$("#add_province").click(function(){
		var addprovince = $("").text($("#addprovince").serialize());  
		$.post("province.php", {add_province:1, addprovince:addprovince}, function(data){
			$('').html(data);
		});
	});
	
	$("#update_province").click(function(){
		var id = $(this).data('id');
		var updateprovince = $("").text($("#updateprovince").serialize());  
		$.post("province.php", {update_province:1, updateprovince:updateprovince, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$("#remove_province").click(function(){
		var id = $(this).data('id');
		$.post("province.php", {remove_province:1, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$(document.body).on('change', '.province_id' ,function(){
		var province_id = $('.province_id').find(":selected").val();
		$.post("province.php", {select_province:1, province_id:province_id}, function(data){
			$('#result_province').html(data);
		});
	});
});