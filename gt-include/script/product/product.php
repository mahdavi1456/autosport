<?php
if(isset($_POST['add_product'])){
	$product = new gt_product();
	$addproduct = $_POST['addproduct'];
	$arry = explode("&", $addproduct);
	$array = array();
	$arrlength=count($arry);
	for($x=0; $x<$arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$id = $product->add_product($array);
	exit();
}

if(isset($_POST['update_product'])){
	$product = new gt_product();
	$array = array();
	$updateproduct = $_POST['updateproduct'];
	$id = $_POST['id'];
	$array[id] = $id;
	$arry = explode("&", $updateproduct);
	$arrlength=count($array);

	for($x=0; $x<$arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$product->update_product($array);
	exit();
}

if(isset($_POST['remove_product'])){
	$product = new gt_product();
	$id = $_POST['id'];
	$product->remove_product($id);
	exit();
}

if(isset($_POST['add_category'])){
	$category = new gt_category_product();
	$addcategory = $_POST['addcategory'];
	$arry = explode("&", $addcategory);
	$array = array();
	$arrlength=count($array);
	for($x=0; $x<$arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$id = $category->add_category_product($array);
	exit();
}

if(isset($_POST['update_category'])){
	$category = new gt_category_product();
	$array = array();
	$updatecategory = $_POST['updatecategory'];
	$id = $_POST['id'];
	$array[id] = $id;
	$arry = explode("&", $updatecategory);
	$arrlength=count($array);

	for($x=0; $x<$arrlength; $x++)
	{
		$row = $arry[$x];
		$ar = explode("=", $row);
		$name = $ar[0];
		$value = $ar[1];
		$array[$name] = $value;
	}
	$category->update_category_product($array);
	exit();
}

if(isset($_POST['remove_category'])){
	$category = new gt_category_product();
	$id = $_POST['id'];
	$category->remove_category_product($id);
	exit();
}