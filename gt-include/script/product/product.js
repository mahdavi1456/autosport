$(document).ready(function(){
	$("#add_product").click(function(){
		var addproduct = $("").text($("#addproduct").serialize());  
		$.post("product.php", {add_product:1, addproduct:addproduct}, function(data){
			$('').html(data);
		});
	});
	
	$("#update_product").click(function(){
		var id = $(this).data('id');
		var updateproduct = $("").text($("#updateproduct").serialize());  
		$.post("product.php", {update_product:1, updateproduct:updateproduct, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$("#remove_product").click(function(){
		var id = $(this).data('id');
		$.post("product.php", {remove_product:1, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$("#add_category").click(function(){
		var addcategory = $("").text($("#addcategory").serialize());  
		$.post("product.php", {add_category:1, addcategory:addcategory}, function(data){
			$('').html(data);
		});
	});
	
	$("#update_category").click(function(){
		var id = $(this).data('id');
		var updatecategory = $("").text($("#updatecategory").serialize());  
		$.post("product.php", {update_category:1, updatecategory:updatecategory, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$("#remove_category").click(function(){
		var id = $(this).data('id');
		$.post("product.php", {remove_category:1, id:id}, function(data){
			$('').html(data);
		});
	});
});