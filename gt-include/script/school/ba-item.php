<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-item.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_item'])) {
	$gt_item = new gt_item();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_item->add_item($new);
	$gt_item->list_item_view();
	exit();
}

if(isset($_POST['update_item_form'])){
	$gt_item = new gt_item();
	$ID = $_POST['ID'];
	$gt_item->create_form($ID);
	exit();
}

if(isset($_POST['update_item'])) {
	$gt_item = new gt_item();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_item->update_item($new);
	$gt_item->list_item_view();
	exit();
}


if(isset($_POST['remove_item'])){
	$gt_item = new gt_item();
	$ID = $_POST['ID'];
	$gt_item->remove_item($ID);
	$gt_item->list_item_view();
	exit();
}