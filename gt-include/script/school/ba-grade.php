<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_grade'])) {
	$gt_grade = new gt_grade();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_grade->add_grade($new);
	$gt_grade->create_form();
	exit();
}

if(isset($_POST['update_grade_form'])){
	$gt_grade = new gt_grade();
	$ID = $_POST['ID'];
	$gt_grade->create_form($ID);
	exit();
}

if(isset($_POST['update_grade'])) {
	$gt_grade = new gt_grade();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_grade->update_grade($new);
	$gt_grade->create_form();
	exit();
}


if(isset($_POST['remove_grade'])){
	$gt_grade = new gt_grade();
	$ID = $_POST['ID'];
	$gt_grade->remove_grade($ID);
	$gt_grade->create_form();
	exit();
}