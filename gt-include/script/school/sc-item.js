$(document).ready(function(){
	
	$(document.body).on('click', '.add-item' ,function(){
		$('#list-item-view-result').html("Loading...");
		var list = $("#add-item-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-item.php";
		$.post(full_url, {add_item:1, list:list}, function(data) {
			$('#list-item-view-result').html(data);
		});
	});
	
	$(document.body).on('click', '.update-item-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-item.php";
		$.post(full_url, {update_item_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
		});
	});
	
	$(document.body).on('click', '.update-item' ,function(){
		$('#list-item-view-result').html("Loading...");
		var list = $("#add-item-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-item.php";
		$.post(full_url, {update_item:1, list:list}, function(data) {
			$('#list-item-view-result').html(data);
		});
	});
	
	$(document.body).on('click', '.remove-item' ,function(){
		$('#list-item-view-result').html("Loading...");
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-item.php";
		var ID = $(this).data('id');
		$.post(full_url, {remove_item:1, ID:ID }, function(data) {
			$('#list-item-view-result').html(data);
		});
	});
	
});