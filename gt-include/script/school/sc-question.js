$(document).ready(function(){
	
	$(document.body).on('click', '.add-question' ,function(){
		$('#list-question-view-result').html("Loading...");
		var list = $("#add-question-view *").serialize();
		var home_url = $('#home-url').val();
		var EID = $(this).data('eid');
		var full_url = home_url + "gt-include/script/school/ba-question.php";
		$.post(full_url, {add_question:1, list:list, EID:EID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-question-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var EID = $(this).data('eid');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-question.php";
		$.post(full_url, {update_question_form:1, ID:ID, EID:EID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-question' ,function(){
		$('#list-question-view-result').html("Loading...");
		var list = $("#add-question-view *").serialize();
		var home_url = $('#home-url').val();
		var EID = $(this).data('eid');
		var full_url = home_url + "gt-include/script/school/ba-question.php";
		$.post(full_url, {update_question:1, list:list, EID:EID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-question' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('#list-question-view-result').html("Loading...");
			var home_url = $('#home-url').val();
			var EID = $(this).data('eid');
			var full_url = home_url + "gt-include/script/school/ba-question.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_question:1, ID:ID, EID:EID}, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
				$('.date-picker').persianDatepicker();
			});
		}
	});
	
	$(document.body).on('click', '.open-design-box' ,function(){
		$('.design-question-box').hide();
		var id = $(this).data('id');
		$('#design-question-box' + id).show();
		$('html, body').animate({
			scrollTop: $('#design-question-box' + id).offset().top - 100
		}, 1000);
	});
	
	$(document.body).on('click', '.open-option-box' ,function(){
		$('.option-question-box').hide();
		var id = $(this).data('id');
		$('#option-question-box' + id).show();
		$('html, body').animate({
			scrollTop: $('#option-question-box' + id).offset().top - 100
		}, 1000);
	});
	
	$(document.body).on('blur', '.q_content' ,function(){
		var id = $(this).data('id');
		var v = $(this).val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-question.php";
		$.post(full_url, {update_q_content:1, id:id, v:v} );
	});
	
	$(document.body).on('blur', '.correct_answer' ,function(){
		var id = $(this).data('id');
		var v = $(this).val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-question.php";
		$.post(full_url, {update_correct_answer:1, id:id, v:v} );
	});
	
	$(document.body).on('blur', '.q_opt' ,function(){
		var id = $(this).data('id');
		var num = $(this).data('num');
		var v = $(this).val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-question.php";
		$.post(full_url, {update_q_opt:1, id:id, v:v, num:num});
	});
	
	$(".uploadForm").on('submit',(function(e) {
		var q_id = $(this).find('.q_id').val();
		$("#targetLayer" + q_id).html("Loading...");
		e.preventDefault();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-question.php";
		$.ajax(
			{
				url: full_url,
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				{
					$("#targetLayer" + q_id).html(data);
				},
				error: function() 
				{
					
				} 	        
			});
		})
	);
	
	$(document.body).on('click', '.remove-file' ,function(){
		var qid = $(this).data('qid');
		$("#targetLayer" + qid).html("Loading...");
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/school/ba-question.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_file:1, ID:ID, qid:qid}, function(data) {
				$("#targetLayer" + qid).html(data);
			});
		}
	});
	
	$(".upload_item_form").on('submit',(function(e) {
		e.preventDefault();
		var q_id = $(this).find('.q_id').val();
		var opt_id = $(this).find('.opt_id').val();
		$("#item_target_layer" + q_id + "-" + opt_id).html("Loading...");
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-question.php";
		$.ajax(
			{
				url: full_url,
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				{
					$("#item_target_layer" + q_id + "-" + opt_id).html(data);
				},
				error: function() 
				{
					
				} 	        
			});
		})
	);
	
});