<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-teacher.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_teacher'])) {
	$gt_teacher = new gt_teacher();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_teacher->add_teacher($new);
	$gt_teacher->create_form();
	exit();
}

if(isset($_POST['update_teacher_form'])){
	$gt_teacher = new gt_teacher();
	$ID = $_POST['ID'];
	$gt_teacher->create_form($ID);
	exit();
}

if(isset($_POST['update_teacher'])) {
	$gt_teacher = new gt_teacher();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_teacher->update_teacher($new);
	$gt_teacher->create_form();
	exit();
}


if(isset($_POST['remove_teacher'])){
	$gt_teacher = new gt_teacher();
	$ID = $_POST['ID'];
	$gt_teacher->remove_teacher($ID);
	$gt_teacher->create_form();
	exit();
}

if(isset($_POST['change_password'])){
	$gt_teacher = new gt_teacher();
	$ID = $_POST['ID'];
	$new_password = $_POST['new_password'];
	//$new_password = md5($new_password);
	//$gt_teacher->change_password($ID, $new_password);
	//$gt_teacher->create_form();
	echo $new_password;
	exit();
}