$(document).ready(function(){
	
	$(document.body).on('click', '.add-teacher' ,function(){
		$('#list-teacher-view-result').html("Loading...");
		var list = $("#add-teacher-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-teacher.php";
		$.post(full_url, {add_teacher:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-teacher-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-teacher.php";
		$.post(full_url, {update_teacher_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-teacher' ,function(){
		$('#list-teacher-view-result').html("Loading...");
		var list = $("#add-teacher-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-teacher.php";
		$.post(full_url, {update_teacher:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-teacher' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/school/ba-teacher.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_teacher:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
				$('.date-picker').persianDatepicker();
			});
		}
	});
	
	$(document.body).on('click', '.change-password' ,function(){
		$('#list-teacher-view-result').html("Loading...");
		var home_url = $('#home-url').val();
		var ID = $(this).data('id');
		var new_password = $('#new-password' + ID + ' *').serialize();
		alert(new_password);
		var full_url = home_url + "gt-include/script/school/ba-teacher.php";
		$.post(full_url, {change_password:1, new_password:new_password, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('#myModal' + ID).modal('toggle');
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
});