<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-exam.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-answer.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-question.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-teacher.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-lesson.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade-year.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-classroom.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";

if(isset($_POST['add_exam'])) {
	$gt_exam = new gt_exam();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_exam->add_exam($new);
	$gt_exam->create_form();
	exit();
}

if(isset($_POST['update_exam_form'])){
	$gt_exam = new gt_exam();
	$ID = $_POST['ID'];
	$gt_exam->create_form($ID);
	exit();
}

if(isset($_POST['update_exam'])) {
	$gt_exam = new gt_exam();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_exam->update_exam($new);
	$gt_exam->create_form();
	exit();
}


if(isset($_POST['remove_exam'])){
	$gt_exam = new gt_exam();
	$ID = $_POST['ID'];
	$gt_exam->remove_exam($ID);
	$gt_exam->create_form();
	exit();
}

if(isset($_POST['next_question'])){
	$gt_exam = new gt_exam();
	$exam_id = $_POST['exam_id'];
	$next = $_POST['next'];
	$gt_exam->do_exam($exam_id, $next);
	exit();
}

if(isset($_POST['update_answer_content'])){
	$gt_answer = new gt_answer();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_answer->add_answer($new);
	exit();
}

if(isset($_POST['update_answer_score'])){
	$gt_exam = new gt_exam();
	$gt_answer = new gt_answer();
	$pr = new prime();
	$exam_id = $_POST['eid'];
	$st_id = $_POST['stid'];
	$ID = $_POST['ID'];
	$score = $_POST['score'];
	$gt_answer->update_answer_score($ID, $score);
	$exam_score = $gt_exam->get_exam_score($exam_id, $st_id);
	echo $pr->per_number($exam_score);
	exit();
}

if(is_array($_FILES)) {
	if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
		$sourcePath = $_FILES['userImage']['tmp_name'];
		$filename = $_FILES['userImage']['name'];
		$filename_list = explode(".", $filename);
		$new_filename = time() . "." . $filename_list[1]; 
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/answer/" . $new_filename;
		if(move_uploaded_file($sourcePath, $targetPath)) {
			$db = new db();
			$gt_date = new gt_date();
			$q_id = $_POST['q_id'];
			$st_id = $_POST['st_id'];
			$a_date = $_POST['a_date'];
			$a_date = $gt_date->jgdate($a_date);
			$a_time = $_POST['a_time'];
			$a_time = $gt_date->PersianToEnglish($a_time);
			$ip = $_POST['ip'];
			$mac = $_POST['mac'];
			$sql = "insert into answer_media(am_file) values('$new_filename')";
			$answer = $db->ex_query($sql);
			$a_type = 1;
			$st_score = 0;
			$db->ex_query("insert into answer(q_id, answer, st_id, st_score, a_date, a_time, ip, mac, a_type) values($q_id, '$answer', $st_id, '$st_score', '$a_date', '$a_time', '$ip', '$mac', $a_type)");
			$gt_exam = new gt_exam();
			$gt_exam->get_media_answer($q_id, $st_id);
		}
	}
}