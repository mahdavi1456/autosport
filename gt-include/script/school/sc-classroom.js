$(document).ready(function(){
	
	$(document.body).on('click', '.add-classroom' ,function(){
		$('#list-classroom-view-result').html("Loading...");
		var list = $("#add-classroom-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-classroom.php";
		$.post(full_url, {add_classroom:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-classroom-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-classroom.php";
		$.post(full_url, {update_classroom_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-classroom' ,function(){
		$('#list-classroom-view-result').html("Loading...");
		var list = $("#add-classroom-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-classroom.php";
		$.post(full_url, {update_classroom:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-classroom' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/school/ba-classroom.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_classroom:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
				$('.date-picker').persianDatepicker();
			});
		}
	});
	
});