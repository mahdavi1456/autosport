<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-school.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-teacher.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_school'])) {
	$gt_school = new gt_school();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_school->add_school($new);
	$gt_school->create_form();
	exit();
}

if(isset($_POST['update_school_form'])){
	$gt_school = new gt_school();
	$ID = $_POST['ID'];
	$gt_school->create_form($ID);
	exit();
}

if(isset($_POST['update_school'])) {
	$gt_school = new gt_school();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_school->update_school($new);
	$gt_school->create_form();
	exit();
}


if(isset($_POST['remove_school'])){
	$gt_school = new gt_school();
	$ID = $_POST['ID'];
	$gt_school->remove_school($ID);
	$gt_school->create_form();
	exit();
}