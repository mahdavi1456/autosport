<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-question.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-teacher.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-exam.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";

if(isset($_POST['add_question'])) {
	$gt_question = new gt_question();
	$list = $_POST['list'];
	$EID = $_POST['EID'];
	parse_str($list, $new);
	$gt_question->add_question($new);
	$gt_question->create_form($EID);
	exit();
}

if(isset($_POST['update_question_form'])){
	$gt_question = new gt_question();
	$ID = $_POST['ID'];
	$EID = $_POST['EID'];
	$gt_question->create_form($EID, $ID);
	exit();
}

if(isset($_POST['update_question'])) {
	$gt_question = new gt_question();
	$list = $_POST['list'];
	$EID = $_POST['EID'];
	parse_str($list, $new);
	$gt_question->update_question($new);
	$gt_question->create_form($EID);
	exit();
}


if(isset($_POST['remove_question'])){
	$gt_question = new gt_question();
	$ID = $_POST['ID'];
	$EID = $_POST['EID'];
	$e_id = $_GET['exam_id'];
	$gt_question->remove_question($ID);
	$gt_question->create_form($EID);
	exit();
}

if(isset($_POST['remove_file'])){
	$gt_question = new gt_question();
	$db = new db();
	$ID = $_POST['ID'];
	$q_id = $_POST['qid'];
	$link = $db->get_var_query("select filename from question_media where ID = $ID");
	$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/question/" . $link;
	if(unlink($path)){
		$sql = "delete from question_media where ID = $ID";
		$db->ex_query($sql);
	}
	$gt_question->get_media($q_id);
}

if(isset($_POST['update_q_content'])){
	$db = new db();
	$id = $_POST['id'];
	$v = $_POST['v'];
	$sql = "update question set q_content = '$v' where ID = $id";
	$db->ex_query($sql);
	exit();
}

if(isset($_POST['update_correct_answer'])){
	$db = new db();
	$id = $_POST['id'];
	$v = $_POST['v'];
	$sql = "update question set correct_answer = '$v' where ID = $id";
	$db->ex_query($sql);
	exit();
}

if(isset($_POST['update_q_opt'])){
	$db = new db();
	$id = $_POST['id'];
	$v = $_POST['v'];
	$num = $_POST['num'];
	$field_name = "q_opt" . $num;
	$sql = "update question set $field_name = '$v' where ID = $id";
	$db->ex_query($sql);
	exit();
}

if(is_array($_FILES)) {
	if(is_uploaded_file($_FILES['userImage']['tmp_name']) && $_POST['type'] == "question") {
		$sourcePath = $_FILES['userImage']['tmp_name'];
		$filename = $_FILES['userImage']['name'];
		$filename_list = explode(".", $filename);
		$new_filename = time() . "." . $filename_list[1]; 
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/question/" . $new_filename;
		if(move_uploaded_file($sourcePath, $targetPath)) {
			$db = new db();
			$q_id = $_POST['q_id'];
			$sql = "insert into question_media(q_id, filename) values($q_id, '$new_filename')";
			$db->ex_query($sql);
			$gt_question = new gt_question();
			$gt_question->get_media($q_id);
		}
	}
	
	if(is_uploaded_file($_FILES['item_image']['tmp_name']) && $_POST['type'] == "item") {
		$sourcePath = $_FILES['item_image']['tmp_name'];
		$filename = $_FILES['item_image']['name'];
		$filename_list = explode(".", $filename);
		$new_filename = time() . "." . $filename_list[1]; 
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/item/" . $new_filename;
		if(move_uploaded_file($sourcePath, $targetPath)) {
			$db = new db();
			$q_id = $_POST['q_id'];
			$opt_id = $_POST['opt_id'];
			$field_name = "q_opt_src" . $opt_id;
			$sql = "update question set $field_name = '$new_filename' where ID = $q_id";
			$db->ex_query($sql);
			$gt_question = new gt_question();
			$gt_question->get_item_media($q_id, $opt_id);
		}
	}
}