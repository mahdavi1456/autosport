$(document).ready(function(){
	
	$(document.body).on('click', '.add-student' ,function(){
		$('#list-student-view-result').html("Loading...");
		var list = $("#add-student-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-student.php";
		$.post(full_url, {add_student:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-student-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-student.php";
		$.post(full_url, {update_student_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-student' ,function(){
		$('#list-student-view-result').html("Loading...");
		var list = $("#add-student-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-student.php";
		$.post(full_url, {update_student:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-student' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/school/ba-student.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_student:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
				$('.date-picker').persianDatepicker();
			});
		}
	});
	
	$(document.body).on('click', '.change-password' ,function(){
		$('#list-student-view-result').html("Loading...");
		var home_url = $('#home-url').val();
		var ID = $(this).data('id');
		var new_password = $('#new-password' + ID + ' *').serialize();
		alert(new_password);
		var full_url = home_url + "gt-include/script/school/ba-student.php";
		$.post(full_url, {change_password:1, new_password:new_password, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('#myModal' + ID).modal('toggle');
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-file' ,function(){
		var st_id = $(this).data('stid');
		$("#targetLayer").html("Loading...");
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/school/ba-student.php";
			$.post(full_url, {remove_file:1, st_id:st_id}, function(data) {
				$("#targetLayer").html(data);
			});
		}
	});
	
	$(".uploadForm").on('submit',(function(e) {
		$("#targetLayer").html("Loading...");
		e.preventDefault();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-student.php";
		$.ajax(
			{
				url: full_url,
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				{
					$("#targetLayer").html(data);
				},
				error: function() 
				{
					
				} 	        
			});
		})
	);
	
});