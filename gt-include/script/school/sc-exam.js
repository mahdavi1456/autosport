$(document).ready(function(){

	$(document.body).on('click', '.add-exam' ,function(){
		$('#list-exam-view-result').html("Loading...");
		var list = $("#add-exam-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.post(full_url, {add_exam:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-exam-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.post(full_url, {update_exam_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-exam' ,function(){
		$('#list-exam-view-result').html("Loading...");
		var list = $("#add-exam-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.post(full_url, {update_exam:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-exam' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('#list-exam-view-result').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/school/ba-exam.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_exam:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
				$('.date-picker').persianDatepicker();
			});
		}
	});
	
	$(document.body).on('click', '#next-question' ,function(){
		$('#next-question-result').html("Loading...");
		var next = $(this).data('next');
		var exam_id = $(this).data('exam_id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.post(full_url, {next_question:1, next:next, exam_id:exam_id}, function(data){
			$('#next-question-result').html(data);
		});
	});
	
	$(document.body).on('click', '#prev-question' ,function(){
		$('#next-question-result').html("Loading...");
		var next = $(this).data('next');
		var exam_id = $(this).data('exam_id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.post(full_url, {next_question:1, next:next, exam_id:exam_id}, function(data){
			$('#next-question-result').html(data);
		});
	});
	
	$(document.body).on('blur', '.answer_content' ,function(){
		var list = $("#add-answer-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.post(full_url, {update_answer_content:1, list:list}, function(data){
		});
	});
	
	$(document.body).on('change', 'input[type="radio"]' ,function(){
		var list = $("#add-answer-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.post(full_url, {update_answer_content:1, list:list}, function(data){
		});
	});
	
	$(document.body).on('keyup', '.score' ,function(){
		var ID = $(this).data('id');
		var eid = $(this).data('eid');
		var stid = $(this).data('stid');
		var score = $(this).val();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.post(full_url, {update_answer_score:1, ID:ID, score:score, eid:eid, stid:stid}, function(data){
			$('#result-score').html(data);
		});
	});
	
	$(document.body).on('click', '.participation' ,function(){
		alert("شما مجاز به شرکت در آزمون نمی باشید.");
	});
	
	$("#uploadanswerForm").on('submit',(function(e) {
		e.preventDefault();
		alert("aaaaaa");
		$("#targetLayer").html("Loading...");
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-exam.php";
		$.ajax(
			{
				url: full_url,
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				{
					$("#targetLayer").html(data);
				},
				error: function() 
				{
					
				} 	        
			});
		})
	);

	var timer = $('#timer').html();
	setInterval(
		function(){
			var timer = $('#timer').html();
			timer--;
			$('#timer').html(timer);
			if(timer == -1) {
				$('#next-question').click();
			}
		}, 1000
	);

	
});