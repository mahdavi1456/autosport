<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-lesson.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade-year.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_lesson'])) {
	$gt_lesson = new gt_lesson();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_lesson->add_lesson($new);
	$gt_lesson->create_form();
	exit();
}

if(isset($_POST['update_lesson_form'])){
	$gt_lesson = new gt_lesson();
	$ID = $_POST['ID'];
	$gt_lesson->create_form($ID);
	exit();
}

if(isset($_POST['update_lesson'])) {
	$gt_lesson = new gt_lesson();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_lesson->update_lesson($new);
	$gt_lesson->create_form();
	exit();
}


if(isset($_POST['remove_lesson'])){
	$gt_lesson = new gt_lesson();
	$ID = $_POST['ID'];
	$gt_lesson->remove_lesson($ID);
	$gt_lesson->create_form();
	exit();
}