<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-register.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-student.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-classroom.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-teacher.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-school.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_register'])) {
	$gt_register = new gt_register();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_register->add_register($new);
	$gt_register->create_form();
	exit();
}

if(isset($_POST['update_register_form'])){
	$gt_register = new gt_register();
	$ID = $_POST['ID'];
	$gt_register->create_form($ID);
	exit();
}

if(isset($_POST['update_register'])) {
	$gt_register = new gt_register();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_register->update_register($new);
	$gt_register->create_form();
	exit();
}


if(isset($_POST['remove_register'])){
	$gt_register = new gt_register();
	$ID = $_POST['ID'];
	$gt_register->remove_register($ID);
	$gt_register->create_form();
	exit();
}