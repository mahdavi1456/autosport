$(document).ready(function(){
	
	$(document.body).on('click', '.add-teaching' ,function(){
		$('#list-teaching-view-result').html("Loading...");
		var list = $("#add-teaching-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-teaching.php";
		$.post(full_url, {add_teaching:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-teaching-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-teaching.php";
		$.post(full_url, {update_teaching_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.update-teaching' ,function(){
		$('#list-teaching-view-result').html("Loading...");
		var list = $("#add-teaching-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/school/ba-teaching.php";
		$.post(full_url, {update_teaching:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
			$('.date-picker').persianDatepicker();
		});
	});
	
	$(document.body).on('click', '.remove-teaching' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('#list-teaching-view-result').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/school/ba-teaching.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_teaching:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
				$('.date-picker').persianDatepicker();
			});
		}
	});
	
});