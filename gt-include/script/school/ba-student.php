<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-student.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/jdf.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-date.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_student'])) {
	$gt_student = new gt_student();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_student->add_student($new);
	$gt_student->create_form();
	exit();
}

if(isset($_POST['update_student_form'])){
	$gt_student = new gt_student();
	$ID = $_POST['ID'];
	$gt_student->create_form($ID);
	exit();
}

if(isset($_POST['update_student'])) {
	$gt_student = new gt_student();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_student->update_student($new);
	$gt_student->create_form();
	exit();
}


if(isset($_POST['remove_student'])){
	$gt_student = new gt_student();
	$ID = $_POST['ID'];
	$gt_student->remove_student($ID);
	$gt_student->create_form();
	exit();
}

if(isset($_POST['remove_file'])){
	$gt_student = new gt_student();
	$db = new db();
	$st_id = $_POST['st_id'];
	$link = $db->get_var_query("select st_file from student where ID = $st_id");
	$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $link;
	if(unlink($path)){
		$sql = "update student set st_file = '' where ID = $st_id";
		$db->ex_query($sql);
	}
	$gt_student->get_profile($st_id);
}

if(is_array($_FILES)) {
	if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
		$sourcePath = $_FILES['userImage']['tmp_name'];
		$filename = $_FILES['userImage']['name'];
		$filename_list = explode(".", $filename);
		$new_filename = time() . "." . $filename_list[1]; 
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $new_filename;
		if(move_uploaded_file($sourcePath, $targetPath)) {
			$db = new db();
			$st_id = $_POST['st_id'];
			$st_file = $db->get_var_query("select st_file from student where ID = $st_id");
			if($st_file != ""){
				$path = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/profile/" . $st_file;
				if(unlink($path)){
				}
			}
			$sql = "update student set st_file = '$new_filename' where ID = $st_id";
			$db->ex_query($sql);
			$gt_student = new gt_student();
			$gt_student->get_profile($st_id);
		}
	}
}