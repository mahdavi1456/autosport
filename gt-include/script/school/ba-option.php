<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/gt-option.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_option'])) {
	$gt_option = new gt_option();
	$list = $_POST['list'];
	parse_str($list, $new);
	$o_type = $new['o_type'];
	$o_exam_create = $new['o_exam_create'];
	$o_exam_delete = $new['o_exam_delete'];
	$gt_option->save_option("o_type", $o_type);
	$gt_option->save_option("o_exam_create", $o_exam_create);
	$gt_option->save_option("o_exam_delete", $o_exam_delete);
	$gt_option->create_form();
	exit();
}