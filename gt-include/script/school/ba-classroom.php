<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-school.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade-year.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-classroom.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_classroom'])) {
	$gt_classroom = new gt_classroom();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_classroom->add_classroom($new);
	$gt_classroom->create_form();
	exit();
}

if(isset($_POST['update_classroom_form'])){
	$gt_classroom = new gt_classroom();
	$ID = $_POST['ID'];
	$gt_classroom->create_form($ID);
	exit();
}

if(isset($_POST['update_classroom'])) {
	$gt_classroom = new gt_classroom();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_classroom->update_classroom($new);
	$gt_classroom->create_form();
	exit();
}


if(isset($_POST['remove_classroom'])){
	$gt_classroom = new gt_classroom();
	$ID = $_POST['ID'];
	$gt_classroom->remove_classroom($ID);
	$gt_classroom->create_form();
	exit();
}