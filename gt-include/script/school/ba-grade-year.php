<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade-year.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_grade_year'])) {
	$gt_grade_year = new gt_grade_year();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_grade_year->add_grade_year($new);
	$gt_grade_year->create_form();
	exit();
}

if(isset($_POST['update_grade_year_form'])){
	$gt_grade_year = new gt_grade_year();
	$ID = $_POST['ID'];
	$gt_grade_year->create_form($ID);
	exit();
}

if(isset($_POST['update_grade_year'])) {
	$gt_grade_year = new gt_grade_year();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_grade_year->update_grade_year($new);
	$gt_grade_year->create_form();
	exit();
}


if(isset($_POST['remove_grade_year'])){
	$gt_grade_year = new gt_grade_year();
	$ID = $_POST['ID'];
	$gt_grade_year->remove_grade_year($ID);
	$gt_grade_year->create_form();
	exit();
}