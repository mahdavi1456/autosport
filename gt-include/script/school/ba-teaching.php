<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-teaching.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-lesson.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-school.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade-year.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-grade.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-teacher.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/school/gt-classroom.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_teaching'])) {
	$gt_teaching = new gt_teaching();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_teaching->add_teaching($new);
	$gt_teaching->create_form();
	exit();
}

if(isset($_POST['update_teaching_form'])){
	$gt_teaching = new gt_teaching();
	$ID = $_POST['ID'];
	$gt_teaching->create_form($ID);
	exit();
}

if(isset($_POST['update_teaching'])) {
	$gt_teaching = new gt_teaching();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_teaching->update_teaching($new);
	$gt_teaching->create_form();
	exit();
}


if(isset($_POST['remove_teaching'])){
	$gt_teaching = new gt_teaching();
	$ID = $_POST['ID'];
	$gt_teaching->remove_teaching($ID);
	$gt_teaching->create_form();
	exit();
}