$(document).ready(function(){
	
	$(document.body).on('click', '.add-archive-category' ,function(){
		$('#list-archive-category-view-result').html("Loading...");
		var list = $("#add-archive-category-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/archive/ba-archive-category.php";
		$.post(full_url, {add_archive_category:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-archive-category-form' ,function(){
		$('.create-form').html("Loading...");
		var ID = $(this).data('id');
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/archive/ba-archive-category.php";
		$.post(full_url, {update_archive_category_form:1, ID:ID}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.update-archive-category' ,function(){
		$('#list-archive-category-view-result').html("Loading...");
		var list = $("#add-archive-category-view *").serialize();
		var home_url = $('#home-url').val();
		var full_url = home_url + "gt-include/script/archive/ba-archive-category.php";
		$.post(full_url, {update_archive_category:1, list:list}, function(data) {
			$('.create-form').html(data);
			$('.select2').select2();
		});
	});
	
	$(document.body).on('click', '.remove-archive-category' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			$('.create-form').html("Loading...");
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/archive/ba-archive-category.php";
			var ID = $(this).data('id');
			$.post(full_url, {remove_archive_category:1, ID:ID }, function(data) {
				$('.create-form').html(data);
				$('.select2').select2();
			});
		}
	});
	
});