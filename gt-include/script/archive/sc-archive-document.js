$(document).ready(function(){
	
	$(document.body).on('click', '.add-archive-category' ,function(){
		var req = $('.required').val();
		if(req != ''){
			var list = $("#add-archive-category-view *").serialize();
			var home_url = $('#home-url').val();
			var ID = $(this).data('id');
			var full_url = home_url + "gt-include/script/archive/ba-archive-document.php";
			$.post(full_url, {add_archive_category:1, list:list, ID:ID}, function(data) {
				$('#archive-category').html(data);
			});
			$('#myModal').modal('toggle');
			$('#ac-name').val('');
		}
		else{
			$('.required').css("border" , "2px solid red");
		}
	});
	
	$(document.body).on('click', '.update-archive-document' ,function(){
		var ID = $(this).data('id');
		var iid = $(this).data('iid');
		var req = $('.required3' + iid).val();
		if(req != ''){
			var list = $("#update-archive-document-view *").serialize();
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/archive/ba-archive-document.php";
			$.post(full_url, {update_archive_document:1, list:list, ID:ID}, function(data) {
				$('#archive-document').html(data);
			});
			$('#updatedocument' + iid).modal('toggle');
			$('.modal-backdrop').hide();
		} else{
			$('.required3' + iid).css("border" , "2px solid red");
		}
	});
	
	$(document.body).on('click', '.update-archive-category' ,function(){
		var ID = $(this).data('id');
		var req = $('.required1' + ID).val();
		if(req != ''){
			var list = $("#update-archive-category-view *").serialize();
			var home_url = $('#home-url').val();
			var acid = $(this).data('acid');
			var full_url = home_url + "gt-include/script/archive/ba-archive-document.php";
			$.post(full_url, {update_archive_category:1, list:list, ID:ID, acid:acid}, function(data) {
				$('#archive-category').html(data);
			});
			$('#updatecategory' + ID).modal('toggle');
			alert('#updatecategory' + ID);
			$('.modal-backdrop').hide();
		} else{
			$('.required1' + ID).css("border" , "2px solid red");
		}
	});
	
	$(document.body).on('click', '.remove-document' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			var home_url = $('#home-url').val();
			var ID = $(this).data('id');
			var acid = $(this).data('acid');
			var full_url = home_url + "gt-include/script/archive/ba-archive-document.php";
			$.post(full_url, {remove_archive_document:1, ID:ID, acid:acid}, function(data) {
				$('#archive-document').html(data);
			});
		}
	});
	
	$(document.body).on('click', '.remove-category' ,function(){
		var h = confirm("آیا از انجام این عملیات اطمینان دارید؟!");
		if (h == true) {
			var home_url = $('#home-url').val();
			var ID = $(this).data('id');
			var acid = $(this).data('acid');
			var full_url = home_url + "gt-include/script/archive/ba-archive-document.php";
			$.post(full_url, {remove_archive_category:1, ID:ID, acid:acid}, function(data) {
				$('#archive-category').html(data);
			});
		}
	});
	
	$(document.body).on('submit', '#uploadForm' ,function(e){
		e.preventDefault();
		var req = $('.required2').val();
		if(req != ''){
			var home_url = $('#home-url').val();
			var full_url = home_url + "gt-include/script/archive/ba-archive-document.php";
			$.ajax({
				url: full_url,
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				{
					$("#archive-document").html(data);
				},
				error: function() 
				{
				}
		   });
		   $('#uploadModal').modal('toggle');
			// $('#ad-name"').val('');
		   //$('#ad-file').val('');
		   //$('#ad-note').val('');
		   //document.getElementById('ad-note').value = '';
		} else{
			$('.required2').css("border" , "2px solid red");
		}
	});
});