<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/archive/gt-archive-document.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/archive/gt-archive-category.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_archive_category'])) {
	$db = new db();
	$ac_parent = $_POST['ID'];
	$gt_archive_category = new gt_archive_category();
	$gt_archive_document = new gt_archive_document();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_archive_category->add_archive_category($new);
	?>
	<div class="col-md-12">
		<?php
		$res = $db->get_select_query("select * from archive_category where ac_parent = $ac_parent order by ID desc");
		if(count($res) > 0) {
			foreach($res as $row2) {
				?>
				<a href="/archive_document?id=<?php echo $row2["ID"]; ?>"><i class="icofont-folder"></i> <?php echo $row2["ac_name"]; ?></a>
				<button type="button" class="btn btn-danger btn-xs remove-category" data-id="<?php echo $row2['ID']; ?>" data-acid="<?php echo $ac_parent; ?>"><i class="icofont-close"></i></button>
				<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#updatecategory<?php echo $row2['ID']; ?>"><i class="icofont-ui-edit"></i></button>&nbsp;
				<div id="updatecategory<?php echo $row2['ID']; ?>" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">ویرایش پوشه</h4>
							</div>
							<div class="modal-body">
								<div id="update-archive-category-view">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>نام پوشه <span class="red">*</span></label>
												<input type="text" placeholder="نام پوشه" class="form-control required1<?php echo $row2['ID']; ?>" value="<?php echo $row2['ac_name']; ?>" name="ac_name<?php echo $row2['ID']; ?>">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<center>
									<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
									<button class="btn btn-primary btn-sm update-archive-category" data-id="<?php echo $row2['ID']; ?>" data-acid="<?php echo $ac_parent; ?>" type="submit">ذخیره</button>
								</center>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
	<?php
	exit();
}

if(isset($_POST['update_archive_category'])) {
	$db = new db();
	$ID = $_POST['ID'];
	$ac_id = $_POST['acid'];
	$list = $_POST['list'];
	parse_str($list, $new);
	$ac_name = $new['ac_name' . $ID];
	$ary = array();
	$ary['ID'] = $ID;
	$ary['ac_name'] = $ac_name;
	$ary['ac_parent'] = $ac_id;
	$gt_archive_category = new gt_archive_category();
	$gt_archive_category->update_archive_category($ary);
	?>
	<div class="col-md-12">
		<?php
		$res = $db->get_select_query("select * from archive_category where ac_parent = $ac_id order by ID desc");
		if(count($res) > 0) {
			foreach($res as $row2) {
				?>
				<a href="/archive_document?id=<?php echo $row2["ID"]; ?>"><i class="icofont-folder"></i> <?php echo $row2["ac_name"]; ?></a>
				<button type="button" class="btn btn-danger btn-xs remove-category" data-id="<?php echo $row2['ID']; ?>" data-acid="<?php echo $ac_id; ?>"><i class="icofont-close"></i></button>
				<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#updatecategory<?php echo $row2['ID']; ?>"><i class="icofont-ui-edit"></i></button>&nbsp;
				<div id="updatecategory<?php echo $row2['ID']; ?>" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">ویرایش پوشه</h4>
							</div>
							<div class="modal-body">
								<div id="update-archive-category-view">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>نام پوشه <span class="red">*</span></label>
												<input type="text" placeholder="نام پوشه" class="form-control required1<?php echo $row2['ID']; ?>" value="<?php echo $row2['ac_name']; ?>" name="ac_name<?php echo $row2['ID']; ?>">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<center>
									<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
									<button class="btn btn-primary btn-sm update-archive-category" data-id="<?php echo $row2['ID']; ?>" data-acid="<?php echo $ac_id; ?>" type="submit">ذخیره</button>
								</center>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
	<?php
}
if(isset($_POST['update_archive_document'])) {
	$db = new db();
	$ID = $_POST['ID'];
	$list = $_POST['list'];
	parse_str($list, $new);
	$ad_name = $new['ad_name' . $ID];
	$ad_note = $new['ad_note' . $ID];
	$ac_id = $new['ac_parent' . $ID];
	$ary = array();
	$ary['ID'] = $ID;
	$ary['ad_name'] = $ad_name;
	$ary['ad_note'] = $ad_note;
	$ary['ac_id'] = $ac_id;
	$gt_archive_document = new gt_archive_document();
	$gt_archive_document->update_archive_document($ary);
	?>
	<div class="col-md-12">
		<?php
		$list = $db->get_select_query("select * from archive_document where ac_id = $ac_id");
		if(count($list) > 0) {
			foreach($list as $l) {
			?>
			<a href="http://gtserver.ir/gt-content/uploads/archive/<?php echo $l['ad_file']; ?>" target="_blank">
				<img style="width: 10%; border-radius: 4px; padding: 1px; border: 2px solid #ddd; display: inline;" src="http://gtserver.ir/gt-content/uploads/archive/<?php echo $l['ad_file']; ?>" class="upload-preview image-preview img-responsive">
			</a>
			<button type="button" class="btn btn-danger btn-xs remove-document" data-id="<?php echo $l['ID']; ?>" data-acid="<?php echo $ac_id; ?>"><i class="icofont-close"></i></button>
			<button type="button" class="btn btn-danger btn-xs update-document" data-toggle="modal" data-target="#updatedocument<?php echo $l['ID']; ?>"><i class="icofont-ui-edit"></i></button>
			<div id="updatedocument<?php echo $l['ID']; ?>" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">ویرایش فایل</h4>
						</div>
						<div class="modal-body">
							<div id="update-archive-document-view">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>عنوان فایل <span class="red">*</span></label>
											<input type="text" placeholder="عنوان فایل" class="form-control required3<?php echo $l['ID']; ?>" value="<?php echo $l['ad_name']; ?>" name="ad_name">
											<input type="hidden" name="ac_parent" value="<?php echo $ac_id; ?>">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>یادداشت</label>
											<input type="text" placeholder="یادداشت" class="form-control" name="ad_note" value="<?php echo $l['ad_note']; ?>">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<center>
								<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
								<button class="btn btn-primary btn-sm update-archive-document" data-id="<?php echo $ac_parent; ?>"  data-iid="<?php echo $l['ID']; ?>" type="submit">ذخیره</button>
							</center>
						</div>
					</div>
				</div>
			</div>
			<?php
			}
		}
		?>
	</div>
	<?php
	exit();
}

if(isset($_POST['remove_archive_document'])) {
	$db = new db();
	$ID = $_POST['ID'];
	$ac_id = $_POST['acid'];
	$gt_archive_document = new gt_archive_document();
	$gt_archive_document->remove_archive_document($ID);
	?>
	<div class="col-md-12">
		<?php
		$list = $db->get_select_query("select * from archive_document where ac_id = $ac_id");
		if(count($list) > 0) {
			foreach($list as $l) {
			?>
			<a href="http://gtserver.ir/gt-content/uploads/archive/<?php echo $l['ad_file']; ?>" target="_blank">
				<img style="width: 10%; border-radius: 4px; padding: 1px; border: 2px solid #ddd; display: inline;" src="http://gtserver.ir/gt-content/uploads/archive/<?php echo $l['ad_file']; ?>" class="upload-preview image-preview img-responsive">
			</a>
			<button type="button" class="btn btn-danger btn-xs remove-document" data-id="<?php echo $l['ID']; ?>" data-acid="<?php echo $ac_id; ?>"><i class="icofont-close"></i></button>
			<button type="button" class="btn btn-danger btn-xs update-document" data-toggle="modal" data-target="#updatedocument<?php echo $l['ID']; ?>"><i class="icofont-ui-edit"></i></button>
			<div id="updatedocument<?php echo $l['ID']; ?>" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">ویرایش فایل</h4>
						</div>
						<div class="modal-body">
							<div id="update-archive-document-view">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>عنوان فایل <span class="red">*</span></label>
											<input type="text" placeholder="عنوان فایل" class="form-control required3<?php echo $l['ID']; ?>" value="<?php echo $l['ad_name']; ?>" name="ad_name<?php echo $l['ID']; ?>">
											<input type="hidden" name="ac_parent<?php echo $l['ID']; ?>" value="<?php echo $ac_id; ?>">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>یادداشت</label>
											<input type="text" placeholder="یادداشت" class="form-control" name="ad_note<?php echo $l['ID']; ?>" value="<?php echo $l['ad_note']; ?>">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<center>
								<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
								<button class="btn btn-primary btn-sm update-archive-document" data-id="<?php echo $l['ID']; ?>" data-iid="<?php echo $l['ID']; ?>" type="submit">ذخیره</button>
							</center>
						</div>
					</div>
				</div>
			</div>
			<?php
			}
		}
		?>
	</div>
	<?php
	exit();
}

if(isset($_POST['remove_archive_category'])) {
	$db = new db();
	$ID = $_POST['ID'];
	$ac_id = $_POST['acid'];
	$gt_archive_category = new gt_archive_category();
	$gt_archive_category->remove_archive_category($ID);
	?>
	<div class="col-md-12">
		<?php
		$res = $db->get_select_query("select * from archive_category where ac_parent = $ac_id order by ID desc");
		if(count($res) > 0) {
			foreach($res as $row2) {
				?>
				<a href="/archive_document?id=<?php echo $row2["ID"]; ?>"><i class="icofont-folder"></i> <?php echo $row2["ac_name"]; ?></a>
				<button type="button" class="btn btn-danger btn-xs remove-category" data-id="<?php echo $row2['ID']; ?>" data-acid="<?php echo $ac_id; ?>"><i class="icofont-close"></i></button>
				<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#updatecategory<?php echo $row2['ID']; ?>"><i class="icofont-ui-edit"></i></button>&nbsp;
				<div id="updatecategory<?php echo $row2['ID']; ?>" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">ویرایش پوشه</h4>
							</div>
							<div class="modal-body">
								<div id="update-archive-category-view">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>نام پوشه <span class="red">*</span></label>
												<input type="text" placeholder="نام پوشه" class="form-control required1<?php echo $row2['ID']; ?>" value="<?php echo $row2['ac_name']; ?>" name="ac_name<?php echo $row2['ID']; ?>">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<center>
									<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
									<button class="btn btn-primary btn-sm update-archive-category" data-id="<?php echo $row2['ID']; ?>" data-acid="<?php echo $ac_id; ?>" type="submit">ذخیره</button>
								</center>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
	<?php
	exit();
}

if(is_array($_FILES)) {
	if(is_uploaded_file($_FILES['ad_file']['tmp_name'])) {
		$sourcePath = $_FILES['ad_file']['tmp_name'];
		$filename = $_FILES['ad_file']['name'];
		$filename_list = explode(".", $filename);
		$new_filename = time() . "." . $filename_list[1]; 
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/gt-content/uploads/archive/" . $new_filename;
		if(move_uploaded_file($sourcePath, $targetPath)) {
			$db = new db();
			$gt_archive_document = new gt_archive_document();
			$ac_id = $_POST['ac_id'];
			$ad_name = $_POST['ad_name'];
			$ad_note = $_POST['ad_note'];
			$array = array();
			$array['ac_id'] = $ac_id;
			$array['ad_name'] = $ad_name;
			$array['ad_note'] = $ad_note;
			$array['ad_file'] = $new_filename;
			$gt_archive_document->add_archive_document($array);?>
			<div class="col-md-12">
				<?php
				$list = $db->get_select_query("select * from archive_document where ac_id = $ac_id");
				if(count($list) > 0) {
					foreach($list as $l) {
					?>
					<a href="http://gtserver.ir/gt-content/uploads/archive/<?php echo $l['ad_file']; ?>" target="_blank">
						<img style="width: 10%; border-radius: 4px; padding: 1px; border: 2px solid #ddd; display: inline;" src="http://gtserver.ir/gt-content/uploads/archive/<?php echo $l['ad_file']; ?>" class="upload-preview image-preview img-responsive">
					</a>
					<button type="button" class="btn btn-danger btn-xs remove-document" data-id="<?php echo $l['ID']; ?>" data-acid="<?php echo $ac_id; ?>"><i class="icofont-close"></i></button>
					<button type="button" class="btn btn-danger btn-xs update-document" data-toggle="modal" data-target="#updatedocument<?php echo $l['ID']; ?>"><i class="icofont-ui-edit"></i></button>
					<div id="updatedocument<?php echo $l['ID']; ?>" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">ویرایش فایل</h4>
								</div>
								<div class="modal-body">
									<div id="update-archive-document-view">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>عنوان فایل <span class="red">*</span></label>
													<input type="text" placeholder="عنوان فایل" class="form-control required3<?php echo $l['ID']; ?>" value="<?php echo $l['ad_name']; ?>" name="ad_name<?php echo $l['ID']; ?>">
													<input type="hidden" name="ac_parent<?php echo $l['ID']; ?>" value="<?php echo $ac_id; ?>">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label>یادداشت</label>
													<input type="text" placeholder="یادداشت" class="form-control" name="ad_note<?php echo $l['ID']; ?>" value="<?php echo $l['ad_note']; ?>">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<center>
										<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">انصراف</button>
										<button class="btn btn-primary btn-sm update-archive-document" data-id="<?php echo $l['ID']; ?>" data-iid="<?php echo $l['ID']; ?>" type="submit">ذخیره</button>
									</center>
								</div>
							</div>
						</div>
					</div>
					<?php
					}
				}
				?>
			</div>
			<?php
		}
	}
}
?>