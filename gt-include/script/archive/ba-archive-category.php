<?php
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/archive/gt-archive-category.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/db.php";
include $_SERVER['DOCUMENT_ROOT'] . "/gt-include/class/base/prime.php";
if(isset($_POST['add_archive_category'])) {
	$gt_archive_category = new gt_archive_category();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_archive_category->add_archive_category($new);
	$gt_archive_category->create_form();
	exit();
}

if(isset($_POST['update_archive_category_form'])){
	$gt_archive_category = new gt_archive_category();
	$ID = $_POST['ID'];
	$gt_archive_category->create_form($ID);
	exit();
}

if(isset($_POST['update_archive_category'])) {
	$gt_archive_category = new gt_archive_category();
	$list = $_POST['list'];
	parse_str($list, $new);
	$gt_archive_category->update_archive_category($new);
	$gt_archive_category->create_form();
	exit();
}


if(isset($_POST['remove_archive_category'])){
	$gt_archive_category = new gt_archive_category();
	$ID = $_POST['ID'];
	$gt_archive_category->remove_archive_category($ID);
	$gt_archive_category->create_form();
	exit();
}