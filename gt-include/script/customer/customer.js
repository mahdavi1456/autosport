$(document).ready(function(){
	$("#customer_type").change(function(){
		var val = $('#customer_type').find(":selected").val();
		if(val =='real' ){
			$("#customer_real").css("display" , "none");
			$("#customer_legal").css("display" , "block");
		}
		else if(val=='legal'){
			$("#customer_legal").css("display" , "none");
			$("#customer_real").css("display" , "block");
		}
	});
	
	$("#add_customer").click(function(){
		var addcustomer = $("").text($("#addcustomer").serialize());  
		$.post("customer.php", {add_customer:1, addcustomer:addcustomer}, function(data){
			$('').html(data);
		});
	});
	
	$("#update_customer").click(function(){
		var id = $(this).data('id');
		var updatecustomer = $("").text($("#updatecustomer").serialize());  
		$.post("customer.php", {update_customer:1, updatecustomer:updatecustomer, id:id}, function(data){
			$('').html(data);
		});
	});
	
	$("#remove_customer").click(function(){
		var id = $(this).data('id');
		$.post("customer.php", {remove_customer:1, id:id}, function(data){
			$('').html(data);
		});
	});
});