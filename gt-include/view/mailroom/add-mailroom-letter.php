<div class="row">
	<div class="col-md-12">
		<form action="" method="post">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>نوع نامه<span class="red">*</span></label>
						<select name="letter_type" class="form-control">
							<option value="ارسالی">ارسالی</option>
							<option value="دریافتی">دریافتی</option>
						</select>
					</div>
				</div>
			</div>
			<div id="letter_send">
				<input type="hidden" name="received_date" value="0">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>عنوان نامه </label>
							<input type="text" name="l_title" placeholder="عنوان نامه">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>فرستنده <span class="red">*</span></label>
							<select name="province_id" class="form-control province_id select2">
								<?php
								$db = new db();
								$gt_user = new gt_user();
								$res = $db->get_select_query("select * from user");
								if(count($res) > 0){
									foreach($res as $row){
										?>
										<option value="<?php echo $row['ID']; ?>"><?php echo $gt_user->get_user_name($row['ID']); ?></option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>گیرنده <span class="red">*</span></label>
							<select name="province_id" class="form-control province_id select2">
								<?php
								$db = new db();
								$gt_customer = new gt_customer();
								$res = $db->get_select_query("select * from customer");
								if(count($res) > 0){
									foreach($res as $row){
										?>
										<option value="<?php echo $row['ID']; ?>"><?php echo $gt_customer->get_customer_name($row['ID']); ?></option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>تاریخ ارسال <span class="red">*</span></label>
							<input type="text" name="postage_date" placeholder="تاریخ ارسال" class="date-picker form-control" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>تاریخ نامه <span class="red">*</span></label>
							<input type="text" name="l_date" placeholder="تاریخ نامه" class="date-picker form-control" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره شناسه نامه</label>
							<input type="text" name="l_code" placeholder="شماره شناسه نامه">
						</div>
					</div>
				</div>
			</div>
			<div id="letter_received">
				<input type="hidden" name="postage_date" value="0">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>عنوان نامه </label>
							<input type="text" name="l_title" placeholder="عنوان نامه">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>فرستنده <span class="red">*</span></label>
							<select name="province_id" class="form-control province_id select2">
								<?php
								$db = new db();
								$gt_customer = new gt_customer();
								$res = $db->get_select_query("select * from customer");
								if(count($res) > 0){
									foreach($res as $row){
										?>
										<option value="<?php echo $row['ID']; ?>"><?php echo $gt_customer->get_customer_name($row['ID']); ?></option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>گیرنده <span class="red">*</span></label>
							<select name="province_id" class="form-control province_id select2">
								<?php
								$db = new db();
								$gt_user = new gt_user();
								$res = $db->get_select_query("select * from user");
								if(count($res) > 0){
									foreach($res as $row){
										?>
										<option value="<?php echo $row['ID']; ?>"><?php echo $gt_user->get_user_name($row['ID']); ?></option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>تاریخ دریافت <span class="red">*</span></label>
							<input type="text" name="received_date" placeholder="تاریخ دریافت" class="date-picker form-control" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>تاریخ نامه <span class="red">*</span></label>
							<input type="text" name="l_date" placeholder="تاریخ نامه" class="date-picker form-control" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره شناسه نامه</label>
							<input type="text" name="l_code" placeholder="شماره شناسه نامه">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" name="add_customer" id="add_customer" class="btn btn-sm btn-success">ذخیره</button>
				</div>
			</div>
		</form>
	</div>	
</div>