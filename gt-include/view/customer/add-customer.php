<div class="row">
	<div class="col-md-12">
		<form action="" method="post">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>نوع مشتری<span class="red">*</span></label>
						<select name="customer_type" class="form-control">
							<option value="حقیقی">حقیقی</option>
							<option value="حقوقی">حقوقی</option>
						</select>
					</div>
				</div>
			</div>
			<div id="customer_real">
				<input type="hidden" name="c_type" value="real_person">
				<input type="hidden" name="c_national_id" value="0">
				<input type="hidden" name="c_regnumber" value="0">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
						<label>نام <span class="red">*</span></label>
						<input type="text" name="c_name" placeholder="نام" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>نام خانوادگی <span class="red">*</span></label>
							<input type="text" name="c_family" placeholder="نام خانوادگی" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>نام شرکت <span class="red">*</span></label>
							<input type="text" name="c_company" placeholder="نام شرکت" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>کد ملی <span class="red">*</span></label>
							<input type="text" name="c_national" placeholder="کد ملی" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>آدرس </label>
							<input type="text" name="c_address" placeholder="آدرس">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره شناسنامه <span class="red">*</span></label>
							<input type="text" name="c_idnumber" placeholder="شماره شناسنامه" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره تماس </label>
							<input type="text" name="c_phone" placeholder="شماره تماس">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره تلفن همراه <span class="red">*</span></label>
							<input type="text" name="c_mobile" placeholder="شماره تلفن همراه" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>آدرس پست الکترونیک</label>
							<input type="text" name="c_email" placeholder="آدرس پست الکترونیک">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>نوع فعالیت</label>
							<input type="text" name="c_activity" placeholder="نوع فعالیت">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره اقتصادی</label>
							<input type="text" name="c_economic" placeholder="شماره اقتصادی">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>ارزش افزوده</label>
							<input type="text" name="c_vat" placeholder="ارزش افزوده">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>کدپستی</label>
							<input type="text" name="c_postalcode" placeholder="کدپستی">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>تلفن شرکت </label>
							<input type="text" name="c_c_phone" placeholder="تلفن شرکت">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>شماره فکس</label>
							<input type="text" name="c_fax" placeholder="شماره فکس">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>آدرس شرکت</label>
							<input type="text" name="c_c_address" placeholder="آدرس شرکت">
						</div>
					</div>
				</div>
			</div>
			<div id="customer_legal">
				<input type="hidden" name="c_type" value="legal_person">
				<input type="hidden" name="c_idnumber" value="0">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
						<label>نام <span class="red">*</span></label>
						<input type="text" name="c_name" placeholder="نام" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>نام خانوادگی <span class="red">*</span></label>
							<input type="text" name="c_family" placeholder="نام خانوادگی" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>نام شرکت <span class="red">*</span></label>
							<input type="text" name="c_company" placeholder="نام شرکت" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>کد ملی <span class="red">*</span></label>
							<input type="text" name="c_national" placeholder="کد ملی" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>آدرس</label>
							<input type="text" name="c_address" placeholder="آدرس">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>شناسه ملی <span class="red">*</span></label>
							<input type="text" name="c_natinal_id" placeholder="شناسه ملی" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره تماس</label>
							<input type="text" name="c_phone" placeholder="شماره تماس">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره تلفن همراه <span class="red">*</span></label>
							<input type="text" name="c_mobile" placeholder="شماره تلفن همراه" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>آدرس پست الکترونیک</label>
							<input type="text" name="c_email" placeholder="آدرس پست الکترونیک">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>نوع فعالیت</label>
							<input type="text" name="c_activity" placeholder="نوع فعالیت">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>شماره اقتصادی</label>
							<input type="text" name="c_economic" placeholder="شماره اقتصادی">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>ارزش افزوده</label>
							<input type="text" name="c_vat" placeholder="ارزش افزوده">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>کدپستی</label>
							<input type="text" name="c_postalcode" placeholder="کدپستی">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>شماره فکس</label>
							<input type="text" name="c_fax" placeholder="شماره فکس">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>تلفن شرکت </label>
							<input type="text" name="c_c_phone" placeholder="تلفن شرکت">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>شماره ثبت شرکت</label>
							<input type="text" name="c_regnumber" placeholder="شماره ثبت شرکت">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>آدرس شرکت</label>
							<input type="text" name="c_c_address" placeholder="آدرس شرکت">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" name="add_customer" id="add_customer" class="btn btn-sm btn-success">ذخیره</button>
				</div>
			</div>
		</form>
	</div>	
</div>