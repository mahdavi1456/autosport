<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>ردیف</th>
					<th>نام و نام خانوادگی</th>
					<th>نوع مشتری</th>
					<th>نام شرکت</th>
					<th>موبایل</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_customer = new gt_customer();
				$list = $db->get_select_query("select * from customer order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo per_number($i); ?></td>
							<td><?php $name = $gt_customer->get_customer_name($l['ID']); echo $name ; ?></td>
							<td><?php 
								if($l['c_type'] == "real") {
									echo "حقیقی" ;
								} else { 
									echo "حقوقی" ;
								}
								?>
							</td>
							<td><?php echo per_number($l['c_company']); ?></td>
							<td><?php echo per_number($l['c_mobile']); ?></td>
							<td>
								<button name="edit-customer" value="<?php echo $l['ID']; ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</button>
								<button name="remove_customer" id="remove_customer" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}" value="<?php echo $l['ID']; ?>" class="btn btn-danger xs">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="6">بدون مشتری</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
</div>
	