<div class="row">
	<div class="col-md-12">
		<form action="" method="post">
			<div class="col-md-6">
				<div class="form-group">
				<label>نام کالا <span class="red">*</span></label>
				<input type="text" name="p_name" placeholder="نام کالا" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>کد کالا</label>
					<input type="text" name="p_id" placeholder="کد کالا">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>قیمت خرید <span class="red">*</span></label>
					<input type="text" name="p_pprice" placeholder="قیمت خرید" required>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>قیمت فروش <span class="red">*</span></label>
					<input type="text" name="p_sprice" placeholder="قیمت فروش" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>دسته بندی <span class="red">*</span></label>
					<input type="text" name="p_category" placeholder="دسته بندی" required>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" name="add_product" id="add_product" class="btn btn-sm btn-success">ذخیره</button>
				</div>
			</div>
		</form>
	</div>	
</div>