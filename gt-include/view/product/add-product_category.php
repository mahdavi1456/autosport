<div class="row">
	<div class="col-md-12">
		<form action="" method="post">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>نام دسته بندی <span class="red">*</span></label>
						<input type="text" name="c_name" placeholder="نام دسته بندی" required >
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>دسته بندی والد <span class="red">*</span></label>
							<select name="c_parent" class="form-control select2">
							<?php
							$gt_product_category = new gt_product_category();
							$c_parent=$gt_product_category->get_product_category() ;
								if(count ($c_parent > 0)) {
									foreach($c_parent as $row){
										?>
										<option value="<?php echo $row['ID']; ?>"><?php echo $row['c_name']; ?></option>
										<?php
									}
								}
								?>
							</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" name="add_category" id="add_category" class="btn btn-sm btn-success">ذخیره</button>
				</div>
			</div>
		</form>
	</div>	
</div>