<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>ردیف</th>
					<th>نام دسته بندی</th>
					<th>دسته بندی والد</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_product_category = new gt_product_category();
				$list = $db->get_select_query("select * from product_category order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo per_number($i); ?></td>
							<td><?php echo per_number($l['c_name']); ?></td>
							<td><?php echo $gt_category_product->get_product_category_name($l['c_parent']); ?></td>
							<td>
								<button name="edit-category" value="<?php echo $l['ID']; ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</button>
								<button name="remove_category" id="remove_category" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}" value="<?php echo $l['ID']; ?>" class="btn btn-danger xs">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="4">بدون محصول</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
</div>
	