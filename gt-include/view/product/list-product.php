<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>ردیف</th>
					<th>نام محصول</th>
					<th>قیمت خرید</th>
					<th>قیمت فروش</th>
					<th>دسته بندی</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_product = new gt_product();
				$list = $db->get_select_query("select * from product order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo per_number($i); ?></td>
							<td><?php echo per_number($l['p_name']); ?></td>
							<td><?php echo per_number(number_format($l['p_pprice'])); ?></td>
							<td><?php echo per_number(number_format($l['p_sprise'])); ?></td>	
							<td><?php $category = $gt_product->get_product_category($l['ID']); echo $category ; ?></td>
							<td>
								<button name="edit-product" value="<?php echo $l['ID']; ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</button>
								<button name="remove_product" id="remove_product" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}" value="<?php echo $l['ID']; ?>" class="btn btn-danger xs">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="6">بدون محصول</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
</div>
	