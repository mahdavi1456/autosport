<div class="row">
	<div class="col-md-12">
		<form action="" method="post">
			<input type="hidden" name="u_id" value="<?php echo $_SESSION['user_id']; ?>" required >
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>عنوان <span class="red">*</span></label>
						<input type="text" name="p_name" placeholder="عنوان" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>نام مشتری<span class="red">*</span></label>
						<select name="c_id" class="form-control select2">
						<?php
						$sql= "select * from customer" ;
						$db = new db ;
						$gt_customer = new gt_customer();
						$sql1 = $db->get_select_query()
							if (count ($sql1 > 0)) {
								foreach($sql1 as $row){
									?>
									<option value="<?php echo $row['ID']; ?>"><?php echo $gt_customer->get_customer_name($row['ID']); ?></option>
									<?php
								}
							}
							?>
						</select>				
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>مبلغ پرداختی <span class="red">*</span></label>
						<input type="text" name="p_price" placeholder="مبلغ پرداختی" required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>تاریخ پرداخت <span class="red">*</span></label>
						<input type="text" name="p_date" placeholder="تاریخ پرداخت" class="date-picker form-control" required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>وضعیت پرداخت <span class="red">*</span></label>
						<select name="p_status" class="form-control select2">
							<option value="0">پرداخت شد</option>
							<option value="1">پرداخت نشد</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>نوع پرداخت<span class="red">*</span></label>
						<select name="p_type" class="form-control select2">
							<option value="نقدی">نقدی</option>
							<option value="غیرنقدی">غیرنقدی</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>کد پیگیری</label>
						<input type="text" name="p_code" placeholder="کد پیگیری">
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>توضیحات</label>
						<input type="text" name="p_details" placeholder="توضیحات">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" name="add_payment" id="add_payment" class="btn btn-sm btn-success">ذخیره</button>
				</div>
			</div>
		</form>
	</div>	
</div>