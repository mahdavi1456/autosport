<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-condensed">
				<tr>
					<th>ردیف</th>
					<th>عنوان فاکتور</th>
					<th>نام مشتری</th>
					<th>مبلغ پرداختی</th>
					<th>تاریخ پرداخت</th>
					<th>نوع پرداخت</th>
					<th>کد پیگیری</th>
					<th>توسط</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_customer = new gt_customer();
				$gt_user = new gt_user();
				$list = $db->get_select_query("select * from peyment order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo per_number($i); ?></td>
							<td><?php echo per_number($l['p_name']); ?></td>
							<td><?php $name = $gt_customer->gt_customer_name($l['c_id']); echo $name; ?></td>
							<td><?php echo per_number(number_format($l['p_price'])); ?></td>
							<td><?php echo per_number($l['p_date']); ?></td>
							<td><?php echo per_number($l['p_type']); ?></td>
							<td><?php echo per_number($l['p_code']); ?></td>
							<td><?php $name = $gt_user->gt_user_name($l['u_id']); echo $name; ?></td>
							<td>
								<button name="edit-peyment" value="<?php echo $l['ID']; ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</button>
								<button name="remove_peyment" id="remove_peyment" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}" value="<?php echo $l['ID']; ?>" class="btn btn-danger xs">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="9">بدون پرداختی</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
</div>
	