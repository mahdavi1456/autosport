<div class="row">
	<div class="col-md-12">
		<form action="" method="post">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					<label>نام شهر <span class="red">*</span></label>
					<input type="text" name="p_name" placeholder="نام کالا" required >
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>کد والد <span class="red">*</span></label>
						<select name="p_parent" class="form-control select2">
							<?php
							$db = new db();
							$res = $db->get_select_query("select * from province");
							if(count($res) > 0){
								foreach($res as $row){
									?>
									<option value="<?php echo $row['ID']; ?>"><?php echo $row['p_name']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>وضعیت نمایش <span class="red">*</span></label>
						<input type="text" name="p_status" placeholder="قیمت خرید" required>
						<select name="p_status" class="form-control select2">
							<option value="0">فعال</option>
							<option value="1">غیرفعال</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" name="add_user" id="add_user" class="btn btn-sm btn-success">ذخیره</button>
				</div>
			</div>
		</form>
	</div>	
</div>