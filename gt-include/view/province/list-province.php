<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>ردیف</th>
					<th>نام شهر</th>
					<th>شهر والد</th>
					<th>وضعیت نمایش</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_province = new gt_province();
				$list = $db->get_select_query("select * from province order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo per_number($i); ?></td>
							<td><?php echo per_number($l['p_name']); ?></td>
							<td><?php $province = $gt_province->get_province_name($l['p_parent']) ; echo $province ; ?></td>
							<td><?php if($l['p_status']){ echo "فعال"; } else{  echo "غیرفعال"; } ?></td>
							<td>
								<button name="edit-province" value="<?php echo $l['ID']; ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</button>
								<button name="remove_province" id="remove_province" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}" value="<?php echo $l['ID']; ?>" class="btn btn-danger xs">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="5">بدون شهر</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
</div>
	