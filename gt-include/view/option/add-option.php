<?php
$gt_option = new gt_option ;
?>
<div class="row">
	<div class="col-md-12">
		<form action="" method="post">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>شماره ثبت<span class="red">*</span></label>
						<input type="text" class="form-control" placeholder="شماره ثبت" name="reg_number" value="<?php echo $gt_option->get_option('reg_number'); ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<label>نام شرکت<span class="red">*</span></label>
					<input type="text" class="form-control" placeholder="نام شرکت" name="com_name" value="<?php echo $gt_option->get_option('com_name'); ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>نشانی شرکت<span class="red">*</span></label>
						<input type="text" class="form-control" placeholder="نشانی شرکت" name="Office_Address" value="<?php echo $gt_option->get_option('Office_Address'); ?>">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>شماره اقتصادی<span class="red">*</span></label>
						<input type="text" class="form-control" placeholder="شماره اقتصادی" name="eco_number" value="<?php echo $gt_option->get_option('eco_number'); ?>">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>شماره ملی<span class="red">*</span></label>
						<input type="text" class="form-control" placeholder="شماره ملی" name="National_ID" value="<?php echo $gt_option->get_option('National_ID'); ?>">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>تلفن<span class="red">*</span></label>
						<input type="text" class="form-control" placeholder="تلفن" name="Phone" value="<?php echo $gt_option->get_option('Phone'); ?>">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>نمابر<span class="red">*</span></label>
						<input type="text" class="form-control" placeholder="نمابر" name="Fax" value="<?php echo $gt_option->get_option('Fax'); ?>">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>کد پستی 10 رقمی</label>
						<input type="text" class="form-control" placeholder="کد پستی 10 رقمی" name="code_postal" value="<?php echo $gt_option->get_option('code_postal'); ?>">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>استان</label>
						<input type="text" class="form-control" placeholder="استان" name="State" value="<?php echo $gt_option->get_option('State'); ?>">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>شهرستان<span class="red">*</span></label>
						<input type="text" class="form-control" placeholder="شهرستان" name="county" value="<?php echo $gt_option->get_option('county'); ?>">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>شهر<span class="red">*</span></label>
						<input type="text" class="form-control" placeholder="شهر" name="city" value="<?php echo $gt_option->get_option('city'); ?>">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" name="add_option" id="add_option" class="btn btn-sm btn-success">ذخیره</button>
				</div>
			</div>
		</form>
	</div>	
</div>