<div class="row">
	<div class="col-md-12">
		<form action="" method="post" id="adduser">
			<input type="hidden" name="u_regdate" value="<?php echo jdate('Y/m/d H:i:s'); ?>">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>نام <span class="red">*</span></label>
						<input type="tex" name="u_name" placeholder="نام" class="form-control" required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>نام خانوادگی <span class="red">*</span></label>
						<input type="tex" name="u_family" placeholder="نام خانوادگی" class="form-control" required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>تاریخ تولد <span class="red">*</span></label>
						<input type="tex" name="u_birth" placeholder="تاریخ تولد" class="date-picker form-control" required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>کد ملی <span class="red">*</span></label>
						<input type="tex" name="u_national" placeholder="کد ملی" class="form-control" required>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>مدرک تحصیلی <span class="red">*</span></label>
						<select name="u_evidence" class="form-control select2">
							<option value="دیپلم">دیپلم</option>
							<option value="فوق دیپلم">فوق دیپلم</option>
							<option value="لیسانس">لیسانس</option>
							<option value="فوق لیسانس">فوق لیسانس</option>
							<option value="دکتری">دکتری</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>وضعیت تاهل <span class="red">*</span></label>
						<select name="u_marital" class="form-control select2">
							<option value="مجرد">مجرد</option>
							<option value="متاهل">متاهل</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>تعداد فرزند</label>
						<input type="tex" name="u_child_count" placeholder="تعداد فرزند" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>تلفن</label>
						<input type="tex" name="u_phone" placeholder="تلفن" class="form-control">
					</div>
				</div>
			
				<div class="col-md-4">
					<div class="form-group">
						<label>موبایل <span class="red">*</span></label>
						<input type="tex" name="u_mobile" placeholder="موبایل" class="form-control" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>ایمیل</label>
						<input type="tex" name="u_email" placeholder="ایمیل" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">	
				<div class="col-md-4">
					<div class="form-group">
						<label>کد پستی</label>
						<input type="tex" name="u_postal" placeholder="کد پستی" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>استان <span class="red">*</span></label>
						<select name="province_id" class="form-control province_id select2">
							<?php
							$db = new db();
							$res = $db->get_select_query("select * from province where p_parent = 0 and p_status = 1");
							if(count($res) > 0){
								foreach($res as $row){
									?>
									<option value="<?php echo $row['ID']; ?>"><?php echo $row['p_name']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>شهرستان <span class="red">*</span></label>
						<div id="result_province">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>آدرس <span class="red">*</span></label>
						<textarea rows="3" type="tex" name="u_address" placeholder="آدرس" class="form-control" required></textarea>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>شماره کارت</label>
						<input type="tex" name="u_cart" placeholder="شماره کارت" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>نام کاربری</label>
						<input type="tex" name="u_username" placeholder="نام کاربری" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>رمز ورود</label>
						<input type="tex" name="u_password" placeholder="رمز ورود" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>سطح دسترسی <span class="red">*</span></label>
						<select name="u_level" class="form-control select2">
							<option value="مدیر">مدیر</option>
							<option value="حسابدار">حسابدار</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" name="add_user" id="add_user" class="btn btn-sm btn-success">ذخیره</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?php
$gt_option = new gt_option();
$opt_home = $gt_option->get_option('opt_home'); ?>
<script src="<?php echo $opt_home; ?>gt-include/script/user/user.js"></script>