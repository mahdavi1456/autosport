<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table  table-striped">
				<tr>
					<th>ردیف</th>
					<th>نام و نام خانوادگی</th>
					<th>سطح دسترسی</th>
					<th>شماره موبایل</th>
					<th>تاریخ ثبت</th>
					<th>مدیریت</th>
				</tr>
				<?php
				$i = 1;
				$db = new db();
				$gt_user = new gt_user();
				$list = $db->get_select_query("select * from user order by ID desc");
				if(count($list)>0){
					foreach($list as $l){ ?>
						<tr>
							<td><?php echo per_number($i); ?></td>
							<td><?php $name = $gt_user->get_user_name($l['ID']); echo $name; ?></td>
							<td><?php echo $l['u_level']; ?></td>
							<td><?php echo per_number($l['u_mobile']); ?></td>	
							<td><?php echo per_number($l['u_regdate']); ?></td>
							<td>
								<button name="edit-user" value="<?php echo $l['ID']; ?>" class="btn btn-info btn-xs">مشاهده و ویرایش</button>
								<button name="remove_user" id="remove_user" onclick="if(!confirm('آیا از انجام این عملیات اطمینان دارید؟')){return false;}" value="<?php echo $l['ID']; ?>" class="btn btn-danger xs">حذف</button>
							</td>
						</tr>
						<?php
						$i++;
					}
				}else{ ?>
					<tr>
						<td colspan="6">بدون کاربر</td>
					</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
</div>
	