-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2020 at 12:57 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automation`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `ID` int(11) NOT NULL,
  `c_name` varchar(50) NOT NULL,
  `c_parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `ID` int(11) NOT NULL,
  `c_name` varchar(25) NOT NULL COMMENT 'نام',
  `c_family` varchar(50) NOT NULL COMMENT 'نام خانوادگی',
  `c_company` varchar(50) NOT NULL COMMENT 'نام شرکت',
  `c_national` int(11) NOT NULL COMMENT 'کد ملی',
  `c_idnumber` int(11) NOT NULL COMMENT 'شماره شناسنامه',
  `c_national_id` int(11) NOT NULL COMMENT 'شناسه ملی',
  `c_economic` varchar(12) DEFAULT NULL COMMENT 'شماره اقتصادی',
  `c_regnumber` int(25) NOT NULL COMMENT 'شماره ثبت شرکت',
  `c_phone` varchar(13) DEFAULT NULL COMMENT 'شماره تماس',
  `c_fax` varchar(13) DEFAULT NULL COMMENT 'شماره فکس',
  `c_mobile` varchar(12) NOT NULL COMMENT 'تلفن همراه',
  `c_address` text DEFAULT NULL COMMENT 'آدرس',
  `c_email` varchar(255) DEFAULT NULL COMMENT 'آدرس پست الکترونیک',
  `c_postalcode` int(11) DEFAULT NULL COMMENT 'کد پستی',
  `c_type` varchar(15) NOT NULL COMMENT 'نوع مشتری',
  `c_c_address` text DEFAULT NULL COMMENT 'آدرس شرکت',
  `c_c_phone` int(13) DEFAULT NULL COMMENT 'تلفن شرکت',
  `c_vat` datetime DEFAULT NULL COMMENT 'ارزش افزوده',
  `c_activity` varchar(20) DEFAULT NULL COMMENT 'نوع فعالیت'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mailroom_letter`
--

CREATE TABLE `mailroom_letter` (
  `ID` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL COMMENT 'شناسه فرستنده',
  `recepiant_id` int(11) NOT NULL COMMENT 'شناسه گیرنده',
  `postage_date` datetime NOT NULL COMMENT 'تاریخ ارسال',
  `l_date` datetime NOT NULL COMMENT 'تاریخ نامه',
  `received_date` datetime NOT NULL COMMENT 'تاریخ دریات',
  `l_type` tinyint(1) NOT NULL COMMENT 'نوع نامه',
  `l_title` text NOT NULL COMMENT 'عنوان نامه',
  `l_code` varchar(25) NOT NULL COMMENT 'شماره شناسه نامه'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mailroom_media`
--

CREATE TABLE `mailroom_media` (
  `ID` int(11) NOT NULL,
  `l_id` int(11) NOT NULL COMMENT 'کد نامه',
  `mr_filename` text NOT NULL COMMENT 'نام فایل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mailroom_postal_sent`
--

CREATE TABLE `mailroom_postal_sent` (
  `ID` int(11) NOT NULL,
  `ps_type` varchar(10) NOT NULL COMMENT 'نوع مرسوله',
  `ps_sender` int(11) NOT NULL COMMENT 'فرستنده',
  `ps_receiver` int(11) NOT NULL COMMENT 'گیرنده',
  `ps_address` text NOT NULL COMMENT 'آدرس',
  `pr_id` int(11) NOT NULL COMMENT 'کد شهر',
  `ps_code` text NOT NULL COMMENT 'کد پستی'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mailroom_postal_sent_media`
--

CREATE TABLE `mailroom_postal_sent_media` (
  `ID` int(11) NOT NULL,
  `ps_id` int(11) NOT NULL,
  `psm_filename` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mailroom_reference`
--

CREATE TABLE `mailroom_reference` (
  `ID` int(10) NOT NULL,
  `l_id` int(10) NOT NULL COMMENT 'کد نامه',
  `sender_id` int(11) NOT NULL COMMENT 'شناسه فرستنده',
  `recepiant_id` int(11) NOT NULL COMMENT 'شناسه گیرنده',
  `l_status` tinyint(1) NOT NULL COMMENT 'وضغیت نامه',
  `l_paraph` text NOT NULL COMMENT 'پاراف',
  `l_details` text NOT NULL COMMENT 'توضیحات',
  `l_date` datetime NOT NULL COMMENT 'تاریخ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE `option` (
  `ID` int(11) NOT NULL,
  `o_key` varchar(30) NOT NULL,
  `o_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option`
--

INSERT INTO `option` (`ID`, `o_key`, `o_value`) VALUES
(17, 'opt_home', 'http://localhost/Autotech/');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `ID` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `p_name` varchar(50) NOT NULL,
  `c_id` int(11) NOT NULL,
  `p_price` double NOT NULL,
  `p_details` text DEFAULT NULL,
  `p_date` datetime DEFAULT NULL,
  `p_status` int(11) DEFAULT NULL,
  `p_type` text DEFAULT NULL COMMENT 'نوع پرداخت',
  `p_code` text DEFAULT NULL COMMENT 'کد پیگیری'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `ID` int(11) NOT NULL,
  `p_name` varchar(50) NOT NULL COMMENT 'نام کالا',
  `p_id` int(11) DEFAULT NULL COMMENT 'کد کالا',
  `p_pprice` varchar(20) NOT NULL COMMENT 'قیمت خرید',
  `p_sprise` varchar(20) NOT NULL COMMENT 'قیمت فروش',
  `p_category` varchar(50) NOT NULL COMMENT 'دسته بندی محصول'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `ID` int(11) NOT NULL,
  `p_name` varchar(50) NOT NULL COMMENT 'نام شهر',
  `p_parent` int(11) NOT NULL COMMENT 'کد والد',
  `p_status` tinyint(1) NOT NULL COMMENT 'وضعیت نمایش'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rest_day`
--

CREATE TABLE `rest_day` (
  `r_id` int(11) NOT NULL,
  `r_type` varchar(25) CHARACTER SET utf8 NOT NULL,
  `u_id` int(11) NOT NULL,
  `r_fromdate` date NOT NULL,
  `r_todate` date NOT NULL,
  `r_total` int(11) NOT NULL,
  `r_destination` text CHARACTER SET utf8 NOT NULL,
  `r_details` text CHARACTER SET utf8 NOT NULL,
  `r_admin_verify` tinyint(4) NOT NULL DEFAULT 0,
  `r_admin_date` date NOT NULL,
  `r_admin_details` text CHARACTER SET utf8 DEFAULT NULL,
  `r_hr_verify` tinyint(4) NOT NULL DEFAULT 0,
  `r_hr_date` date NOT NULL,
  `r_hr_details` text CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rest_hour`
--

CREATE TABLE `rest_hour` (
  `r_id` int(11) NOT NULL,
  `r_type` varchar(25) CHARACTER SET utf8 NOT NULL,
  `u_id` int(11) NOT NULL,
  `r_date` date NOT NULL,
  `r_fromtime` time NOT NULL,
  `r_totime` time NOT NULL,
  `r_total` time NOT NULL,
  `r_details` text CHARACTER SET utf8 NOT NULL,
  `r_admin_verify` tinyint(4) NOT NULL DEFAULT 0,
  `r_admin_date` date NOT NULL,
  `r_admin_details` text CHARACTER SET utf8 DEFAULT NULL,
  `r_hr_verify` tinyint(4) NOT NULL DEFAULT 0,
  `r_hr_date` date NOT NULL,
  `r_hr_details` text CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL COMMENT 'کد ردیف',
  `u_name` varchar(15) NOT NULL COMMENT 'نام',
  `u_family` varchar(50) NOT NULL COMMENT 'نام خانوادگی',
  `u_birth` date NOT NULL COMMENT 'تاریخ تولد',
  `u_marital` varchar(10) NOT NULL COMMENT 'وضعیت تاهل',
  `u_email` varchar(50) DEFAULT NULL COMMENT 'ایمیل',
  `u_national` varchar(25) NOT NULL COMMENT 'کد ملی',
  `pr_id` int(11) NOT NULL COMMENT 'کد شهر',
  `u_postal` varchar(25) DEFAULT NULL COMMENT 'کد پستی',
  `u_address` text NOT NULL COMMENT 'آدرس',
  `u_phone` varchar(15) DEFAULT NULL COMMENT 'تلفن',
  `u_mobile` varchar(15) NOT NULL COMMENT 'موبایل',
  `u_child_count` text DEFAULT NULL COMMENT 'تعداد فرزند',
  `u_evidence` varchar(50) NOT NULL COMMENT 'مدرک تحصیلی',
  `u_cart` varchar(20) DEFAULT NULL COMMENT 'شماره کارت',
  `u_username` varchar(15) DEFAULT NULL COMMENT 'نام کاربری',
  `u_password` text DEFAULT NULL COMMENT 'رمز ورود',
  `u_regdate` datetime DEFAULT NULL COMMENT 'تاریخ ثبت',
  `u_level` varchar(25) NOT NULL COMMENT 'سطح دسترسی',
  `u_link` text DEFAULT NULL COMMENT 'تصویر پروفایل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `u_name`, `u_family`, `u_birth`, `u_marital`, `u_email`, `u_national`, `pr_id`, `u_postal`, `u_address`, `u_phone`, `u_mobile`, `u_child_count`, `u_evidence`, `u_cart`, `u_username`, `u_password`, `u_regdate`, `u_level`, `u_link`) VALUES
(1, 'سید مرتضی', 'مهدوی', '2020-05-19', '0', NULL, '123', 12, NULL, '...', NULL, '123', NULL, '.........', NULL, 'gratech', '5e76bef6e019b2541ff53db39f407a98', NULL, 'مدیر', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mailroom_letter`
--
ALTER TABLE `mailroom_letter`
  ADD UNIQUE KEY `ID` (`ID`,`sender_id`);

--
-- Indexes for table `mailroom_media`
--
ALTER TABLE `mailroom_media`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mailroom_postal_sent`
--
ALTER TABLE `mailroom_postal_sent`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mailroom_postal_sent_media`
--
ALTER TABLE `mailroom_postal_sent_media`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mailroom_reference`
--
ALTER TABLE `mailroom_reference`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rest_day`
--
ALTER TABLE `rest_day`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `rest_hour`
--
ALTER TABLE `rest_hour`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mailroom_letter`
--
ALTER TABLE `mailroom_letter`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailroom_media`
--
ALTER TABLE `mailroom_media`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailroom_postal_sent`
--
ALTER TABLE `mailroom_postal_sent`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailroom_postal_sent_media`
--
ALTER TABLE `mailroom_postal_sent_media`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mailroom_reference`
--
ALTER TABLE `mailroom_reference`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `option`
--
ALTER TABLE `option`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rest_day`
--
ALTER TABLE `rest_day`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rest_hour`
--
ALTER TABLE `rest_hour`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'کد ردیف', AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
