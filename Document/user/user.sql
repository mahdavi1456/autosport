-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2020 at 01:48 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automation`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL COMMENT 'کد ردیف',
  `u_name` varchar(15) NOT NULL COMMENT 'نام',
  `u_family` varchar(50) NOT NULL COMMENT 'نام خانوادگی',
  `u_birth` date NOT NULL COMMENT 'تاریخ تولد',
  `u_marital` varchar(10) NOT NULL COMMENT 'وضعیت تاهل',
  `u_email` varchar(50) DEFAULT NULL COMMENT 'ایمیل',
  `u_national` varchar(25) NOT NULL COMMENT 'کد ملی',
  `pr_id` int(11) NOT NULL COMMENT 'کد شهر',
  `u_postal` varchar(25) DEFAULT NULL COMMENT 'کد پستی',
  `u_address` text NOT NULL COMMENT 'آدرس',
  `u_phone` varchar(15) DEFAULT NULL COMMENT 'تلفن',
  `u_mobile` varchar(15) NOT NULL COMMENT 'موبایل',
  `u_child_count` text DEFAULT NULL COMMENT 'تعداد فرزند',
  `u_evidence` varchar(50) NOT NULL COMMENT 'مدرک تحصیلی',
  `u_cart` varchar(20) DEFAULT NULL COMMENT 'شماره کارت',
  `u_username` varchar(15) DEFAULT NULL COMMENT 'نام کاربری',
  `u_password` text DEFAULT NULL COMMENT 'رمز ورود',
  `u_regdate` datetime DEFAULT NULL COMMENT 'تاریخ ثبت',
  `u_level` varchar(25) NOT NULL COMMENT 'سطح دسترسی',
  `u_link` text DEFAULT NULL COMMENT 'تصویر پروفایل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'کد ردیف';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
