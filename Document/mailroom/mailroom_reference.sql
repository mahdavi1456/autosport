-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2020 at 08:03 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gt_autotech`
--

-- --------------------------------------------------------

--
-- Table structure for table `mailroom_reference`
--

CREATE TABLE `mailroom_reference` (
  `ID` int(10) NOT NULL,
  `l_id` int(10) NOT NULL COMMENT 'کد نامه',
  `sender_id` int(11) NOT NULL COMMENT 'شناسه فرستنده',
  `recepiant_id` int(11) NOT NULL COMMENT 'شناسه گیرنده',
  `l_status` tinyint(1) NOT NULL COMMENT 'وضغیت نامه',
  `l_paraph` text NOT NULL COMMENT 'پاراف',
  `l_details` text NOT NULL COMMENT 'توضیحات',
  `l_date` datetime NOT NULL COMMENT 'تاریخ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mailroom_reference`
--
ALTER TABLE `mailroom_reference`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mailroom_reference`
--
ALTER TABLE `mailroom_reference`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
