-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2020 at 08:05 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gt_autotech`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `ID` int(11) NOT NULL,
  `c_name` varchar(25) DEFAULT NULL COMMENT 'نام',
  `c_family` varchar(50) DEFAULT NULL COMMENT 'نام خانوادگی',
  `c_company` varchar(50) DEFAULT NULL COMMENT 'نام شرکت',
  `c_national` varchar(10) DEFAULT NULL COMMENT 'کد ملی',
  `c_idnumber` varchar(10) DEFAULT NULL COMMENT 'شماره شناسنامه',
  `c_national_id` varchar(15) DEFAULT NULL COMMENT 'شناسه ملی',
  `c_economic` varchar(12) DEFAULT NULL COMMENT 'شماره اقتصادی',
  `c_regnumber` int(25) NOT NULL COMMENT 'شماره ثبت شرکت',
  `c_phone` varchar(13) DEFAULT NULL COMMENT 'شماره تماس',
  `c_fax` varchar(13) DEFAULT NULL COMMENT 'شماره فکس',
  `c_mobile` varchar(12) DEFAULT NULL COMMENT 'تلفن همراه',
  `c_address` text COMMENT 'آدرس',
  `c_email` varchar(255) DEFAULT NULL COMMENT 'آدرس پست الکترونیک',
  `c_postalcode` varchar(20) DEFAULT NULL COMMENT 'کد پستی',
  `c_type` varchar(15) DEFAULT NULL COMMENT 'نوع مشتری',
  `c_c_address` text NOT NULL COMMENT 'آدرس شرکت',
  `c_c_phone` int(13) NOT NULL COMMENT 'تلفن شرکت',
  `c_vat` datetime NOT NULL COMMENT 'ارزش افزوده',
  `c_activity` varchar(20) NOT NULL COMMENT 'نوع فعالیت'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
